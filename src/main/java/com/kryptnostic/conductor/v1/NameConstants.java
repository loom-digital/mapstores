package com.kryptnostic.conductor.v1;

public class NameConstants {

    public static final String SERVICE_NAME_FIELD         = "serviceName";
    public static final String SERVICE_HOST_FIELD         = "serviceHost";
    public static final String SERVICE_PORT_FIELD         = "servicePort";
    public static final String SERVICE_PINGBACK_URL_FIELD = "servicePingbackUrl";
    public static final String SERVICE_DEPLOY_PATH_FIELD  = "serviceDeployPath";

}
