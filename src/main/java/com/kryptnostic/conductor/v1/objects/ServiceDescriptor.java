package com.kryptnostic.conductor.v1.objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kryptnostic.conductor.v1.NameConstants;

public class ServiceDescriptor {

    private final String serviceName;
    private final String serviceHost;
    private final int    servicePort;
    private final String servicePingbackUrl;
    private final String serviceDeployPath;

    @JsonCreator
    public ServiceDescriptor(
            @JsonProperty( NameConstants.SERVICE_NAME_FIELD ) String serviceName,
            @JsonProperty( NameConstants.SERVICE_HOST_FIELD ) String serviceHost,
            @JsonProperty( NameConstants.SERVICE_PORT_FIELD ) int servicePort,
            @JsonProperty( NameConstants.SERVICE_PINGBACK_URL_FIELD ) String servicePingbackUrl,
            @JsonProperty( NameConstants.SERVICE_DEPLOY_PATH_FIELD ) String serviceDeployPath) {
        this.serviceName = serviceName;
        this.serviceHost = serviceHost;
        this.servicePort = servicePort;
        this.servicePingbackUrl = servicePingbackUrl;
        this.serviceDeployPath = serviceDeployPath;
    }

    @JsonProperty( NameConstants.SERVICE_NAME_FIELD )
    public String getServiceName() {
        return serviceName;
    }

    @JsonProperty( NameConstants.SERVICE_HOST_FIELD )
    public String getServiceHost() {
        return serviceHost;
    }

    @JsonProperty( NameConstants.SERVICE_PORT_FIELD )
    public int getServicePort() {
        return servicePort;
    }

    @JsonProperty( NameConstants.SERVICE_PINGBACK_URL_FIELD )
    public String getServicePingbackUrl() {
        return servicePingbackUrl;
    }

    @JsonProperty( NameConstants.SERVICE_DEPLOY_PATH_FIELD )
    public String getServiceDeployPath() {
        return serviceDeployPath;
    }

    @Override
    public String toString() {
        return "ServiceDescriptor [serviceName=" + serviceName + ", serviceHost=" + serviceHost + ", servicePort="
                + servicePort + ", servicePingbackUrl=" + servicePingbackUrl + ", serviceDeployPath="
                + serviceDeployPath + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( serviceName == null ) ? 0 : serviceName.hashCode() );
        result = prime * result + ( ( serviceHost == null ) ? 0 : serviceHost.hashCode() );
        result = prime * result + servicePort;
        result = prime * result + ( ( servicePingbackUrl == null ) ? 0 : servicePingbackUrl.hashCode() );
        result = prime * result + ( ( serviceDeployPath == null ) ? 0 : serviceDeployPath.hashCode() );

        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( !( obj instanceof ServiceDescriptor ) ) return false;

        ServiceDescriptor other = (ServiceDescriptor) obj;
        if ( serviceName == null ) {
            if ( other.getServiceName() != null ) {
                return false;
            }
        } else if ( !serviceName.equals( other.getServiceName() ) ) {
            return false;
        }

        if ( serviceHost == null ) {
            if ( other.getServiceHost() != null ) {
                return false;
            }
        } else if ( !serviceHost.equals( other.getServiceHost() ) ) {
            return false;
        }

        if ( servicePort != other.getServicePort() ) return false;

        if ( servicePingbackUrl == null ) {
            if ( other.getServicePingbackUrl() != null ) {
                return false;
            }
        } else if ( !servicePingbackUrl.equals( other.getServicePingbackUrl() ) ) {
            return false;
        }

        if ( serviceDeployPath == null ) {
            if ( other.getServiceDeployPath() != null ) {
                return false;
            }
        } else if ( !serviceDeployPath.equals( other.getServiceDeployPath() ) ) {
            return false;
        }

        return true;
    }

}
