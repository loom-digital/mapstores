package com.kryptnostic.conductor.v1.objects;

import java.util.Collection;
import java.util.HashSet;

import com.google.common.collect.ImmutableList;

public class ServiceDescriptorSet extends HashSet<ServiceDescriptor> {

    private static final long serialVersionUID = 3266580780735274219L;

    public ServiceDescriptorSet() {
        super();
    }

    public ServiceDescriptorSet( ServiceDescriptor sd ) {
        super( ImmutableList.of( sd ) );
    }

    public ServiceDescriptorSet( int initialCapacity ) {
        super( initialCapacity );
    }

    public ServiceDescriptorSet( Collection<ServiceDescriptor> objects ) {
        super( objects );
    }

    public static ServiceDescriptorSet of( ServiceDescriptor serviceDescriptor ) {
        ServiceDescriptorSet ss = new ServiceDescriptorSet( 1 );
        ss.add( serviceDescriptor );

        return ss;
    }

}
