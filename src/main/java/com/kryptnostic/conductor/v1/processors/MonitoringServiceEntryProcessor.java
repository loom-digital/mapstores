package com.kryptnostic.conductor.v1.processors;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;
import java.util.Map;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.hazelcast.core.HazelcastInstance;
import com.kryptnostic.conductor.v1.objects.ServiceDescriptor;
import com.kryptnostic.conductor.v1.objects.ServiceDescriptorSet;
import com.kryptnostic.mail.client.MailServiceClient;
import com.kryptnostic.mail.constants.EmailTemplate;
import com.kryptnostic.mail.models.RenderableEmailRequest;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class MonitoringServiceEntryProcessor extends AbstractRhizomeEntryProcessor<String, ServiceDescriptorSet, Void> {
    private static final long       serialVersionUID = 2423765356049257683L;
    private final String            reportEmailAddress;
    private final HazelcastInstance hazelcast;
    private final MailServiceClient mailClient;

    public MonitoringServiceEntryProcessor( HazelcastInstance hazelcast, String reportEmailAddress ) {
        this.reportEmailAddress = reportEmailAddress;
        this.hazelcast = hazelcast;
        this.mailClient = new MailServiceClient( hazelcast );
    }

    @Override
    public Void process( Map.Entry<String, ServiceDescriptorSet> entry ) {
        Collection<ServiceDescriptor> desc = entry.getValue();

        for ( ServiceDescriptor item : desc ) {
            String pingBackUrl = item.getServicePingbackUrl();

            try {
                URL url = new URL( pingBackUrl );
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.disconnect();

            } catch ( IOException e ) {
                RenderableEmailRequest emailRequest = new RenderableEmailRequest(
                        Optional.<String> absent(),
                        new String[] { reportEmailAddress },
                        Optional.<String[]> absent(),
                        Optional.<String[]> absent(),
                        EmailTemplate.SERVICE_OUTAGE.getPath(),
                        Optional.of( "Outage" ),
                        Optional.of( ImmutableMap.of( "service",
                                item.getServiceName(),
                                "host",
                                item.getServiceHost(),
                                "port",
                                item.getServicePort() ) ),
                        Optional.absent(),
                        Optional.absent());
                mailClient.send( emailRequest );
            }
        }
        return null;
    }

    public String getHazelcastInstanceName() {
        return hazelcast.getName();
    }

    public String getReportEmailAddress() {
        return reportEmailAddress;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( reportEmailAddress == null ) ? 0 : reportEmailAddress.hashCode() );
        result = prime * result + ( ( hazelcast.getName() == null ) ? 0 : hazelcast.getName().hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) {
            return true;
        }
        if ( obj == null ) {
            return false;
        }
        if ( !( obj instanceof MonitoringServiceEntryProcessor ) ) {
            return false;
        }

        MonitoringServiceEntryProcessor other = (MonitoringServiceEntryProcessor) obj;
        if ( reportEmailAddress == null ) {
            if ( other.reportEmailAddress != null )
                return false;

        } else if ( hazelcast.getName() == null ) {
            if ( other.hazelcast.getName() != null )
                return false;

        } else if ( !reportEmailAddress.equals( other.reportEmailAddress ) ) {
            return false;
        } else if ( !hazelcast.getName().equals( other.hazelcast.getName() ) ) {
            return false;
        }
        return true;
    }

}