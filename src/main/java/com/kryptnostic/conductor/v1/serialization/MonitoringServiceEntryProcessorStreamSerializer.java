package com.kryptnostic.conductor.v1.serialization;

import java.io.IOException;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.conductor.v1.processors.MonitoringServiceEntryProcessor;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class MonitoringServiceEntryProcessorStreamSerializer implements
        SelfRegisteringStreamSerializer<MonitoringServiceEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, MonitoringServiceEntryProcessor object ) throws IOException {
        serialize( out, object );

    }

    @Override
    public MonitoringServiceEntryProcessor read( ObjectDataInput in ) throws IOException {
        return deserialize( in );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.MONITORING_SERVICE_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<MonitoringServiceEntryProcessor> getClazz() {
        return MonitoringServiceEntryProcessor.class;
    }

    private void serialize( ObjectDataOutput out, MonitoringServiceEntryProcessor object ) throws IOException {
        out.writeUTF( object.getHazelcastInstanceName() );
        out.writeUTF( object.getReportEmailAddress() );
    }

    private MonitoringServiceEntryProcessor deserialize( ObjectDataInput in ) throws IOException {
        String hazelcastInstanceName = in.readUTF();
        String reportEmailAddress = in.readUTF();

        return new MonitoringServiceEntryProcessor(
                getHazelcastInstanceUsingName( hazelcastInstanceName ),
                reportEmailAddress );
    }

    protected HazelcastInstance getHazelcastInstanceUsingName( String hazelcastInstanceName ) {
        return Hazelcast.getHazelcastInstanceByName( hazelcastInstanceName );
    }
}
