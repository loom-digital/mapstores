package com.kryptnostic.conductor.v1.serialization;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.conductor.v1.objects.ServiceDescriptor;
import com.kryptnostic.conductor.v1.objects.ServiceDescriptorSet;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class ServiceDescriptorSetStreamSerializer implements SelfRegisteringStreamSerializer<ServiceDescriptorSet> {

    @Override
    public void write( ObjectDataOutput out, ServiceDescriptorSet object ) throws IOException {
        serialize( out, object );
    }

    @Override
    public ServiceDescriptorSet read( ObjectDataInput in ) throws IOException {
        return deserialize( in );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.SERVICE_DESCRIPTOR_SET.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<ServiceDescriptorSet> getClazz() {
        return ServiceDescriptorSet.class;
    }

    public static void serialize( ObjectDataOutput out, ServiceDescriptorSet object ) throws IOException {
        SetStreamSerializers.serialize( out, object, ( ServiceDescriptor sd ) -> {
            ServiceDescriptorStreamSerializer.serialize( out, sd );
        } );
    }

    public static ServiceDescriptorSet deserialize( ObjectDataInput in ) throws IOException {
        int size = in.readInt();
        // TODO: Fix SetStreamDeserialize to support inherited set types (mtr)
        ServiceDescriptorSet ss = new ServiceDescriptorSet( size );
        for ( int i = 0; i < size; ++i ) {
            ss.add( ServiceDescriptorStreamSerializer.deserialize( in ) );
        }
        return ss;
    }

}
