package com.kryptnostic.conductor.v1.serialization;

import java.io.IOException;
import java.util.Set;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.conductor.v1.objects.ServiceDescriptor;
import com.kryptnostic.conductor.v1.processors.ServiceRegistrationServiceEntryProcessor;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class ServiceRegistrationServiceEntryProcessorStreamSerializer implements
        SelfRegisteringStreamSerializer<ServiceRegistrationServiceEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, ServiceRegistrationServiceEntryProcessor object ) throws IOException {
        SetStreamSerializers.<ServiceDescriptor> serialize( out,
                object.getBackingCollection(),
                ( ServiceDescriptor sd ) -> {
                    ServiceDescriptorStreamSerializer.serialize( out, sd );
                } );
    }

    @Override
    public ServiceRegistrationServiceEntryProcessor read( ObjectDataInput in ) throws IOException {
        Set<ServiceDescriptor> sds = SetStreamSerializers.<ServiceDescriptor> deserialize( in,
                ( ObjectDataInput input ) -> {
                    return ServiceDescriptorStreamSerializer.deserialize( in );
                } );
        return new ServiceRegistrationServiceEntryProcessor( sds );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.SERVICE_REGISTRATION_SERVICE_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<ServiceRegistrationServiceEntryProcessor> getClazz() {
        return ServiceRegistrationServiceEntryProcessor.class;
    }

}
