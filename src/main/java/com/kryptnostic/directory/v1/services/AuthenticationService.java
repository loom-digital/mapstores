package com.kryptnostic.directory.v1.services;

import java.util.UUID;

import com.google.common.base.Optional;

public interface AuthenticationService {
    /**
     * Authenticates a user when only the e-mail and authenticator are known.
     * @param email The e-mail of the user being authenticated.
     * @param authenticator The user's authenticator.
     * @return The UUID of the user if successfully authenticated.
     */
    public Optional<UUID> authenticate( String email, String authenticator );

    /**
     * @param id The UUID of the user being authenticated.
     * @param authenticator The user's authenticator.
     * @return The handle (display name) of the user if successfully authenticated.
     */
    public Optional<String> authenticate( UUID id, String authenticator );
}
