package com.kryptnostic.heracles.authentication.v1;

import java.util.Set;
import java.util.UUID;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.kryptnostic.directory.v1.principal.User;
import com.kryptnostic.heracles.directory.v1.objects.UserProxy;
import com.kryptnostic.heracles.directory.v1.services.UserProxyService;

/**
 * This class is a proxy around directory service for retrieving user details.
 *
 * @author Matthew Tamayo-Rios &lt;matthew@kryptnostic.com&gt;
 *
 */
public class HeraclesAuthentication implements Authentication {

    private static final long      serialVersionUID   = -1072489454052610804L;
    private static final String    SPRING_ROLE_PREFIX = "ROLE_";

    private final UUID             userId;
    private final UserProxyService directory;

    public HeraclesAuthentication( UUID userId, UserProxyService directory ) {
        this.userId = userId;
        this.directory = directory;
    }

    @Override
    public String getName() {
        return directory.getName( userId );
    }

    @Override
    public Set<GrantedAuthority> getAuthorities() {
        return Sets.newHashSet( Iterables.transform( directory.getRoles( userId ), ( role ) -> {
            return new SimpleGrantedAuthority( SPRING_ROLE_PREFIX + role );
        } ) );
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public UUID getPrincipal() {
        return userId;
    }

    public User getUser() {
        return new UserProxy( userId, directory );
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated( boolean isAuthenticated ) throws IllegalArgumentException {

    }

    @Override
    public Object getDetails() {
        // TODO Auto-generated method stub
        return null;
    }

}
