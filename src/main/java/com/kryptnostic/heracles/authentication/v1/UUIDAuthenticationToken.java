package com.kryptnostic.heracles.authentication.v1;

import java.util.Collection;
import java.util.UUID;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class UUIDAuthenticationToken extends UsernamePasswordAuthenticationToken {
    private static final long serialVersionUID = -2415779381280806915L;
    
    public UUIDAuthenticationToken(UUID principal, Object credentials) {
        super( principal , credentials );
    }

    public UUIDAuthenticationToken(UUID principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super( principal , credentials , authorities );
    }
    
    @Override
    public UUID getPrincipal() {
        return (UUID)super.getPrincipal();
    }
}
