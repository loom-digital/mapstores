package com.kryptnostic.heracles.authentication.v1.mappers;

import java.util.UUID;

import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;

public class UUIDKeyMapper implements SelfRegisteringKeyMapper<UUID> {

    @Override
    public String fromKey( UUID key ) {
        return key.toString();
    }

    @Override
    public UUID toKey( String value ) {
        return UUID.fromString( value );
    }

    @Override
    public Class<UUID> getClazz() {
        return UUID.class;
    }

}
