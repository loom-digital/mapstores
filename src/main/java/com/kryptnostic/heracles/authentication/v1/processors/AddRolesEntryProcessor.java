package com.kryptnostic.heracles.authentication.v1.processors;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import com.google.common.collect.Sets;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractMerger;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class AddRolesEntryProcessor extends AbstractMerger<UUID, Set<String>, String> {
    private static final long serialVersionUID = 7651200840047517751L;

    public AddRolesEntryProcessor( Set<String> objects ) {
        super( objects );
    }

    @Override
    protected Set<String> newEmptyCollection() {
        return Sets.newHashSet();
    }

    // TODO: break out into its own class
    public static class AddRolesEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<AddRolesEntryProcessor> {

        @Override
        public void write( ObjectDataOutput out, AddRolesEntryProcessor object ) throws IOException {
            SetStreamSerializers.<String> serialize( out, object.getBackingCollection(), ( String s ) -> {
                out.writeUTF( s );
            } );
        }

        @Override
        public AddRolesEntryProcessor read( ObjectDataInput in ) throws IOException {
            Set<String> s = SetStreamSerializers.<String> deserialize( in, ( ObjectDataInput input ) -> {
                return in.readUTF();
            } );
            return new AddRolesEntryProcessor( s );
        }

        @Override
        public int getTypeId() {
            return HazelcastSerializerTypeIds.ADD_ROLES_ENTRY_PROCESSOR.ordinal();
        }

        @Override
        public void destroy() {}

        @Override
        public Class<AddRolesEntryProcessor> getClazz() {
            return AddRolesEntryProcessor.class;
        }

    }
}
