package com.kryptnostic.heracles.authentication.v1.processors;

import java.util.Set;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractMerger;

public class AddUserToDomainEntryProcessor extends AbstractMerger<UUID, UUIDSet, UUID> {
    private static final long serialVersionUID = -3568663181706961369L;

    public AddUserToDomainEntryProcessor( Set<UUID> objects ) {
        this( new UUIDSet( objects ) );
    }

    public AddUserToDomainEntryProcessor( UUIDSet objects ) {
        super( objects );
    }

    @Override
    protected UUIDSet newEmptyCollection() {
        return new UUIDSet();
    }
}
