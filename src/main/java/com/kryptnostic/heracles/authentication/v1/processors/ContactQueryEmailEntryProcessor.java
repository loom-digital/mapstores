package com.kryptnostic.heracles.authentication.v1.processors;

import java.util.Map.Entry;
import java.util.UUID;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.language.Metaphone;
import org.apache.commons.codec.language.Soundex;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kryptnostic.heracles.directory.v1.services.EmailUtils;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class ContactQueryEmailEntryProcessor extends AbstractRhizomeEntryProcessor<UUID, String, ContactQueryResult> {
    private static final Logger    logger           = LoggerFactory.getLogger( ContactQueryEmailEntryProcessor.class );
    private static final long      serialVersionUID = -1315160041742319196L;
    private static final Soundex   soundex          = new Soundex();
    private static final Metaphone metaphone        = new Metaphone();

    private final String           email;
    private final String           emailLocalPart;
    private final String           emailDomain;

    public ContactQueryEmailEntryProcessor( String email ) {
        this( email, EmailUtils.getLocalPartFromEmail( email ), EmailUtils.getDomainFromEmail( email ) );
    }

    public ContactQueryEmailEntryProcessor( String email, String emailLocalPart, String emailDomain ) {
        this.email = email;
        this.emailLocalPart = emailLocalPart;
        this.emailDomain = emailDomain;
    }

    @Override
    public ContactQueryResult process( Entry<UUID, String> entry ) {
        String entryEmail = entry.getValue();
        String entryLocalPart = EmailUtils.getLocalPartFromEmail( entry.getValue() );
        String entryDomain = EmailUtils.getLocalPartFromEmail( entryEmail );
        UUID entryUserId = entry.getKey();

        int score;
        try {
            score = soundex.difference( emailLocalPart, entryLocalPart )
                    + ( metaphone.isMetaphoneEqual( emailLocalPart, entryLocalPart ) ? 4 : 0 )
                    + ( StringUtils.equals( emailLocalPart, entryLocalPart ) ? 4 : 0 )
                    + ( StringUtils.equals( emailDomain, entryDomain ) ? 4 : 0 );

            // Don't bother paying the serialization cost to return results that are obviously bad matches.
            if ( score == 0 ) {
                return null;
            }
            return new ContactQueryResult( score, entryUserId );

        } catch ( EncoderException e ) {
            logger.error(
                    "Unable to execute contact query email search for e-mail {} on user {} with email {} ",
                    email,
                    entryUserId,
                    entryEmail );
            return null;
        }
    }

    public String getEmail() {
        return email;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( email == null ) ? 0 : email.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) {
            return true;
        }
        if ( obj == null ) {
            return false;
        }
        if ( !( obj instanceof ContactQueryEmailEntryProcessor ) ) {
            return false;
        }
        ContactQueryEmailEntryProcessor other = (ContactQueryEmailEntryProcessor) obj;
        if ( email == null ) {
            if ( other.email != null ) {
                return false;
            }
        } else if ( !email.equals( other.email ) ) {
            return false;
        }
        return true;
    }
}
