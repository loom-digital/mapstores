package com.kryptnostic.heracles.authentication.v1.processors;

import java.util.UUID;

public class ContactQueryResult {
    private final int  score;
    private final UUID userId;

    public ContactQueryResult( int score, UUID userId ) {
        this.score = score;
        this.userId = userId;
    }

    public int getScore() {
        return score;
    }

    public UUID getUserId() {
        return userId;
    }
}
