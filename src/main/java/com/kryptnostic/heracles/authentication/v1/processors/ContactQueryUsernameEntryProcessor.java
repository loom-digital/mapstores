package com.kryptnostic.heracles.authentication.v1.processors;

import java.util.Map.Entry;
import java.util.UUID;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.language.Metaphone;
import org.apache.commons.codec.language.Soundex;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class ContactQueryUsernameEntryProcessor
        extends AbstractRhizomeEntryProcessor<UUID, String, ContactQueryResult> {
    private static final Logger    logger           = LoggerFactory
                                                            .getLogger( ContactQueryUsernameEntryProcessor.class );
    private static final long      serialVersionUID = -1315160041742319196L;
    private static final Soundex   soundex          = new Soundex();
    private static final Metaphone metaphone        = new Metaphone();

    private final String           username;

    public ContactQueryUsernameEntryProcessor( String username ) {
        this.username = username;
    }

    @Override
    public ContactQueryResult process( Entry<UUID, String> entry ) {
        String entryUsername = entry.getValue();
        UUID entryUserId = entry.getKey();
        int score;
        try {
            score = soundex.difference( username, entryUsername )
                    + ( metaphone.isMetaphoneEqual( username, entryUsername ) ? 4 : 0 )
                    + ( StringUtils.equals( username, entryUsername ) ? 4 : 0 );

            // Don't bother paying the serialization cost to return results that are obviously bad matches.
            if ( score == 0 ) {
                return null;
            }
            return new ContactQueryResult( score, entryUserId );

        } catch ( EncoderException e ) {
            logger.error(
                    "Unable to execute contact query email search for e-mail {} on user {} with email {} ",
                    username,
                    entryUserId,
                    entryUsername );
            return null;
        }
    }

    public String getUsername() {
        return username;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( username == null ) ? 0 : username.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) {
            return true;
        }
        if ( obj == null ) {
            return false;
        }
        if ( !( obj instanceof ContactQueryUsernameEntryProcessor ) ) {
            return false;
        }
        ContactQueryUsernameEntryProcessor other = (ContactQueryUsernameEntryProcessor) obj;
        if ( username == null ) {
            if ( other.username != null ) {
                return false;
            }
        } else if ( !username.equals( other.username ) ) {
            return false;
        }
        return true;
    }

}
