package com.kryptnostic.heracles.directory.v1;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import com.kryptnostic.directory.v1.principal.User;
import com.kryptnostic.heracles.authentication.v1.HeraclesAuthentication;
import com.kryptnostic.kodex.v1.constants.Names;

import com.kryptnostic.kodex.v1.serialization.jackson.KodexObjectMapperFactory;

/**
 * Utility class for handling common user operations.
 *
 * @author Matthew Tamayo-Rios &lt;matthew@kryptnostic.com&gt;
 * @author Nick Hewitt &lt;nick@kryptnostic.com&gt;
 */
public final class Users {
    private static final Logger logger = LoggerFactory.getLogger( Users.class );

    private Users() {}

    public static Map<String, Object> toHyperdexMap( User user ) {
        return ImmutableMap
                .<String, Object> builder()
                .put( Names.USERNAME_FIELD, user.getName() )
                .put( Names.REALM_FIELD, user.getRealm() )
                .put( Names.EMAIL_FIELD, user.getEmail() )
                .put( Names.CERTIFICATE_FIELD, new String( user.getCertificate() ) )
                .put( Names.GROUPS_PROPERTY, serializeGroups( user.getGroups() ) )
                .put( Names.ROLES_FIELD, user.getRoles() )
                .put( Names.CONFIRMATION_STATUS_FIELD, user.getConfirmationStatus() )
                .build();
    }

    public static Map<String, Object> toRethinkMapWithNewPassword( User user, String newPassword ) {
        return ImmutableMap
                .<String, Object> builder()
                .put( Names.USERNAME_FIELD, user.getName() )
                .put( Names.REALM_FIELD, user.getRealm() )
                .put( Names.EMAIL_FIELD, user.getEmail() )
                .put( Names.CERTIFICATE_FIELD, new String( user.getCertificate() ) )
                .put( Names.GROUPS_PROPERTY, serializeGroups( user.getGroups() ) )
                .put( Names.ROLES_FIELD, serializeRoles( user.getRoles() ) )
                .put( Names.CONFIRMATION_STATUS_FIELD, user.getConfirmationStatus() )
                .build();
    }

    @SuppressWarnings( "unchecked" )
    @VisibleForTesting
    public static Set<String> deserializeRethinkSet( Object entry ) {
        if ( entry instanceof Set ) {
            return (Set<String>) entry;
        } else if ( entry instanceof String ) {
            String entryString = (String) entry;
            Preconditions.checkArgument( entryString.startsWith( "[" ) );
            Preconditions.checkArgument( entryString.endsWith( "]" ) );
            Preconditions.checkArgument( entryString.indexOf( "[" ) == entryString.lastIndexOf( "[" ) );
            Preconditions.checkArgument( entryString.indexOf( "]" ) == entryString.lastIndexOf( "]" ) );
            String[] parts = entryString.replace( "[", "" ).replace( "]", "" ).split( ",(\\s*)?" );
            return Sets.filter( Sets.newHashSet( parts ), ( str ) -> {
                return StringUtils.isNotEmpty( str );
            } );
        } else if ( entry == null ) {
            return Sets.newHashSet();
        } else {
            logger.error( "unrecognized serialization format {} {} ", entry.getClass(), entry );
            throw new IllegalArgumentException( "failed to deserialize rethinkdb value" );
        }
    }

    @SuppressWarnings( "unchecked" )
    @VisibleForTesting
    public static Set<String> deserializeHyperdexSet( Object entry ) {
        if ( entry instanceof Set ) {
            return ( (Set<Object>) entry ).stream().map( e -> e.toString() ).collect( Collectors.toSet() );
        } else if ( entry instanceof String ) {
            String entryString = (String) entry;
            try {
                String[] groupList = KodexObjectMapperFactory.getObjectMapper().readValue( entryString,
                        String[].class );
                return Sets.newHashSet( groupList );
            } catch ( IOException e ) {
                logger.error( "unrecognized serialization format {} {}", entry.getClass(), entry );
                throw new IllegalArgumentException( "failed to deserialize hyperdex value" );
            }
        } else if ( entry == null ) {
            return Sets.newHashSet();
        } else {
            logger.error( "unrecognized serialization format {} {}", entry.getClass(), entry );
            throw new IllegalArgumentException( "failed to deserialize rethinkdb value" );
        }
    }

    public static Map<String, Object> userLookupFilter( UUID id, String password ) {
        return ImmutableMap.of( Names.ID_FIELD, id, Names.PASSWORD_FIELD, password );
    }

    public static User getCurrentUser() {
        User user = getAuthentication().getUser();
        if(user == null){
            throw new AuthenticationCredentialsNotFoundException("Authorization Failed!");
        }
        return user;
    }

    public static UUID getCurrentUserKey() {
        UUID currentUserKey = getAuthentication().getPrincipal();
        if(currentUserKey == null){
            throw new AuthenticationCredentialsNotFoundException("Authorization Failed!");
        }
        return currentUserKey;
    }

    private static HeraclesAuthentication getAuthentication() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if ( auth == null ) {
            throw new AuthenticationCredentialsNotFoundException( "authentication failed" );
        } else if ( !( auth instanceof HeraclesAuthentication ) ) {
            logger.error( "unknown authentication of type {}, {}", auth.getClass(), auth );
            throw new AuthenticationCredentialsNotFoundException( "authentication failed" );
        } else {
            return (HeraclesAuthentication) auth;
        }
    }

    @Deprecated
    public static boolean isCurrentUserInRealm( String realm ) {
        return getCurrentUser().getRealm().equals( realm );
    }

    public static boolean isCurrentUserInDomain( String domain ) {
        return getCurrentUser().getDomain().equals( domain );
    }

    static String serializeGroups( Set<UUID> keys ) {
        try {
            return KodexObjectMapperFactory.getObjectMapper().writeValueAsString( keys );
        } catch ( JsonProcessingException e ) {
            logger.error( "Failed to write user groups", e );
        }
        return null;
    }

    static Set<UUID> getGroupKeys( Set<String> groups ) {
        Set<UUID> groupKeys = Sets.newHashSet();
        for ( Object group : groups ) {
            groupKeys.add( UUID.fromString( group.toString() ) );
        }
        return groupKeys;
    }

    static String serializeRoles( Set<String> roles ) {
        try {
            return KodexObjectMapperFactory.getObjectMapper().writeValueAsString( roles );
        } catch ( JsonProcessingException e ) {
            logger.error( "Failed to write user groups", e );
        }
        return null;
    }
}