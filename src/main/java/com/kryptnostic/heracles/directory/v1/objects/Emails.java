package com.kryptnostic.heracles.directory.v1.objects;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;

public class Emails extends LinkedHashSet<String> {
    private static final long serialVersionUID = -2960382816616221783L;

    public Emails() {
        super();
    }

    public Emails( int initialCapacity ) {
        super( initialCapacity );
    }

    public Emails( Collection<String> emails ) {
        super( emails );
    }

    public static Emails of( String email ) {
        return new Emails( Arrays.asList( email ) );
    }
}
