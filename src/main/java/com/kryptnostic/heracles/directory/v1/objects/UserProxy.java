package com.kryptnostic.heracles.directory.v1.objects;

import java.util.Arrays;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kryptnostic.directory.v1.principal.User;
import com.kryptnostic.heracles.directory.v1.services.EmailUtils;
import com.kryptnostic.heracles.directory.v1.services.UserProxyService;
import com.kryptnostic.kodex.v1.models.KryptnosticUser;

@JsonSerialize(
    as = User.class )
@JsonDeserialize(
    as = KryptnosticUser.class )
public class UserProxy implements User {
    private final UUID             userId;
    private final UserProxyService userProxy;

    public UserProxy( UUID userId, UserProxyService directory ) {
        this.userId = userId;
        this.userProxy = directory;
    }

    @Override
    public UUID getId() {
        return userId;
    }

    @Override
    public String getName() {
        return userProxy.getName( userId );
    }

    @Override
    @Deprecated
    public String getRealm() {
        return getDomain();
    }

    @Override
    public String getDomain() {
        return EmailUtils.getDomainFromEmail( userProxy.getEmails( userId ).iterator().next() );
    }

    @Override
    public String getEmail() {
        return userProxy.getEmails( userId ).iterator().next();
    }

    @Override
    public byte[] getCertificate() {
        return userProxy.getCertificate( userId );
    }

    @Override
    public Set<String> getRoles() {
        return userProxy.getRoles( userId );
    }

    @Override
    public Set<UUID> getGroups() {
        return userProxy.getGroups( userId );
    }

    @Override
    public boolean getConfirmationStatus() {
        return userProxy.getConfirmationStatus( userId );
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( userId == null ) ? 0 : userId.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) {
            return true;
        }
        if ( obj == null ) {
            return false;
        }
        if ( !( obj instanceof User ) ) {
            return false;
        }
        User other = (User) obj;
        if ( userId == null ) {
            if ( other.getId() != null ) {
                return false;
            }
        } else if ( !userId.equals( other.getId() ) ) {
            return false;
        } else {
            boolean isEqual = ( getConfirmationStatus() ==  other.getConfirmationStatus() )
                    && ( Arrays.equals( getCertificate(), other.getCertificate() ) )
                    && ( getEmail().equals( other.getEmail() ) )
                    && ( getRoles().equals( other.getRoles() ) )
                    && ( getEmail().equals( other.getEmail() ) )
                    && ( getRoles().equals( other.getRoles() ) )
                    && ( getGroups().equals( other.getGroups() ) );
            if ( !isEqual ) {
                return false;
            }

        }
        return true;
    }

}
