package com.kryptnostic.heracles.directory.v1.processors;

import java.util.Set;
import java.util.UUID;

import com.kryptnostic.heracles.directory.v1.objects.Emails;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractMerger;

public class AddEmailEntryProcessor extends AbstractMerger<UUID, Emails, String> {
    private static final long serialVersionUID = 8059443738689195354L;

    public AddEmailEntryProcessor( Emails emails ) {
        super( emails );
    }

    public AddEmailEntryProcessor( Set<String> emails ) {
        this( new Emails( emails ) );
    }

    @Override
    protected Emails newEmptyCollection() {
        return new Emails();
    }
}
