package com.kryptnostic.heracles.directory.v1.processors;

import java.util.Map.Entry;
import java.util.UUID;

import com.kryptnostic.directory.v1.domain.Domain;
import com.kryptnostic.directory.v1.model.DomainUpdate;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class DomainUpdateEntryProcessor extends AbstractRhizomeEntryProcessor<UUID, Domain, Void> {
    private static final long  serialVersionUID = 6184897507832977030L;
    private final DomainUpdate update;

    public DomainUpdateEntryProcessor( DomainUpdate update ) {
        super( false );
        this.update = update;
    }

    @Override
    public Void process( Entry<UUID, Domain> entry ) {
        Domain domain = entry.getValue();
        if ( domain != null ) {
            Domain updated = new Domain.DomainBuilder( domain.getId(), domain.getName() )
                    .withSize( domain.getSize() )
                    .withSharingPolicy( update.getSharingPolicy().or( domain.getDomainSharingPolicy() ) )
                    .publiclyListableIs( update.getPubliclyListable().or( domain.isPubliclyListable() ).booleanValue() )
                    .openRegistrationEnabled( update.getOpenRegistrationEnabled().booleanValue() )
                    .emailConfirmationRequiredIs( domain.isConfirmationEmailRequired() )
                    .notificationEmail( update.getNotificationEmail().or( domain.getNotificationEmail() ) ).build();
            entry.setValue( updated );
        }
        return null;
    }

    public DomainUpdate getDomainUpdate() {
        return update;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( update == null ) ? 0 : update.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( getClass() != obj.getClass() ) return false;
        DomainUpdateEntryProcessor other = (DomainUpdateEntryProcessor) obj;
        if ( update == null ) {
            if ( other.update != null ) return false;
        } else if ( !update.equals( other.update ) ) return false;
        return true;
    }
}
