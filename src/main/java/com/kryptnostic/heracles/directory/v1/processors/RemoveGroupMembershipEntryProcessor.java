package com.kryptnostic.heracles.directory.v1.processors;

import java.util.Set;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRemover;

public class RemoveGroupMembershipEntryProcessor extends AbstractRemover<UUID, UUIDSet, UUID> {
    private static final long serialVersionUID = -3865181878933181194L;

    public RemoveGroupMembershipEntryProcessor( UUID userId ) {
        this( UUIDSet.of( userId ) );
    }

    public RemoveGroupMembershipEntryProcessor( Set<UUID> userIds ) {
        this( new UUIDSet( userIds ) );
    }

    public RemoveGroupMembershipEntryProcessor( UUIDSet userIds ) {
        super( userIds );
    }
}
