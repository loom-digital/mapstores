package com.kryptnostic.heracles.directory.v1.serialization;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.heracles.directory.v1.processors.AddEmailEntryProcessor;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class AddEmailEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<AddEmailEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, AddEmailEntryProcessor object ) throws IOException {
        SetStreamSerializers.serialize( out, object.getBackingCollection(), ( String email ) -> {
            out.writeUTF( email );
        } );

    }

    @Override
    public AddEmailEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new AddEmailEntryProcessor( SetStreamSerializers.deserialize( in, ( ObjectDataInput dataInput ) -> {
            return dataInput.readUTF();
        } ) );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.ADD_EMAIL_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<AddEmailEntryProcessor> getClazz() {
        return AddEmailEntryProcessor.class;
    }

}
