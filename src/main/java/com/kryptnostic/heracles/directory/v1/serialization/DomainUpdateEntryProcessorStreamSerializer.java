package com.kryptnostic.heracles.directory.v1.serialization;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.heracles.directory.v1.processors.DomainUpdateEntryProcessor;
import com.kryptnostic.heracles.v1.serializers.DomainUpdateStreamSerializer;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class DomainUpdateEntryProcessorStreamSerializer implements
        SelfRegisteringStreamSerializer<DomainUpdateEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, DomainUpdateEntryProcessor object ) throws IOException {
        DomainUpdateStreamSerializer.serialize( out, object.getDomainUpdate() );
    }

    @Override
    public DomainUpdateEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new DomainUpdateEntryProcessor( DomainUpdateStreamSerializer.deserialize( in ) );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.UPDATE_DOMAIN_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<DomainUpdateEntryProcessor> getClazz() {
        return DomainUpdateEntryProcessor.class;
    }

}
