package com.kryptnostic.heracles.directory.v1.serialization;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.heracles.directory.v1.processors.RemoveGroupMembershipEntryProcessor;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class RemoveGroupMembershipEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<RemoveGroupMembershipEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, RemoveGroupMembershipEntryProcessor object ) throws IOException {
        SetStreamSerializers.fastUUIDSetSerialize( out, object.getBackingCollection() );
    }

    @Override
    public RemoveGroupMembershipEntryProcessor read( ObjectDataInput in ) throws IOException {
        UUIDSet collection = SetStreamSerializers.fastUUIDSetDeserialize( in );
        return new RemoveGroupMembershipEntryProcessor( collection );
    }
    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.REMOVE_GROUP_MEMBERSHIP_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<RemoveGroupMembershipEntryProcessor> getClazz() {
        return RemoveGroupMembershipEntryProcessor.class;
    }

}
