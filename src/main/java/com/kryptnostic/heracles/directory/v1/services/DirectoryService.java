package com.kryptnostic.heracles.directory.v1.services;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

import com.google.common.base.Optional;
import com.kryptnostic.directory.v1.ContactDiscoveryRequest;
import com.kryptnostic.directory.v1.ContactDiscoveryResponse;
import com.kryptnostic.directory.v1.domain.Domain;
import com.kryptnostic.directory.v1.domain.DomainPolicy;
import com.kryptnostic.directory.v1.exception.AddUserException;
import com.kryptnostic.directory.v1.exception.UserUpdateException;
import com.kryptnostic.directory.v1.model.DomainUpdate;
import com.kryptnostic.directory.v1.model.request.UpdateUserRequest;
import com.kryptnostic.directory.v1.principal.User;
import com.kryptnostic.directory.v1.services.AuthenticationService;
import com.kryptnostic.directory.v2.model.DomainResourcesUsage;
import com.kryptnostic.directory.v2.model.UserSettings;
import com.kryptnostic.kodex.v1.exceptions.types.ForbiddenException;
import com.kryptnostic.kodex.v1.exceptions.types.ResourceNotFoundException;
import com.kryptnostic.mail.models.EmailRequest;
import com.kryptnostic.registration.v1.models.UserCreationRequest;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;

public interface DirectoryService extends AuthenticationService {
    /**
     * @param realmId the UUID of the realm
     * @return the set of users that exist in the realm
     */
    @Nonnull
    Set<UUID> getUsersInRealm( UUID realmId );

    /**
     * Retrieves the heavy-weight representation of a user and all of its attributes.
     *
     * @param userId the UUID of the user
     * @return the full user object
     */
    @Nonnull
    Optional<User> getUser( UUID userId );

    Set<User> getUsers( Set<UUID> userIds );

    void changePassword( UUID user, String oldPassword, String newPassword ) throws UserUpdateException;

    /**
     * This resets the users password without any authentication!!!
     *
     * Be *SURE* to verify that a password reset request is legitimate before calling this method
     *
     * @param user the UUID of the user
     * @param newPassword the new password of the user
     * @throws UserUpdateException
     */
    void setPassword( UUID user, String newPassword ) throws UserUpdateException;

    /**
     * Look up a user's UUID by e-mail.
     *
     * @param email the email of the user
     * @return the users UUID
     */
    @CheckForNull
    Optional<UUID> resolve( String email );

    /**
     * Removes a user from the system.
     *
     * @param user
     * @return
     */
    @CheckForNull
    Optional<UUID> delete( UUID user );

    @CheckForNull
    User update( UUID userKey, UpdateUserRequest request ) throws UserUpdateException;

    /**
     * Adds a user if the user doesn't already exist.
     *
     * @param user The user to create.
     * @return True if the user was added, false if the user already exists.
     * @throws AddUserException if adding the user fails
     */
    @CheckForNull
    UUID create( UserCreationRequest user ) throws AddUserException;

    /**
     * @param realm the realm you want to get the users for
     * @return a set of UUIDs of the users in the realm
     * @throws ResourceNotFoundException 
     */
    Set<UUID> getUsersInRealm( String realm ) throws ResourceNotFoundException;

    /**
     * Returns a list of users that respects the Domain's sharing policy.
     *
     * @param domain
     * @return
     */
    Iterable<UUID> getListableUsersForDomain( UUID domain );

    Iterable<UUID> getUsersForAllDomains();

    Iterable<UUID> getListableUsersForAllDomains();

    void addRole( UUID userId, String role );

    UUID createDomain( String domain );

    Optional<UUID> getDomain( String domain );
    
    String getDomainName( UUID domainUUID );
    
    Map<UUID, String> getDomainNames( Set<UUID> domainUUIDs );

    @Nonnull
    ContactDiscoveryResponse discover( @Nonnull ContactDiscoveryRequest request );

    Optional<Domain> loadDomain( String domain );

    Optional<Domain> loadDomain( UUID domain );

    Optional<UUIDSet> loadMembers( UUID domainId );

    void updateDomain( DomainUpdate update );

    DomainPolicy getDomainSharingPolicy();
    
    void setDomainSharingPolicy( DomainPolicy domainSharingPolicy );

    String getDomainEmailAddress();

    void setDomainEmailAddress( String email );

    /**
     * Update white list settings.
     *
     * @param domains
     */
    void addToDomainWhiteList( @Nonnull Set<String> domains );

    Set<String> getDomainWhiteList();

    void removeFromDomainWhiteList( @Nonnull Set<String> domains );

    /**
     * Show the listable domains for adding to white list
     *
     */
    Set<String> getListableDomainsForWhiteList();

    void sendConfirmationEmail( UUID newUserId );

    boolean verifyConfirmation( UUID userId, String token );

    void resendConfirmationEmail();

    void sendInvitationEmail( EmailRequest emailRequest );

    void sendNewUserRegisterDomainNotificationEmail( String newUserDomainName, String newUserEmail );

    // domain resources
    DomainResourcesUsage getDomainResourcesUsage( UUID domainId, String resource );

    void setSuccessfulFirstLogin();

    UUID getUserIdForSharing( String email ) throws ResourceNotFoundException, ForbiddenException;
    
    boolean getIncomingSharesPermission();

    void setIncomingSharesPermission( boolean permission );   

    boolean getOutgoingSharesPermission();

    void setOutgoingSharesPermission( boolean permission );

    Set<String> getUsersSetting( UUID userId, UserSettings userSetting );

    void addToUserSetting( UUID userId, UserSettings userSetting, Set<String> items );

    void removeFromUserSetting( UUID userId, UserSettings userSetting, Set<String> items );

}
