package com.kryptnostic.heracles.directory.v1.services;

import java.util.UUID;

public class DomainKey {
    private final UUID id;
    protected long     index;

    public DomainKey( UUID id, long index ) {
        this.id = id;
        this.index = index;
    }

    public UUID getId() {
        return id;
    }

    public long getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return "DomainKey [id=" + id + ", index=" + index + "]";
    }
}
