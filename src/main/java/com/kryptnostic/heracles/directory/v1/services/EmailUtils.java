package com.kryptnostic.heracles.directory.v1.services;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

import jodd.mail.EmailAddress;

public final class EmailUtils {
    private EmailUtils() {}

    public static String getDomainFromEmail( String email ) {
        Preconditions.checkArgument( StringUtils.isNotBlank( email ), "Email cannot be blank!" );
        return new EmailAddress( email ).getDomain().toLowerCase();
    }

    public static String getLocalPartFromEmail( String email ) {
        Preconditions.checkArgument( StringUtils.isNotBlank( email ), "Email cannot be blank!" );
        return new EmailAddress( email ).getLocalPart().toLowerCase();
    }

    public static boolean isValidEmail( String email ) {
        Preconditions.checkArgument( StringUtils.isNotBlank( email ), "Email cannot be blank!" );
        return new EmailAddress( email ).isValid();
    }

    /**
     * Ensures that {@code Optional<String>} contains a valid e-mail string.
     * 
     * @param email A possibly e-mail containing string.
     * @return An {@code Optional<String>} that either contains a valid e-mail string or is empty.
     */
    public static Optional<String> sanitizePossibleEmails( Optional<String> email ) {
        if ( email.isPresent() ) {
            if ( new EmailAddress( email.get() ).isValid() ) {
                return email;
            }
            return Optional.absent();
        } else {
            // Don't create a new object
            return email;
        }
    }
}
