package com.kryptnostic.heracles.directory.v1.services;

import java.util.UUID;

public class IncrementableDomainKey extends DomainKey {
    public IncrementableDomainKey( UUID id, long index ) {
        super( id, index );
    }

    public IncrementableDomainKey increment() {
        index++;
        return this;
    }
}
