package com.kryptnostic.heracles.directory.v1.services;

import java.util.Set;
import java.util.UUID;

import javax.annotation.Nonnull;

import com.kryptnostic.directory.v1.services.AuthenticationService;
import com.kryptnostic.heracles.directory.v1.objects.Emails;

public interface UserProxyService extends AuthenticationService {

    @Nonnull
    String getName( UUID userId );

    Emails getEmails( UUID userId );

    /**
     * Retrieves the certificate
     *
     * @param userId the id of the user
     * @return the users certificate as a byte array
     */
    byte[] getCertificate( UUID userId );

    @Nonnull
    Set<String> getRoles( UUID userId );

    @Nonnull
    Set<UUID> getGroups( UUID userId );
    
    @Nonnull
    boolean getConfirmationStatus( UUID userId );

}
