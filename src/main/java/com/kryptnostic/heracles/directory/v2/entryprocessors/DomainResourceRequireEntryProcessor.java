package com.kryptnostic.heracles.directory.v2.entryprocessors;

import java.util.Map.Entry;

import com.kryptnostic.directory.v2.model.DomainResourcesUsage;
import com.kryptnostic.heracles.directory.v2.objects.DomainResourcesKey;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class DomainResourceRequireEntryProcessor
        extends AbstractRhizomeEntryProcessor<DomainResourcesKey, DomainResourcesUsage, Boolean> {
    private static final long serialVersionUID = 3800005873225997196L;
    private final long        requestValue;

    public DomainResourceRequireEntryProcessor( long value ) {
        this.requestValue = value;
    }

    @Override
    public Boolean process( Entry<DomainResourcesKey, DomainResourcesUsage> entry ) {
        DomainResourcesUsage usage = entry.getValue();
        if ( usage == null || usage.getAvailable() < requestValue ) {
            return Boolean.FALSE;
        }
        usage.setAvailable( Long.valueOf( usage.getAvailable() - requestValue ) );
        entry.setValue( usage );
        return Boolean.TRUE;
    }

    public long getRequestValue() {
        return requestValue;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) ( requestValue ^ ( requestValue >>> 32 ) );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( !( obj instanceof DomainResourceRequireEntryProcessor ) ) return false;
        DomainResourceRequireEntryProcessor other = (DomainResourceRequireEntryProcessor) obj;
        if ( requestValue != other.requestValue ) return false;
        return true;
    }

}
