package com.kryptnostic.heracles.directory.v2.mappers.keys;

import java.util.UUID;

import com.kryptnostic.heracles.directory.v2.objects.DomainResourcesKey;
import com.kryptnostic.rhizome.mappers.KeyMapper;
import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;

public class DomainResourcesKeyKeyMapper implements SelfRegisteringKeyMapper<DomainResourcesKey> {

    @Override
    public String fromKey( DomainResourcesKey key ) {
        return key.getDomainId().toString() + KeyMapper.DEFAULT_SEPARATOR + key.getResourceName();
    }

    @Override
    public DomainResourcesKey toKey( String value ) {
        String[] split = value.split( KeyMapper.DEFAULT_SEPARATOR );
        return new DomainResourcesKey( UUID.fromString( split[ 0 ] ), split[ 1 ] );
    }

    @Override
    public Class<DomainResourcesKey> getClazz() {
        return DomainResourcesKey.class;
    }

}
