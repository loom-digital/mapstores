package com.kryptnostic.heracles.directory.v2.objects;

import java.util.UUID;

public class DomainResourcesKey {
    private UUID domainId;
    private String resourceName;
    
    public DomainResourcesKey(UUID domainId, String resourceName){
        this.domainId = domainId;
        this.resourceName = resourceName;
    }

    public UUID getDomainId() {
        return domainId;
    }

    public String getResourceName() {
        return resourceName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( domainId == null ) ? 0 : domainId.hashCode() );
        result = prime * result + ( ( resourceName == null ) ? 0 : resourceName.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( !( obj instanceof DomainResourcesKey ) ) return false;
        DomainResourcesKey other = (DomainResourcesKey) obj;
        if ( domainId == null ) {
            if ( other.domainId != null ) return false;
        } else if ( !domainId.equals( other.domainId ) ) return false;
        if ( resourceName == null ) {
            if ( other.resourceName != null ) return false;
        } else if ( !resourceName.equals( other.resourceName ) ) return false;
        return true;
    }
    

}
