package com.kryptnostic.heracles.directory.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.heracles.directory.v2.entryprocessors.DomainResourceRequireEntryProcessor;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class DomainResourceRequireEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<DomainResourceRequireEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, DomainResourceRequireEntryProcessor object ) throws IOException {
        out.writeLong( object.getRequestValue() );

    }

    @Override
    public DomainResourceRequireEntryProcessor read( ObjectDataInput in ) throws IOException {
        long requestValue = in.readLong();
        return new DomainResourceRequireEntryProcessor( requestValue );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.DOMAIN_RESOURCE_REQUIRE_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<DomainResourceRequireEntryProcessor> getClazz() {
        return DomainResourceRequireEntryProcessor.class;
    }

}
