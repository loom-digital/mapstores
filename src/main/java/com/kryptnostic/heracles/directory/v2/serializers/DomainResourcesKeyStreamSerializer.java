package com.kryptnostic.heracles.directory.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.heracles.directory.v2.objects.DomainResourcesKey;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class DomainResourcesKeyStreamSerializer implements SelfRegisteringStreamSerializer<DomainResourcesKey> {

    @Override
    public void write( ObjectDataOutput out, DomainResourcesKey object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getDomainId() );
        out.writeUTF( object.getResourceName() );

    }

    @Override
    public DomainResourcesKey read( ObjectDataInput in ) throws IOException {
        UUID domainId = UUIDStreamSerializer.deserialize( in );
        String resourceName = in.readUTF();
        return new DomainResourcesKey( domainId, resourceName );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.DOMAIN_RESOURCES_KEY.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<DomainResourcesKey> getClazz() {
        return DomainResourcesKey.class;
    }

}
