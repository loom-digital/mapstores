package com.kryptnostic.heracles.directory.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.directory.v2.model.DomainResourcesUsage;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class DomainResourcesUsageStreamSerializer implements SelfRegisteringStreamSerializer<DomainResourcesUsage> {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.DOMAIN_RESOURCES_USAGE.ordinal();
    }

    @Override
    public void write( ObjectDataOutput out, DomainResourcesUsage object ) throws IOException {
        out.writeLong( object.getAvailable() );
        out.writeLong( object.getTotal() );

    }

    @Override
    public DomainResourcesUsage read( ObjectDataInput in ) throws IOException {
        long available = in.readLong();
        long total = in.readLong();
        return new DomainResourcesUsage( available, total );
    }

    @Override
    public void destroy() {}

    @Override
    public Class<DomainResourcesUsage> getClazz() {
        return DomainResourcesUsage.class;
    }

}
