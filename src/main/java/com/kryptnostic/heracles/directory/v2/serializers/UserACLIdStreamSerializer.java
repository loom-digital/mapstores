package com.kryptnostic.heracles.directory.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.mapstores.UserACLId;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class UserACLIdStreamSerializer implements SelfRegisteringStreamSerializer<UserACLId> {

    @Override
    public void write( ObjectDataOutput out, UserACLId object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getUserId() );
        UUIDStreamSerializer.serialize( out, object.getAclId() );
    }

    @Override
    public UserACLId read( ObjectDataInput in ) throws IOException {
        UUID user = UUIDStreamSerializer.deserialize( in );
        UUID aclId = UUIDStreamSerializer.deserialize( in );
        return new UserACLId( user, aclId );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.USER_ACL_ID.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<UserACLId> getClazz() {
        return UserACLId.class;
    }

}
