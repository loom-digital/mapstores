package com.kryptnostic.heracles.v1.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.heracles.authentication.v1.processors.ContactQueryEmailEntryProcessor;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class ContactQueryEmailEntryProcessorStreamSerializer implements
        SelfRegisteringStreamSerializer<ContactQueryEmailEntryProcessor> {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.CONTACT_QUERY_EMAIL_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public void write( ObjectDataOutput out, ContactQueryEmailEntryProcessor object ) throws IOException {
        out.writeUTF( object.getEmail() );
    }

    @Override
    public ContactQueryEmailEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new ContactQueryEmailEntryProcessor( in.readUTF() );
    }

    @Override
    public Class<ContactQueryEmailEntryProcessor> getClazz() {
        return ContactQueryEmailEntryProcessor.class;
    }
}
