package com.kryptnostic.heracles.v1.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.heracles.authentication.v1.processors.ContactQueryResult;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class ContactQueryResultStreamSerializer implements SelfRegisteringStreamSerializer<ContactQueryResult> {

    @Override
    public void write( ObjectDataOutput out, ContactQueryResult object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getUserId() );
        out.writeInt( object.getScore() );
    }

    @Override
    public ContactQueryResult read( ObjectDataInput in ) throws IOException {
        UUID userId = UUIDStreamSerializer.deserialize( in );
        int value = in.readInt();
        return new ContactQueryResult( value, userId );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.CONTACT_QUERY_RESULT.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<ContactQueryResult> getClazz() {
        return ContactQueryResult.class;
    }

}
