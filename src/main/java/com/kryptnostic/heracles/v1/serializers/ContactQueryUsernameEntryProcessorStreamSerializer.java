package com.kryptnostic.heracles.v1.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.heracles.authentication.v1.processors.ContactQueryUsernameEntryProcessor;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class ContactQueryUsernameEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<ContactQueryUsernameEntryProcessor> {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.CONTACT_QUERY_USERNAME_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public void write( ObjectDataOutput out, ContactQueryUsernameEntryProcessor object ) throws IOException {
        out.writeUTF( object.getUsername() );
    }

    @Override
    public ContactQueryUsernameEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new ContactQueryUsernameEntryProcessor( in.readUTF() );
    }

    @Override
    public Class<ContactQueryUsernameEntryProcessor> getClazz() {
        return ContactQueryUsernameEntryProcessor.class;
    }
}
