package com.kryptnostic.heracles.v1.serializers;

import com.kryptnostic.directory.v1.domain.Domain;
import com.kryptnostic.kodex.v1.serialization.jackson.KodexObjectMapperFactory;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.serializers.AbstractJacksonStreamSerializer;

public class DomainStreamSerializer extends AbstractJacksonStreamSerializer<Domain>{

    public DomainStreamSerializer() {
        super( Domain.class, KodexObjectMapperFactory.getSmileMapper() );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.DOMAIN.ordinal();
    }

}
