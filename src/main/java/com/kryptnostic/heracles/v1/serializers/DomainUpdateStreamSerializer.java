package com.kryptnostic.heracles.v1.serializers;

import java.io.IOException;
import java.util.UUID;

import com.google.common.base.Optional;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.directory.v1.domain.DomainSharingPolicy;
import com.kryptnostic.directory.v1.model.DomainUpdate;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class DomainUpdateStreamSerializer implements SelfRegisteringStreamSerializer<DomainUpdate> {

    @Override
    public void write( ObjectDataOutput out, DomainUpdate object ) throws IOException {
        serialize( out, object );
    }

    @Override
    public DomainUpdate read( ObjectDataInput in ) throws IOException {
        return deserialize( in );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.DOMAIN_UPDATE.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<DomainUpdate> getClazz() {
        return DomainUpdate.class;
    }

    public static void serialize( ObjectDataOutput out, DomainUpdate object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getDomainId() );
        out.writeBoolean( object.getOpenRegistrationEnabled() );
        out.writeBoolean( object.getSharingPolicy().isPresent() );
        if ( object.getSharingPolicy().isPresent() ) {
            out.writeUTF( object.getSharingPolicy().get().toString() );
        }
        out.writeBoolean( object.getPubliclyListable().isPresent() );
        if ( object.getPubliclyListable().isPresent() ) {
            out.writeBoolean( object.getPubliclyListable().get() );
        }
        out.writeBoolean( object.getNotificationEmail().isPresent() );
        if ( object.getNotificationEmail().isPresent() ) {
            out.writeUTF( object.getNotificationEmail().get() );
        }
    }

    public static DomainUpdate deserialize( ObjectDataInput in ) throws IOException {
        UUID id = UUIDStreamSerializer.deserialize( in );
        boolean openRegistration = in.readBoolean();
        Optional<DomainSharingPolicy> sharingPolicy;
        Optional<String> notificationEmail;
        if ( in.readBoolean() ) {
            sharingPolicy = Optional.of( DomainSharingPolicy.createDomainSharingPolicy( in.readUTF() ) );
        } else {
            sharingPolicy = Optional.absent();
        }
        Optional<Boolean> publiclyListable;
        if ( in.readBoolean() ) {
            publiclyListable = Optional.of( in.readBoolean() );
        } else {
            publiclyListable = Optional.absent();
        }
        if( in.readBoolean() ) {
            notificationEmail = Optional.of( in.readUTF() );
        }else {
            notificationEmail = Optional.absent();
        }
        return new DomainUpdate( id, sharingPolicy, Optional.of( openRegistration ), publiclyListable, notificationEmail );
    }
}
