package com.kryptnostic.heracles.v1.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.heracles.directory.v1.services.IncrementableDomainKey;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class IncrementableDomainKeyStreamSerializer implements SelfRegisteringStreamSerializer<IncrementableDomainKey> {
    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.INCREMENTABLE_DOMAIN_KEY.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public void write( ObjectDataOutput out, IncrementableDomainKey object ) throws IOException {
        UUID id = object.getId();
        out.writeLong( id.getLeastSignificantBits() );
        out.writeLong( id.getMostSignificantBits() );
        out.writeLong( object.getIndex() );

    }

    @Override
    public IncrementableDomainKey read( ObjectDataInput in ) throws IOException {
        long lsb = in.readLong();
        long msb = in.readLong();
        long index = in.readLong();
        return new IncrementableDomainKey( new UUID( msb, lsb ), index );
    }

    @Override
    public Class<IncrementableDomainKey> getClazz() {
        return IncrementableDomainKey.class;
    }

}
