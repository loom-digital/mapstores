package com.kryptnostic.heracles.v1.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.heracles.authentication.v1.UUIDAuthenticationToken;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class UUIDAuthenticationTokenStreamSerializer implements SelfRegisteringStreamSerializer<UUIDAuthenticationToken> {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.UUID_AUTHENTICATION_TOKEN.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public void write( ObjectDataOutput out, UUIDAuthenticationToken object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getPrincipal() );
        out.writeUTF( (String) object.getCredentials() );
    }

    @Override
    public UUIDAuthenticationToken read( ObjectDataInput in ) throws IOException {
        return new UUIDAuthenticationToken( UUIDStreamSerializer.deserialize( in ), in.readUTF() );
    }


    @Override
    public Class<UUIDAuthenticationToken> getClazz() {
        return UUIDAuthenticationToken.class;
    }
}
