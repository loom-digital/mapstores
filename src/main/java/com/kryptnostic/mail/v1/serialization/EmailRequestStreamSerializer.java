package com.kryptnostic.mail.v1.serialization;

import java.io.IOException;

import com.google.common.base.Optional;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mail.models.EmailRequest;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class EmailRequestStreamSerializer implements SelfRegisteringStreamSerializer<EmailRequest> {

    @Override
    public void write( ObjectDataOutput out, EmailRequest object ) throws IOException {
        serialize( out, object );
    }

    @Override
    public EmailRequest read( ObjectDataInput in ) throws IOException {
        return deserialize( in );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.EMAIL_REQUEST.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<EmailRequest> getClazz() {
        return EmailRequest.class;
    }

    private void serialize( ObjectDataOutput out, EmailRequest object ) throws IOException {
        serializeStringArray( out, object.getTo() );
        out.writeBoolean( object.getFrom().isPresent() );
        if ( object.getFrom().isPresent() ) {
            out.writeUTF( object.getFrom().get() );
        }
        out.writeBoolean( object.getCc().isPresent() );
        if ( object.getCc().isPresent() ) {
            serializeStringArray( out, object.getCc().get() );
        }
        out.writeBoolean( object.getBcc().isPresent() );
        if ( object.getBcc().isPresent() ) {
            serializeStringArray( out, object.getBcc().get() );
        }
    }

    public static void serializeStringArray( ObjectDataOutput output, String[] strings ) throws IOException {
        output.writeInt( strings.length );
        for ( String string : strings ) {
            output.writeUTF( string );
        }
    }

    public static String[] readStringArray( ObjectDataInput input ) throws IOException {
        int size = input.readInt();
        String[] strings = new String[ size ];

        for ( int i = 0; i < size; i++ ) {
            strings[ i ] = input.readUTF();
        }
        return strings;
    }

    private EmailRequest deserialize( ObjectDataInput in ) throws IOException {
        String[] to = readStringArray( in );
        Optional<String> from = in.readBoolean() ? Optional.of( in.readUTF() ) : Optional.absent();
        Optional<String[]> cc = in.readBoolean() ? Optional.of( readStringArray( in ) ) : Optional.absent();
        Optional<String[]> bcc = in.readBoolean() ? Optional.of( readStringArray( in ) ) : Optional.absent();
        return new EmailRequest( from, to, cc, bcc );
    }

}
