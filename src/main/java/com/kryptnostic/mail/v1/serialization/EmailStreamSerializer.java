package com.kryptnostic.mail.v1.serialization;

import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.emails.AbstractEmailStreamSerializer;

public class EmailStreamSerializer extends AbstractEmailStreamSerializer {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.EMAIL.ordinal();
    }

}
