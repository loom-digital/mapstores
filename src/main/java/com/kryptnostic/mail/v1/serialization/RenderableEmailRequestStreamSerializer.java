package com.kryptnostic.mail.v1.serialization;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.kodex.v1.serialization.jackson.KodexObjectMapperFactory;
import com.kryptnostic.mail.models.RenderableEmailRequest;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

import jodd.mail.att.ByteArrayAttachment;

public class RenderableEmailRequestStreamSerializer
        implements SelfRegisteringStreamSerializer<RenderableEmailRequest> {

    private static final ObjectMapper objectMapper = KodexObjectMapperFactory.getObjectMapper();

    @Override
    public void write( ObjectDataOutput out, RenderableEmailRequest object ) throws IOException {
        serialize( out, object );
    }

    @Override
    public RenderableEmailRequest read( ObjectDataInput in ) throws IOException {
        return deserialize( in );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.RENDERABLE_EMAIL_REQUEST.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<RenderableEmailRequest> getClazz() {
        return RenderableEmailRequest.class;
    }

    public static void serializeStringArray( ObjectDataOutput output, String[] strings ) throws IOException {
        output.writeInt( strings.length );
        for ( String string : strings ) {
            output.writeUTF( string );
        }
    }

    public static String[] readStringArray( ObjectDataInput input ) throws IOException {
        int size = input.readInt();
        String[] strings = new String[ size ];

        for ( int i = 0; i < size; i++ ) {
            strings[ i ] = input.readUTF();
        }
        return strings;
    }

    public static void serializeByteArrayAttachmentArray( ObjectDataOutput output, ByteArrayAttachment[] attachments )
            throws IOException {
        output.writeInt( attachments.length );
        for ( ByteArrayAttachment attachment : attachments ) {
            output.writeUTF( attachment.getName() );
            output.writeUTF( attachment.getContentType() );
            output.writeUTF( attachment.getContentId() );
            output.writeByteArray( attachment.toByteArray() );
        }
    }

    public static ByteArrayAttachment[] readByteArrayAttachmentArray( ObjectDataInput input ) throws IOException {
        int size = input.readInt();
        ByteArrayAttachment[] attachments = new ByteArrayAttachment[ size ];
        for ( int i = 0; i < size; i++ ) {
            String name = input.readUTF();
            String contentType = input.readUTF();
            String contentId = input.readUTF();
            attachments[ i ] = new ByteArrayAttachment( input.readByteArray(), contentType, name, contentId );
        }
        return attachments;
    }

    private void serialize( ObjectDataOutput out, RenderableEmailRequest object ) throws IOException {
        serializeStringArray( out, object.getTo() );
        out.writeUTF( object.getTemplatePath() );

        out.writeBoolean( object.getFrom().isPresent() );
        if ( object.getFrom().isPresent() ) {
            out.writeUTF( object.getFrom().get() );
        }

        out.writeBoolean( object.getCc().isPresent() );
        if ( object.getCc().isPresent() ) {
            serializeStringArray( out, object.getCc().get() );
        }

        out.writeBoolean( object.getBcc().isPresent() );
        if ( object.getBcc().isPresent() ) {
            serializeStringArray( out, object.getBcc().get() );
        }

        out.writeBoolean( object.getSubject().isPresent() );
        if ( object.getSubject().isPresent() ) {
            out.writeUTF( object.getSubject().get() );
        }

        out.writeBoolean( object.getTemplateObjs().isPresent() );
        if ( object.getTemplateObjs().isPresent() ) {
            out.writeByteArray( objectMapper.writeValueAsBytes( object.getTemplateObjs().get() ) );

        }
        out.writeBoolean( object.getByteArrayAttachment().isPresent() );
        if ( object.getByteArrayAttachment().isPresent() ) {
            serializeByteArrayAttachmentArray( out, object.getByteArrayAttachment().get() );
        }
        out.writeBoolean( object.getAttachmentPaths().isPresent() );
        if ( object.getAttachmentPaths().isPresent() ) {
            serializeStringArray( out, object.getAttachmentPaths().get() );
        }
    }

    private RenderableEmailRequest deserialize( ObjectDataInput in ) throws IOException {
        String[] to = readStringArray( in );
        String templateName = in.readUTF();
        Optional<String> from = in.readBoolean() ? Optional.of( in.readUTF() ) : Optional.<String> absent();
        Optional<String[]> cc = in.readBoolean() ? Optional.of( readStringArray( in ) ) : Optional.<String[]> absent();
        Optional<String[]> bcc = in.readBoolean() ? Optional.of( readStringArray( in ) ) : Optional.<String[]> absent();
        Optional<String> subject = in.readBoolean() ? Optional.of( in.readUTF() ) : Optional.<String> absent();
        Optional<Object> templateObjs = in.readBoolean()
                ? Optional.of( objectMapper.readValue( in.readByteArray(), Map.class ) ) : Optional.<Object> absent();
        Optional<ByteArrayAttachment[]> byteArrayAttachment = in.readBoolean()
                ? Optional.of( readByteArrayAttachmentArray( in ) ) : Optional.<ByteArrayAttachment[]> absent();
        Optional<String[]> attachmentPath = in.readBoolean() ? Optional.of( readStringArray( in ) )
                : Optional.<String[]> absent();

        return new RenderableEmailRequest(
                from,
                to,
                cc,
                bcc,
                templateName,
                subject,
                templateObjs,
                byteArrayAttachment,
                attachmentPath );
    }
}
