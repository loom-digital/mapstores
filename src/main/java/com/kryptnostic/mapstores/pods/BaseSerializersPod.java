package com.kryptnostic.mapstores.pods;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.kryptnostic.conductor.v1.serialization.MonitoringServiceEntryProcessorStreamSerializer;
import com.kryptnostic.conductor.v1.serialization.ServiceDescriptorSetStreamSerializer;
import com.kryptnostic.conductor.v1.serialization.ServiceDescriptorStreamSerializer;
import com.kryptnostic.conductor.v1.serialization.ServiceRegistrationServiceEntryProcessorStreamSerializer;
import com.kryptnostic.heracles.directory.v1.serialization.AddEmailEntryProcessorStreamSerializer;
import com.kryptnostic.heracles.directory.v1.serialization.DomainUpdateEntryProcessorStreamSerializer;
import com.kryptnostic.heracles.directory.v1.serialization.RemoveGroupMembershipEntryProcessorStreamSerializer;
import com.kryptnostic.heracles.directory.v2.serializers.DomainResourceRequireEntryProcessorStreamSerializer;
import com.kryptnostic.heracles.directory.v2.serializers.DomainResourcesKeyStreamSerializer;
import com.kryptnostic.heracles.directory.v2.serializers.DomainResourcesUsageStreamSerializer;
import com.kryptnostic.heracles.directory.v2.serializers.UserACLIdStreamSerializer;
import com.kryptnostic.heracles.v1.serializers.ContactQueryEmailEntryProcessorStreamSerializer;
import com.kryptnostic.heracles.v1.serializers.ContactQueryResultStreamSerializer;
import com.kryptnostic.heracles.v1.serializers.ContactQueryUsernameEntryProcessorStreamSerializer;
import com.kryptnostic.heracles.v1.serializers.DomainStreamSerializer;
import com.kryptnostic.heracles.v1.serializers.DomainUpdateStreamSerializer;
import com.kryptnostic.heracles.v1.serializers.IncrementableDomainKeyStreamSerializer;
import com.kryptnostic.heracles.v1.serializers.UUIDAuthenticationTokenStreamSerializer;
import com.kryptnostic.mail.v1.serialization.EmailRequestStreamSerializer;
import com.kryptnostic.mail.v1.serialization.EmailStreamSerializer;
import com.kryptnostic.mail.v1.serialization.RenderableEmailRequestStreamSerializer;
import com.kryptnostic.mapstores.serializers.ContainsKeyEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v1.serializers.GroupStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.AccessGrantingEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.AclPermissionPairStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.AddIndexSegmentIdEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.AddPolicyToDomainEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.AddSharedObjectEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.AddToLevelFilteringMapEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.AddToUserSettingsEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.AddTrustsToDomainEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.AuthorizationInfoStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.BlockCiphertextStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.CheckExistsAndLatestVersionEPStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.CheckObjectFinalizedEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.CheckPermissionOnObjectEPStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.ClockIncrementEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.CryptomaterialStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.CypherStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.DomainNotificationEmailUpdateEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.EmailsStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.GetACLIdEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.GetDomainNotificationEmailEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.GetObjectACLIdEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.GetObjectIdsByTypeEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.GetObjectsWithPermissionEPStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.IncomingShareRemoverEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.IndexSegmentAddressStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.IndexSegmentCountEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.ObjectMetadataLevelKeyStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.OperateOnEdgeSetEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.OrderedUUIDSetStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.PermissionCheckEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.PermissionUpdatingEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.RemoveFromUserSettingsEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.RemovePolicyFromDomainEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.RemoveTrustsFromDomainEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.ShareStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.TokenVerificationEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.UUIDSetContainsAnyEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.UUIDSetContainsEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.UpdateAndUploadProgressEntryProcessorStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.UpdateObjectTypeEPStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.UpdateReadableTypedObjectsEPStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.UserObjectTypePairStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.UserSettingsKeyStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.VersionedObjectKeySetStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.VersionedObjectKeyStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.VersionedObjectUserKeyStreamSerializer;
import com.kryptnostic.metrics.v1.serialization.MetricsLogRecordStreamSerializer;
import com.kryptnostic.services.v1.serialization.AddUserToDomainEntryProcessorStreamSerializer;
import com.kryptnostic.services.v1.serialization.IncomingSharesStreamSerializer;
import com.kryptnostic.services.v1.serialization.NotificationPreferenceStreamSerializer;
import com.kryptnostic.services.v1.serialization.ObjectBlockKeyStreamSerializer;
import com.kryptnostic.services.v1.serialization.ObjectMetadataStreamSerializer;
import com.kryptnostic.services.v1.serialization.ObjectSearchPairStreamSerializer;
import com.kryptnostic.services.v1.serialization.ObjectUserKeyStreamSerializer;
import com.kryptnostic.services.v1.serialization.SearchEntryProcessorStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;
import com.kryptnostic.sharing.v1.serialization.IncomingShareRemoverStreamSerializer;
import com.kryptnostic.sharing.v1.serialization.KeyUpdateResponseStreamSerializer;

@Configuration
public class BaseSerializersPod {

    @Bean
    public ContainsKeyEntryProcessorStreamSerializer containsKeyEPSS() {
        return new ContainsKeyEntryProcessorStreamSerializer();
    }

    @Bean
    public IncomingSharesStreamSerializer incomingSharesStreamSerializer() {
        return new IncomingSharesStreamSerializer();
    }

    @Bean
    public AddSharedObjectEntryProcessorStreamSerializer shareObjectStreamSerializer() {
        return new AddSharedObjectEntryProcessorStreamSerializer();
    }

    @Bean
    public com.kryptnostic.services.v1.serialization.ShareStreamSerializer shareStreamSerializer() {
        return new com.kryptnostic.services.v1.serialization.ShareStreamSerializer();
    }

    @Bean
    public KeyUpdateResponseStreamSerializer kurss() {
        // TODO: These are just echoed-back. Make the useful or get rid of them.
        return new KeyUpdateResponseStreamSerializer();
    }

    @Bean
    public IncomingShareRemoverStreamSerializer incomingShareRemoveStreamSerializer() {
        return new IncomingShareRemoverStreamSerializer();
    }

    @Bean
    public UpdateReadableTypedObjectsEPStreamSerializer atrtoepss() {
        return new UpdateReadableTypedObjectsEPStreamSerializer();
    }

    @Bean
    public IncomingShareRemoverEntryProcessorStreamSerializer isrepss() {
        return new IncomingShareRemoverEntryProcessorStreamSerializer();
    }

    @Bean
    public UpdateObjectTypeEPStreamSerializer uotepss() {
        return new UpdateObjectTypeEPStreamSerializer();
    }

    @Bean
    public UUIDStreamSerializer uuidss() {
        return new UUIDStreamSerializer();
    }

    @Bean
    public UserObjectTypePairStreamSerializer uotpss() {
        return new UserObjectTypePairStreamSerializer();
    }

    @Bean
    public ContactQueryEmailEntryProcessorStreamSerializer cqempss() {
        return new ContactQueryEmailEntryProcessorStreamSerializer();
    }

    @Bean
    public ContactQueryResultStreamSerializer contactQueryResultStreamSerializer() {
        return new ContactQueryResultStreamSerializer();
    }

    @Bean
    public AddUserToDomainEntryProcessorStreamSerializer addUserToDomainEntryProcessStreamSerializer() {
        return new AddUserToDomainEntryProcessorStreamSerializer();
    }

    @Bean
    public SearchEntryProcessorStreamSerializer searchEntryProcessor() {
        return new SearchEntryProcessorStreamSerializer();
    }

    @Bean
    public DomainStreamSerializer domainSerializer() {
        return new DomainStreamSerializer();
    }

    @Bean
    public IncrementableDomainKeyStreamSerializer domainKeySerializer() {
        return new IncrementableDomainKeyStreamSerializer();
    }

    @Bean
    public ObjectMetadataStreamSerializer omss() {
        return new ObjectMetadataStreamSerializer();
    }

    @Bean
    public ObjectMetadataLevelKeyStreamSerializer omlvkss() {
        return new ObjectMetadataLevelKeyStreamSerializer();
    }

    @Bean
    public NotificationPreferenceStreamSerializer npss() {
        return new NotificationPreferenceStreamSerializer();
    }

    @Bean
    public ObjectUserKeyStreamSerializer ouks() {
        return new ObjectUserKeyStreamSerializer();
    }

    @Bean
    public com.kryptnostic.mapstores.v2.serializers.ObjectUserKeyStreamSerializer ouks2() {
        return new com.kryptnostic.mapstores.v2.serializers.ObjectUserKeyStreamSerializer();
    }

    @Bean
    public VersionedObjectUserKeyStreamSerializer vouks() {
        return new VersionedObjectUserKeyStreamSerializer();
    }

    @Bean
    public ObjectBlockKeyStreamSerializer obk() {
        return new ObjectBlockKeyStreamSerializer();
    }

    @Bean
    public ObjectSearchPairStreamSerializer sip() {
        return new ObjectSearchPairStreamSerializer();
    }

    @Bean
    public ContactQueryUsernameEntryProcessorStreamSerializer cquepss() {
        return new ContactQueryUsernameEntryProcessorStreamSerializer();
    }

    @Bean
    public OrderedUUIDSetStreamSerializer oiss() {
        return new OrderedUUIDSetStreamSerializer();
    }

    @Bean
    public AddEmailEntryProcessorStreamSerializer aeepss() {
        return new AddEmailEntryProcessorStreamSerializer();
    }

    @Bean
    public RemoveGroupMembershipEntryProcessorStreamSerializer rgmepss() {
        return new RemoveGroupMembershipEntryProcessorStreamSerializer();
    }

    @Bean
    public BlockCiphertextStreamSerializer bcss() {
        return new BlockCiphertextStreamSerializer();
    }

    @Bean
    public ShareStreamSerializer shStreamSerializer() {
        return new ShareStreamSerializer();
    }

    @Bean
    public com.kryptnostic.mapstores.v2.serializers.ShareStreamSerializer sStreamSerializer() {
        return new com.kryptnostic.mapstores.v2.serializers.ShareStreamSerializer();
    }

    @Bean
    public AuthorizationInfoStreamSerializer aiss() {
        return new AuthorizationInfoStreamSerializer();
    }

    @Bean
    public UUIDAuthenticationTokenStreamSerializer uatss() {
        return new UUIDAuthenticationTokenStreamSerializer();
    }

    @Bean
    public VersionedObjectKeyStreamSerializer vokss() {
        return new VersionedObjectKeyStreamSerializer();
    }

    @Bean
    public VersionedObjectKeySetStreamSerializer voksss() {
        return new VersionedObjectKeySetStreamSerializer();
    }

    @Bean
    public com.kryptnostic.mapstores.v2.serializers.ObjectMetadataStreamSerializer omss2() {
        return new com.kryptnostic.mapstores.v2.serializers.ObjectMetadataStreamSerializer();
    }

    @Bean
    public OperateOnEdgeSetEntryProcessorStreamSerializer ooesepss() {
        return new OperateOnEdgeSetEntryProcessorStreamSerializer();
    }

    @Bean
    public BlockCiphertextStreamSerializer blockCiphertextStreamSerializer() {
        return new BlockCiphertextStreamSerializer();
    }

    @Bean
    public GroupStreamSerializer gss() {
        return new GroupStreamSerializer();
    }

    @Bean
    public AccessGrantingEntryProcessorStreamSerializer agepss() {
        return new AccessGrantingEntryProcessorStreamSerializer();
    }

    @Bean
    public AddIndexSegmentIdEntryProcessorStreamSerializer aisiepss() {
        return new AddIndexSegmentIdEntryProcessorStreamSerializer();
    }

    @Bean
    public IndexSegmentAddressStreamSerializer isass() {
        return new IndexSegmentAddressStreamSerializer();
    }

    @Bean
    public UpdateAndUploadProgressEntryProcessorStreamSerializer uupepss() {
        return new UpdateAndUploadProgressEntryProcessorStreamSerializer();
    }

    @Bean
    public CheckObjectFinalizedEntryProcessorStreamSerializer cofepss() {
        return new CheckObjectFinalizedEntryProcessorStreamSerializer();
    }

    @Bean
    public UUIDSetContainsEntryProcessorStreamSerializer uuidscep() {
        return new UUIDSetContainsEntryProcessorStreamSerializer();
    }

    @Bean
    public UUIDSetContainsAnyEntryProcessorStreamSerializer uuidscaep() {
        return new UUIDSetContainsAnyEntryProcessorStreamSerializer();
    }

    @Bean
    public GetObjectIdsByTypeEntryProcessorStreamSerializer goibtepss() {
        return new GetObjectIdsByTypeEntryProcessorStreamSerializer();
    }

    @Bean
    public IndexSegmentCountEntryProcessorStreamSerializer iscepss() {
        return new IndexSegmentCountEntryProcessorStreamSerializer();
    }

    @Bean
    public CypherStreamSerializer cyss() {
        return new CypherStreamSerializer();
    }

    @Bean
    public CryptomaterialStreamSerializer cmss() {
        return new CryptomaterialStreamSerializer();
    }

    @Bean
    public GetACLIdEntryProcessorStreamSerializer gaiepss() {
        return new GetACLIdEntryProcessorStreamSerializer();
    }

    /**
     * Krypt-services
     */

    @Bean
    public TokenVerificationEntryProcessorStreamSerializer tvepss() {
        return new TokenVerificationEntryProcessorStreamSerializer();
    }

    @Bean
    public ClockIncrementEntryProcessorStreamSerializer ciepss() {
        return new ClockIncrementEntryProcessorStreamSerializer();
    }

    @Bean
    public AddToLevelFilteringMapEntryProcessorStreamSerializer atlfmepss() {
        return new AddToLevelFilteringMapEntryProcessorStreamSerializer();
    }

    // authz
    @Bean
    public PermissionUpdatingEntryProcessorStreamSerializer puepss() {
        return new PermissionUpdatingEntryProcessorStreamSerializer();
    }

    @Bean
    public GetObjectACLIdEntryProcessorStreamSerializer goaepss() {
        return new GetObjectACLIdEntryProcessorStreamSerializer();
    }

    @Bean
    public CheckPermissionOnObjectEPStreamSerializer cpooepss() {
        return new CheckPermissionOnObjectEPStreamSerializer();
    }

    @Bean
    public PermissionCheckEntryProcessorStreamSerializer pcepss() {
        return new PermissionCheckEntryProcessorStreamSerializer();
    }

    @Bean
    public GetObjectsWithPermissionEPStreamSerializer gowpepss() {
        return new GetObjectsWithPermissionEPStreamSerializer();
    }

    @Bean
    public AclPermissionPairStreamSerializer appss() {
        return new AclPermissionPairStreamSerializer();
    }

    /**
     * Sharing
     */
    @Bean
    public CheckExistsAndLatestVersionEPStreamSerializer cealvepss() {
        return new CheckExistsAndLatestVersionEPStreamSerializer();
    }

    @Bean
    public UserACLIdStreamSerializer uaiss() {
        return new UserACLIdStreamSerializer();
    }

    //
    // Heracles
    //

    @Bean
    public UUIDAuthenticationTokenStreamSerializer usernamePasswordRealmAuthenticationTokenStreamSerializer() {
        return new UUIDAuthenticationTokenStreamSerializer();
    }

    @Bean
    public DomainUpdateStreamSerializer domainUpdateStreamSerializer() {
        return new DomainUpdateStreamSerializer();
    }

    @Bean
    public DomainUpdateEntryProcessorStreamSerializer domainUpdateEntryProcessorStreamSerializer() {
        return new DomainUpdateEntryProcessorStreamSerializer();
    }

    @Bean
    public DomainNotificationEmailUpdateEntryProcessorStreamSerializer domainNotificationEmailUpdateEntryProcessorStreamSerializer() {
        return new DomainNotificationEmailUpdateEntryProcessorStreamSerializer();
    }

    @Bean
    public AddTrustsToDomainEntryProcessorStreamSerializer addAddIncomingTrustsEntryProcessorStreamSerializer() {
        return new AddTrustsToDomainEntryProcessorStreamSerializer();
    }

    @Bean
    public RemoveTrustsFromDomainEntryProcessorStreamSerializer removeIncomingTrustsEntryProcessorStreamSerializer() {
        return new RemoveTrustsFromDomainEntryProcessorStreamSerializer();
    }

    @Bean
    public AddPolicyToDomainEntryProcessorStreamSerializer addPolicyToDomainEntryProcessorStreamSerializer() {
        return new AddPolicyToDomainEntryProcessorStreamSerializer();
    }

    @Bean
    public RemovePolicyFromDomainEntryProcessorStreamSerializer removePolicyFromDomainEntryProcessorStreamSerializer() {
        return new RemovePolicyFromDomainEntryProcessorStreamSerializer();
    }

    @Bean
    public UserSettingsKeyStreamSerializer userSettingsKeyStreamSerialzier() {
        return new UserSettingsKeyStreamSerializer();
    }

    @Bean
    public AddToUserSettingsEntryProcessorStreamSerializer addToUserSettingsEPSS() {
        return new AddToUserSettingsEntryProcessorStreamSerializer();
    }

    @Bean
    public RemoveFromUserSettingsEntryProcessorStreamSerializer removeFromUserSettingsEPSS() {
        return new RemoveFromUserSettingsEntryProcessorStreamSerializer();
    }

    //
    // kryptnostic-metrics
    //

    @Bean
    public MetricsLogRecordStreamSerializer metricsLogRecordStreamSerializer() {
        return new MetricsLogRecordStreamSerializer();
    }

    //
    // kryptnostic-conductor
    //

    @Bean
    public MonitoringServiceEntryProcessorStreamSerializer monitoringServiceEntryProcessorStreamSerializer() {
        return new MonitoringServiceEntryProcessorStreamSerializer();
    }

    @Bean
    public ServiceRegistrationServiceEntryProcessorStreamSerializer serviceRegistrationServiceEntryProcessorStreamSerializer() {
        return new ServiceRegistrationServiceEntryProcessorStreamSerializer();
    }

    @Bean
    public ServiceDescriptorStreamSerializer serviceDescriptorStreamSerializer() {
        return new ServiceDescriptorStreamSerializer();
    }

    @Bean
    public ServiceDescriptorSetStreamSerializer serviceDescriptorSetStreamSerializer() {
        return new ServiceDescriptorSetStreamSerializer();
    }

    //
    // kryptnostic-mail-service
    //
    @Bean
    public EmailStreamSerializer emailStreamSerializer() {
        return new EmailStreamSerializer();
    }

    @Bean
    public EmailsStreamSerializer emailsStreamSerializer() {
        return new EmailsStreamSerializer();
    }

    @Bean
    public EmailRequestStreamSerializer emailRequestStreamSerializer() {
        return new EmailRequestStreamSerializer();
    }

    @Bean
    public RenderableEmailRequestStreamSerializer renderableEmailRequestStreamSerializer() {
        return new RenderableEmailRequestStreamSerializer();
    }

    //
    // domain-resource related
    //

    @Bean
    public DomainResourcesKeyStreamSerializer domainResourcesKeyStreamSerializer() {
        return new DomainResourcesKeyStreamSerializer();
    }

    @Bean
    public DomainResourcesUsageStreamSerializer domainResourcesUsageStreamSerializer() {
        return new DomainResourcesUsageStreamSerializer();
    }

    @Bean
    public DomainResourceRequireEntryProcessorStreamSerializer domainResourceRequireEntryProcessor() {
        return new DomainResourceRequireEntryProcessorStreamSerializer();
    }

    @Bean
    public GetDomainNotificationEmailEntryProcessorStreamSerializer getDomainNotificationEmailEP() {
        return new GetDomainNotificationEmailEntryProcessorStreamSerializer();
    }

}
