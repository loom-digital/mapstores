package com.kryptnostic.mapstores.pods;

import java.util.concurrent.ConcurrentMap;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PagingState;
import com.datastax.driver.core.Session;
import com.kryptnostic.mapstores.v2.mapstores.ReadableObjectsSetProxy;
import com.kryptnostic.mapstores.v2.serializers.CassandraSetProxyStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.PagingStateStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.ReadableObjectsSetProxyStreamSerializer;
import com.kryptnostic.rhizome.configuration.ConfigurationConstants.HZ;
import com.kryptnostic.rhizome.configuration.ConfigurationKey;
import com.kryptnostic.rhizome.configuration.cassandra.CassandraConfiguration;
import com.kryptnostic.rhizome.mappers.SelfRegisteringValueMapper;
import com.kryptnostic.rhizome.mapstores.TestableSelfRegisteringMapStore;
import com.kryptnostic.rhizome.mapstores.cassandra.CassandraMapStoreFactory;
import com.kryptnostic.rhizome.mapstores.cassandra.DefaultCassandraSetProxy;
import com.kryptnostic.rhizome.pods.CassandraPod;
import com.kryptnostic.rhizome.pods.RegistryBasedMappersPod;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

@Configuration
@Profile( "cassandra" )
@Import( CassandraPod.class )
public class CassandraSerializersPod {

    @Inject
    private CassandraConfiguration                         cassandraConfiguration;

    @Inject
    private RegistryBasedMappersPod                        mappers;

    @Inject
    private Cluster                                        cluster;

    @Inject
    private Session                                        session;

    @Resource(
        name = "valueMappers" )
    ConcurrentMap<Class<?>, SelfRegisteringValueMapper<?>> map;

    @Bean
    public SelfRegisteringStreamSerializer<DefaultCassandraSetProxy> spss() {
        return new CassandraSetProxyStreamSerializer( session, map );
    }

    @Bean
    public SelfRegisteringStreamSerializer<ReadableObjectsSetProxy> rospss() {
        return new ReadableObjectsSetProxyStreamSerializer( session );
    }

    @Bean
    public SelfRegisteringStreamSerializer<PagingState> pagingStateStreamSerializer() {
        return new PagingStateStreamSerializer();
    }

    @Bean
    public TestableSelfRegisteringMapStore<ConfigurationKey, String> getConfigurationMapStore() {
        CassandraConfiguration config = cassandraConfiguration;
        return new CassandraMapStoreFactory.Builder()
                .withConfiguration( config )
                .withSession( cluster.newSession() )
                .withMappers( mappers )
                .build()
                .build( ConfigurationKey.class, String.class )
                .withTableAndMapName( HZ.MAPS.CONFIGURATION )
                .build();
    }

}
