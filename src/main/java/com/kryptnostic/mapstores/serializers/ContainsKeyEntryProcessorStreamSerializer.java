package com.kryptnostic.mapstores.serializers;

import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.ContainsKeyEntryProcessor;
import com.kryptnostic.rhizome.serializers.NoContentsStreamSerializer;

public class ContainsKeyEntryProcessorStreamSerializer extends NoContentsStreamSerializer<ContainsKeyEntryProcessor> {

    public ContainsKeyEntryProcessorStreamSerializer() {
        super( ContainsKeyEntryProcessor.class );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.CONTAINS_KEY_EP_SS.ordinal();
    }

}
