package com.kryptnostic.mapstores.v1.constants;

@Deprecated
public final class HazelcastNames {

    private HazelcastNames() {}

    public static final class Maps {
        private Maps() {}

        public static final String DATE_COUNT                    = "dateCount";
        public static final String DATE_USERS                    = "dateUsers";
        public static final String DATEUSER_DATA                 = "dateuserData";

        public static final String CONDUCTOR_MANAGED_SERVICES    = "conductorManagedServices";
    }

    public static final class Queues {
        private Queues() {}

        public static final String LOG_RECORDS    = "logRecords";
        public static final String MAIL_SERVICE   = "outgoingMail";
        public static final String EMAIL_REQUESTS = "emailRequests";
    }
}
