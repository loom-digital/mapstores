package com.kryptnostic.mapstores.v1.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.kodex.v1.models.Group;
import com.kryptnostic.kodex.v1.models.KryptnosticGroup;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class GroupStreamSerializer implements SelfRegisteringStreamSerializer<Group> {

    @Override
    public void write( ObjectDataOutput out, Group object ) throws IOException {
        out.writeUTF( object.getName() );
        UUIDStreamSerializer.serialize( out, object.getId() );
        SetStreamSerializers.fastUUIDSetSerialize( out, object.getGroups() );
        SetStreamSerializers.fastUUIDSetSerialize( out, object.getUsers() );
    }

    @Override
    public Group read( ObjectDataInput in ) throws IOException {
        String name = in.readUTF();
        UUID id = UUIDStreamSerializer.deserialize( in );
        UUIDSet groups = SetStreamSerializers.fastUUIDSetDeserialize( in );
        UUIDSet users = SetStreamSerializers.fastUUIDSetDeserialize( in );
        return new KryptnosticGroup( id, name, groups, users );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.GROUP.ordinal();
    }

    @Override
    public void destroy() {
    }

    @Override
    public Class<Group> getClazz() {
        return Group.class;
    }

}
