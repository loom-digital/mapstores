package com.kryptnostic.mapstores.v2;

import java.util.HashMap;
import java.util.UUID;

public class AuthorizationInfo extends HashMap<UUID, Boolean> {
    private static final long serialVersionUID = 5530248855108675437L;
}
