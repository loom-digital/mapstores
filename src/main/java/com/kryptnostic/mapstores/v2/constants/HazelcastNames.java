package com.kryptnostic.mapstores.v2.constants;

public final class HazelcastNames {

    public static final class Locks {

        public static final String TYPE_LOCK = "type_map_lock";

    }

    public static final class Caches {

    }

    private HazelcastNames() {}

    public static final class Suffixes {

        public static final String SALT_MAP    = "_salt";
        public static final String IV_MAP      = "_iv";
        public static final String TAG_MAP     = "_tag";
        public static final String CONTENT_MAP = "_content";
    }

    public static final class Maps {

        private static final String VERSION = "_v2";

        private Maps() {}

        // Storage service maps
        public static final String OBJECT_FORWARD_EDGE_MAP              = "object_forward_edge_map" + VERSION;
        public static final String OBJECT_BACK_EDGE_MAP                 = "object_back_edge_map" + VERSION;

        public static final String ENCRYPTED_OBJECTS                    = "encrypted_objects" + VERSION;
        public static final String PLAINTEXT_OBJECTS                    = "plaintext_objects";
        public static final String EVENT_OBJECTS                        = "events";
        public static final String OBJECT_UPLOAD_PROGRESS_MAP           = "object_upload_progress_map";
        public static final String OBJECT_ENCRYPTION_TYPE_MAP           = "object_encryption_type_map";

        public static final String OBJECT_CLOCK_OLD                     = "object_clock_v2";
        public static final String OBJECT_CLOCK                         = "object_clock_v3";
        public static final String CLOCK_SERVER                         = "clock_server" + VERSION;

        // Authorization service maps
        public static final String OBJECT_LOOKUP_ACL_MAP                = "acl_object_lookup";
        public static final String USER_LOOKUP_ACL_MAP                  = "acl_user_lookup_acl_index";
        public static final String REVERSE_USER_LOOKUP_ACL_MAP          = "reverse_acl_user_lookup";

        @Deprecated
        public static final String USER_LOOKUP_ACL_MAP_OLD              = "acl_user_lookup";

        // HazelcastKeyService maps
        public static final String AES_ENCRYPTED_OBJECT_CRYPTO_SERVICES = "aes_encrypted_object_crypto_services"
                                                                                + VERSION;
        public static final String USER_RSA_PRIVATE_KEYS                = "user_rsa_private_keys" + VERSION;
        public static final String OBJECT_CRYPTO_SERVICE_MAP            = "object_crypto_service_map" + VERSION;
        public static final String ENCRYPTED_MASTER_KEYS                = "encryptedMasterKeys";
        public static final String ENCRYPTED_KEY_SALTS_MAP              = "encrypted_key_salts_map" + VERSION;
        public static final String USER_RSA_PUBLIC_KEYS                 = "user_rsa_public_keys" + VERSION;
        public static final String USER_FHE_PRIVATE_KEYS                = "user_fhe_private_keys" + VERSION;
        public static final String USER_FHE_PUBLIC_KEYS                 = "user_fhe_public_keys" + VERSION;
        public static final String USER_FHE_SEARCH_PRIVATE_KEYS         = "user_fhe_search_private_keys" + VERSION;
        public static final String USER_FHE_HASH_FUNCTION               = "user_fhe_hash_function" + VERSION;

        // Sharing
        public static final String INCOMING_SHARED_OBJECT_KEYS          = "incoming_shared_object_ids_map" + VERSION;
        public static final String INCOMING_SHARES                      = "incoming_shares" + VERSION;

        // Searching
        public static final String OBJECT_SEARCH_PAIRS_MAP              = "search_pairs" + VERSION;
        public static final String INDEX_SEGMENT_COUNTS                 = "index_segment_counts" + VERSION;
        public static final String INDEX_SEGMENT_IDS                    = "index_segment_ids" + VERSION;
        public static final String INDEX_SEGMENT_IDS_INLINE             = "index_segment_ids_inline" + VERSION;

        // Maps that replace V1 directory maps
        public static final String USER_EMAIL_TO_USER_ID                = "username_to_uuid" + VERSION;
        public static final String USER_EMAILS                          = "user_emails" + VERSION;
        public static final String USER_IDS_TO_NAMES                    = "user_ids_to_names" + VERSION;
        public static final String USER_AUTHENTICATORS                  = "user_authenticators" + VERSION;
        public static final String USER_CERTIFICATES                    = "user_certificates" + VERSION;
        public static final String USER_ROLES                           = "user_roles" + VERSION;
        public static final String USER_SETTINGS                        = "user_settings" + VERSION;
        public static final String DOMAINS                              = "domains" + VERSION;
        public static final String DOMAIN_NAME_TO_ID                    = "domain_name_to_id" + VERSION;
        public static final String DOMAIN_MEMBERS                       = "domain_members" + VERSION;
        public static final String DOMAIN_ID_TO_TRUSTED_DOMAINS         = "domain_id_to_trusted_domains" + VERSION;
        public static final String DOMAIN_ID_TO_INCOMING_TRUSTS         = "domain_id_to_incoming_trusts" + VERSION;
        public static final String DOMAINS_WITH_POLICY                  = "domains_with_policy" + VERSION;
        public static final String USER_TO_GROUPS                       = "user_to_groups" + VERSION;
        public static final String DOMAIN_ID_TO_NAME                    = "domain_id_to_name" + VERSION;
        public static final String DOMAIN_RESOURCES                     = "domain_resources" + VERSION;

        // Types
        public static final String NAME_TO_TYPE_MAP                     = "name_to_type_map" + VERSION;
        public static final String TYPE_TO_NAME_MAP                     = "type_to_name_map" + VERSION;
        public static final String SCOPES                               = "scopes";
        public static final String TYPES_TO_SCOPES                      = "types_to_scopes";

        // Have mapstore but not actually in use anywhere
        public static final String GROUPS                               = "groups" + VERSION;
        public static final String GROUP_ID_TO_NAME                     = "group_id_to_name" + VERSION;
        // Metrics
        public static final String DATE_USERS                           = "date_users" + VERSION;
        public static final String DATE_COUNT                           = "date_count" + VERSION;
        public static final String DATE_USERDATA                        = "date_userdata" + VERSION;

        public static final String NEW_OBJECT_METADATA                  = "new_object_metadata";
        public static final String READABLE_TYPED_USER_OBJECTS          = "user_type_objectid_map";
        public static final String METADATA_LEVELS                      = "metadata_levels_v3";
        public static final String METADATA_HIERARCHY                   = "hierarchy_metadata";
        @Deprecated
        public static final String METADATA_LEVELS_OLD                  = "metadata_levels_v2";
        public static final String MAP_NAMES                            = "map_names";

        @Deprecated
        public static final String USER_ATTRIBUTES                      = "user_attributes" + VERSION;

    }

    public static final class Queues {
        private Queues() {}

        public static final String EMAIL_REQUESTS = "emailRequests";
    }

    public static final class Tables {
        /**
         * Only to be used for cases where multiple MapStores use the same table or when the table name should
         * reasonably differ from the MapStore name
         */
        public static final String NEW_OBJECT_METADATA    = "new_object_metadata";
        public static final String METADATA_LEVELS        = "metadata_levels_v3";
        public static final String METADATA_HIERARCHY     = "hierarchy_metadata";
        @Deprecated
        public static final String METADATA_LEVELS_LEGACY = "metadata_levels_v2";
        public static final String USER_LOOKUP_ACL_MAP    = "acl_user_lookup_acl_index";
    }
}
