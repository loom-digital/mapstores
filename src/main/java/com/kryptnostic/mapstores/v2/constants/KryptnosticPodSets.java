package com.kryptnostic.mapstores.v2.constants;

import java.util.Arrays;

import com.kryptnostic.instrumentation.v1.InstrumentationPod;
import com.kryptnostic.mapstores.pods.BaseSerializersPod;
import com.kryptnostic.mapstores.pods.CassandraSerializersPod;
import com.kryptnostic.mapstores.v2.pods.CassandraMapStoresPod;
import com.kryptnostic.mapstores.v2.pods.CodecsPod;
import com.kryptnostic.mapstores.v2.pods.KeymappersPod;
import com.kryptnostic.mapstores.v2.pods.ValueMappersPod;
import com.kryptnostic.rhizome.pods.CassandraPod;
import com.kryptnostic.rhizome.pods.RegistryBasedMappersPod;
import com.kryptnostic.rhizome.pods.hazelcast.RegistryBasedHazelcastInstanceConfigurationPod;

public class KryptnosticPodSets {

    /**
     * Depends on SerializationAndMappingPods
     */
    public static final Class<?>[] CassandraPods      = new Class[] {
                                                              CassandraPod.class,
                                                              CassandraMapStoresPod.class,                           // RegBasedMappers
                                                              CassandraSerializersPod.class,                         // RegBasedMappers
                                                      };

    /**
     * Leaf node, no outside dependencies
     */
    public static final Class<?>[] DataFormatPods     = new Class[] {
                                                              BaseSerializersPod.class,
                                                              KeymappersPod.class,
                                                              ValueMappersPod.class,
                                                              RegistryBasedMappersPod.class,
                                                              CodecsPod.class,
                                                      };

    /**
     * Leaf node, soft dependence on StreamSerializers and Mapstores
     */
    public static final Class<?>[] HazelcastPods      = new Class[] {
                                                              RegistryBasedHazelcastInstanceConfigurationPod.class };

    /**
     * Leaf node, no dependencies.
     */
    public static final Class<?>[] MetricsPods        = new Class[] { InstrumentationPod.class };

    public static final Class<?>[] PersistencePods    = concatAll( DataFormatPods,
                                                              CassandraPods );

    public static final Class<?>[] DefaultBackendPods = concatAll( MetricsPods,
                                                              PersistencePods,
                                                              HazelcastPods );

    public static <T> T[] concatAll( T[] first, T[]... rest ) {
        int totalLength = first.length;
        for ( T[] array : rest ) {
            totalLength += array.length;
        }
        T[] result = Arrays.copyOf( first, totalLength );
        int offset = first.length;
        for ( T[] array : rest ) {
            System.arraycopy( array, 0, result, offset, array.length );
            offset += array.length;
        }
        return result;
    }
}
