package com.kryptnostic.mapstores.v2.core;

import com.hazelcast.core.MessageListener;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

public interface EventProcessingService extends MessageListener<VersionedObjectKey> {

}
