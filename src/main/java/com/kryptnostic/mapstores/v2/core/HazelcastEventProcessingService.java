package com.kryptnostic.mapstores.v2.core;

import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import com.hazelcast.core.IMap;
import com.hazelcast.core.Message;
import com.kryptnostic.heracles.directory.v1.services.UserProxyService;
import com.kryptnostic.mail.client.MailServiceClient;
import com.kryptnostic.mapstores.v2.mapstores.ObjectMetadataLevelKey;
import com.kryptnostic.v2.storage.models.ObjectMetadata;
import com.kryptnostic.v2.storage.models.ScrollDirection;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;
import com.kryptnostic.v2.storage.models.VersionedObjectUserKey;
import com.kryptnostic.v2.storage.types.TypeUUIDs;

public class HazelcastEventProcessingService implements EventProcessingService {

//    private final ConcurrentMap<VersionedObjectKey, Set<UUID>> subscriptions;
//    private final ConcurrentMap<VersionedObjectKey, Lock>      locks;

    // Email subscriptions
//    private final IMap<VersionedObjectUserKey, Long>           emailSubscriptions;
//    private final IMap<VersionedObjectKey, Long>               eventScrolls;
//    private final IMap<UUID, ObjectMetadata>                   objectMetadataMap;

    private final MailServiceClient                            mailer;
    private final UserProxyService                             users;

    public HazelcastEventProcessingService( MailServiceClient mailer, UserProxyService users ) {
        this.mailer = mailer;
        this.users = users;
    }

    @Override
    public void onMessage( Message<VersionedObjectKey> message ) {
        VersionedObjectKey signaledObject = message.getMessageObject();
//        Set<UUID> subscribers = subscriptions.getOrDefault( signaledObject, ImmutableSet.of() );
//        if ( !subscribers.isEmpty() ) {}

    }

    void notifySubscriber( UUID user, VersionedObjectKey signaledObject ) {
        // Lookup scroll + metadata
//        UUID clock = eventScrolls.get( signaledObject );
//        ObjectMetadata objectMetadata = objectMetadataMap.get( signaledObject );

        // Prepare query key for retrieving events since last notification
//        ObjectMetadataLevelKey levelKey = new ObjectMetadataLevelKey.Builder(
//                objectMetadata.getACLId(),
//                ImmutableSet.of( TypeUUIDs.EVENT ) ).withBaseClock( clock ).withPaging( Optional.of( 1000 ),
//                        ScrollDirection.FORWARDS ).build();
//        try {
//            emailSubscriptions.lock( signaledObject, 500, TimeUnit.MILLISECONDS );
//            emailSubscriptions.getOrDefault( signaledObject, ImmutableSet.of() )
//                    .forEach( subscriber -> sendEmail( signaledObject, subscriber ) );
//        } finally {
//            emailSubscriptions.unlock( signaledObject );
//        }
    }

    void sendEmail( VersionedObjectKey event, UUID user ) {

    }

}
