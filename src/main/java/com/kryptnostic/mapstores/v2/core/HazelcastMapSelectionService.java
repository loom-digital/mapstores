package com.kryptnostic.mapstores.v2.core;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.kryptnostic.mapstores.v2.constants.HazelcastNames;
import com.kryptnostic.mapstores.v2.utils.EncryptedObjectDistributedMap;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;
import com.kryptnostic.v2.storage.types.TypeUUIDs;

public class HazelcastMapSelectionService implements MapSelectionService {
    private static final Logger                                                           logger                       = LoggerFactory
                                                                                                                               .getLogger(
                                                                                                                                       HazelcastMapSelectionService.class );
    private static final String                                                           DEFAULT_ENCRYPTED_OBJECT_MAP = HazelcastNames.Maps.ENCRYPTED_OBJECTS;
    private static final String                                                           DEFAULT_OBJECT_MAP           = HazelcastNames.Maps.PLAINTEXT_OBJECTS;
    private static final int                                                              MAX_CACHED_TYPES             = 10000;
    private final IMap<UUID, String>                                                      mapNames;
    private final LoadingCache<String, EncryptedObjectDistributedMap<VersionedObjectKey>> encryptedMapCache;
    private final LoadingCache<String, IMap<VersionedObjectKey, ?>>                       plaintextMapCache;

    @Inject
    public HazelcastMapSelectionService( final HazelcastInstance hazelcast ) {
        this.mapNames = hazelcast.getMap( HazelcastNames.Maps.MAP_NAMES );
        this.encryptedMapCache = CacheBuilder
                .newBuilder()
                .concurrencyLevel( Runtime.getRuntime().availableProcessors() )
                .maximumSize( MAX_CACHED_TYPES )
                .build( new CacheLoader<String, EncryptedObjectDistributedMap<VersionedObjectKey>>() {

                    @Override
                    public EncryptedObjectDistributedMap<VersionedObjectKey> load( String mapName ) throws Exception {
                        return new EncryptedObjectDistributedMap<>(
                                hazelcast,
                                mapName );
                    }

                } );

        this.plaintextMapCache = CacheBuilder
                .newBuilder()
                .concurrencyLevel( Runtime.getRuntime().availableProcessors() )
                .maximumSize( MAX_CACHED_TYPES )
                .build( new CacheLoader<String, IMap<VersionedObjectKey, ?>>() {

                    @Override
                    public IMap<VersionedObjectKey, ?> load( String mapName ) throws Exception {
                        return hazelcast.getMap( mapName );
                    }

                } );
        /*
         * For now we hard-code events in here. Plan is that registering type should require selecting type options such
         * as size, encrypted, ttl (long/short). We'll correctly select the correct object map for data based on options
         * for each particular type. Need to get type service into rotation.
         */
        setMapNameForType( TypeUUIDs.EVENT, HazelcastNames.Maps.EVENT_OBJECTS );
    }

    @Override
    public EncryptedObjectDistributedMap<VersionedObjectKey> getDefaultEncryptedDistributedMap() {
        try {
            return encryptedMapCache.get( DEFAULT_ENCRYPTED_OBJECT_MAP );
        } catch ( ExecutionException e ) {
            logger.error( "Unable to retrieve the default object map.", e );
        }
        return null;
    }

    @SuppressWarnings( "unchecked" )
    @Override
    public <V> IMap<VersionedObjectKey, V> getIMapForType( UUID typeId ) {
        try {
            return (IMap<VersionedObjectKey, V>) plaintextMapCache.get( getMapName( typeId ) );
        } catch ( ExecutionException e ) {
            logger.error( "Unable to retrieve the plaintext object map.", e );
        }
        return null;
    }

    @Override
    public EncryptedObjectDistributedMap<VersionedObjectKey> getEncryptedDistributedMapForType( UUID typeId ) {
        try {
            return encryptedMapCache.get( getEncryptedObjectMapName( typeId ) );
        } catch ( ExecutionException e ) {
            logger.error( "Unable to retrieve the default object map.", e );
        }
        return null;
    }

    @Override
    public void setMapNameForType( UUID typeId, String mapName ) {
        mapNames.set( typeId, mapName );
    }

    private String getEncryptedObjectMapName( UUID typeId ) {
        return mapNames.getOrDefault( typeId, DEFAULT_ENCRYPTED_OBJECT_MAP );
    }

    private String getMapName( UUID typeId ) {
        return mapNames.getOrDefault( typeId, DEFAULT_OBJECT_MAP );
    }
}
