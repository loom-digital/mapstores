package com.kryptnostic.mapstores.v2.core;

import java.util.UUID;

import com.hazelcast.core.IMap;
import com.kryptnostic.mapstores.v2.utils.EncryptedObjectDistributedMap;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

public interface MapSelectionService {

    EncryptedObjectDistributedMap<VersionedObjectKey> getDefaultEncryptedDistributedMap();

    <V> IMap<VersionedObjectKey, V> getIMapForType( UUID typeId );

    EncryptedObjectDistributedMap<VersionedObjectKey> getEncryptedDistributedMapForType( UUID typeId );

    void setMapNameForType( UUID typeId, String mapName );

}