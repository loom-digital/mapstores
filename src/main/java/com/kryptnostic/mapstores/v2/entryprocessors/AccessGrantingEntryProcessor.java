package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Set;
import java.util.UUID;

import com.google.common.collect.Sets;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractMerger;

public class AccessGrantingEntryProcessor extends AbstractMerger<UUID, Set<UUID>, UUID> {
    private static final long serialVersionUID = -2301514646177165310L;

    public AccessGrantingEntryProcessor( Set<UUID> newObjects ) {
        super(newObjects);
    }

    @Override
    protected Set<UUID> newEmptyCollection() {
        return Sets.newLinkedHashSet();
    }

}
