package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Arrays;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractMerger;
import com.kryptnostic.v2.search.IndexSegmentAddress;

public class AddIndexSegmentIdEntryProcessor extends AbstractMerger<IndexSegmentAddress, UUIDSet, UUID> {

    private static final long serialVersionUID = 7360842167044009896L;


    public AddIndexSegmentIdEntryProcessor( Iterable<UUID> indexSegmentIds ) {
        super( indexSegmentIds );

    }
    public AddIndexSegmentIdEntryProcessor( UUID indexSegmentId ) {
        super( Arrays.asList( indexSegmentId ) );
    }

    @Override
    protected UUIDSet newEmptyCollection() {
        return new UUIDSet();
    }

}
