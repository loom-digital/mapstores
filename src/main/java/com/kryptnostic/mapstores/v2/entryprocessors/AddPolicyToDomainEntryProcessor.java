package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Set;
import java.util.UUID;

import com.kryptnostic.directory.v1.domain.DomainPolicy;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractMerger;

import jersey.repackaged.com.google.common.collect.Sets;

public class AddPolicyToDomainEntryProcessor extends AbstractMerger<DomainPolicy, Set<UUID>, UUID>{
    private static final long serialVersionUID = 8159377907466104358L;

    public AddPolicyToDomainEntryProcessor( Iterable<UUID> objects ) {
        super( objects );
    }

    @Override
    protected Set<UUID> newEmptyCollection() {
        return Sets.newHashSet();
    }

}
