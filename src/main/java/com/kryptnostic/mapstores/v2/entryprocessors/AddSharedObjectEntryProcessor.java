package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Arrays;
import java.util.Set;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractMerger;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;
import com.kryptnostic.v2.storage.models.VersionedObjectKeySet;

public class AddSharedObjectEntryProcessor extends AbstractMerger<UUID, Set<VersionedObjectKey>, VersionedObjectKey> {

    private static final long serialVersionUID = 2324315934569367397L;

    public AddSharedObjectEntryProcessor( VersionedObjectKey objectKey ) {
        this( Arrays.asList( objectKey ) );
    }

    public AddSharedObjectEntryProcessor( Iterable<VersionedObjectKey> objectKeys ) {
        super( objectKeys );
    }

    @Override
    protected Set<VersionedObjectKey> newEmptyCollection() {
        return new VersionedObjectKeySet();
    }

}
