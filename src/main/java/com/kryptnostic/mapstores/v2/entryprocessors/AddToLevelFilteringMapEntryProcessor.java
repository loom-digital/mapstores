package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.kryptnostic.mapstores.v2.mapstores.ObjectMetadataLevelKey;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;
import com.kryptnostic.v2.storage.models.VersionedObjectKeySet;

public class AddToLevelFilteringMapEntryProcessor
        extends AbstractRhizomeEntryProcessor<ObjectMetadataLevelKey, VersionedObjectKeySet, Void> {

    private static final long             serialVersionUID = 4487947759275943599L;
    private final Set<VersionedObjectKey> keysToAdd;

    public AddToLevelFilteringMapEntryProcessor( Set<VersionedObjectKey> keys ) {
        this.keysToAdd = keys;
    }

    public AddToLevelFilteringMapEntryProcessor( VersionedObjectKey key ) {
        this.keysToAdd = ImmutableSet.of( key );
    }

    @Override
    public Void process( Entry<ObjectMetadataLevelKey, VersionedObjectKeySet> entry ) {
        entry.setValue( new VersionedObjectKeySet( keysToAdd ) );
        return null;
    }

    /**
     * @return the keysToAdd
     */
    public Set<VersionedObjectKey> getKeysToAdd() {
        return keysToAdd;
    }

}
