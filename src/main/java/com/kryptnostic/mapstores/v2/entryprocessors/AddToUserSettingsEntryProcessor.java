package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Set;

import com.kryptnostic.mapstores.v2.mapstores.UserSettingsKey;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractMerger;

import jersey.repackaged.com.google.common.collect.Sets;

public class AddToUserSettingsEntryProcessor extends AbstractMerger<UserSettingsKey, Set<String>, String>{
    private static final long serialVersionUID = -4749167498775116272L;

    public AddToUserSettingsEntryProcessor( Iterable<String> objects ) {
        super( objects );
    }

    @Override
    protected Set<String> newEmptyCollection() {
        return Sets.newHashSet();
    }

}
