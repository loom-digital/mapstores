package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Set;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractMerger;

import jersey.repackaged.com.google.common.collect.Sets;

public class AddTrustsToDomainEntryProcessor extends AbstractMerger<UUID, Set<UUID>, UUID> {
    private static final long serialVersionUID = 3540100596101011619L;

    public AddTrustsToDomainEntryProcessor( Iterable<UUID> objects ) {
        super( objects );
    }

    @Override
    protected Set<UUID> newEmptyCollection() {
        return Sets.newHashSet();
    }

}
