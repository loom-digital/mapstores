package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.storage.models.ObjectMetadata;

public class CheckMetadataExistsAndIsLatestVersionEntryProcessor
        extends AbstractRhizomeEntryProcessor<UUID, ObjectMetadata, Boolean> {

    private static final long serialVersionUID = 4163907846912705442L;

    private long version;

    public CheckMetadataExistsAndIsLatestVersionEntryProcessor( long version ) {
        this.version = version;
    }

    @Override
    public Boolean process( Entry<UUID, ObjectMetadata> entry ) {
        ObjectMetadata value = entry.getValue();
        return !( value == null || value.getVersion() > version );
    }

    public long getVersion() {
        return version;
    }


}
