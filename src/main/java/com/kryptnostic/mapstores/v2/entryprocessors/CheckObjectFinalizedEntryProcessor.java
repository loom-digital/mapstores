package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.EnumSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import com.kryptnostic.kodex.v1.crypto.ciphers.Cypher;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.crypto.CryptoMaterial;

public class CheckObjectFinalizedEntryProcessor
        extends AbstractRhizomeEntryProcessor<UUID, Set<CryptoMaterial>, Boolean> {

    private static final long serialVersionUID = -8811295698989009820L;
    private final Cypher      cypher;

    public CheckObjectFinalizedEntryProcessor( Cypher cypher ) {
        this.cypher = cypher;
    }

    @Override
    public Boolean process( Entry<UUID, Set<CryptoMaterial>> entry ) {
        EnumSet<CryptoMaterial> requiredByCypher = CryptoMaterial.requiredByCypher( cypher );
        if ( requiredByCypher.isEmpty() ) {
            return Boolean.TRUE;
        }
        Set<CryptoMaterial> value = entry.getValue();
        if ( value == null ) {
            return Boolean.FALSE;
        }
        // Note: we're basically cloning the value here b/c if we dont then setproxy runs contains in a loop below
        // so previously we were doing O(#cryptomaterialsRequired) db calls.
        // Now we're doing one
        EnumSet<CryptoMaterial> existingMats = EnumSet.<CryptoMaterial> noneOf( CryptoMaterial.class );
        for ( CryptoMaterial material : value ) {
            existingMats.add( material );
        }
        for (CryptoMaterial material : requiredByCypher) {
            if ( !existingMats.contains( material ) ) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    public Cypher getCypher() {
        return cypher;
    }
}
