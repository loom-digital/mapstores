package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.mapstores.UserACLId;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class CheckPermissionOnObjectEP extends AbstractRhizomeEntryProcessor<UserACLId, Set<Permission>, Boolean> {

    private static final long serialVersionUID = -2418726945491866632L;

    private Permission        permission;
    private UUID              aclIdForObject;

    public CheckPermissionOnObjectEP( Permission permission, UUID aclIdForObject ) {
        this.permission = permission;
        this.aclIdForObject = aclIdForObject;
    }

    @Override
    public Boolean process( Entry<UserACLId, Set<Permission>> entry ) {
        Set<Permission> value = entry.getValue();
        UserACLId key = entry.getKey();
        return Boolean.valueOf(
                !( !key.getAclId().equals( aclIdForObject ) || value == null || !value.contains( permission ) ) );
    }

    /**
     * @return the permission
     */
    public Permission getPermission() {
        return permission;
    }

    /**
     * @return the aclIdForObject
     */
    public UUID getAclIdForObject() {
        return aclIdForObject;
    }

}
