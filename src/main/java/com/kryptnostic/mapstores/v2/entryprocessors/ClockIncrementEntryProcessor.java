package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

public class ClockIncrementEntryProcessor extends AbstractRhizomeEntryProcessor<VersionedObjectKey, Integer, Integer> {
    private static final long serialVersionUID = 3970571158709270601L;
    private static final Integer INITIAL_CLOCK    = Integer.valueOf( 1 );

    @Override
    public Integer process( Entry<VersionedObjectKey, Integer> entry ) {
        Integer current = entry.getValue();
        if ( current == null ) {
            current = INITIAL_CLOCK;
            entry.setValue( current );
        } else {
            current = Integer.valueOf( current.intValue() + 1 );
            entry.setValue( current );
        }
        return current;
    }

}
