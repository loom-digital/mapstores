package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class ContainsKeyEntryProcessor<K, V> extends AbstractRhizomeEntryProcessor<K, V, Boolean> {

    private static final long serialVersionUID = 1583610842311087389L;

    @Override
    public Boolean process( Entry<K, V> entry ) {
        return entry.getKey() != null;
    }

}
