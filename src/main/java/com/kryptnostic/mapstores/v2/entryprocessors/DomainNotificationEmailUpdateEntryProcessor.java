package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.UUID;

import com.kryptnostic.directory.v1.domain.Domain;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class DomainNotificationEmailUpdateEntryProcessor extends AbstractRhizomeEntryProcessor<UUID, Domain, Void> {
    private static final long serialVersionUID = 6905977279699551039L;
    private final String      email;

    public DomainNotificationEmailUpdateEntryProcessor( String email ) {
        this.email = email;
    }

    @Override
    public Void process( Entry<UUID, Domain> entry ) {
        Domain domain = entry.getValue();
        if ( domain == null ) {
            return null;
        }
        domain.setNotificationEmail( email );
        entry.setValue( domain );
        return null;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( email == null ) ? 0 : email.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( !( obj instanceof DomainNotificationEmailUpdateEntryProcessor ) ) return false;
        DomainNotificationEmailUpdateEntryProcessor other = (DomainNotificationEmailUpdateEntryProcessor) obj;
        if ( email == null ) {
            if ( other.email != null ) return false;
        } else if ( !email.equals( other.email ) ) return false;
        return true;
    }

}
