package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.storage.models.ObjectMetadata;

public class GetACLIdEntryProcessor extends AbstractRhizomeEntryProcessor<UUID, ObjectMetadata, UUID> {

    private static final long   serialVersionUID = 3480519429192625042L;

    private static final Logger logger           = LoggerFactory.getLogger( GetACLIdEntryProcessor.class );

    @Override
    public UUID process( Entry<UUID, ObjectMetadata> entry ) {
        ObjectMetadata value = entry.getValue();
        if ( value == null ) {
            System.err.println( "GetACLIdEntryProcessor should only be run on objects that already exist" );
            logger.error( "GetACLIdEntryProcessor should only be run on objects that already exist" );
            return null;
        }
        return value.getACLId();
    }

}
