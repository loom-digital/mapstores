package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.UUID;

import com.kryptnostic.kodex.v1.crypto.ciphers.Cypher;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class GetCypherForObjectEntryProcessor extends AbstractRhizomeEntryProcessor<UUID, Cypher, Cypher> {

    private static final long serialVersionUID = 7700434056488166091L;

    @Override
    public Cypher process( Entry<UUID, Cypher> entry ) {
        Cypher value = entry.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }

}
