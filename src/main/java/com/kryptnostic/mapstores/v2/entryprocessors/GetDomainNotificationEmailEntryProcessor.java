package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.UUID;

import com.kryptnostic.directory.v1.domain.Domain;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class GetDomainNotificationEmailEntryProcessor extends AbstractRhizomeEntryProcessor<UUID, Domain, String> {
    private static final long serialVersionUID = 6280440516501641602L;

    @Override
    public String process( Entry<UUID, Domain> entry ) {
        Domain domain = entry.getValue();
        if(domain == null){
            return null;
        }

        return domain.getNotificationEmail();
    }

}
