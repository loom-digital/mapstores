package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.storage.models.ObjectMetadata;

public class GetObjectACLIdEntryProcessor extends AbstractRhizomeEntryProcessor<UUID, ObjectMetadata, UUID> {
    public static final GetObjectACLIdEntryProcessor singleton = new GetObjectACLIdEntryProcessor();
    private static final long serialVersionUID = -1658561665871409138L;

    @Override
    public UUID process( Entry<UUID, ObjectMetadata> entry ) {
        ObjectMetadata value = entry.getValue();
        if ( value == null ) {
            return null;
        }
        return value.getACLId();
    }

}
