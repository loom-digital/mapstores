package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.storage.models.ObjectMetadata;

public class GetObjectIdsByTypeEntryProcessor extends AbstractRhizomeEntryProcessor<UUID, ObjectMetadata, Boolean> {

    private static final long serialVersionUID = -1968731300303709075L;
    private final UUID        typeIdToCheck;

    public GetObjectIdsByTypeEntryProcessor( UUID typeIdToCheck ) {
        this.typeIdToCheck = typeIdToCheck;
    }

    @Override
    public Boolean process( Entry<UUID, ObjectMetadata> entry ) {
        ObjectMetadata value = entry.getValue();
        return value != null && typeIdToCheck.equals( value.getType() );
    }

    public UUID getTypeIdToCheck() {
        return typeIdToCheck;
    }

}
