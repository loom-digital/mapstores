package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.storage.models.ObjectMetadata;

public class GetObjectTypeEntryProcessor extends AbstractRhizomeEntryProcessor<UUID, ObjectMetadata, UUID> {

    private static final long serialVersionUID = -3281724620724429701L;

    @Override
    public UUID process( Entry<UUID, ObjectMetadata> entry ) {
        ObjectMetadata value = entry.getValue();
        if ( value == null ) {
            return null;
        }
        return value.getType();
    }

}
