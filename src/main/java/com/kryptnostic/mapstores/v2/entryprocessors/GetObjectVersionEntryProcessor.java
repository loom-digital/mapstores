package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.storage.models.ObjectMetadata;

public class GetObjectVersionEntryProcessor extends AbstractRhizomeEntryProcessor<UUID, ObjectMetadata, Long> {

    private static final long serialVersionUID = -4282385449867291562L;

    @Override
    public Long process( Entry<UUID, ObjectMetadata> entry ) {
        ObjectMetadata metadata = entry.getValue();
        if ( metadata == null ) {
            return Long.valueOf( 0 );
        }
        return Long.valueOf( metadata.getVersion() );
    }

}
