package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.mapstores.UserACLId;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class GetObjectsWithPermissionEntryProcessor
        extends AbstractRhizomeEntryProcessor<UserACLId, Set<Permission>, UUID> {

    private static final long serialVersionUID = 5598924592375808391L;

    private Permission permission;

    public GetObjectsWithPermissionEntryProcessor( Permission permission ) {
        this.permission = permission;
    }

    @Override
    public UUID process( Entry<UserACLId, Set<Permission>> entry ) {
        Set<Permission> value = entry.getValue();
        if ( value == null || !value.contains( permission ) ) {
            return null;
        }
        return entry.getKey().getAclId();
    }

    /**
     * @return the permission
     */
    public Permission getPermission() {
        return permission;
    }

    /**
     * @param permission the permission to set
     */
    public void setPermission( Permission permission ) {
        this.permission = permission;
    }

}
