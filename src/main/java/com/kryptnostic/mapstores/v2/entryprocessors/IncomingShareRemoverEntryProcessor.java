package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRemover;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

/**
 * @author Drew Bailey drew@kryptnostic.com
 *
 */
public class IncomingShareRemoverEntryProcessor
        extends AbstractRemover<UUID, Collection<VersionedObjectKey>, VersionedObjectKey> {

    private static final long serialVersionUID = 7919090128232882782L;

    public IncomingShareRemoverEntryProcessor( VersionedObjectKey objectKey ) {
        this( Arrays.asList( objectKey ) );
    }

    public IncomingShareRemoverEntryProcessor( Collection<VersionedObjectKey> objectIds ) {
        super( objectIds );
    }
}
