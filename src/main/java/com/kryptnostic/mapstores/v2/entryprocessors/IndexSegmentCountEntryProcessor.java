package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class IndexSegmentCountEntryProcessor extends AbstractRhizomeEntryProcessor<UUID, Integer, Integer> {

    private static final long serialVersionUID = 2493201353484273823L;
    private final Integer     count;

    public IndexSegmentCountEntryProcessor( int count ) {
        this.count = Integer.valueOf( count );
    }

    @Override
    public Integer process( Entry<UUID, Integer> entry ) {
        Integer value = entry.getValue();
        if (value == null) {
            entry.setValue( count );
            return Integer.valueOf( 0 );
        }
        entry.setValue( Integer.valueOf( value.intValue() + count.intValue() ) );
        return value;
    }

    public int getCount() {
        return count.intValue();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + count.intValue();
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( getClass() != obj.getClass() ) return false;
        IndexSegmentCountEntryProcessor other = (IndexSegmentCountEntryProcessor) obj;
        if ( count != other.count ) return false;
        return true;
    }

}
