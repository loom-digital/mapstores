package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.Set;

import com.kryptnostic.rhizome.hazelcast.objects.SetProxy;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;
import com.kryptnostic.v2.storage.models.VersionedObjectKeySet;

public class OperateOnEdgeSetEntryProcessor
        extends AbstractRhizomeEntryProcessor<VersionedObjectKey, Set<VersionedObjectKey>, Void> {

    private static final long        serialVersionUID = 2939594847614368656L;

    private final VersionedObjectKey valueToUse;
    private final EdgeSetOperation   operation;

    public enum EdgeSetOperation {
        ADD,
        DELETE;
    }

    public OperateOnEdgeSetEntryProcessor( EdgeSetOperation operation, VersionedObjectKey valueToUse ) {
        this.operation = operation;
        this.valueToUse = valueToUse;
    }

    @Override
    public Void process( Entry<VersionedObjectKey, Set<VersionedObjectKey>> entry ) {
        Set<VersionedObjectKey> value = entry.getValue();
        if ( value == null ) {
            value = new VersionedObjectKeySet( 1 );
        }
        switch ( operation ) {
            case ADD:
                value.add( valueToUse );
                break;
            case DELETE:
                value.remove( valueToUse );
                break;
            default:
                return null;
        }
        if ( !( value instanceof SetProxy<?, ?> ) ) {
            entry.setValue( value );
        }
        return null;
    }

    public VersionedObjectKey getValueToUse() {
        return valueToUse;
    }

    public EdgeSetOperation getOperation() {
        return operation;
    }

}
