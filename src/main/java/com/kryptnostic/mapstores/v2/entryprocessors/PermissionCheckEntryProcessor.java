package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.mapstores.UserACLId;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class PermissionCheckEntryProcessor extends AbstractRhizomeEntryProcessor<UserACLId, Set<Permission>, Boolean> {
    private static final Logger logger = LoggerFactory.getLogger( PermissionCheckEntryProcessor.class );
    private static final long serialVersionUID = -6631856769607627133L;
    private Permission        permission;

    public PermissionCheckEntryProcessor( Permission permission ) {
        this.permission = permission;
    }

    @Override
    public Boolean process( Entry<UserACLId, Set<Permission>> entry ) {
        Set<Permission> value = entry.getValue();
        if( value == null ) {
            logger.warn( "Encountered null permission set." );
        }
        return Boolean.valueOf( value != null && value.contains( permission ) );
    }

    /**
     * @return the permission
     */
    public Permission getPermission() {
        return permission;
    }

}
