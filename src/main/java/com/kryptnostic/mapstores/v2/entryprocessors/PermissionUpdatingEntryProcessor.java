package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.EnumSet;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.mapstores.UserACLId;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class PermissionUpdatingEntryProcessor extends AbstractRhizomeEntryProcessor<UserACLId, Set<Permission>, Void> {
    private static final Logger logger = LoggerFactory.getLogger( PermissionUpdatingEntryProcessor.class );

    public enum PermissionOperation {
        SET,
        REMOVE
    };

    private static final long serialVersionUID = -7791219527255549625L;

    private Set<Permission>     permissionsToUpdate;
    private PermissionOperation operation;

    private PermissionUpdatingEntryProcessor( Set<Permission> permissionToUpdate, PermissionOperation operation ) {
        this.permissionsToUpdate = permissionToUpdate;
        this.operation = operation;
    }

    public static PermissionUpdatingEntryProcessor setPermissions( Set<Permission> permissionToUpdate ) {
        return new PermissionUpdatingEntryProcessor( permissionToUpdate, PermissionOperation.SET );
    }

    public static PermissionUpdatingEntryProcessor setPermission( Permission permissionToUpdate ) {
        return setPermissions( EnumSet.of( permissionToUpdate ) );
    }

    public static PermissionUpdatingEntryProcessor removePermissions( Set<Permission> permissionToUpdate ) {
        return new PermissionUpdatingEntryProcessor( permissionToUpdate, PermissionOperation.REMOVE );
    }

    public static PermissionUpdatingEntryProcessor removePermission( Permission permissionToUpdate ) {
        return removePermissions( EnumSet.of( permissionToUpdate ) );
    }

    @Override
    public Void process( Entry<UserACLId, Set<Permission>> entry ) {
        Set<Permission> value = entry.getValue();
        if ( value == null ) {
            value = EnumSet.<Permission> noneOf( Permission.class );
        }
        switch ( operation ) {
            case SET:
                value.addAll( permissionsToUpdate );
                break;
            case REMOVE:
                if ( value.isEmpty() ) {
                    logger.warn(
                            "RemovePermissions was run on an empty set. There may be an inefficient or unused invocation that should be updated." );
                    return null;
                }
                value.removeAll( permissionsToUpdate );
                break;
            default:
                throw new UnsupportedOperationException(
                        "PermissionUpdateEntryProcessor must specify an operation to apply to permissions!" );
        }
        entry.setValue( value );
        return null;
    }

    /**
     * @return the permissionToUpdate
     */
    public Set<Permission> getPermissionsToUpdate() {
        return permissionsToUpdate;
    }

    /**
     * @return the operation
     */
    public PermissionOperation getOperation() {
        return operation;
    }

}
