package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Set;

import com.kryptnostic.mapstores.v2.mapstores.UserSettingsKey;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRemover;

public class RemoveFromUserSettingsEntryProcessor extends AbstractRemover<UserSettingsKey, Set<String>, String>{
    private static final long serialVersionUID = -8720703896668364000L;

    public RemoveFromUserSettingsEntryProcessor( Iterable<String> objectsToRemove ) {
        super( objectsToRemove );
    }

}
