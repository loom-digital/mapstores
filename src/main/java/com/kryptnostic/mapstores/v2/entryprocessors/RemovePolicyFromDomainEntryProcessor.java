package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Set;
import java.util.UUID;

import com.kryptnostic.directory.v1.domain.DomainPolicy;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRemover;

public class RemovePolicyFromDomainEntryProcessor extends AbstractRemover<DomainPolicy, Set<UUID>, UUID>{
    private static final long serialVersionUID = -6240779445977583236L;

    public RemovePolicyFromDomainEntryProcessor( Iterable<UUID> objectsToRemove ) {
        super( objectsToRemove );
    }

}
