package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Set;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRemover;

public class RemoveTrustsFromEntryProcessor extends AbstractRemover<UUID, Set<UUID>, UUID> {
    private static final long serialVersionUID = -1193957581555400517L;

    public RemoveTrustsFromEntryProcessor( Iterable<UUID> objectsToRemove ) {
        super( objectsToRemove );
    }

}
