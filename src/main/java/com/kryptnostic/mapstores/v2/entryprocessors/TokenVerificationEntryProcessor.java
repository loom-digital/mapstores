package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.Set;

import com.kryptnostic.mapstores.v2.mapstores.UserSettingsKey;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class TokenVerificationEntryProcessor
        extends AbstractRhizomeEntryProcessor<UserSettingsKey, Set<String>, Boolean> {
    private static final long serialVersionUID = 3424053851646730044L;
    private final String      confirmationToken;

    public TokenVerificationEntryProcessor( String confirmationToken ) {
        this.confirmationToken = confirmationToken;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    @Override
    public Boolean process( Entry<UserSettingsKey, Set<String>> entry ) {
        return entry.getValue().iterator().next().equals( confirmationToken );
    }
}
