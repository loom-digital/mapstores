package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Set;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.SetContainsAnyEntryProcessor;

public class UUIDSetContainsAnyEntryProcessor extends SetContainsAnyEntryProcessor<UUID> {

    private static final long serialVersionUID = -8257863078578779512L;

    public UUIDSetContainsAnyEntryProcessor( Set<UUID> objectsToCheck ) {
        super( objectsToCheck );
    }

}
