package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.SetContainsEntryProcessor;

public class UUIDSetContainsEntryProcessor extends SetContainsEntryProcessor<UUID> {

    private static final long serialVersionUID = 498956458752041996L;

    public UUIDSetContainsEntryProcessor( UUID object ) {
        super( object );
    }

}
