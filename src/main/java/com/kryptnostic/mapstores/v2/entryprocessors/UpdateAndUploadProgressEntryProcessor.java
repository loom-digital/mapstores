package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.EnumSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.objects.SetProxy;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.crypto.CryptoMaterial;

public class UpdateAndUploadProgressEntryProcessor
        extends AbstractRhizomeEntryProcessor<UUID, Set<CryptoMaterial>, Void> {
    private static final long serialVersionUID = 3412972944630075736L;
    private final Set<CryptoMaterial> material;

    public UpdateAndUploadProgressEntryProcessor( Set<CryptoMaterial> material ) {
        this.material = material;
    }

    @Override
    public Void process( Entry<UUID, Set<CryptoMaterial>> entry ) {
        Set<CryptoMaterial> value = entry.getValue();
        if ( value == null ) {
            value = EnumSet.noneOf( CryptoMaterial.class );
        }
        value.addAll( material );
        if ( !( value instanceof SetProxy<?, ?> ) ) {
            entry.setValue( value );
        }
        return null;
    }

    public Set<CryptoMaterial> getMaterialType() {
        return material;
    }

}
