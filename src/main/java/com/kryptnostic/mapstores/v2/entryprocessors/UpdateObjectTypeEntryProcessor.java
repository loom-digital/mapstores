package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.storage.models.ObjectMetadata;

public class UpdateObjectTypeEntryProcessor extends AbstractRhizomeEntryProcessor<UUID, ObjectMetadata, Boolean> {

    private static final long serialVersionUID = 2747969397943816089L;
    private final UUID        newTypeId;

    public UpdateObjectTypeEntryProcessor( UUID newTypeId ) {
        this.newTypeId = newTypeId;
    }

    @Override
    public Boolean process( Entry<UUID, ObjectMetadata> entry ) {
        ObjectMetadata oM = entry.getValue();
        if ( oM == null ) {
            return Boolean.FALSE;
        }
        entry.setValue( new ObjectMetadata(
                oM.getId(),
                oM.getVersion(),
                oM.getSize(),
                newTypeId,
                oM.getACLId(),
                oM.getCreator(),
                oM.getCipherMethod(),
                oM.getCreatedTime() ) );
        return Boolean.TRUE;
    }

    public UUID getNewTypeId() {
        return newTypeId;
    }

}
