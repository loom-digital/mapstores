package com.kryptnostic.mapstores.v2.entryprocessors;

import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;
import com.kryptnostic.mapstores.v2.mapstores.UserObjectTypePair;
import com.kryptnostic.rhizome.hazelcast.objects.SetProxy;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class UpdateReadableTypedObjectsEP extends AbstractRhizomeEntryProcessor<UserObjectTypePair, Set<UUID>, Void> {

    protected final Logger    logger           = LoggerFactory.getLogger( getClass() );

    private static final long serialVersionUID = 1609505434420849471L;
    private final UUID        id;
    private final Operation   op;

    public enum Operation {
        ADD,
        REMOVE;
    }

    public UpdateReadableTypedObjectsEP( UUID id, Operation op ) {
        this.id = id;
        this.op = op;
    }

    static public UpdateReadableTypedObjectsEP addToReadableTypedObjects( UUID id ) {
        return new UpdateReadableTypedObjectsEP( id, Operation.ADD );
    }

    static public UpdateReadableTypedObjectsEP removeFromReadableTypedObjects( UUID id ) {
        return new UpdateReadableTypedObjectsEP( id, Operation.REMOVE );
    }

    @Override
    public Void process( Entry<UserObjectTypePair, Set<UUID>> entry ) {
        Set<UUID> value = entry.getValue();
        if ( !( value instanceof SetProxy<?, ?> ) ) {
            if ( value == null ) {
                value = Sets.newHashSetWithExpectedSize( 1 );
            }
        }
        switch ( op ) {
            case ADD:
                value.add( id );
                break;
            case REMOVE:
                value.remove( id );
                break;
            default:
                break;
        }
        if ( !( value instanceof SetProxy<?, ?> ) ) {
            entry.setValue( value );
        }
        return null;
    }

    /**
     * @return the id
     */
    public UUID getId() {
        return id;
    }

    public Operation getOp() {
        return op;
    }
}
