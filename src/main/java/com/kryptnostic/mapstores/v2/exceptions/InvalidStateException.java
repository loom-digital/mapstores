package com.kryptnostic.mapstores.v2.exceptions;
public class InvalidStateException extends RuntimeException {
    private static final long serialVersionUID = 7069223499968418397L;
    public InvalidStateException( String string ) { super( string ); }
}