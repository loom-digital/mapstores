package com.kryptnostic.mapstores.v2.exceptions;

public class ObjectNotFoundException extends RuntimeException {
    private static final long   serialVersionUID      = -8617173231722374141L;
    private static final String OBJ_NOT_FOUND_MESSAGE = "Object not found";

    public ObjectNotFoundException() {
        super( OBJ_NOT_FOUND_MESSAGE );
    }
    
    public ObjectNotFoundException( String msg ) {
        super( msg );
    }
}
