package com.kryptnostic.mapstores.v2.exceptions;

public class StatusGoneException extends RuntimeException {
    private static final long serialVersionUID = 5821321386525752928L;
    
    public StatusGoneException(){}

    public StatusGoneException( String msg ) {
        super( msg );
    }
}
