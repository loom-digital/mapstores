package com.kryptnostic.mapstores.v2.helper;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.datastax.driver.core.exceptions.UnauthorizedException;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.hazelcast.core.HazelcastInstance;
import com.kryptnostic.directory.v1.exception.ActiveReservationException;
import com.kryptnostic.directory.v1.exception.AddUserException;
import com.kryptnostic.directory.v1.exception.AlreadyApprovedException;
import com.kryptnostic.directory.v1.exception.AlreadyDeniedException;
import com.kryptnostic.directory.v1.exception.AlreadyOpenException;
import com.kryptnostic.directory.v1.exception.InvalidEmailException;
import com.kryptnostic.directory.v1.exception.MailException;
import com.kryptnostic.directory.v1.exception.RealmMismatchException;
import com.kryptnostic.directory.v1.exception.ReservationTakenException;
import com.kryptnostic.directory.v1.exception.ReservationTokenMismatchException;
import com.kryptnostic.directory.v1.exception.UnauthorizedGroupRequestException;
import com.kryptnostic.directory.v1.exception.UserUpdateException;
import com.kryptnostic.kodex.v1.exceptions.types.BadRequestException;
import com.kryptnostic.kodex.v1.exceptions.types.ForbiddenException;
import com.kryptnostic.kodex.v1.exceptions.types.ResourceNotFoundException;
import com.kryptnostic.kodex.v1.models.response.BasicResponse;
import com.kryptnostic.mail.client.MailServiceClient;
import com.kryptnostic.mail.constants.EmailTemplate;
import com.kryptnostic.mail.models.RenderableEmailRequest;
import com.kryptnostic.mapstores.v2.exceptions.InvalidStateException;
import com.kryptnostic.mapstores.v2.exceptions.ObjectNotFoundException;
import com.kryptnostic.mapstores.v2.exceptions.StatusGoneException;

@ControllerAdvice
public class KryptnosticExceptionHandler {
    @Inject
    HazelcastInstance hazelcast;

    @ExceptionHandler( { ResourceNotFoundException.class, ObjectNotFoundException.class } )
    @ResponseStatus( HttpStatus.NOT_FOUND )
    public @ResponseBody BasicResponse<String> handleResourceNotFound( Exception ex ) {
        logErrorAndSendEmailReport( ex );
        return new BasicResponse<String>( ex.getMessage(), HttpStatus.NOT_FOUND.value(), false );
    }

    @ExceptionHandler( ForbiddenException.class )
    @ResponseStatus( HttpStatus.FORBIDDEN )
    public @ResponseBody BasicResponse<String> handleForbidden( ForbiddenException ex ) {
        logErrorAndSendEmailReport( ex );
        return new BasicResponse<String>( ex.getMessage(), HttpStatus.FORBIDDEN.value(), false );
    }

    @ExceptionHandler( { IllegalArgumentException.class, ActiveReservationException.class, AddUserException.class,
            AlreadyApprovedException.class, AlreadyDeniedException.class, AlreadyOpenException.class,
            MailException.class, RealmMismatchException.class, BadRequestException.class,
            ReservationTakenException.class, ReservationTokenMismatchException.class,
            UnauthorizedGroupRequestException.class, UserUpdateException.class, InvalidEmailException.class } )
    @ResponseStatus( HttpStatus.BAD_REQUEST )
    public @ResponseBody BasicResponse<String> handleBadRequest( Exception ex ) {
        logErrorAndSendEmailReport( ex );
        return new BasicResponse<String>( ex.getMessage(), HttpStatus.BAD_REQUEST.value(), false );
    }

    @ExceptionHandler( { NotAuthorizedException.class, InsufficientAuthenticationException.class,
            UnauthorizedException.class, AuthenticationCredentialsNotFoundException.class } )
    @ResponseStatus( HttpStatus.UNAUTHORIZED )
    public @ResponseBody BasicResponse<String> handleInvalidState( Exception ex ) {
        logErrorAndSendEmailReport( ex );
        return new BasicResponse<>( ex.getMessage(), HttpStatus.UNAUTHORIZED.value(), false );
    }

    @ExceptionHandler( InvalidStateException.class )
    @ResponseStatus( HttpStatus.CONFLICT )
    public @ResponseBody BasicResponse<String> handleInvalidState( InvalidStateException ex ) {
        logErrorAndSendEmailReport( ex );
        return new BasicResponse<>( ex.getMessage(), HttpStatus.CONFLICT.value(), false );
    }
    
    @ExceptionHandler( StatusGoneException.class )
    @ResponseStatus( HttpStatus.GONE )
    public @ResponseBody BasicResponse<String> handleGoneStatus( StatusGoneException ex) {
        logErrorAndSendEmailReport( ex );
        return new BasicResponse<>( ex.getMessage(), HttpStatus.GONE.value(), false );
    }
    
    
    @ExceptionHandler( Exception.class )
    @ResponseStatus( HttpStatus.INTERNAL_SERVER_ERROR )
    public @ResponseBody BasicResponse<String> handleException( Exception ex ) {
        logErrorAndSendEmailReport( ex );
        return new BasicResponse<String>( ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value(), false );
    }

    private void logErrorAndSendEmailReport( Exception ex ) {

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter( sw );
        ex.printStackTrace( pw );

        System.err.println( sw.toString() );

        MailServiceClient mailService = new MailServiceClient( hazelcast );
        RenderableEmailRequest emailRequest = new RenderableEmailRequest(
                Optional.of( EmailTemplate.getCourierEmailAddress() ),
                new String[] { "outage@kryptnostic.com" },
                Optional.absent(),
                Optional.absent(),
                EmailTemplate.INTERNAL_ERROR.getPath(),
                Optional.of( EmailTemplate.INTERNAL_ERROR.getSubject() ),
                Optional.of( ImmutableMap.of( "message", sw.toString() ) ),
                Optional.absent(),
                Optional.absent() );
        mailService.send( emailRequest );

    }
}
