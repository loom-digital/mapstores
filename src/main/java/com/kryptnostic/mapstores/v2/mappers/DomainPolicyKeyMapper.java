package com.kryptnostic.mapstores.v2.mappers;

import com.kryptnostic.directory.v1.domain.DomainPolicy;
import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;

public class DomainPolicyKeyMapper implements SelfRegisteringKeyMapper<DomainPolicy> {

    @Override
    public String fromKey( DomainPolicy key ) {
        return key.toString() ;
    }

    @Override
    public DomainPolicy toKey( String value ) {
        return DomainPolicy.valueOf( value );
    }

    @Override
    public Class<DomainPolicy> getClazz() {
        return DomainPolicy.class;
    }

}
