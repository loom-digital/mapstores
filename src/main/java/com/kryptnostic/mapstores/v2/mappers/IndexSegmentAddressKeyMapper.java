package com.kryptnostic.mapstores.v2.mappers;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;
import com.kryptnostic.v2.search.IndexSegmentAddress;

public class IndexSegmentAddressKeyMapper implements SelfRegisteringKeyMapper<IndexSegmentAddress> {

    private static final Charset CHARSET = StandardCharsets.UTF_8;

    @Override
    public String fromKey( IndexSegmentAddress key ) {
        return key == null ? null : new String( key.getAddress(), CHARSET );
    }

    @Override
    public IndexSegmentAddress toKey( String value ) {
        return new IndexSegmentAddress( value.getBytes( CHARSET ) );
    }

    @Override
    public Class<IndexSegmentAddress> getClazz() {
        return IndexSegmentAddress.class;
    }

}
