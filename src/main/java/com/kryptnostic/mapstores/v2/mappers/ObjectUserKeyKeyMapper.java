package com.kryptnostic.mapstores.v2.mappers;

import java.util.UUID;

import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;
import com.kryptnostic.v2.sharing.models.ObjectUserKey;

public class ObjectUserKeyKeyMapper implements SelfRegisteringKeyMapper<ObjectUserKey> {

    @Override
    public String fromKey( ObjectUserKey key ) {
        return key.getObjectId() + DEFAULT_SEPARATOR + key.getUserId();
    }

    @Override
    public ObjectUserKey toKey( String value ) {
        String[] split = value.split( DEFAULT_SEPARATOR );
        return new ObjectUserKey(
                UUID.fromString( split[ 0 ] ),
                UUID.fromString( split[ 1 ] ) );
    }

    @Override
    public Class<ObjectUserKey> getClazz() {
        return ObjectUserKey.class;
    }

}
