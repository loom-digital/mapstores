package com.kryptnostic.mapstores.v2.mappers;

import java.util.UUID;

import com.kryptnostic.directory.v2.model.UserSettings;
import com.kryptnostic.mapstores.v2.mapstores.UserSettingsKey;
import com.kryptnostic.rhizome.mappers.KeyMapper;
import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;

public class UserSettingsKeyMapper implements SelfRegisteringKeyMapper<UserSettingsKey>{

    @Override
    public String fromKey( UserSettingsKey key ) {
        return key.getUserId().toString() + KeyMapper.DEFAULT_SEPARATOR + key.getUserSetting().toString();
    }

    @Override
    public UserSettingsKey toKey( String value ) {
        String[] split = value.split( KeyMapper.DEFAULT_SEPARATOR );
        return new UserSettingsKey( UUID.fromString( split[0] ), UserSettings.valueOf( split[1] ) ) ;
    }

    @Override
    public Class<UserSettingsKey> getClazz() {
        return UserSettingsKey.class;
    }

}
