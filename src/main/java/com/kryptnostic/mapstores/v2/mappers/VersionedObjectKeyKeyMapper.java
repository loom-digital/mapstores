package com.kryptnostic.mapstores.v2.mappers;

import java.util.UUID;

import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

public class VersionedObjectKeyKeyMapper implements SelfRegisteringKeyMapper<VersionedObjectKey> {

    @Override
    public String fromKey( VersionedObjectKey key ) {
        return key.getObjectId().toString() + "," + key.getVersion();
    }

    @Override
    public VersionedObjectKey toKey( String value ) {
        String[] split = value.split( "," );
        return new VersionedObjectKey( UUID.fromString( split[ 0 ] ), Long.parseLong( split[ 1 ] ) );
    }

    @Override
    public Class<VersionedObjectKey> getClazz() {
        return VersionedObjectKey.class;
    }

}
