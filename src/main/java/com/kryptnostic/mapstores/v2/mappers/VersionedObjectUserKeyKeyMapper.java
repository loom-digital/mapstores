package com.kryptnostic.mapstores.v2.mappers;

import java.util.UUID;

import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;
import com.kryptnostic.v2.storage.models.VersionedObjectUserKey;

public class VersionedObjectUserKeyKeyMapper implements SelfRegisteringKeyMapper<VersionedObjectUserKey> {

    @Override
    public String fromKey( VersionedObjectUserKey key ) {
        return key.getUserKey() + DEFAULT_SEPARATOR + key.getObjectId() + DEFAULT_SEPARATOR + key.getVersion();
    }

    @Override
    public VersionedObjectUserKey toKey( String value ) {
        String[] split = value.split( DEFAULT_SEPARATOR );
        return new VersionedObjectUserKey(
                UUID.fromString( split[ 1 ] ),
                UUID.fromString( split[ 0 ] ),
                Long.valueOf( split[ 2 ] ).longValue() );
    }

    @Override
    public Class<VersionedObjectUserKey> getClazz() {
        return VersionedObjectUserKey.class;
    }

}
