package com.kryptnostic.mapstores.v2.mapstores;

import java.util.UUID;

import com.kryptnostic.mapstores.v2.Permission;

public class ACLPermissionPair {

    private final UUID       aclId;
    private final Permission permission;

    public ACLPermissionPair( UUID aclId, Permission permission ) {
        this.aclId = aclId;
        this.permission = permission;
    }

    public UUID getAclId() {
        return aclId;
    }

    public Permission getPermission() {
        return permission;
    }

}
