package com.kryptnostic.mapstores.v2.mapstores;

import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.datastax.driver.core.utils.UUIDs;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.base.Optional;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.hazelcast.config.InMemoryFormat;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MapStoreConfig;
import com.kryptnostic.rhizome.configuration.cassandra.CassandraConfiguration;
import com.kryptnostic.rhizome.mapstores.cassandra.BaseCassandraMapStore;
import com.kryptnostic.v2.storage.models.ScrollDirection;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;
import com.kryptnostic.v2.storage.models.VersionedObjectKeySet;
import com.kryptnostic.v2.storage.types.TypeUUIDs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * Created by yao on 5/23/16.
 */
public class ObjectMetadataHierarchyMapstore extends BaseCassandraMapStore<ObjectMetadataHierarchyKey, VersionedObjectKeySet> {

    private static final Logger logger             = LoggerFactory.getLogger( ObjectMetadataHierarchyMapstore.class );

    static final String             KEYSPACE_QUERY          = "CREATE KEYSPACE IF NOT EXISTS %s WITH replication = {'class':'SimpleStrategy', 'replication_factor':%d};";
    static final String             BASE_TABLE_QUERY        = "CREATE TABLE IF NOT EXISTS %s.%s (%s uuid, %s bigint, %s uuid, %s timeuuid, %s uuid, %s bigint, PRIMARY KEY ( (%s, %s), %s, %s) )";
    static final String             NEWLY_ORDERED_EXT       = " WITH CLUSTERING ORDER BY( %s ASC, %s DESC );";
    static final String             PARENT_ID_COL_NAME      = "parentObjectId";
    static final String             PARENT_VERSION_COL_NAME = "parentVersion";
    static final String             TYPE_ID_COL_NAME        = "typeId";
    static final String             CLOCK_COL_NAME          = "clock";
    static final String             OBJECT_ID_COL_NAME      = "objectId";
    static final String             VERSION_COL_NAME        = "version";
    public static final String[]    ALL_COLUMNS             = { PARENT_ID_COL_NAME, PARENT_VERSION_COL_NAME, TYPE_ID_COL_NAME, CLOCK_COL_NAME,
            OBJECT_ID_COL_NAME, VERSION_COL_NAME };

    private final Select SELECT_ALL_KEYS;
    private final PreparedStatement ADD_STATEMENT;
    private final PreparedStatement LOAD_BY_TYPE_LT_STATEMENT;
    private final PreparedStatement LOAD_BY_TYPE_GT_STATEMENT;
    private final PreparedStatement LOAD_BY_TYPE_UNPAGED_STATEMENT;
    private final PreparedStatement DELETE_STATEMENT;

    public ObjectMetadataHierarchyMapstore(
            String tableName,
            String mapname,
            CassandraConfiguration config,
            Session session ) {
        super(
                tableName,
                mapname,
                null,
                null,
                config,
                session );

        this.SUPPORTS_ASYNC_LOADS = false;

        // create keyspace
        session.execute( String.format( KEYSPACE_QUERY,
                keyspace,
                replicationFactor ) );

        // create table
        session.execute( String.format( BASE_TABLE_QUERY + NEWLY_ORDERED_EXT,
                keyspace,
                table,
                // columns
                PARENT_ID_COL_NAME,
                PARENT_VERSION_COL_NAME,
                TYPE_ID_COL_NAME,
                CLOCK_COL_NAME,
                OBJECT_ID_COL_NAME,
                VERSION_COL_NAME,
                // Primary key
                PARENT_ID_COL_NAME,
                PARENT_VERSION_COL_NAME,
                TYPE_ID_COL_NAME,
                CLOCK_COL_NAME,
                // Ordering by typeid => default, clock descending
                TYPE_ID_COL_NAME,
                CLOCK_COL_NAME ) );

        ADD_STATEMENT = session.prepare( QueryBuilder.insertInto( keyspace, table )
                .value( PARENT_ID_COL_NAME, QueryBuilder.bindMarker() )
                .value( PARENT_VERSION_COL_NAME, QueryBuilder.bindMarker() )
                .value( TYPE_ID_COL_NAME, QueryBuilder.bindMarker() )
                .value( CLOCK_COL_NAME, QueryBuilder.bindMarker() )
                .value( OBJECT_ID_COL_NAME, QueryBuilder.bindMarker() )
                .value( VERSION_COL_NAME, QueryBuilder.bindMarker() ) );

        LOAD_BY_TYPE_UNPAGED_STATEMENT = session
                .prepare( QueryBuilder.select( CLOCK_COL_NAME, OBJECT_ID_COL_NAME, VERSION_COL_NAME )
                        .from( keyspace, table )
                        .where( QueryBuilder.eq( PARENT_ID_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.eq( PARENT_VERSION_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.eq( TYPE_ID_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.gt( CLOCK_COL_NAME, QueryBuilder.bindMarker() ) )
                        .limit( QueryBuilder.bindMarker() ) );

        LOAD_BY_TYPE_LT_STATEMENT = session
                .prepare( QueryBuilder.select( CLOCK_COL_NAME, OBJECT_ID_COL_NAME, VERSION_COL_NAME )
                        .from( keyspace, table )
                        .where( QueryBuilder.eq( PARENT_ID_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.eq( PARENT_VERSION_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.eq( TYPE_ID_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.lt( CLOCK_COL_NAME, QueryBuilder.bindMarker() ) )
                        .limit( QueryBuilder.bindMarker() ) );

        LOAD_BY_TYPE_GT_STATEMENT = session
                .prepare( QueryBuilder.select( CLOCK_COL_NAME, OBJECT_ID_COL_NAME, VERSION_COL_NAME )
                        .from( keyspace, table )
                        .where( QueryBuilder.eq( PARENT_ID_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.eq( PARENT_VERSION_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.eq( TYPE_ID_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.gt( CLOCK_COL_NAME, QueryBuilder.bindMarker() ) )
                        .orderBy( QueryBuilder.desc( TYPE_ID_COL_NAME ), QueryBuilder.asc( CLOCK_COL_NAME ) )
                        .limit( QueryBuilder.bindMarker() ) );

        DELETE_STATEMENT = session
                .prepare( QueryBuilder.delete().from( keyspace, table )
                        .where( QueryBuilder.eq( PARENT_ID_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.eq( PARENT_VERSION_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.in( TYPE_ID_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.eq( CLOCK_COL_NAME, QueryBuilder.bindMarker() ) ) );

        SELECT_ALL_KEYS = QueryBuilder.select( PARENT_ID_COL_NAME, PARENT_VERSION_COL_NAME, TYPE_ID_COL_NAME, CLOCK_COL_NAME )
                .from( keyspace, table );
    }

    @Override
    public void store( ObjectMetadataHierarchyKey key, VersionedObjectKeySet value ) {
        UUID parentObjectId = key.getParentObjectVersionedKey().getObjectId();
        Long parentObjectVersion = key.getParentObjectVersionedKey().getVersion();
        UUID latestClock = key.getClock();
        Set<UUID> objectTypes = key.getTypes();

        if ( parentObjectId == null || objectTypes.size() != 1 ) {
            throw new UnsupportedOperationException();
        }
        UUID type = Iterables.getOnlyElement( objectTypes );
        for ( VersionedObjectKey kee : value ) {
            session.execute( ADD_STATEMENT.bind(
                    parentObjectId,
                    parentObjectVersion,
                    type,
                    latestClock,
                    kee.getObjectId(),
                    kee.getVersion() ) );
        }
    }

    @Override
    public void delete( ObjectMetadataHierarchyKey key ) {
        UUID parentObjectId = key.getParentObjectVersionedKey().getObjectId();
        Long parentObjectVersion = key.getParentObjectVersionedKey().getVersion();
        UUID latestClock = key.getClock();
        Set<UUID> objectTypes = key.getTypes();
        session.execute( DELETE_STATEMENT.bind( parentObjectId, parentObjectVersion, new ArrayList<>( objectTypes ), latestClock ) );
    }
    
    Comparator<UUID> longComp        = new Comparator<UUID>() {
        @Override
        public int compare( UUID o1, UUID o2 ) {
            return Long.compare( UUIDs.unixTimestamp( o1 ), UUIDs.unixTimestamp( o2 ) );
        }
    };

    Comparator<UUID> reverseLongComp = new Comparator<UUID>() {
        @Override
        public int compare( UUID o1, UUID o2 ) {
            return Long.compare( UUIDs.unixTimestamp( o2 ), UUIDs.unixTimestamp( o1 ) );
        }
    };

    private VersionedObjectKeySet loadByType(
            UUID parentObjectId,
            Long parentObjectVersion,
            Set<UUID> objectTypes,
            UUID latestClock,
            int limit,
            boolean isPaged,
            ScrollDirection direction ) {

        TreeMap<UUID, VersionedObjectKey> orderedByClock;
        PreparedStatement toBind = LOAD_BY_TYPE_UNPAGED_STATEMENT;
        if ( ScrollDirection.BACKWARDS.equals( direction ) ) {
            orderedByClock = Maps.newTreeMap( reverseLongComp );
            if ( isPaged ) {
               toBind = LOAD_BY_TYPE_LT_STATEMENT; 
            }
        } else {
            orderedByClock = Maps.newTreeMap( longComp );
            if ( isPaged ) {
                toBind = LOAD_BY_TYPE_GT_STATEMENT;
            }
        }

        final CountDownLatch latch = new CountDownLatch( objectTypes.size() );
        for ( UUID objectType : objectTypes ) {
            Statement bound = toBind.bind( parentObjectId, parentObjectVersion, objectType, latestClock, limit );

            // TOOD: Future callbacks are blocking and inserting into a TreeMap is relatively expensive.
            // Parallelizing rowset processing is an optimization target.
            bound.setFetchSize( limit );
            Futures.addCallback( session.executeAsync( bound ), new FutureCallback<ResultSet>() {
                @Override
                public void onSuccess( @Nullable ResultSet rs ) {
                    for ( Row r : rs ) {
                        UUID clock = r.getUUID( CLOCK_COL_NAME );
                        UUID objectId = r.getUUID( OBJECT_ID_COL_NAME );
                        long version = r.getLong( VERSION_COL_NAME );
                        orderedByClock.put( clock,
                                new VersionedObjectKey( objectId, version ) );
                    }
                    latch.countDown();
                }

                @Override
                public void onFailure( Throwable t ) {
                    System.out.println( "Asynchronous cassandra queries failed" );
                    t.printStackTrace();
                    latch.countDown();
                }
            } );
        }

        try {
            latch.await();
        } catch ( InterruptedException e ) {
            System.out.println( "Asynchronous cassandra queries timed out." );
            e.printStackTrace();
        }
        VersionedObjectKeySet latest = new VersionedObjectKeySet( limit );

        Iterator<VersionedObjectKey> iterator = orderedByClock.values().iterator();

        for ( int i = 0; iterator.hasNext() && ( i < limit ); i++ ) {
            latest.add( iterator.next() );
        }

        return latest;
    }

    @Override
    public VersionedObjectKeySet load( ObjectMetadataHierarchyKey key ) {
        UUID parentObjectId = key.getParentObjectVersionedKey().getObjectId();
        Long parentObjectVersion = key.getParentObjectVersionedKey().getVersion();
        UUID latestClock = key.getClock();
        Set<UUID> objectTypes = key.getTypes();

        int limit = DEFAULT_QUERY_LIMIT;
        if ( key.isPaged() ) {
            limit = key.getPageSize().get();
        }

        VersionedObjectKeySet results = loadByType(
                parentObjectId,
                parentObjectVersion,
                objectTypes,
                latestClock,
                limit,
                key.isPaged(),
                key.getDirection() );
        return results;
    }

    private static ObjectMetadataHierarchyKey toKey( Row row ) {
        UUID parentObjectId = row.getUUID( PARENT_ID_COL_NAME );
        Long parentObjectVersion = row .getLong( PARENT_VERSION_COL_NAME );
        UUID typeId = row.getUUID( TYPE_ID_COL_NAME );
        UUID clock = row.getUUID( CLOCK_COL_NAME );
        return new ObjectMetadataHierarchyKey( new VersionedObjectKey( parentObjectId, parentObjectVersion ), ImmutableSet.of( typeId ), clock, com.google.common.base.Optional.absent() );
    }

    @Override
    public Iterable<ObjectMetadataHierarchyKey> loadAllKeys() {
        return null;
    }

    @Override
    public ObjectMetadataHierarchyKey generateTestKey() {
        return new ObjectMetadataHierarchyKey(
                new VersionedObjectKey( new UUID( 0, 1 ), 0l),
                ImmutableSet.of( TypeUUIDs.INDEX_METADATA ),
                UUIDs.timeBased(),
                Optional.absent() );
    }

    @Override
    public VersionedObjectKeySet generateTestValue() {
        VersionedObjectKeySet results = new VersionedObjectKeySet( new VersionedObjectKey( new UUID( 0, 1 ), 0l ) );
        return results;
    }

    @Override
    public void deleteAll( Collection<ObjectMetadataHierarchyKey> keys ) {
        for ( ObjectMetadataHierarchyKey key : keys ) {
            delete( key );
        }
    }

    @Override
    public MapStoreConfig getMapStoreConfig() {
        return new MapStoreConfig().setImplementation( this )
                .setEnabled( true )
                .setInitialLoadMode( MapStoreConfig.InitialLoadMode.LAZY )
                .setWriteDelaySeconds( 0 );
    }

    @Override
    public MapConfig getMapConfig() {
        return new MapConfig( mapName )
                .setBackupCount( this.replicationFactor )
                .setMapStoreConfig( getMapStoreConfig() )
                .setTimeToLiveSeconds( 1 )
                .setInMemoryFormat( InMemoryFormat.OBJECT );
    }

    @Override
    protected ResultSetFuture asyncLoad( ObjectMetadataHierarchyKey key ) {
        throw new UnsupportedOperationException( "ObjectMetadataHierarchyMapstore doesn't support async calls" );
    }

    @Override
    protected ObjectMetadataHierarchyKey mapToKey( Row row ) {
        return toKey( row );
    }

    public static VersionedObjectKey toValue( Row row ) {
        UUID objectId = row.getUUID( OBJECT_ID_COL_NAME );
        long version = row.getLong( VERSION_COL_NAME );
        return new VersionedObjectKey( objectId, version );
    }

    @Override
    protected VersionedObjectKeySet mapToValue( Row row ) {
        throw new UnsupportedOperationException(
                "ObjectMetadataHierarchyMapstore doesn't support mapping multiple values" );
    }
}
