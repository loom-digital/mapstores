package com.kryptnostic.mapstores.v2.mapstores;

import java.util.Set;
import java.util.UUID;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.kryptnostic.v2.storage.models.ScrollDirection;

public class ObjectMetadataLevelKey {

    private final UUID              aclId;
    private final Set<UUID>         types;
    private final UUID              latestClock;
    private final Optional<UUID>    optionalObjectValue;

    private final Optional<Integer> pageSize;
    private final ScrollDirection   direction;

    public ObjectMetadataLevelKey(
            UUID aclId,
            Set<UUID> types,
            UUID latestClock,
            Optional<UUID> optionalObjectValue ) {
        this( aclId, types, latestClock, optionalObjectValue, Optional.absent(), ScrollDirection.BACKWARDS );
    }

    protected ObjectMetadataLevelKey(
            UUID aclId,
            Set<UUID> types,
            UUID latestClock,
            Optional<UUID> optionalObjectValue,
            Optional<Integer> pageSize,
            ScrollDirection direction ) {
        Preconditions.checkNotNull( types );
        Preconditions.checkArgument( !types.isEmpty() );
        this.types = types;
        this.aclId = aclId;
        this.latestClock = latestClock;
        this.optionalObjectValue = optionalObjectValue;
        this.pageSize = pageSize;
        this.direction = direction;
    }

    public static class Builder {

        private final UUID        aclid;
        private final Set<UUID>   typesToFetch;
        private UUID              clock;
        private Optional<UUID>    objectValue = Optional.absent();
        private Optional<Integer> pageSize    = Optional.absent();
        private ScrollDirection   direction   = ScrollDirection.BACKWARDS;

        public Builder( UUID aclId, Set<UUID> typesToFetch ) {
            this.aclid = aclId;
            this.typesToFetch = typesToFetch;
        }

        public Builder withBaseClock( UUID latestClock ) {
            this.clock = latestClock;
            return this;
        }

        public Builder withObjectValue( UUID objectValue ) {
            this.objectValue = Optional.fromNullable( objectValue );
            return this;
        }

        public Builder withPaging(
                Optional<Integer> pageSize,
                ScrollDirection direction ) {
            this.pageSize = pageSize;
            this.direction = direction;
            return this;
        }

        public ObjectMetadataLevelKey build() {
            return new ObjectMetadataLevelKey(
                    aclid,
                    typesToFetch,
                    clock,
                    objectValue,
                    pageSize,
                    direction );
        }

    }

    /**
     * @return the aclIds
     */
    public UUID getAclId() {
        return aclId;
    }

    public Set<UUID> getTypes() {
        return types;
    }

    public UUID getClock() {
        return latestClock;
    }

    public Optional<UUID> getOptionalObjectValue() {
        return optionalObjectValue;
    }

    public Optional<Integer> getPageSize() {
        return pageSize;
    }

    public boolean isPaged() {
        return pageSize.isPresent();
    }

    public ScrollDirection getDirection() {
        return this.direction;
    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();
        build.append( aclId );
        build.append( "=>{" );
        build.append( "}" );
        return build.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( aclId == null ) ? 0 : aclId.hashCode() );
        result = prime * result + ( ( latestClock == null ) ? 0 : latestClock.hashCode() );
        result = prime * result + ( ( optionalObjectValue == null ) ? 0 : optionalObjectValue.hashCode() );
        result = prime * result + ( ( pageSize == null ) ? 0 : pageSize.hashCode() );
        result = prime * result + ( ( direction == null ) ? 0 : direction.hashCode() );
        result = prime * result + ( ( types == null ) ? 0 : types.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( getClass() != obj.getClass() ) return false;
        ObjectMetadataLevelKey other = (ObjectMetadataLevelKey) obj;
        if ( aclId == null ) {
            if ( other.aclId != null ) return false;
        } else if ( !aclId.equals( other.aclId ) ) return false;
        if ( latestClock == null ) {
            if ( other.latestClock != null ) return false;
        } else if ( !latestClock.equals( other.latestClock ) ) return false;
        if ( optionalObjectValue == null ) {
            if ( other.optionalObjectValue != null ) return false;
        } else if ( !optionalObjectValue.equals( other.optionalObjectValue ) ) return false;
        if ( pageSize == null ) {
            if ( other.pageSize != null ) return false;
        } else if ( !pageSize.equals( other.pageSize ) ) return false;
        if ( direction == null ) {
            if ( other.direction != null ) return false;
        } else if ( !direction.equals( other.direction ) ) return false;
        if ( types == null ) {
            if ( other.types != null ) return false;
        } else if ( !types.equals( other.types ) ) return false;
        return true;
    }

}
