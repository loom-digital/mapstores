package com.kryptnostic.mapstores.v2.mapstores;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.querybuilder.Delete;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.google.common.collect.Maps;
import com.kryptnostic.kodex.v1.crypto.ciphers.Cypher;
import com.kryptnostic.rhizome.configuration.cassandra.CassandraConfiguration;
import com.kryptnostic.rhizome.mapstores.cassandra.BaseCassandraMapStore;
import com.kryptnostic.rhizome.mapstores.cassandra.PagingCassandraIterator;
import com.kryptnostic.v2.storage.models.CreateObjectRequest;
import com.kryptnostic.v2.storage.models.ObjectMetadata;

public class ObjectMetadataMapStore extends BaseCassandraMapStore<UUID, ObjectMetadata> {

    public static final Logger      LOGGER                = LoggerFactory.getLogger( ObjectMetadataMapStore.class );

    static final String             OBJECT_ID_COL_NAME    = "objectId";
    private static final String     VERSION_COL_NAME      = "version";
    static final String             TYPE_ID_COL_NAME      = "typeId";
    static final String             ACL_ID_COL_NAME       = "aclId";
    private static final String     SIZE_COL_NAME         = "size";
    private static final String     CREATOR_COL_NAME      = "creator";
    private static final String     CREATED_TIME_COL_NAME = "createdTime";
    private static final String     CYPHER_COL_NAME       = "cypher";
    static final String[]           ALL_COLUMNS           = new String[] {
                                                                  OBJECT_ID_COL_NAME, VERSION_COL_NAME,
                                                                  TYPE_ID_COL_NAME,
                                                                  ACL_ID_COL_NAME, SIZE_COL_NAME,
                                                                  CREATOR_COL_NAME, CREATED_TIME_COL_NAME,
                                                                  CYPHER_COL_NAME };

    private static final String     KEYSPACE_QUERY        = "CREATE KEYSPACE IF NOT EXISTS %s WITH replication = {'class':'SimpleStrategy', 'replication_factor':%d};";
    private static final String     TABLE_QUERY           = "CREATE TABLE IF NOT EXISTS %s.%s (%s uuid, %s bigint, %s uuid, %s uuid, %s bigint, %s uuid, %s timestamp, %s text, PRIMARY KEY ( %s, %s) );";

    private final Select            SELECT_ALL_KEYS;
    private final PreparedStatement STORE_SINGLE_METADATA;
    private final PreparedStatement LOAD_STATEMENT;

    public ObjectMetadataMapStore(
            Session session,
            String mapName,
            String table,
            CassandraConfiguration config ) {
        super( table, mapName, null, null, config, session );

        // create keyspace
        session.execute( String.format( KEYSPACE_QUERY,
                keyspace,
                Integer.valueOf( replicationFactor ) ) );

        // create table
        session.execute( String.format( TABLE_QUERY,
                keyspace,
                table,
                // columns
                OBJECT_ID_COL_NAME,
                VERSION_COL_NAME,
                TYPE_ID_COL_NAME,
                ACL_ID_COL_NAME,
                SIZE_COL_NAME,
                CREATOR_COL_NAME,
                CREATED_TIME_COL_NAME,
                CYPHER_COL_NAME,
                // Primary key
                OBJECT_ID_COL_NAME,
                VERSION_COL_NAME ) );

        LOAD_STATEMENT = session.prepare(
                QueryBuilder.select( ALL_COLUMNS ).from( keyspace, table )
                        .where( QueryBuilder.eq( OBJECT_ID_COL_NAME, QueryBuilder.bindMarker() ) )
                        .limit( 1 ) );

        STORE_SINGLE_METADATA = session.prepare(
                QueryBuilder.insertInto( keyspace, table )
                        .values( ALL_COLUMNS,
                                new Object[] {
                                        QueryBuilder.bindMarker(),
                                        QueryBuilder.bindMarker(),
                                        QueryBuilder.bindMarker(),
                                        QueryBuilder.bindMarker(),
                                        QueryBuilder.bindMarker(),
                                        QueryBuilder.bindMarker(),
                                        QueryBuilder.bindMarker(),
                                        QueryBuilder.bindMarker() } ) );

        SELECT_ALL_KEYS = QueryBuilder
                .select( OBJECT_ID_COL_NAME, TYPE_ID_COL_NAME, ACL_ID_COL_NAME )
                .from( keyspace, table );
    }

    @Override
    public void store( UUID key, ObjectMetadata value ) {
        session.execute( STORE_SINGLE_METADATA.bind(
                new Object[] { key,
                        Long.valueOf( value.getVersion() ),
                        value.getType(),
                        value.getACLId(),
                        Long.valueOf( value.getSize() ),
                        value.getCreator(),
                        value.getCreatedTime().toDate(),
                        value.getCipherMethod().name() } ) );
    }

    @Override
    public void delete( UUID key ) {
        Delete.Where baseDelete = QueryBuilder.delete().from( keyspace, table )
                .where( QueryBuilder.eq( OBJECT_ID_COL_NAME, key ) );
        session.execute( baseDelete );
    }

    @Override
    public ObjectMetadata load( UUID key ) {
        ResultSet execute = session.execute( LOAD_STATEMENT.bind( key ) );
        for ( Row r : execute ) {
            return mapToValue( r );
        }
        return null;
    }

    @Override
    public UUID generateTestKey() {
        return new UUID( 0L, 2L );
    }

    @Override
    public ObjectMetadata generateTestValue() {
        return ObjectMetadata.newRootObject( new CreateObjectRequest(),
                new UUID( 0L, 4L ),
                new UUID( 0L, 2L ) );
    }

    @Override
    protected ResultSetFuture asyncLoad( UUID key ) {
        return session.executeAsync( LOAD_STATEMENT.bind( key ) );
    }

    @Override
    protected UUID mapToKey( Row row ) {
        return row.getUUID( OBJECT_ID_COL_NAME );
    }

    @Override
    public void storeAll( Map<UUID, ObjectMetadata> map ) {
        for ( Entry<UUID, ObjectMetadata> ent : map.entrySet() ) {
            store( ent.getKey(), ent.getValue() );
        }
    }

    @Override
    public void deleteAll( Collection<UUID> keys ) {
        for ( UUID key : keys ) {
            delete( key );
        }
    }

    @Override
    public Map<UUID, ObjectMetadata> loadAll( Collection<UUID> keys ) {
        Map<UUID, ObjectMetadata> results = Maps.newHashMap();
        for ( UUID key : keys ) {
            ObjectMetadata value = load( key );
            results.put( key, value );
        }
        return results;
    }

    @Override
    public Iterable<UUID> loadAllKeys() {
        return PagingCassandraIterator.<UUID> asIterable( session, SELECT_ALL_KEYS, ( Row row ) -> mapToKey( row ) );
    }

    @Override
    protected ObjectMetadata mapToValue( Row row ) {
        UUID objectId = row.getUUID( OBJECT_ID_COL_NAME );
        long version = row.getLong( VERSION_COL_NAME );
        UUID typeId = row.getUUID( TYPE_ID_COL_NAME );
        Date date = row.getTimestamp( CREATED_TIME_COL_NAME );
        long size = row.getLong( SIZE_COL_NAME );
        UUID creatorId = row.getUUID( CREATOR_COL_NAME );
        Cypher cypher = Cypher.valueOf( Cypher.class, row.getString( CYPHER_COL_NAME ) );
        UUID aclId = row.getUUID( ACL_ID_COL_NAME );

        ObjectMetadata metadata = new ObjectMetadata(
                objectId,
                version,
                size,
                typeId,
                aclId,
                creatorId,
                cypher,
                new DateTime( date ) );
        return metadata;
    }

}