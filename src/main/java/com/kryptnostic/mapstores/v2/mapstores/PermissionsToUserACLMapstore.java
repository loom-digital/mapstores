package com.kryptnostic.mapstores.v2.mapstores;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.google.common.collect.ImmutableSet;
import com.hazelcast.config.MapConfig;
import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.constants.HazelcastNames;
import com.kryptnostic.rhizome.configuration.cassandra.CassandraConfiguration;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.mapstores.cassandra.BaseCassandraMapStore;

public class PermissionsToUserACLMapstore extends BaseCassandraMapStore<ACLPermissionPair, Set<UUID>> {

    private final PreparedStatement LOAD_USERACLS_FOR_PERMISSION_QUERY_ACL;

    public PermissionsToUserACLMapstore(
            Session session,
            String map,
            String table,
            CassandraConfiguration config ) {
        super(
                table,
                map,
                null,
                null,
                config,
                session );

        // create keyspace
        session.execute( String.format( UserACLToPermissionsMapstore.KEYSPACE_QUERY,
                keyspace,
                Integer.valueOf( replicationFactor ) ) );

        // create table
        session.execute( String.format( UserACLToPermissionsMapstore.TABLE_QUERY,
                keyspace,
                table,
                // columns
                UserACLToPermissionsMapstore.USER_ID_COL_NAME,
                UserACLToPermissionsMapstore.ACL_ID_COL_NAME,
                UserACLToPermissionsMapstore.PERMISSIONS_COL_NAME,
                // Primary key
                UserACLToPermissionsMapstore.USER_ID_COL_NAME,
                UserACLToPermissionsMapstore.ACL_ID_COL_NAME ) );

        if ( HazelcastNames.Maps.USER_LOOKUP_ACL_MAP.equals( mapName ) ) {
        session.execute( String.format( UserACLToPermissionsMapstore.INDEX_ACL_QUERY,
                keyspace,
                table,
                UserACLToPermissionsMapstore.ACL_ID_COL_NAME ) );
        } else {
            // remove when the userlookupaclmapstoreOld is removed
            session.execute( String.format( UserACLToPermissionsMapstore.INDEX_PERMISSIONS_QUERY,
                    keyspace,
                    table,
                    UserACLToPermissionsMapstore.PERMISSIONS_COL_NAME ) );
        }

        LOAD_USERACLS_FOR_PERMISSION_QUERY_ACL = session.prepare(
                QueryBuilder.select(
                        UserACLToPermissionsMapstore.USER_ID_COL_NAME,
                        UserACLToPermissionsMapstore.PERMISSIONS_COL_NAME )
                        .from( keyspace, table )
                        .where( QueryBuilder.eq( UserACLToPermissionsMapstore.ACL_ID_COL_NAME,
                                QueryBuilder.bindMarker() ) ) );
    }

    @Override
    public UUIDSet load( ACLPermissionPair key ) {
        ResultSet execute = session.execute( LOAD_USERACLS_FOR_PERMISSION_QUERY_ACL.bind( key.getAclId() ) );
        UUIDSet results = new UUIDSet( 1 );
        for ( Row row : execute ) {
            UUID userId = row.getUUID( UserACLToPermissionsMapstore.USER_ID_COL_NAME );
            Set<Integer> perms = row.getSet( UserACLToPermissionsMapstore.PERMISSIONS_COL_NAME, Integer.class );
            if ( perms.contains( Integer.valueOf( key.getPermission().ordinal() ) ) ) {
                results.add( userId );
            }
        }
        return results;
    }

    @Override
    public void store( ACLPermissionPair key, Set<UUID> value ) {
        throw new UnsupportedOperationException(
                "This mapstore is read-only! Use the UserACLToPermissionsMapstore for writes!" );
    }

    @Override
    public void delete( ACLPermissionPair key ) {
        throw new UnsupportedOperationException(
                "This mapstore is read-only! Use the UserACLToPermissionsMapstore for writes!" );
    }

    @Override
    public void deleteAll( Collection<ACLPermissionPair> keys ) {
        throw new UnsupportedOperationException(
                "This mapstore is read-only! Use the UserACLToPermissionsMapstore for writes!" );
    }

    @Override
    public Iterable<ACLPermissionPair> loadAllKeys() {
        return null;
    }

    @Override
    public MapConfig getMapConfig() {
        MapConfig mapConfig = super.getMapConfig();
        return mapConfig.setTimeToLiveSeconds( 1 );
    }

    @Override
    protected ResultSetFuture asyncLoad( ACLPermissionPair key ) {
        return session.executeAsync( LOAD_USERACLS_FOR_PERMISSION_QUERY_ACL.bind( key.getPermission().ordinal() ) );
    }

    @Override
    protected ACLPermissionPair mapToKey( Row row ) {
        throw new UnsupportedOperationException( "Reverse lookup maps dont support key mapping" );
    }

    @Override
    protected UUIDSet mapToValue( Row row ) {
        throw new UnsupportedOperationException( "Reverse lookup maps dont support value mapping" );
    }

    @Override
    public ACLPermissionPair generateTestKey() {
        return new ACLPermissionPair( new UUID( 0l, 2l ), Permission.OWNER );
    }

    @Override
    public Set<UUID> generateTestValue() {
        return ImmutableSet.of( new UUID( 0l, 1l ) );
    }
}
