package com.kryptnostic.mapstores.v2.mapstores;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Futures;
import com.kryptnostic.rhizome.mapstores.cassandra.BaseCassandraSetProxy;

public class ReadableObjectsSetProxy extends BaseCassandraSetProxy<UserObjectTypePair, UUID> {

    protected final Logger          logger = LoggerFactory.getLogger( getClass() );

    private final PreparedStatement ADD_STATEMENT;
    private final PreparedStatement CONTAINS_STATEMENT;
    private final PreparedStatement REMOVE_STATEMENT;
    private final PreparedStatement REMOVE_ALL_STATEMENT;
    private UserObjectTypePair      keyPair;

    public ReadableObjectsSetProxy(
            Session session,
            String keyspace,
            String table,
            UserObjectTypePair keyPair ) {
        super(
                session,
                keyspace,
                table,
                getStatement( keyspace, table, keyPair ),
                size( keyspace, table, keyPair ) );
        this.keyPair = keyPair;

        ProxyKey key = new ProxyKey( keyspace, table );
        ADD_STATEMENT = ReadableObjectsSetProxyMapStore.SP_ADD_STATEMENTS.getIfPresent( key );
        CONTAINS_STATEMENT = ReadableObjectsSetProxyMapStore.SP_CONTAINS_STATEMENTS.getIfPresent( key );
        REMOVE_STATEMENT = ReadableObjectsSetProxyMapStore.SP_DELETE_STATEMENTS.getIfPresent( key );
        REMOVE_ALL_STATEMENT = ReadableObjectsSetProxyMapStore.SP_REMOVE_ALL_STATEMENTS.getIfPresent( key );
        Preconditions.checkNotNull( ADD_STATEMENT,
                "something terribly terribly wrong happenend to the SetProxy add statement" );
        Preconditions.checkNotNull( CONTAINS_STATEMENT,
                "something terribly terribly wrong happenend to the SetProxy contains statement" );
        Preconditions.checkNotNull( REMOVE_STATEMENT,
                "something terribly terribly wrong happenend to the SetProxy remove statement" );
    }

    private static Select.Where size( String keyspace, String table, UserObjectTypePair uotp ) {
        return QueryBuilder.select().countAll()
                .from( keyspace, table )
                .where( QueryBuilder.eq( ReadableObjectsSetProxyMapStore.USER_ID_COLUMN, uotp.getUserId() ) )
                .and( QueryBuilder.eq( ReadableObjectsSetProxyMapStore.TYPE_ID_COLUMN, uotp.getTypeId() ) );
    }

    private static Select.Where getStatement( String keyspace, String table, UserObjectTypePair uotp ) {
        return QueryBuilder.select( ReadableObjectsSetProxyMapStore.OBJECT_ID_COLUMN )
                .from( keyspace, table )
                .where( QueryBuilder.eq( ReadableObjectsSetProxyMapStore.USER_ID_COLUMN, uotp.getUserId() ) )
                .and( QueryBuilder.eq( ReadableObjectsSetProxyMapStore.TYPE_ID_COLUMN, uotp.getTypeId() ) );
    }

    @Override
    public boolean contains( Object o ) {
        if ( UUID.class.isAssignableFrom( o.getClass() ) ) {
            return containsValue( UUID.class.cast( o ) );
        }
        return false;
    }

    private boolean containsValue( UUID o ) {
        ResultSet execute = session.execute( CONTAINS_STATEMENT.bind( keyPair.getUserId(), keyPair.getTypeId(), o ) );
        execute.one();
        return true;
    }

    @Override
    public void clear() {
        session.executeAsync( REMOVE_ALL_STATEMENT.bind( keyPair.getUserId(), keyPair.getTypeId() ) );
    }

    @Override
    public boolean add( UUID e ) {
        if ( e == null ) {
            return false;
        }
        ResultSet execute = session.execute( ADD_STATEMENT.bind( keyPair.getUserId(), keyPair.getTypeId(), e ) );
        return execute.wasApplied();
    }

    @Override
    public boolean remove( Object o ) {
        if ( UUID.class.isAssignableFrom( o.getClass() ) ) {
            UUID cast = UUID.class.cast( o );
            ResultSet execute = session
                    .execute( REMOVE_STATEMENT.bind( keyPair.getUserId(), keyPair.getTypeId(), cast ) );
            return execute.wasApplied();
        }
        return false;
    }

    @Override
    public boolean containsAll( Collection<?> c ) {
        if ( c.size() == 1 ) {
            return contains( Iterables.getFirst( c, null ) );
        }
        for ( Object id : c ) {
            if ( !UUID.class.isAssignableFrom( id.getClass() ) ) {
                return false;
            }
        }
        List<ResultSet> futureResultSets = Lists.newArrayList();
        try {
            futureResultSets = Futures.allAsList( Iterables.transform( c,
                    ( Object id ) -> {
                        return session.executeAsync( CONTAINS_STATEMENT.bind( keyPair.getUserId(),
                                keyPair.getTypeId(),
                                UUID.class.cast( id ) ) );
                    } ) ).get();
        } catch ( InterruptedException | ExecutionException e ) {
            logger.error( "Failed trying to bulk store entries", e );
        }
        for ( ResultSet futureResult : futureResultSets ) {
            if ( !futureResult.wasApplied() ) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll( Collection<? extends UUID> c ) {
        if ( c.size() == 1 ) {
            return add( Iterables.getFirst( c, null ) );
        }
        List<ResultSet> futureResultSets = Lists.newArrayList();
        try {
            futureResultSets = Futures.allAsList( Iterables.transform( c,
                    ( Object id ) -> session
                            .executeAsync( ADD_STATEMENT.bind( keyPair.getUserId(), keyPair.getTypeId(), id ) ) ) )
                    .get();
        } catch ( InterruptedException | ExecutionException e ) {
            logger.error( "Failed trying to bulk store entries", e );
        }
        for ( ResultSet futureResult : futureResultSets ) {
            if ( !futureResult.wasApplied() ) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected UUID mapRowToValue( Row row ) {
        return row.getUUID( ReadableObjectsSetProxyMapStore.OBJECT_ID_COLUMN );
    }

    @Override
    public Class<UUID> getTypeClazz() {
        return UUID.class;
    }

    public UserObjectTypePair getKeyPair() {
        return keyPair;
    }

}
