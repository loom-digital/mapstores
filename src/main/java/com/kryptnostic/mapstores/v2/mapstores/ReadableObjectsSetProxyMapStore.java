package com.kryptnostic.mapstores.v2.mapstores;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.google.common.base.Throwables;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableSet;
import com.kryptnostic.mapstores.v2.constants.HazelcastNames;
import com.kryptnostic.rhizome.configuration.cassandra.CassandraConfiguration;
import com.kryptnostic.rhizome.hazelcast.objects.SetProxy;
import com.kryptnostic.rhizome.mapstores.cassandra.BaseCassandraMapStore;
import com.kryptnostic.rhizome.mapstores.cassandra.BaseCassandraSetProxy;
import com.kryptnostic.rhizome.mapstores.cassandra.BaseCassandraSetProxy.ProxyKey;
import com.kryptnostic.rhizome.mapstores.cassandra.PagingCassandraIterator;
import com.kryptnostic.v2.storage.types.TypeUUIDs;

public class ReadableObjectsSetProxyMapStore extends BaseCassandraMapStore<UserObjectTypePair, Set<UUID>> {

    private static final String                     KEYSPACE_QUERY         = "CREATE KEYSPACE IF NOT EXISTS %s WITH replication = {'class':'SimpleStrategy', 'replication_factor':%d};";
    private static final String                     TABLE_QUERY            = "CREATE TABLE IF NOT EXISTS %s.%s (%s UUID, %s UUID, %s UUID, PRIMARY KEY( %s, %s, %s ) );";
    static final String                             USER_ID_COLUMN         = "userId";
    static final String                             TYPE_ID_COLUMN         = "typeId";
    static final String                             OBJECT_ID_COLUMN       = "objectId";
    Select                                          SELECT_ALL_KEYS;

    static final Cache<ProxyKey, PreparedStatement> SP_REMOVE_ALL_STATEMENTS = CacheBuilder.newBuilder()
                                                                                     .build();
    static final Cache<ProxyKey, PreparedStatement> SP_CONTAINS_STATEMENTS = CacheBuilder.newBuilder()
                                                                                   .build();
    static final Cache<ProxyKey, PreparedStatement> SP_ADD_STATEMENTS      = CacheBuilder.newBuilder()
                                                                                   .build();
    static final Cache<ProxyKey, PreparedStatement> SP_DELETE_STATEMENTS   = CacheBuilder.newBuilder()
                                                                                   .build();

    public ReadableObjectsSetProxyMapStore(
            CassandraConfiguration config,
            Session session ) {
        super(
                HazelcastNames.Maps.READABLE_TYPED_USER_OBJECTS,
                HazelcastNames.Maps.READABLE_TYPED_USER_OBJECTS,
                null,
                null,
                config,
                session );

        this.SUPPORTS_ASYNC_LOADS = false;

        session.execute( String.format( KEYSPACE_QUERY, keyspace, replicationFactor ) );

        session.execute( String.format( TABLE_QUERY,
                keyspace,
                table,
                USER_ID_COLUMN,
                TYPE_ID_COLUMN,
                OBJECT_ID_COLUMN,

                USER_ID_COLUMN,
                TYPE_ID_COLUMN,
                OBJECT_ID_COLUMN ) );

        SELECT_ALL_KEYS = QueryBuilder.select( USER_ID_COLUMN, TYPE_ID_COLUMN ).from( keyspace, table );

        ProxyKey key = new BaseCassandraSetProxy.ProxyKey( keyspace, table );
        try {
            SP_ADD_STATEMENTS.get( key, () -> session.prepare(
                    QueryBuilder.insertInto( keyspace, table )
                            .value( ReadableObjectsSetProxyMapStore.USER_ID_COLUMN, QueryBuilder.bindMarker() )
                            .value( ReadableObjectsSetProxyMapStore.TYPE_ID_COLUMN, QueryBuilder.bindMarker() )
                            .value( ReadableObjectsSetProxyMapStore.OBJECT_ID_COLUMN, QueryBuilder.bindMarker() ) ) );
            // Read-only calls
            SP_CONTAINS_STATEMENTS.get( key, () -> session.prepare(
                    QueryBuilder.select().countAll().from( keyspace, table )
                            .where( QueryBuilder.eq( ReadableObjectsSetProxyMapStore.USER_ID_COLUMN,
                                    QueryBuilder.bindMarker() ) )
                            .and( QueryBuilder.eq( ReadableObjectsSetProxyMapStore.TYPE_ID_COLUMN,
                                    QueryBuilder.bindMarker() ) )
                            .and( QueryBuilder.eq( ReadableObjectsSetProxyMapStore.OBJECT_ID_COLUMN,
                                    QueryBuilder.bindMarker() ) ) ) );

            SP_DELETE_STATEMENTS.get( key, () -> session.prepare(
                    QueryBuilder.delete().from( keyspace, table )
                            .where( QueryBuilder.eq( ReadableObjectsSetProxyMapStore.USER_ID_COLUMN,
                                    QueryBuilder.bindMarker() ) )
                            .and( QueryBuilder.eq( ReadableObjectsSetProxyMapStore.TYPE_ID_COLUMN,
                                    QueryBuilder.bindMarker() ) )
                            .and( QueryBuilder.eq( ReadableObjectsSetProxyMapStore.OBJECT_ID_COLUMN,
                                    QueryBuilder.bindMarker() ) ) ) );

            SP_REMOVE_ALL_STATEMENTS.get( key, () -> session.prepare(
                    QueryBuilder.delete().from( keyspace, table )
                            .where( QueryBuilder.eq( ReadableObjectsSetProxyMapStore.USER_ID_COLUMN,
                                    QueryBuilder.bindMarker() ) )
                            .and( QueryBuilder.eq( ReadableObjectsSetProxyMapStore.TYPE_ID_COLUMN,
                                    QueryBuilder.bindMarker() ) ) ) );
        } catch ( ExecutionException e ) {
            throw Throwables.propagate( e );
        }

    }

    @Override
    public void store( UserObjectTypePair key, Set<UUID> value ) {
        SetProxy<UserObjectTypePair, UUID> proxy = new ReadableObjectsSetProxy( session, keyspace, table, key );
        proxy.addAll( value );
    }

    @Override
    public void delete( UserObjectTypePair key ) {
        SetProxy<UserObjectTypePair, UUID> proxy = new ReadableObjectsSetProxy( session, keyspace, table, key );
        proxy.clear();
    }

    @Override
    public void deleteAll( Collection<UserObjectTypePair> keys ) {
        for ( UserObjectTypePair key : keys ) {
            SetProxy<UserObjectTypePair, UUID> proxy = new ReadableObjectsSetProxy( session, keyspace, table, key );
            proxy.clear();
        }
    }

    @Override
    public Set<UUID> load( UserObjectTypePair key ) {
        return new ReadableObjectsSetProxy( session, keyspace, table, key );
    }

    @Override
    public Iterable<UserObjectTypePair> loadAllKeys() {
        return PagingCassandraIterator.<UserObjectTypePair> asIterable( session,
                SELECT_ALL_KEYS,
                ReadableObjectsSetProxyMapStore::toKey );
    }

    private static UserObjectTypePair toKey( Row row ) {
        return new UserObjectTypePair(
                row.getUUID( USER_ID_COLUMN ),
                row.getUUID( TYPE_ID_COLUMN ) );
    }

    @Override
    public UserObjectTypePair generateTestKey() {
        return new UserObjectTypePair( new UUID( 0, 1 ), TypeUUIDs.INDEX_METADATA );
    }

    @Override
    public Set<UUID> generateTestValue() {
        return ImmutableSet.of( new UUID( 0, 2 ) );
    }

    @Override
    protected ResultSetFuture asyncLoad( UserObjectTypePair key ) {
        throw new UnsupportedOperationException( "ReadableObjectsSetProxyMapStore doesn't support async calls" );
    }

    @Override
    protected UserObjectTypePair mapToKey( Row row ) {
        return toKey( row );
    }

    @Override
    protected Set<UUID> mapToValue( Row row ) {
        throw new UnsupportedOperationException(
                "ReadableObjectsSetProxyMapStore doesn't support mapping multiple values" );
    }

}