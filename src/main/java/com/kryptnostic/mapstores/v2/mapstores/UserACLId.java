package com.kryptnostic.mapstores.v2.mapstores;

import java.util.UUID;

public class UserACLId {

    private final UUID userId;
    private final UUID aclId;

    public UserACLId( UUID userId, UUID aclId ) {
        this.userId = userId;
        this.aclId = aclId;
    }

    /**
     * @return the userId
     */
    public UUID getUserId() {
        return userId;
    }

    /**
     * @return the aclId
     */
    public UUID getAclId() {
        return aclId;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UserACLId [userId=" + userId + ", aclId=" + aclId + "]";
    }

}
