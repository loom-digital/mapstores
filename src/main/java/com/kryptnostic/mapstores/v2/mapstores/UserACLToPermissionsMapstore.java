package com.kryptnostic.mapstores.v2.mapstores;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import javax.annotation.CheckForNull;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.hazelcast.config.MapStoreConfig;
import com.hazelcast.config.MapStoreConfig.InitialLoadMode;
import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.constants.HazelcastNames;
import com.kryptnostic.rhizome.configuration.cassandra.CassandraConfiguration;
import com.kryptnostic.rhizome.mapstores.cassandra.BaseCassandraMapStore;
import com.kryptnostic.rhizome.mapstores.cassandra.PagingCassandraIterator;

public class UserACLToPermissionsMapstore extends BaseCassandraMapStore<UserACLId, Set<Permission>> {

    static final String             KEYSPACE_QUERY          = "CREATE KEYSPACE IF NOT EXISTS %s WITH replication = {'class':'SimpleStrategy', 'replication_factor':%d};";
    static final String             TABLE_QUERY             = "CREATE TABLE IF NOT EXISTS %s.%s (%s uuid, %s uuid, %s set<int>, PRIMARY KEY (%s, %s) );";
    static final String             INDEX_PERMISSIONS_QUERY = "CREATE INDEX IF NOT EXISTS useracls_permission_idx ON %s.%s (%s);";
    static final String             INDEX_ACL_QUERY         = "CREATE INDEX IF NOT EXISTS useracls_acl_idx ON %s.%s (%s);";

    static final String             USER_ID_COL_NAME        = "user";
    static final String             ACL_ID_COL_NAME         = "acl";
    static final String             PERMISSIONS_COL_NAME    = "permissions";

    public static final String[]    ALL_COLUMNS             = new String[] { USER_ID_COL_NAME, ACL_ID_COL_NAME,
            PERMISSIONS_COL_NAME };

    private final PreparedStatement ADD_ENTRY_STATEMENT;
    private final PreparedStatement GET_ENTRY_STATEMENT;
    // private final PreparedStatement GET_ALL_ENTRY_STATEMENT;
    private final PreparedStatement DELETE_STATEMENT;

    final Select                    SELECT_ALL_KEYS;

    public UserACLToPermissionsMapstore(
            Session session,
            String mapName,
            String table,
            CassandraConfiguration config ) {
        super( table, mapName, null, null, config, session );

        // create keyspace
        session.execute( String.format( KEYSPACE_QUERY,
                keyspace,
                Integer.valueOf( replicationFactor ) ) );

        // create table
        session.execute( String.format( TABLE_QUERY,
                keyspace,
                table,
                // columns
                USER_ID_COL_NAME,
                ACL_ID_COL_NAME,
                PERMISSIONS_COL_NAME,
                // Primary key
                USER_ID_COL_NAME,
                ACL_ID_COL_NAME ) );

        if ( HazelcastNames.Maps.USER_LOOKUP_ACL_MAP.equals( mapName ) ) {
            session.execute( String.format( INDEX_ACL_QUERY,
                    keyspace,
                    table,
                    ACL_ID_COL_NAME ) );
        } else {
            // remove when the userlookupaclmapstoreOld is removed
            session.execute( String.format( INDEX_PERMISSIONS_QUERY,
                    keyspace,
                    table,
                    PERMISSIONS_COL_NAME ) );
        }

        ADD_ENTRY_STATEMENT = session.prepare( QueryBuilder.insertInto( keyspace, table )
                .values( new String[] { USER_ID_COL_NAME, ACL_ID_COL_NAME, PERMISSIONS_COL_NAME },
                        new Object[] { QueryBuilder.bindMarker(), QueryBuilder.bindMarker(),
                                QueryBuilder.bindMarker() } ) );

        GET_ENTRY_STATEMENT = session.prepare(
                QueryBuilder.select( PERMISSIONS_COL_NAME )
                        .from( keyspace, table )
                        .where( QueryBuilder.eq( USER_ID_COL_NAME, QueryBuilder.bindMarker() ) )
                        .and( QueryBuilder.eq( ACL_ID_COL_NAME, QueryBuilder.bindMarker() ) ) );
        DELETE_STATEMENT = session.prepare(
                QueryBuilder.delete().from( keyspace, table )
                        .where( QueryBuilder.eq( USER_ID_COL_NAME, QueryBuilder.bindMarker() ) ) );
        // .and( QueryBuilder.eq( ACL_ID_COL_NAME, QueryBuilder.bindMarker() ) ) );

        SELECT_ALL_KEYS = QueryBuilder.select( USER_ID_COL_NAME, ACL_ID_COL_NAME ).from( keyspace, table );
    }

    @Override
    public void store( UserACLId key, Set<Permission> value ) {
        session.execute( ADD_ENTRY_STATEMENT.bind( key.getUserId(), key.getAclId(), toValue( value ) ) );
    }

    static Set<Integer> toValue( Set<Permission> permissions ) {
        Set<Integer> asInts = Sets.newHashSetWithExpectedSize( permissions.size() );
        for ( Permission permission : permissions ) {
            asInts.add( Integer.valueOf( permission.ordinal() ) );
        }
        return asInts;
    }

    @Override
    public void storeAll( Map<UserACLId, Set<Permission>> map ) {
        for ( Entry<UserACLId, Set<Permission>> ent : map.entrySet() ) {
            store( ent.getKey(), ent.getValue() );
        }
    }

    @Override
    public Set<Permission> load( UserACLId key ) {
        ResultSet execute = session.execute( GET_ENTRY_STATEMENT.bind( key.getUserId(), key.getAclId() ) );
        return mapToValue( execute.one() );
    }

    @Override
    public Map<UserACLId, Set<Permission>> loadAll( Collection<UserACLId> keys ) {
        Map<UserACLId, Set<Permission>> results = Maps.newHashMapWithExpectedSize( keys.size() );
        for ( UserACLId id : keys ) {
            results.put( id, load( id ) );
        }
        return results;
    }

    static UserACLId toKey( Row next ) {
        UUID userId = next.getUUID( USER_ID_COL_NAME );
        UUID aclId = next.getUUID( ACL_ID_COL_NAME );
        return new UserACLId( userId, aclId );
    }

    @Override
    public Iterable<UserACLId> loadAllKeys() {
        return PagingCassandraIterator.<UserACLId> asIterable( session,
                SELECT_ALL_KEYS,
                UserACLToPermissionsMapstore::toKey );
    }

    @Override
    public void delete( UserACLId key ) {
        session.execute( DELETE_STATEMENT.bind( key.getUserId() ) );// , key.getAclId() ) );
    }

    @Override
    public void deleteAll( Collection<UserACLId> keys ) {
        for ( UserACLId key : keys ) {
            delete( key );
        }
    }

    @Override
    public UserACLId generateTestKey() {
        return new UserACLId( new UUID( 0L, 1L ), new UUID( 1L, 2L ) );
    }

    @Override
    public Set<Permission> generateTestValue() {
        return EnumSet.of( Permission.READ, Permission.WRITE );
    }

    @Override
    public MapStoreConfig getMapStoreConfig() {
        return new MapStoreConfig().setImplementation( this )
                .setEnabled( true )
                .setInitialLoadMode( InitialLoadMode.LAZY )
                .setWriteDelaySeconds( 0 );
    }

    @Override
    protected ResultSetFuture asyncLoad( UserACLId key ) {
        return session.executeAsync( GET_ENTRY_STATEMENT.bind( key.getUserId(), key.getAclId() ) );
    }

    @Override
    protected UserACLId mapToKey( Row row ) {
        return toKey( row );
    }

    @Override
    protected Set<Permission> mapToValue( @CheckForNull Row row ) {
        if ( row == null ) {
            return EnumSet.<Permission> noneOf( Permission.class );
        }
        Set<Integer> value = row.getSet( PERMISSIONS_COL_NAME, Integer.class );
        EnumSet<Permission> permissions = EnumSet.noneOf( Permission.class );
        for ( Integer ordinal : value ) {
            permissions.add( Permission.values()[ ordinal.intValue() ] );
        }
        return permissions;
    }
}