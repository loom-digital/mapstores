package com.kryptnostic.mapstores.v2.mapstores;

import java.util.UUID;

public class UserObjectTypePair {

    private final UUID userId;
    private final UUID typeId;

    public UserObjectTypePair( UUID userId, UUID typeId ) {
        this.userId = userId;
        this.typeId = typeId;
    }

    /**
     * @return the userId
     */
    public UUID getUserId() {
        return userId;
    }

    /**
     * @return the typeId
     */
    public UUID getTypeId() {
        return typeId;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UserObjectTypePair [userId=" + userId + ", typeId=" + typeId + "]";
    }
}
