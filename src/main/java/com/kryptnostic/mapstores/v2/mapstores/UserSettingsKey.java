package com.kryptnostic.mapstores.v2.mapstores;

import java.util.UUID;

import com.kryptnostic.directory.v2.model.UserSettings;

public class UserSettingsKey {

    private UUID userId;
    private UserSettings userSetting;
    
    public UserSettingsKey( UUID userId, UserSettings userSetting ) {
        this.userId = userId;
        this.userSetting = userSetting;
    }

    public UUID getUserId() {
        return userId;
    }

    public UserSettings getUserSetting() {
        return userSetting;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( userId == null ) ? 0 : userId.hashCode() );
        result = prime * result + ( ( userSetting == null ) ? 0 : userSetting.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( !( obj instanceof UserSettingsKey ) ) return false;
        UserSettingsKey other = (UserSettingsKey) obj;
        if ( userId == null ) {
            if ( other.userId != null ) return false;
        } else if ( !userId.equals( other.userId ) ) return false;
        if ( userSetting != other.userSetting ) return false;
        return true;
    }
    
}
