package com.kryptnostic.mapstores.v2.pods;

import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;

import org.joda.time.LocalDate;
import org.springframework.context.annotation.Bean;

import com.kryptnostic.directory.v1.domain.Domain;
import com.kryptnostic.directory.v1.domain.DomainPolicy;
import com.kryptnostic.directory.v2.model.DomainResourcesUsage;
import com.kryptnostic.heracles.directory.v1.objects.Emails;
import com.kryptnostic.heracles.directory.v2.objects.DomainResourcesKey;
import com.kryptnostic.kodex.v1.crypto.ciphers.BlockCiphertext;
import com.kryptnostic.kodex.v1.crypto.ciphers.Cypher;
import com.kryptnostic.kodex.v1.models.Group;
import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.constants.HazelcastNames;
import com.kryptnostic.mapstores.v2.mapstores.ACLPermissionPair;
import com.kryptnostic.mapstores.v2.mapstores.ObjectMetadataHierarchyKey;
import com.kryptnostic.mapstores.v2.mapstores.ObjectMetadataLevelKey;
import com.kryptnostic.mapstores.v2.mapstores.UserACLId;
import com.kryptnostic.mapstores.v2.mapstores.UserObjectTypePair;
import com.kryptnostic.mapstores.v2.mapstores.UserSettingsKey;
import com.kryptnostic.mapstores.v2.test.DataGeneratorFactory;
import com.kryptnostic.metrics.v1.MetricsUserStatsMetadata;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.mappers.SelfRegisteringValueMapper;
import com.kryptnostic.rhizome.mapstores.KryptnosticMapStoreFactory;
import com.kryptnostic.rhizome.mapstores.TestableSelfRegisteringMapStore;
import com.kryptnostic.v2.crypto.CryptoMaterial;
import com.kryptnostic.v2.search.IndexSegmentAddress;
import com.kryptnostic.v2.sharing.models.Share;
import com.kryptnostic.v2.storage.models.Event;
import com.kryptnostic.v2.storage.models.ObjectMetadata;
import com.kryptnostic.v2.storage.models.Scope;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;
import com.kryptnostic.v2.storage.models.VersionedObjectKeySet;
import com.kryptnostic.v2.storage.models.VersionedObjectUserKey;

public abstract class BaseKryptnosticMapstoresPod implements KryptnosticMapStoresPod {

    KryptnosticMapStoreFactory         mapstoreFactory;

    @Inject // I know there must be a better way to specify that this depends on the ValueMappersPod
    Set<SelfRegisteringValueMapper<?>> defaultValueMappers;

    public abstract void init();

    // ObjectStorageService mapstores

    // Authoritative mapstore for getReadableObjectsOftype
    abstract TestableSelfRegisteringMapStore<UserObjectTypePair, Set<UUID>> readableObjectsForUserAndTypeMapStore();

    // Authoritative OM mapstore
    abstract TestableSelfRegisteringMapStore<UUID, ObjectMetadata> objectMetadataMapStoreV2();

    // Authoritative mapstore for levels call
    abstract TestableSelfRegisteringMapStore<ObjectMetadataLevelKey, VersionedObjectKeySet> levelFilteringMap();

    abstract TestableSelfRegisteringMapStore<ObjectMetadataHierarchyKey, VersionedObjectKeySet> hierarchyFilteringMap();

    @Bean
    @Override
    public TestableSelfRegisteringMapStore<UUID, Set<UUID>> objectLookupACLMapStore() {
        return mapstoreFactory.buildSetProxy( UUID.class, UUID.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createSet( UUID.class ) )
                .withTableAndMapName( HazelcastNames.Maps.OBJECT_LOOKUP_ACL_MAP )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<VersionedObjectKey, Set<VersionedObjectKey>> forwardEdgeMapStore() {
        return mapstoreFactory.buildSetProxy( VersionedObjectKey.class, VersionedObjectKey.class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectKey.class ) )
                .withTestValue( DataGeneratorFactory.createSet( VersionedObjectKey.class ) )
                .withTableAndMapName( HazelcastNames.Maps.OBJECT_FORWARD_EDGE_MAP )
                .enableEagerLoading()
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<VersionedObjectKey, VersionedObjectKey> backEdgeMapStore() {
        return mapstoreFactory.build( VersionedObjectKey.class, VersionedObjectKey.class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( VersionedObjectKey.class ) )
                .withTableAndMapName( HazelcastNames.Maps.OBJECT_BACK_EDGE_MAP )
                .build();
    }

    @Override
    public TestableSelfRegisteringMapStore<VersionedObjectKey, Event> eventsMapStore() {
        return mapstoreFactory.build( VersionedObjectKey.class, Event.class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( Event.class ) )
                .withTableAndMapName( HazelcastNames.Maps.EVENT_OBJECTS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<VersionedObjectKey, byte[]> encryptedObjectIvMapStore() {
        return mapstoreFactory.build( VersionedObjectKey.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
                .withTableAndMapName( HazelcastNames.Maps.ENCRYPTED_OBJECTS + HazelcastNames.Suffixes.IV_MAP )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<VersionedObjectKey, byte[]> encryptedObjectTagMapStore() {
        return mapstoreFactory.build( VersionedObjectKey.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
                .withTableAndMapName( HazelcastNames.Maps.ENCRYPTED_OBJECTS + HazelcastNames.Suffixes.TAG_MAP )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<VersionedObjectKey, byte[]> encryptedObjectSaltMapStore() {
        return mapstoreFactory.build( VersionedObjectKey.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
                .withTableAndMapName( HazelcastNames.Maps.ENCRYPTED_OBJECTS + HazelcastNames.Suffixes.SALT_MAP )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<VersionedObjectKey, byte[]> encryptedObjectContentMapStore() {
        return mapstoreFactory.build( VersionedObjectKey.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
                .withTableAndMapName( HazelcastNames.Maps.ENCRYPTED_OBJECTS + HazelcastNames.Suffixes.CONTENT_MAP )
                .build();
    }

    @Bean
    @Override
    public TestableSelfRegisteringMapStore<UUID, Cypher> objectEncryptionTypeMapStore() {
        return mapstoreFactory.build( UUID.class, Cypher.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( Cypher.class ) )
                .withTableAndMapName( HazelcastNames.Maps.OBJECT_ENCRYPTION_TYPE_MAP )
                .build();
    }

    @Bean
    @Override
    public TestableSelfRegisteringMapStore<UUID, Set<CryptoMaterial>> objectUploadProgressMapStore() {
        return mapstoreFactory.buildSetProxy( UUID.class, CryptoMaterial.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createSet( CryptoMaterial.class ) )
                .withTableAndMapName( HazelcastNames.Maps.OBJECT_UPLOAD_PROGRESS_MAP )
                .build();
    }

    @Bean
    @Override
    @Deprecated
    public TestableSelfRegisteringMapStore<VersionedObjectKey, Integer> objectClockMapStoreV1() {
        return mapstoreFactory.build( VersionedObjectKey.class, Integer.class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( Integer.class ) )
                .withTableAndMapName( HazelcastNames.Maps.OBJECT_CLOCK_OLD )
                .enableEagerLoading()
                .build();
    }

    @Bean
    @Override
    public TestableSelfRegisteringMapStore<VersionedObjectKey, UUID> objectClockMapStore() {
        return mapstoreFactory.build( VersionedObjectKey.class, UUID.class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( UUID.class ) )
                .withTableAndMapName( HazelcastNames.Maps.OBJECT_CLOCK )
                .enableEagerLoading()
                .build();
    }

    @Bean
    @Override
    @Deprecated
    public TestableSelfRegisteringMapStore<VersionedObjectKey, Integer> clockServerMapStore() {
        return mapstoreFactory.build( VersionedObjectKey.class, Integer.class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( Integer.class ) )
                .withTableAndMapName( HazelcastNames.Maps.CLOCK_SERVER )
                .build();
    }

    // AuthzService mapstores
    @Override
    public abstract TestableSelfRegisteringMapStore<ACLPermissionPair, Set<UUID>> indexedPermissionsMapstore();

    @Override
    public abstract TestableSelfRegisteringMapStore<UserACLId, Set<Permission>> userLookupACLMapStore();

    // KeyStorageService mapstores
    @Override
    @Bean
    public TestableSelfRegisteringMapStore<VersionedObjectUserKey, byte[]> objectCryptoServicesMapStore() {
        return mapstoreFactory.build( VersionedObjectUserKey.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectUserKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
                .withTableAndMapName( HazelcastNames.Maps.OBJECT_CRYPTO_SERVICE_MAP )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, BlockCiphertext> encryptedUserSaltsMapStore() {
        return mapstoreFactory.build( UUID.class, BlockCiphertext.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( BlockCiphertext.class ) )
                .withTableAndMapName( HazelcastNames.Maps.ENCRYPTED_KEY_SALTS_MAP )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, BlockCiphertext> rsaPrivateKeysMapStore() {
        return mapstoreFactory.build( UUID.class, BlockCiphertext.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( BlockCiphertext.class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_RSA_PRIVATE_KEYS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, byte[]> rsaPublicKeysMapStore() {
        return mapstoreFactory.build( UUID.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_RSA_PUBLIC_KEYS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, byte[]> userFheHashFunctionMapStore() {
        return mapstoreFactory.build( UUID.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_FHE_HASH_FUNCTION )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, byte[]> userFHEPublicKeyMapStore() {
        return mapstoreFactory.build( UUID.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_FHE_PUBLIC_KEYS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, BlockCiphertext> userFHEPrivateKeyMapStore() {
        return mapstoreFactory.build( UUID.class, BlockCiphertext.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( BlockCiphertext.class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_FHE_PRIVATE_KEYS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, BlockCiphertext> userFHESearchPrivateKeyMapStore() {
        return mapstoreFactory.build( UUID.class, BlockCiphertext.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( BlockCiphertext.class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_FHE_SEARCH_PRIVATE_KEYS )
                .build();
    }

    @Override
	@Bean
	public TestableSelfRegisteringMapStore<VersionedObjectUserKey, byte[]> aesEncryptedObjectCryptoServicesIvMapStore() {
	    return mapstoreFactory.build( VersionedObjectUserKey.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectUserKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
	            .withTableAndMapName( HazelcastNames.Maps.AES_ENCRYPTED_OBJECT_CRYPTO_SERVICES + HazelcastNames.Suffixes.IV_MAP )
	            .build();
	}

	@Override
	@Bean
	public TestableSelfRegisteringMapStore<VersionedObjectUserKey, byte[]> aesEncryptedObjectCryptoServicesTagMapStore() {
	    return mapstoreFactory.build( VersionedObjectUserKey.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectUserKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
	            .withTableAndMapName( HazelcastNames.Maps.AES_ENCRYPTED_OBJECT_CRYPTO_SERVICES + HazelcastNames.Suffixes.TAG_MAP )
	            .build();
	}

	@Override
	@Bean
	public TestableSelfRegisteringMapStore<VersionedObjectUserKey, byte[]> aesEncryptedObjectCryptoServicesSaltMapStore() {
	    return mapstoreFactory.build( VersionedObjectUserKey.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectUserKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
	            .withTableAndMapName( HazelcastNames.Maps.AES_ENCRYPTED_OBJECT_CRYPTO_SERVICES + HazelcastNames.Suffixes.SALT_MAP )
	            .build();
	}

	@Override
	@Bean
	public TestableSelfRegisteringMapStore<VersionedObjectUserKey, byte[]> aesEncryptedObjectCryptoServicesContentMapStore() {
	    return mapstoreFactory.build( VersionedObjectUserKey.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectUserKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
	            .withTableAndMapName( HazelcastNames.Maps.AES_ENCRYPTED_OBJECT_CRYPTO_SERVICES + HazelcastNames.Suffixes.CONTENT_MAP )
	            .build();
	}

	@Override
	@Bean
	public TestableSelfRegisteringMapStore<UUID, byte[]> masterKeysMapStore() {
	    return mapstoreFactory.build( UUID.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
	            .withTableAndMapName( HazelcastNames.Maps.ENCRYPTED_MASTER_KEYS )
	            .build();
	}

	// Search service mapstores
	@Override
	@Bean
    public TestableSelfRegisteringMapStore<UUID, Integer> indexSegmentCountsMapStore() {
        return mapstoreFactory.build( UUID.class, Integer.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( Integer.class ) )
                .withTableAndMapName( HazelcastNames.Maps.INDEX_SEGMENT_COUNTS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<IndexSegmentAddress, Set<UUID>> indexSegmentIdsMapStore() {
        return mapstoreFactory.buildSetProxy( IndexSegmentAddress.class, UUID.class )
                .withTestKey( DataGeneratorFactory.createData( IndexSegmentAddress.class ) )
                .withTestValue( DataGeneratorFactory.createSet( UUID.class ) )
                .withTableAndMapName( HazelcastNames.Maps.INDEX_SEGMENT_IDS )
                .setObjectInMemoryFormat()
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<IndexSegmentAddress, UUIDSet> indexSegmentIdsInlineMapStore() {
        return mapstoreFactory.build( IndexSegmentAddress.class, UUIDSet.class )
                .withTestKey( DataGeneratorFactory.createData( IndexSegmentAddress.class ) )
                .withTestValue( DataGeneratorFactory.createData( UUIDSet.class ) )
                .withTableAndMapName( HazelcastNames.Maps.INDEX_SEGMENT_IDS_INLINE )
                .build();
    }

    // Sharing service mapstores
    @Override
    @Bean
    public TestableSelfRegisteringMapStore<VersionedObjectUserKey, Share> incomingSharesMapStore() {
        return mapstoreFactory.build( VersionedObjectUserKey.class, Share.class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectUserKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( Share.class ) )
                .withTableAndMapName( HazelcastNames.Maps.INCOMING_SHARES )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, Set<VersionedObjectKey>> incomingSharedObjectKeysMapStore() {
        return mapstoreFactory.buildSetProxy( UUID.class, VersionedObjectKey.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createSet( VersionedObjectKey.class ) )
                .withTableAndMapName( HazelcastNames.Maps.INCOMING_SHARED_OBJECT_KEYS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<VersionedObjectUserKey, byte[]> sharingMetadataMapStore() {
        return mapstoreFactory.build( VersionedObjectUserKey.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( VersionedObjectUserKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
                .withTableAndMapName( HazelcastNames.Maps.OBJECT_SEARCH_PAIRS_MAP )
                .build();
    }

    // Directory service mapstores
    @Override
    @Bean
    public TestableSelfRegisteringMapStore<String, UUID> users() {
        return mapstoreFactory.build( String.class, UUID.class )
                .withTestKey( DataGeneratorFactory.createData( String.class ) )
                .withTestValue( DataGeneratorFactory.createData( UUID.class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_EMAIL_TO_USER_ID )
                .build();
    }

    @Bean
    @Override
    public TestableSelfRegisteringMapStore<UUID, Emails> emails() {
        // TODO: refactor Emails and make this a setproxy
        return mapstoreFactory.build( UUID.class, Emails.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( Emails.class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_EMAILS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, String> userIdsToNames() {
        return mapstoreFactory.build( UUID.class, String.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( String.class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_IDS_TO_NAMES )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, String> authenticators() {
        return mapstoreFactory.build( UUID.class, String.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( String.class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_AUTHENTICATORS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, byte[]> certificates() {
        return mapstoreFactory.build( UUID.class, byte[].class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( byte[].class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_CERTIFICATES )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, Set<String>> roles() {
        return mapstoreFactory.buildSetProxy( UUID.class, String.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createSet( String.class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_ROLES )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UserSettingsKey, Set<String>> userSettings() {
        return mapstoreFactory.buildSetProxy( UserSettingsKey.class, String.class )
                .withTestKey( DataGeneratorFactory.createData( UserSettingsKey.class ) )
                .withTestValue( DataGeneratorFactory.createSet( String.class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_SETTINGS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, Set<UUID>> groupMemberships() {
        return mapstoreFactory.buildSetProxy( UUID.class, UUID.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createSet( UUID.class ) )
                .withTableAndMapName( HazelcastNames.Maps.USER_TO_GROUPS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, Domain> domains() {
        return mapstoreFactory.build( UUID.class, Domain.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( Domain.class ) )
                .withTableAndMapName( HazelcastNames.Maps.DOMAINS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, Set<UUID>> trustedDomains() {
        return mapstoreFactory.buildSetProxy( UUID.class, UUID.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createSet( UUID.class ) )
                .withTableAndMapName( HazelcastNames.Maps.DOMAIN_ID_TO_TRUSTED_DOMAINS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, Set<UUID>> incomingTrusts() {
        return mapstoreFactory.buildSetProxy( UUID.class, UUID.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createSet( UUID.class ) )
                .withTableAndMapName( HazelcastNames.Maps.DOMAIN_ID_TO_INCOMING_TRUSTS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<DomainPolicy, Set<UUID>> domainsWithPolicy() {
        return mapstoreFactory.buildSetProxy( DomainPolicy.class, UUID.class )
                .withTestKey( DataGeneratorFactory.createData( DomainPolicy.class ) )
                .withTestValue( DataGeneratorFactory.createSet( UUID.class ) )
                .withTableAndMapName( HazelcastNames.Maps.DOMAINS_WITH_POLICY )
                .build();
    }

    // TODO: get rid of this map, domains() already has the domain name stored in Domain object
    // just write an entryprocessor to return the domain name and run it on the domains() map
    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, String> domainIdToName() {
        return mapstoreFactory.build( UUID.class, String.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( String.class ) )
                .withTableAndMapName( HazelcastNames.Maps.DOMAIN_ID_TO_NAME )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<String, UUID> domainNameToId() {
        return mapstoreFactory.build( String.class, UUID.class )
                .withTestKey( DataGeneratorFactory.createData( String.class ) )
                .withTestValue( DataGeneratorFactory.createData( UUID.class ) )
                .withTableAndMapName( HazelcastNames.Maps.DOMAIN_NAME_TO_ID )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, Set<UUID>> domainMembers() {
        return mapstoreFactory.buildSetProxy( UUID.class, UUID.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createSet( UUID.class ) )
                .withTableAndMapName( HazelcastNames.Maps.DOMAIN_MEMBERS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, Group> groups() {
        return mapstoreFactory.build( UUID.class, Group.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( Group.class ) )
                .withTableAndMapName( HazelcastNames.Maps.GROUPS )
                .build();
    }

    // TODO: get rid of this map, groups() already has the group name stored in Group object
    // just write an entryprocessor to return the group name and run it on the groups() map
    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, String> groupNames() {
        return mapstoreFactory.build( UUID.class, String.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( String.class ) )
                .withTableAndMapName( HazelcastNames.Maps.GROUP_ID_TO_NAME )
                .build();
    }

    // TODO: MetricsUserStatsMetadata is just a string. We should get rid of the class and just use the string
    @Override
    @Bean
    public TestableSelfRegisteringMapStore<String, MetricsUserStatsMetadata> dateuserDataMapStore() {
        return mapstoreFactory.build( String.class, MetricsUserStatsMetadata.class )
                .withTestKey( DataGeneratorFactory.createData( String.class ) )
                .withTestValue( DataGeneratorFactory.createData( MetricsUserStatsMetadata.class ) )
                .withTableAndMapName( HazelcastNames.Maps.DATE_USERDATA )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<LocalDate, String> dateUsersMapStore() {
        return mapstoreFactory.build( LocalDate.class, String.class )
                .withTestKey( DataGeneratorFactory.createData( LocalDate.class ) )
                .withTestValue( DataGeneratorFactory.createData( String.class ) )
                .withTableAndMapName( HazelcastNames.Maps.DATE_USERS )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<LocalDate, Integer> dateCountMapStore() {
        return mapstoreFactory.build( LocalDate.class, Integer.class )
                .withTestKey( DataGeneratorFactory.createData( LocalDate.class ) )
                .withTestValue( DataGeneratorFactory.createData( Integer.class ) )
                .withTableAndMapName( HazelcastNames.Maps.DATE_COUNT )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<String, UUID> nameToTypeMap() {
        return mapstoreFactory.build( String.class, UUID.class )
                .withTestKey( DataGeneratorFactory.createData( String.class ) )
                .withTestValue( DataGeneratorFactory.createData( UUID.class ) )
                .withTableAndMapName( HazelcastNames.Maps.NAME_TO_TYPE_MAP )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, String> typeToNameMap() {
        return mapstoreFactory.build( UUID.class, String.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( String.class ) )
                .withTableAndMapName( HazelcastNames.Maps.TYPE_TO_NAME_MAP )
                .build();
    }

    // Types MapStores
    @Override
    @Bean
    public TestableSelfRegisteringMapStore<UUID, String> typesToScopesMapStore() {
        return mapstoreFactory.build( UUID.class, String.class )
                .withTestKey( DataGeneratorFactory.createData( UUID.class ) )
                .withTestValue( DataGeneratorFactory.createData( String.class ) )
                .withTableAndMapName( HazelcastNames.Maps.TYPES_TO_SCOPES )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<String, Scope> scopeMapStore() {
        return mapstoreFactory.build( String.class, Scope.class )
                .withTestKey( DataGeneratorFactory.createData( String.class ) )
                .withTestValue( DataGeneratorFactory.createData( Scope.class ) )
                .withTableAndMapName( HazelcastNames.Maps.SCOPES )
                .build();
    }

    @Override
    @Bean
    public TestableSelfRegisteringMapStore<DomainResourcesKey, DomainResourcesUsage> domainResourcesMap() {
        return mapstoreFactory.build( DomainResourcesKey.class, DomainResourcesUsage.class )
                .withTestKey( DataGeneratorFactory.createData( DomainResourcesKey.class ) )
                .withTestValue( DataGeneratorFactory.createData( DomainResourcesUsage.class ) )
                .withTableAndMapName( HazelcastNames.Maps.DOMAIN_RESOURCES )
                .build();
    }

}
