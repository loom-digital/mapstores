package com.kryptnostic.mapstores.v2.pods;

import java.util.Set;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.datastax.driver.core.Session;
import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.constants.HazelcastNames;
import com.kryptnostic.mapstores.v2.mapstores.ACLPermissionPair;
import com.kryptnostic.mapstores.v2.mapstores.ObjectMetadataHierarchyKey;
import com.kryptnostic.mapstores.v2.mapstores.ObjectMetadataHierarchyMapstore;
import com.kryptnostic.mapstores.v2.mapstores.ObjectMetadataLevelKey;
import com.kryptnostic.mapstores.v2.mapstores.ObjectMetadataLevelMapstore;
import com.kryptnostic.mapstores.v2.mapstores.ObjectMetadataMapStore;
import com.kryptnostic.mapstores.v2.mapstores.PermissionsToUserACLMapstore;
import com.kryptnostic.mapstores.v2.mapstores.ReadableObjectsSetProxyMapStore;
import com.kryptnostic.mapstores.v2.mapstores.UserACLId;
import com.kryptnostic.mapstores.v2.mapstores.UserACLToPermissionsMapstore;
import com.kryptnostic.mapstores.v2.mapstores.UserObjectTypePair;
import com.kryptnostic.metrics.v1.MetricsLogRecord;
import com.kryptnostic.rhizome.configuration.cassandra.CassandraConfiguration;
import com.kryptnostic.rhizome.mapstores.SelfRegisteringQueueStore;
import com.kryptnostic.rhizome.mapstores.TestableSelfRegisteringMapStore;
import com.kryptnostic.rhizome.mapstores.cassandra.CassandraMapStoreFactory;
import com.kryptnostic.rhizome.pods.RegistryBasedMappersPod;
import com.kryptnostic.v2.storage.models.ObjectMetadata;
import com.kryptnostic.v2.storage.models.VersionedObjectKeySet;

@Configuration
@Profile( "cassandra" )
public class CassandraMapStoresPod extends BaseKryptnosticMapstoresPod {

    @Inject
    private Session                session;

    @Inject
    RegistryBasedMappersPod        mappers;

    @Inject
    private CassandraConfiguration cassandraConfig;

    @Override
    @PostConstruct
    public void init() {
        mapstoreFactory = new CassandraMapStoreFactory.Builder()
                .withSession( session )
                .withMappers( mappers )
                .withConfiguration( cassandraConfig )
                .build();
    }

    @Override
    public SelfRegisteringQueueStore<MetricsLogRecord> logRecordsQueueStore() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException(
                "THIS METHOD HAS NOT BEEN IMPLEMENTED, BLAME Drew Bailey drew@kryptnostic.com" );
    }

    @Bean
    @Override
    public TestableSelfRegisteringMapStore<ObjectMetadataLevelKey, VersionedObjectKeySet> levelFilteringMap() {
        return new ObjectMetadataLevelMapstore(
                HazelcastNames.Tables.METADATA_LEVELS,
                HazelcastNames.Maps.METADATA_LEVELS,
                cassandraConfig,
                session );
    }

    @Bean
    @Override
    public TestableSelfRegisteringMapStore<ObjectMetadataHierarchyKey, VersionedObjectKeySet> hierarchyFilteringMap() {
        return new ObjectMetadataHierarchyMapstore(
                HazelcastNames.Tables.METADATA_HIERARCHY,
                HazelcastNames.Maps.METADATA_HIERARCHY,
                cassandraConfig,
                session);
    }

    @Bean
    @Override
    public TestableSelfRegisteringMapStore<UUID, ObjectMetadata> objectMetadataMapStoreV2() {
        return new ObjectMetadataMapStore(
                session,
                HazelcastNames.Maps.NEW_OBJECT_METADATA,
                HazelcastNames.Tables.NEW_OBJECT_METADATA,
                cassandraConfig );
    }

    @Bean
    @Override
    public TestableSelfRegisteringMapStore<UserACLId, Set<Permission>> userLookupACLMapStore() {
        return new UserACLToPermissionsMapstore(
                session,
                HazelcastNames.Maps.USER_LOOKUP_ACL_MAP,
                HazelcastNames.Maps.USER_LOOKUP_ACL_MAP,
                cassandraConfig );
    }

    @Bean
    @Override
    public TestableSelfRegisteringMapStore<UserObjectTypePair, Set<UUID>> readableObjectsForUserAndTypeMapStore() {
        return new ReadableObjectsSetProxyMapStore(
                cassandraConfig,
                session );
    }

    @Bean
    @Override
    public TestableSelfRegisteringMapStore<ACLPermissionPair, Set<UUID>> indexedPermissionsMapstore() {
        return new PermissionsToUserACLMapstore(
                session,
                HazelcastNames.Maps.REVERSE_USER_LOOKUP_ACL_MAP,
                HazelcastNames.Tables.USER_LOOKUP_ACL_MAP,
                cassandraConfig );
    }

    public void setCassandraConfig( CassandraConfiguration cassConfig ) {
        this.cassandraConfig = cassConfig;
    }

    public void setSession( Session sess ) {
        this.session = sess;
    }

    public void setMappers( RegistryBasedMappersPod mapp ) {
        this.mappers = mapp;
    }

}
