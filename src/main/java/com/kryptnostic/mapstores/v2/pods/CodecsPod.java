package com.kryptnostic.mapstores.v2.pods;

import java.nio.ByteBuffer;
import java.util.UUID;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.exceptions.InvalidTypeException;
import com.datastax.driver.extras.codecs.enums.EnumOrdinalCodec;
import com.kryptnostic.directory.v1.domain.DomainPolicy;
import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;
import com.kryptnostic.rhizome.mappers.SelfRegisteringValueMapper;
import com.kryptnostic.rhizome.mapstores.MappingException;
import com.kryptnostic.rhizome.pods.RegistryBasedMappersPod;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

@Profile( "cassandra" )
@Configuration
public class CodecsPod {

    @Inject
    private RegistryBasedMappersPod mappers;

    @Bean
    public TypeCodec<Permission> permissionCodec() {
        return new EnumOrdinalCodec<>( Permission.class );
    }

    @Bean
    public TypeCodec<VersionedObjectKey> vokBlobCodec() {
        return new ValueMapperCodec<VersionedObjectKey>( mappers.getValueMapper( VersionedObjectKey.class ) );
    }

    @Bean
    public TypeCodec<UUID> uuidCodec() {
        return new KeyMapperCodec<UUID>( mappers.getKeyMapper( UUID.class ) );
    }
    
    @Bean
    public TypeCodec<DomainPolicy> domainPolicyCodec() {
        return new EnumOrdinalCodec<>( DomainPolicy.class);
    }

    public class KeyMapperCodec<V> extends TypeCodec<V> {

        private SelfRegisteringKeyMapper<V> keyMapper;

        public KeyMapperCodec( SelfRegisteringKeyMapper<V> keyMapper ) {
            super( DataType.varchar(), keyMapper.getClazz() );
            this.keyMapper = keyMapper;
        }

        @Override
        public ByteBuffer serialize( V value, ProtocolVersion protocolVersion ) throws InvalidTypeException {
            return varchar().serialize( keyMapper.fromKey( value ), protocolVersion );
        }

        @Override
        public V deserialize( ByteBuffer bytes, ProtocolVersion protocolVersion ) throws InvalidTypeException {
            String deserialize = varchar().deserialize( bytes, protocolVersion );
            return keyMapper.toKey( deserialize );
        }

        @Override
        public V parse( String value ) throws InvalidTypeException {
            return keyMapper.toKey( value );
        }

        @Override
        public String format( V value ) throws InvalidTypeException {
            return keyMapper.fromKey( value );
        }
    }

    public class ValueMapperCodec<V> extends TypeCodec<V> {

        private SelfRegisteringValueMapper<V> valueMapper;

        public ValueMapperCodec( SelfRegisteringValueMapper<V> valueMapper ) {
            super( DataType.blob(), valueMapper.getClazz() );
            this.valueMapper = valueMapper;
        }

        @Override
        public ByteBuffer serialize( V value, ProtocolVersion protocolVersion ) throws InvalidTypeException {
            try {
                return ByteBuffer.wrap( valueMapper.toBytes( value ) );
            } catch ( MappingException e ) {
                throw new InvalidTypeException(
                        "Failed mapping from class " + valueMapper.getClazz() + " to bytes",
                        e );
            }
        }

        @Override
        public V deserialize( ByteBuffer bytes, ProtocolVersion protocolVersion ) throws InvalidTypeException {
            try {
                return valueMapper.fromBytes( bytes.array() );
            } catch ( MappingException e ) {
                throw new InvalidTypeException( "Failed mapping from bytes to class " + valueMapper.getClazz(), e );
            }
        }

        @Override
        public V parse( String value ) throws InvalidTypeException {
            // TODO Auto-generated method stub
            throw new UnsupportedOperationException(
                    "THIS METHOD HAS NOT BEEN IMPLEMENTED, BLAME Drew Bailey drew@kryptnostic.com" );
        }

        @Override
        public String format( V value ) throws InvalidTypeException {
            return value.toString();
        }

    }

    public void setMappers( RegistryBasedMappersPod mappers ) {
        this.mappers = mappers;
    }

}
