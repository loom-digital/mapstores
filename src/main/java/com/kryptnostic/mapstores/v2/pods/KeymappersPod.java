package com.kryptnostic.mapstores.v2.pods;

import java.util.UUID;

import org.joda.time.LocalDate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.kryptnostic.directory.v1.domain.DomainPolicy;
import com.kryptnostic.directory.v1.model.ObjectUserKey;
import com.kryptnostic.heracles.authentication.v1.mappers.UUIDKeyMapper;
import com.kryptnostic.heracles.directory.v2.mappers.keys.DomainResourcesKeyKeyMapper;
import com.kryptnostic.heracles.directory.v2.objects.DomainResourcesKey;
import com.kryptnostic.mapstores.v2.mappers.DomainPolicyKeyMapper;
import com.kryptnostic.mapstores.v2.mappers.IndexSegmentAddressKeyMapper;
import com.kryptnostic.mapstores.v2.mappers.ObjectUserKeyKeyMapper;
import com.kryptnostic.mapstores.v2.mappers.UserSettingsKeyMapper;
import com.kryptnostic.mapstores.v2.mappers.VersionedObjectKeyKeyMapper;
import com.kryptnostic.mapstores.v2.mappers.VersionedObjectUserKeyKeyMapper;
import com.kryptnostic.mapstores.v2.mapstores.UserSettingsKey;
import com.kryptnostic.metrics.v1.MetricsDateKeyMapper;
import com.kryptnostic.rhizome.configuration.ConfigurationKey;
import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;
import com.kryptnostic.rhizome.mappers.keys.ConfigurationKeyMapper;
import com.kryptnostic.rhizome.mappers.keys.StringKeyMapper;
import com.kryptnostic.storage.v1.services.document.mappers.ObjectUserKeyMapper;
import com.kryptnostic.v2.search.IndexSegmentAddress;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;
import com.kryptnostic.v2.storage.models.VersionedObjectUserKey;

@Configuration
public class KeymappersPod {

    @Bean
    public SelfRegisteringKeyMapper<ConfigurationKey> ckm() {
        return new ConfigurationKeyMapper();
    }

    @Bean
    public SelfRegisteringKeyMapper<LocalDate> mdkm() {
        return new MetricsDateKeyMapper();
    }

    @Bean
    public SelfRegisteringKeyMapper<ObjectUserKey> oukm() {
        return new ObjectUserKeyMapper();
    }

    @Bean
    public SelfRegisteringKeyMapper<com.kryptnostic.v2.sharing.models.ObjectUserKey> oukm2() {
        return new ObjectUserKeyKeyMapper();
    }

    @Bean
    public SelfRegisteringKeyMapper<String> skm() {
        return new StringKeyMapper();
    }

    @Bean
    public SelfRegisteringKeyMapper<IndexSegmentAddress> isakm() {
        return new IndexSegmentAddressKeyMapper();
    }

    @Bean
    public SelfRegisteringKeyMapper<UUID> uuikm() {
        return new UUIDKeyMapper();
    }

    @Bean
    public SelfRegisteringKeyMapper<VersionedObjectKey> vokkm() {
        return new VersionedObjectKeyKeyMapper();
    }

    @Bean
    public SelfRegisteringKeyMapper<VersionedObjectUserKey> voukkm() {
        return new VersionedObjectUserKeyKeyMapper();
    }

    @Bean
    public SelfRegisteringKeyMapper<DomainResourcesKey> drkkm() {
        return new DomainResourcesKeyKeyMapper();
    }

    @Bean
    public SelfRegisteringKeyMapper<DomainPolicy> dpkm() {
        return new DomainPolicyKeyMapper();
    }
    
    @Bean
    public SelfRegisteringKeyMapper<UserSettingsKey> uskm() {
        return new UserSettingsKeyMapper();
    }
    
}
