package com.kryptnostic.mapstores.v2.pods;

import java.util.Set;
import java.util.UUID;

import org.joda.time.LocalDate;

import com.kryptnostic.directory.v1.domain.Domain;
import com.kryptnostic.directory.v1.domain.DomainPolicy;
import com.kryptnostic.directory.v2.model.DomainResourcesUsage;
import com.kryptnostic.heracles.directory.v1.objects.Emails;
import com.kryptnostic.heracles.directory.v2.objects.DomainResourcesKey;
import com.kryptnostic.kodex.v1.crypto.ciphers.BlockCiphertext;
import com.kryptnostic.kodex.v1.crypto.ciphers.Cypher;
import com.kryptnostic.kodex.v1.models.Group;
import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.mapstores.ACLPermissionPair;
import com.kryptnostic.mapstores.v2.mapstores.UserACLId;
import com.kryptnostic.mapstores.v2.mapstores.UserSettingsKey;
import com.kryptnostic.metrics.v1.MetricsLogRecord;
import com.kryptnostic.metrics.v1.MetricsUserStatsMetadata;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.mapstores.SelfRegisteringQueueStore;
import com.kryptnostic.rhizome.mapstores.TestableSelfRegisteringMapStore;
import com.kryptnostic.v2.crypto.CryptoMaterial;
import com.kryptnostic.v2.search.IndexSegmentAddress;
import com.kryptnostic.v2.sharing.models.Share;
import com.kryptnostic.v2.storage.models.Event;
import com.kryptnostic.v2.storage.models.Scope;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;
import com.kryptnostic.v2.storage.models.VersionedObjectUserKey;

public interface KryptnosticMapStoresPod {

    //
    // === ObjectStorageService Mapstores ===
    //

    TestableSelfRegisteringMapStore<UUID, Set<UUID>> objectLookupACLMapStore();

    TestableSelfRegisteringMapStore<VersionedObjectKey, Set<VersionedObjectKey>> forwardEdgeMapStore();

    TestableSelfRegisteringMapStore<VersionedObjectKey, VersionedObjectKey> backEdgeMapStore();

    @Deprecated
    TestableSelfRegisteringMapStore<VersionedObjectKey, Integer> objectClockMapStoreV1();

    TestableSelfRegisteringMapStore<VersionedObjectKey, UUID> objectClockMapStore();

    @Deprecated
    TestableSelfRegisteringMapStore<VersionedObjectKey, Integer> clockServerMapStore();

    // Plaintext Object Maps
    TestableSelfRegisteringMapStore<VersionedObjectKey, Event> eventsMapStore();

    // Composite maps used in EncryptedObjectDistributedMap<K>
    TestableSelfRegisteringMapStore<VersionedObjectKey, byte[]> encryptedObjectIvMapStore();

    TestableSelfRegisteringMapStore<VersionedObjectKey, byte[]> encryptedObjectTagMapStore();

    TestableSelfRegisteringMapStore<VersionedObjectKey, byte[]> encryptedObjectSaltMapStore();

    TestableSelfRegisteringMapStore<VersionedObjectKey, byte[]> encryptedObjectContentMapStore();

    TestableSelfRegisteringMapStore<UUID, Set<CryptoMaterial>> objectUploadProgressMapStore();

    TestableSelfRegisteringMapStore<UUID, Cypher> objectEncryptionTypeMapStore();

    //
    // === ObjectAuthorizationService Mapstores ===
    //
    TestableSelfRegisteringMapStore<ACLPermissionPair, Set<UUID>> indexedPermissionsMapstore();

    TestableSelfRegisteringMapStore<UserACLId, Set<Permission>> userLookupACLMapStore();

    //
    // === KeyStorageService Mapstores ===
    //
    TestableSelfRegisteringMapStore<VersionedObjectUserKey, byte[]> objectCryptoServicesMapStore();

    TestableSelfRegisteringMapStore<UUID, BlockCiphertext> encryptedUserSaltsMapStore();

    // RSA stores
    TestableSelfRegisteringMapStore<UUID, BlockCiphertext> rsaPrivateKeysMapStore();

    TestableSelfRegisteringMapStore<UUID, byte[]> rsaPublicKeysMapStore();

    // FHE stores
    TestableSelfRegisteringMapStore<UUID, byte[]> userFheHashFunctionMapStore();

    TestableSelfRegisteringMapStore<UUID, byte[]> userFHEPublicKeyMapStore();

    TestableSelfRegisteringMapStore<UUID, BlockCiphertext> userFHEPrivateKeyMapStore();

    TestableSelfRegisteringMapStore<UUID, BlockCiphertext> userFHESearchPrivateKeyMapStore();

    // Composite maps used in EncryptedObjectDistributedMap<K>
    TestableSelfRegisteringMapStore<VersionedObjectUserKey, byte[]> aesEncryptedObjectCryptoServicesIvMapStore();

    TestableSelfRegisteringMapStore<VersionedObjectUserKey, byte[]> aesEncryptedObjectCryptoServicesTagMapStore();

    TestableSelfRegisteringMapStore<VersionedObjectUserKey, byte[]> aesEncryptedObjectCryptoServicesSaltMapStore();

    TestableSelfRegisteringMapStore<VersionedObjectUserKey, byte[]> aesEncryptedObjectCryptoServicesContentMapStore();

    TestableSelfRegisteringMapStore<UUID, byte[]> masterKeysMapStore();

    //
    // === ObjectListingService Mapstores ===
    // see:
    // this#objectMetadataMapStore() }
    // this#ownedObjectsMapStore() }
    TestableSelfRegisteringMapStore<String, UUID> nameToTypeMap();

    TestableSelfRegisteringMapStore<UUID, String> typeToNameMap();

    //
    // === SearchService Mapstores ===
    //
    TestableSelfRegisteringMapStore<UUID, Integer> indexSegmentCountsMapStore();

    TestableSelfRegisteringMapStore<IndexSegmentAddress, Set<UUID>> indexSegmentIdsMapStore();

    TestableSelfRegisteringMapStore<IndexSegmentAddress, UUIDSet> indexSegmentIdsInlineMapStore();

    //
    // === SharingService Mapstores ===
    //
    TestableSelfRegisteringMapStore<VersionedObjectUserKey, Share> incomingSharesMapStore();

    TestableSelfRegisteringMapStore<UUID, Set<VersionedObjectKey>> incomingSharedObjectKeysMapStore();

    TestableSelfRegisteringMapStore<VersionedObjectUserKey, byte[]> sharingMetadataMapStore();

    //
    // === DirectoryService Mapstores ===
    //
    TestableSelfRegisteringMapStore<String, UUID> users();

    TestableSelfRegisteringMapStore<UUID, Emails> emails();

    TestableSelfRegisteringMapStore<UUID, String> userIdsToNames();

    TestableSelfRegisteringMapStore<UUID, String> authenticators();

    TestableSelfRegisteringMapStore<UUID, byte[]> certificates();

    TestableSelfRegisteringMapStore<UUID, Set<String>> roles();

    TestableSelfRegisteringMapStore<UserSettingsKey, Set<String>> userSettings();

    TestableSelfRegisteringMapStore<UUID, Set<UUID>> groupMemberships();

    TestableSelfRegisteringMapStore<UUID, Domain> domains();

    TestableSelfRegisteringMapStore<UUID, Set<UUID>> trustedDomains();

    TestableSelfRegisteringMapStore<UUID, Set<UUID>> incomingTrusts();

    TestableSelfRegisteringMapStore<DomainPolicy, Set<UUID>> domainsWithPolicy();

    TestableSelfRegisteringMapStore<UUID, String> domainIdToName();

    TestableSelfRegisteringMapStore<String, UUID> domainNameToId();

    TestableSelfRegisteringMapStore<UUID, Set<UUID>> domainMembers();

    TestableSelfRegisteringMapStore<UUID, Group> groups();

    TestableSelfRegisteringMapStore<UUID, String> groupNames();

    //
    // === Metrics mapstores ===
    //
    SelfRegisteringQueueStore<MetricsLogRecord> logRecordsQueueStore();

    TestableSelfRegisteringMapStore<String, MetricsUserStatsMetadata> dateuserDataMapStore();

    TestableSelfRegisteringMapStore<LocalDate, String> dateUsersMapStore();

    TestableSelfRegisteringMapStore<LocalDate, Integer> dateCountMapStore();

    //
    // === Types mastores ===
    //
    TestableSelfRegisteringMapStore<String, Scope> scopeMapStore();

    TestableSelfRegisteringMapStore<UUID, String> typesToScopesMapStore();

    //
    // === domain resources ===
    //
    TestableSelfRegisteringMapStore<DomainResourcesKey, DomainResourcesUsage> domainResourcesMap();

}