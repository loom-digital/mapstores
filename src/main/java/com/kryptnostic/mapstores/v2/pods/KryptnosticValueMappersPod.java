package com.kryptnostic.mapstores.v2.pods;

import java.util.UUID;

import com.kryptnostic.directory.v1.domain.Domain;
import com.kryptnostic.directory.v2.model.DomainResourcesUsage;
import com.kryptnostic.heracles.directory.v1.objects.Emails;
import com.kryptnostic.kodex.v1.crypto.ciphers.BlockCiphertext;
import com.kryptnostic.kodex.v1.crypto.ciphers.Cypher;
import com.kryptnostic.kodex.v1.models.Group;
import com.kryptnostic.metrics.v1.MetricsUserStatsMetadata;
import com.kryptnostic.rhizome.hazelcast.objects.OrderedUUIDSet;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.mappers.SelfRegisteringValueMapper;
import com.kryptnostic.v2.crypto.CryptoMaterial;
import com.kryptnostic.v2.sharing.models.Share;
import com.kryptnostic.v2.storage.models.Event;
import com.kryptnostic.v2.storage.models.ObjectMetadata;
import com.kryptnostic.v2.storage.models.Scope;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

public interface KryptnosticValueMappersPod {

    SelfRegisteringValueMapper<byte[]> srbm();

    SelfRegisteringValueMapper<VersionedObjectKey> vokvm();

    SelfRegisteringValueMapper<OrderedUUIDSet> ousvm();

    SelfRegisteringValueMapper<BlockCiphertext> bcvm();

    SelfRegisteringValueMapper<Group> gvm();

    SelfRegisteringValueMapper<Domain> dvm();

    SelfRegisteringValueMapper<String> svm();

    SelfRegisteringValueMapper<UUIDSet> usvm();

    SelfRegisteringValueMapper<ObjectMetadata> omvm();

    SelfRegisteringValueMapper<UUID> uuidvm();

    SelfRegisteringValueMapper<Emails> evm();

    SelfRegisteringValueMapper<Integer> ivm();

    SelfRegisteringValueMapper<MetricsUserStatsMetadata> musmvm();

    SelfRegisteringValueMapper<Share> shvm();

    SelfRegisteringValueMapper<Scope> scvm();

    SelfRegisteringValueMapper<DomainResourcesUsage> druvm();

    SelfRegisteringValueMapper<Cypher> cyvm();

    SelfRegisteringValueMapper<CryptoMaterial> cmvm();

    SelfRegisteringValueMapper<Event> evvm();

}
