package com.kryptnostic.mapstores.v2.pods;

import java.io.IOException;
import java.util.UUID;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.google.common.annotations.VisibleForTesting;
import com.hazelcast.config.SerializationConfig;
import com.hazelcast.internal.serialization.InternalSerializationService;
import com.hazelcast.internal.serialization.impl.ConstantSerializers;
import com.hazelcast.internal.serialization.impl.ConstantSerializers.IntegerSerializer;
import com.hazelcast.internal.serialization.impl.DefaultSerializationServiceBuilder;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.directory.v1.domain.Domain;
import com.kryptnostic.directory.v2.model.DomainResourcesUsage;
import com.kryptnostic.heracles.directory.v1.objects.Emails;
import com.kryptnostic.heracles.directory.v2.serializers.DomainResourcesUsageStreamSerializer;
import com.kryptnostic.heracles.v1.serializers.DomainStreamSerializer;
import com.kryptnostic.kodex.v1.crypto.ciphers.BlockCiphertext;
import com.kryptnostic.kodex.v1.crypto.ciphers.Cypher;
import com.kryptnostic.kodex.v1.models.Group;
import com.kryptnostic.mapstores.pods.BaseSerializersPod;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v1.serializers.GroupStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.BlockCiphertextStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.CryptomaterialStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.CypherStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.EmailsStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.EventStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.ObjectMetadataStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.OrderedUUIDSetStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.ShareStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.UUIDSetStreamSerializer;
import com.kryptnostic.mapstores.v2.serializers.VersionedObjectKeyStreamSerializer;
import com.kryptnostic.metrics.v1.MetricsUserStatsMetadata;
import com.kryptnostic.rhizome.hazelcast.objects.OrderedUUIDSet;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.mappers.SelfRegisteringValueMapper;
import com.kryptnostic.rhizome.mappers.StreamSerializerBasedValueMapper;
import com.kryptnostic.rhizome.mappers.values.ByteArrayValueMapper;
import com.kryptnostic.rhizome.mappers.values.StringValueMapper;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;
import com.kryptnostic.types.serializers.ScopeStreamSerializer;
import com.kryptnostic.v2.crypto.CryptoMaterial;
import com.kryptnostic.v2.sharing.models.Share;
import com.kryptnostic.v2.storage.models.Event;
import com.kryptnostic.v2.storage.models.ObjectMetadata;
import com.kryptnostic.v2.storage.models.Scope;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

@Configuration
@Import( { BaseSerializersPod.class } )
public class ValueMappersPod implements KryptnosticValueMappersPod {

    @Inject
    BaseSerializersPod          serializers;

    @Inject
    private SerializationConfig serializationConfig;

    @Bean
    public InternalSerializationService serializationService() {
        return new DefaultSerializationServiceBuilder().setConfig( serializationConfig ).build();
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<byte[]> srbm() {
        return new ByteArrayValueMapper();
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<VersionedObjectKey> vokvm() {
        return new StreamSerializerBasedValueMapper<>(
                new VersionedObjectKeyStreamSerializer(),
                serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<UUID> uuidvm() {
        return new StreamSerializerBasedValueMapper<>( new UUIDStreamSerializer(), serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<OrderedUUIDSet> ousvm() {
        return new StreamSerializerBasedValueMapper<>( new OrderedUUIDSetStreamSerializer(), serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<BlockCiphertext> bcvm() {
        return new StreamSerializerBasedValueMapper<>( new BlockCiphertextStreamSerializer(), serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<Group> gvm() {
        return new StreamSerializerBasedValueMapper<>( new GroupStreamSerializer(), serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<Domain> dvm() {
        return new StreamSerializerBasedValueMapper<>( new DomainStreamSerializer(), serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<String> svm() {
        return new StringValueMapper();
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<UUIDSet> usvm() {
        return new StreamSerializerBasedValueMapper<>( new UUIDSetStreamSerializer(), serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<ObjectMetadata> omvm() {
        return new StreamSerializerBasedValueMapper<>( new ObjectMetadataStreamSerializer(), serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<Emails> evm() {
        return new StreamSerializerBasedValueMapper<>( new EmailsStreamSerializer(), serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<MetricsUserStatsMetadata> musmvm() {
        return new StreamSerializerBasedValueMapper<>( new SelfRegisteringStreamSerializer<MetricsUserStatsMetadata>() {

            @Override
            public void write( ObjectDataOutput out, MetricsUserStatsMetadata object ) throws IOException {
                out.writeUTF( object.getDomain() );
            }

            @Override
            public MetricsUserStatsMetadata read( ObjectDataInput in ) throws IOException {
                return new MetricsUserStatsMetadata( in.readUTF() );
            }

            @Override
            public int getTypeId() {
                return HazelcastSerializerTypeIds.USERSTATS_METADATA.ordinal();
            }

            @Override
            public void destroy() {}

            @Override
            public Class<MetricsUserStatsMetadata> getClazz() {
                return MetricsUserStatsMetadata.class;
            }

        }, serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<Integer> ivm() {
        IntegerSerializer serializer = new ConstantSerializers.IntegerSerializer();
        return new StreamSerializerBasedValueMapper<>( new SelfRegisteringStreamSerializer<Integer>() {

            @Override
            public void write( ObjectDataOutput out, Integer object ) throws IOException {
                serializer.write( out, object );
            }

            @Override
            public Integer read( ObjectDataInput in ) throws IOException {
                return serializer.read( in );
            }

            @Override
            public int getTypeId() {
                return serializer.getTypeId();
            }

            @Override
            public void destroy() {}

            @Override
            public Class<Integer> getClazz() {
                return Integer.class;
            }

        }, serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<Share> shvm() {
        return new StreamSerializerBasedValueMapper<>( new ShareStreamSerializer(), serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<DomainResourcesUsage> druvm() {
        return new StreamSerializerBasedValueMapper<>(new DomainResourcesUsageStreamSerializer(), serializationService());
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<Scope> scvm() {
        return new StreamSerializerBasedValueMapper<>( new ScopeStreamSerializer(), serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<Cypher> cyvm() {
        return new StreamSerializerBasedValueMapper<>( new CypherStreamSerializer(), serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<CryptoMaterial> cmvm() {
        return new StreamSerializerBasedValueMapper<>( new CryptomaterialStreamSerializer(), serializationService() );
    }

    @Bean
    @Override
    public SelfRegisteringValueMapper<Event> evvm() {
        return new StreamSerializerBasedValueMapper<>( new EventStreamSerializer(), serializationService() );
    }

    @VisibleForTesting
    public void setSerializationConfig( SerializationConfig serializationConfig ) {
        this.serializationConfig = serializationConfig;
    }

}
