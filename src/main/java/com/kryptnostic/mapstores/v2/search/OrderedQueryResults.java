package com.kryptnostic.mapstores.v2.search;

import java.util.Set;
import java.util.UUID;

import com.kryptnostic.v2.search.SearchResult;

/**
 * API for paging through results.
 * 
 * @author Matthew Tamayo-Rios &lt;matthew@kryptnostic.com&gt;
 * 
 */
public interface OrderedQueryResults {

    void updateQueryResults( UUID objectId, double score );

    Set<SearchResult> initialResults( int count );

    Set<SearchResult> getNextBatch( UUID metadataObjectId, double score, int count );

}
