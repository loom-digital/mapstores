package com.kryptnostic.mapstores.v2.search;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.kryptnostic.rethinkdb.RethinkDbConnectionPool;
import com.kryptnostic.v2.search.SearchResult;
import com.rethinkdb.RethinkDB;
import com.rethinkdb.gen.ast.Table;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.Cursor;

// TODO: appears to be unused --consider removing
public class QueryResultRethinkDbProxy implements OrderedQueryResults {
    // TODO: implement ttl
    private static final int              TTL_IN_SECONDS = 5 * 60;
    private static final Logger           logger         = LoggerFactory.getLogger( QueryResultRethinkDbProxy.class );

    private final RethinkDbConnectionPool pool;

    private final String                  db;
    private final String                  table;
    private final UUID                    queryId;

    public QueryResultRethinkDbProxy(
            RethinkDbConnectionPool pool,
            String db,
            String table,
            UUID queryId ) throws TimeoutException {
        // use cluster.newSession() here to avoid having to connect to cassandra on object creation
        this.pool = pool;
        this.db = db;
        this.table = table;
        this.queryId = queryId;
        Connection conn = pool.acquire();

        if ( !(boolean) RethinkDB.r.dbList().contains( db ).run( conn ) ) {
            RethinkDB.r.dbCreate( db ).run( conn );
        }

        if ( !(boolean) RethinkDB.r.db( db ).tableList().contains( table ).run( conn ) ) {
            RethinkDB.r.db( db ).tableCreate( table ).run( conn );
        }

        if ( !(boolean) start().indexList().contains( "scoredQuery" ).run( conn ) ) {
            start().indexCreate( "scoredQuery", row -> RethinkDB.r.array( row.g( "id" ),
                    row.g( "score" ),
                    row.g( "metadataObjectId" ) ) )
                    .run( conn );
            start().indexWait( "scoredQuery" ).run( conn );
        }
    }

    @Override
    public void updateQueryResults( UUID objectId, double score ) {
        Connection conn = null;
        try {
            conn = pool.acquire();
            start().insert( RethinkDB.r.hashMap( "id", queryId.toString() + objectId.toString() )
                    .with( "metadataObjectId", objectId.toString() ) ).run( conn );

            start().get( queryId.toString() + objectId.toString() )
                    .update( result -> RethinkDB.r.hashMap( "score",
                            result.g( "score" ).add( score ).default_( score ) ) )
                    .run( conn );
        } catch ( TimeoutException e ) {
            logger.error( "Timed out while executing rethinkdb query." );
        } finally {
            if ( conn != null ) {
                pool.release( conn );
            }
        }
    }

    // TODO: Update query results need to take a term as an argument so that we know what search token the results is
    // for.
    @Override
    public Set<SearchResult> getNextBatch( UUID metadataObjectId, double score, int count ) {
        Connection conn = null;
        try {
            conn = pool.acquire();
            Cursor<Map<String, Object>> cursor = start().between( RethinkDB.r.array(
                    score, metadataObjectId.toString() ), RethinkDB.r.maxval() ).distinct()
                    .limit( count ).run( conn );
            List<Map<String, Object>> rawResults = cursor.toList();
            SortedSet<SearchResult> results = Sets
                    .newTreeSet( Iterables.transform( rawResults,
                            m -> new SearchResult(
                                    "",
                                    UUID.fromString( (String) m.get( "metadataObjectId" ) ),
                                    (double) m.get( "score" ) ) ) );
            return results;

        } catch ( TimeoutException e ) {
            logger.error( "Timed out while executing rethinkdb query." );
            return null;
        } finally {
            if ( conn != null ) {
                pool.release( conn );
            }
        }

    }

    private Table start() {
        return RethinkDB.r.db( db ).table( table );
    }

    @Override
    public Set<SearchResult> initialResults( int count ) {
        Connection conn = null;
        try {
            conn = pool.acquire();
            Cursor<Map<String, Object>> cursor = start().between( RethinkDB.r.minval(), RethinkDB.r.maxval() )
                    .distinct()
                    .limit( count ).run( conn );
            List<Map<String, Object>> rawResults = cursor.toList();
            SortedSet<SearchResult> results = Sets
                    .newTreeSet( Iterables.transform( rawResults,
                            m -> new SearchResult(
                                    "",
                                    UUID.fromString( (String) m.get( "metadataObjectId" ) ),
                                    (double) m.get( "score" ) ) ) );
            return results;

        } catch ( TimeoutException e ) {
            logger.error( "Timed out while executing rethinkdb query." );
            return null;
        } finally {
            if ( conn != null ) {
                pool.release( conn );
            }
        }

    }
}
