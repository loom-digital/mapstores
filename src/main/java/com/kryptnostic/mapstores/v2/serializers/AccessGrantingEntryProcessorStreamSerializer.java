package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.AccessGrantingEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class AccessGrantingEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<AccessGrantingEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, AccessGrantingEntryProcessor object ) throws IOException {
        SetStreamSerializers.fastUUIDSetSerialize( out, object.getBackingCollection() );
    }

    @Override
    public AccessGrantingEntryProcessor read( ObjectDataInput in ) throws IOException {
        UUIDSet objects = SetStreamSerializers.fastUUIDSetDeserialize( in );
        return new AccessGrantingEntryProcessor( objects );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.ACCESS_GRANTING_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<AccessGrantingEntryProcessor> getClazz() {
        return AccessGrantingEntryProcessor.class;
    }

}
