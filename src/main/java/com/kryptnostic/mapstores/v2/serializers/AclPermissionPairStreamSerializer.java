package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.mapstores.ACLPermissionPair;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class AclPermissionPairStreamSerializer implements SelfRegisteringStreamSerializer<ACLPermissionPair> {

    @Override
    public void write( ObjectDataOutput out, ACLPermissionPair object ) throws IOException {
        out.writeInt( object.getPermission().ordinal() );
        UUIDStreamSerializer.serialize( out, object.getAclId() );
    }

    @Override
    public ACLPermissionPair read( ObjectDataInput in ) throws IOException {
        Permission permission = Permission.getValues()[ in.readInt() ];
        UUID aclId = UUIDStreamSerializer.deserialize( in );
        return new ACLPermissionPair( aclId, permission );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.ACL_PERMISSION_PAIR_SS.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<ACLPermissionPair> getClazz() {
        return ACLPermissionPair.class;
    }

}
