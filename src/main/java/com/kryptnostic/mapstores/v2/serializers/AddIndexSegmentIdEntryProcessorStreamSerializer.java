package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.AddIndexSegmentIdEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class AddIndexSegmentIdEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<AddIndexSegmentIdEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, AddIndexSegmentIdEntryProcessor object ) throws IOException {
        SetStreamSerializers.serialize( out, object.getBackingCollection(), ( UUID key ) -> {
            out.writeLong( key.getLeastSignificantBits() );
            out.writeLong( key.getMostSignificantBits() );
        } );
    }

    @Override
    public AddIndexSegmentIdEntryProcessor read( ObjectDataInput in ) throws IOException {
        Iterable<UUID> keys = SetStreamSerializers.deserialize( in, ( ObjectDataInput input ) -> {
            long lsb = input.readLong();
            long msb = input.readLong();
            return new UUID( msb, lsb );
        } );
        return new AddIndexSegmentIdEntryProcessor( keys );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.ADD_INDEX_SEGMENT_ID_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<AddIndexSegmentIdEntryProcessor> getClazz() {
        return AddIndexSegmentIdEntryProcessor.class;
    }

}
