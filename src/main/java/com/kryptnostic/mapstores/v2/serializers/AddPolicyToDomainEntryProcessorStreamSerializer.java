package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.AddPolicyToDomainEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class AddPolicyToDomainEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<AddPolicyToDomainEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, AddPolicyToDomainEntryProcessor object ) throws IOException {
        SetStreamSerializers.fastUUIDSetSerialize( out, object.getBackingCollection() );
    }

    @Override
    public AddPolicyToDomainEntryProcessor read( ObjectDataInput in ) throws IOException {
        UUIDSet collection = SetStreamSerializers.fastUUIDSetDeserialize( in );
        return new AddPolicyToDomainEntryProcessor( collection );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.ADD_POLICY_TO_DOMAIN_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<AddPolicyToDomainEntryProcessor> getClazz() {
        return AddPolicyToDomainEntryProcessor.class;
    }

}
