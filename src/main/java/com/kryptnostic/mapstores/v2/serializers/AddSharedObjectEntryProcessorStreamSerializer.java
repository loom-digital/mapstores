package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.Arrays;

import com.google.common.collect.Iterables;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.AddSharedObjectEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

public class AddSharedObjectEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<AddSharedObjectEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, AddSharedObjectEntryProcessor object ) throws IOException {
        Iterable<VersionedObjectKey> keys = object.getBackingCollection();
        out.writeInt( Iterables.size( keys ) );
        for ( VersionedObjectKey k : keys ) {
            VersionedObjectKeyStreamSerializer.serialize( out, k );
        }
    }

    @Override
    public AddSharedObjectEntryProcessor read( ObjectDataInput in ) throws IOException {
        int size = in.readInt();
        VersionedObjectKey[] keys = new VersionedObjectKey[ size ];
        for ( int i = 0; i < keys.length; ++i ) {
            keys[ i ] = VersionedObjectKeyStreamSerializer.deserialize( in );
        }
        return new AddSharedObjectEntryProcessor( Arrays.asList( keys ) );

    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.SHARE_OBJECT_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<AddSharedObjectEntryProcessor> getClazz() {
        return AddSharedObjectEntryProcessor.class;
    }

}
