package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.AddToLevelFilteringMapEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.v2.storage.models.VersionedObjectKeySet;

public class AddToLevelFilteringMapEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<AddToLevelFilteringMapEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, AddToLevelFilteringMapEntryProcessor object ) throws IOException {
        VersionedObjectKeySetStreamSerializer.fastVersionedObjectKeySetSerialize( out, object.getKeysToAdd() );
    }

    @Override
    public AddToLevelFilteringMapEntryProcessor read( ObjectDataInput in ) throws IOException {
        VersionedObjectKeySet keys = VersionedObjectKeySetStreamSerializer.fastVersionedObjectKeySetDeserialize( in );
        return new AddToLevelFilteringMapEntryProcessor( keys );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.ADD_TO_LEVEL_MAP_EP.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<AddToLevelFilteringMapEntryProcessor> getClazz() {
        return AddToLevelFilteringMapEntryProcessor.class;
    }

}
