package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.Set;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.AddToUserSettingsEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class AddToUserSettingsEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<AddToUserSettingsEntryProcessor>{

    @Override
    public void write( ObjectDataOutput out, AddToUserSettingsEntryProcessor object ) throws IOException {
        SetStreamSerializers.fastStringSetSerialize( out, object.getBackingCollection() );
        
    }

    @Override
    public AddToUserSettingsEntryProcessor read( ObjectDataInput in ) throws IOException {
        Set<String> collection = SetStreamSerializers.fastStringSetDeserialize( in );
        return new AddToUserSettingsEntryProcessor( collection );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.ADD_TO_USER_SETTINGS_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<AddToUserSettingsEntryProcessor> getClazz() {
        return AddToUserSettingsEntryProcessor.class;
    }

}
