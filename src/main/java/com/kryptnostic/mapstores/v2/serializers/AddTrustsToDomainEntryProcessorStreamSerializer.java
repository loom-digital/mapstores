package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.AddTrustsToDomainEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class AddTrustsToDomainEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<AddTrustsToDomainEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, AddTrustsToDomainEntryProcessor object ) throws IOException {
        SetStreamSerializers.fastUUIDSetSerialize( out, object.getBackingCollection() );

    }

    @Override
    public AddTrustsToDomainEntryProcessor read( ObjectDataInput in ) throws IOException {
        UUIDSet collection = SetStreamSerializers.fastUUIDSetDeserialize( in );
        return new AddTrustsToDomainEntryProcessor( collection );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.ADD_TRUSTS_TO_DOMAIN_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<AddTrustsToDomainEntryProcessor> getClazz() {
        return AddTrustsToDomainEntryProcessor.class;
    }

}
