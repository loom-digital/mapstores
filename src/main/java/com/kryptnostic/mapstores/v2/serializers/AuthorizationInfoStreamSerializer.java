package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.AuthorizationInfo;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class AuthorizationInfoStreamSerializer implements SelfRegisteringStreamSerializer<AuthorizationInfo> {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.AUTHORIZATION_INFO.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public void write( ObjectDataOutput out, AuthorizationInfo object ) throws IOException {
        serialize( out, object );
    }

    @Override
    public AuthorizationInfo read( ObjectDataInput in ) throws IOException {
        return deserialize( in );
    }

    public static AuthorizationInfo deserialize( ObjectDataInput in ) throws IOException {
        AuthorizationInfo authzInfo = new AuthorizationInfo();
        int count = in.readInt();
        for ( int i = 0; i < count; ++i ) {
            UUID key = UUIDStreamSerializer.deserialize( in );
            authzInfo.put( key, Boolean.valueOf( in.readBoolean() ) );
        }
        return authzInfo;
    }

    public static void serialize( ObjectDataOutput out, AuthorizationInfo object ) throws IOException {
        out.writeInt( object.size() );
        for ( Map.Entry<UUID, Boolean> entry : object.entrySet() ) {
            UUIDStreamSerializer.serialize( out, entry.getKey() );
            out.writeBoolean( entry.getValue().booleanValue() );
        }
    }

    @Override
    public Class<AuthorizationInfo> getClazz() {
        return AuthorizationInfo.class;
    }

}
