package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.google.common.base.Optional;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.kodex.v1.crypto.ciphers.BlockCiphertext;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class BlockCiphertextStreamSerializer implements SelfRegisteringStreamSerializer<BlockCiphertext> {

    @Override
    public void write( ObjectDataOutput out, BlockCiphertext object ) throws IOException {
        serialize( out, object );
    }

    @Override
    public BlockCiphertext read( ObjectDataInput in ) throws IOException {
        return deserialize( in );
    }

    public static void serialize( ObjectDataOutput out, BlockCiphertext object ) throws IOException {
        out.writeByteArray( object.getIv() );
        out.writeByteArray( object.getSalt() );
        out.writeByteArray( object.getContents() );
        out.writeByteArray( object.getEncryptedLength().or( new byte[0] ) );
        out.writeByteArray( object.getTag().or( new byte[0] ) );
    }

    public static BlockCiphertext deserialize( ObjectDataInput in ) throws IOException {
        byte[] iv = in.readByteArray();
        byte[] salt = in.readByteArray();
        byte[] contents = in.readByteArray();
        byte[] encLength = in.readByteArray();
        byte[] tag = in.readByteArray();
        Optional<byte[]> tagOpt = optionalByteArray( tag );
        Optional<byte[]> encLengthOpt = optionalByteArray( encLength );
        return new BlockCiphertext( iv, salt, contents, encLengthOpt, tagOpt);
    }

    public static Optional<byte[]> optionalByteArray( byte[] bytes ) {
        Optional<byte[]> bytesOpt = Optional.of( bytes );
        if ( bytes.length == 0 ) {
            bytesOpt = Optional.absent();
        }
        return bytesOpt;
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.BLOCK_CIPHERTEXT.ordinal();
    }

    @Override
    public void destroy() {
    }

    @Override
    public Class<BlockCiphertext> getClazz() {
        return BlockCiphertext.class;
    }

}
