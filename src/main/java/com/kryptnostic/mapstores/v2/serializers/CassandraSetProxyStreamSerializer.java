package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.concurrent.ConcurrentMap;

import javax.inject.Inject;

import com.datastax.driver.core.Session;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.mappers.SelfRegisteringValueMapper;
import com.kryptnostic.rhizome.mapstores.cassandra.DefaultCassandraSetProxy;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class CassandraSetProxyStreamSerializer implements SelfRegisteringStreamSerializer<DefaultCassandraSetProxy> {

    private final Session                                                session;
    private final ConcurrentMap<Class<?>, SelfRegisteringValueMapper<?>> map;

    @Inject
    public CassandraSetProxyStreamSerializer( Session session, ConcurrentMap<Class<?>, SelfRegisteringValueMapper<?>> map ) {
        this.session = session;
        this.map = map;
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.CASSANDRA_SET_PROXY.ordinal();
    }

    @Override
    public void write( ObjectDataOutput out, DefaultCassandraSetProxy object ) throws IOException {
        out.writeUTF( object.getKeyspace() );
        out.writeUTF( object.getTable() );
        out.writeUTF( object.getSetId() );
        out.writeUTF( object.getTypeClazz().getName() );
    }

    @Override
    public DefaultCassandraSetProxy read( ObjectDataInput in ) throws IOException {
        String keysp = in.readUTF();
        String table = in.readUTF();
        String mappedSetId = in.readUTF();
        Class<?> forName;
        try {
            forName = Class.forName( in.readUTF() );
        } catch ( ClassNotFoundException e ) {
            throw new IOException( e );
        }
        // SelfRegisteringValueMapper vmMapper = map.readValue( in.readByteArray(), SelfRegisteringValueMapper.class );
        return new DefaultCassandraSetProxy( session, keysp, table, mappedSetId, forName, map.get( forName ) );
    }

    @Override
    public void destroy() { /* No-Op */}

    @Override
    public Class<DefaultCassandraSetProxy> getClazz() {
        return DefaultCassandraSetProxy.class;
    }

}

