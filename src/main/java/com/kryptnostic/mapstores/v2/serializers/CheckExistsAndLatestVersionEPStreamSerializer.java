package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.CheckMetadataExistsAndIsLatestVersionEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class CheckExistsAndLatestVersionEPStreamSerializer
        implements SelfRegisteringStreamSerializer<CheckMetadataExistsAndIsLatestVersionEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, CheckMetadataExistsAndIsLatestVersionEntryProcessor object )
            throws IOException {
        out.writeLong( object.getVersion() );
    }

    @Override
    public CheckMetadataExistsAndIsLatestVersionEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new CheckMetadataExistsAndIsLatestVersionEntryProcessor( in.readLong() );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.CHECK_OM_EXISTS_EP.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<CheckMetadataExistsAndIsLatestVersionEntryProcessor> getClazz() {
        return CheckMetadataExistsAndIsLatestVersionEntryProcessor.class;
    }

}
