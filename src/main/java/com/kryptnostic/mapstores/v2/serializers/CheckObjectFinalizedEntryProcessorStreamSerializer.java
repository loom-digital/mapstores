package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.kodex.v1.crypto.ciphers.Cypher;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.CheckObjectFinalizedEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class CheckObjectFinalizedEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<CheckObjectFinalizedEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, CheckObjectFinalizedEntryProcessor object ) throws IOException {
        out.writeInt( object.getCypher().ordinal() );
    }

    @Override
    public CheckObjectFinalizedEntryProcessor read( ObjectDataInput in ) throws IOException {
        Cypher requiredCryptoMats = Cypher.getValues()[ in.readInt() ];
        return new CheckObjectFinalizedEntryProcessor( requiredCryptoMats );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.CHECK_OBJECT_FINALIZED_EP.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<CheckObjectFinalizedEntryProcessor> getClazz() {
        return CheckObjectFinalizedEntryProcessor.class;
    }

}
