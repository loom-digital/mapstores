package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.entryprocessors.CheckPermissionOnObjectEP;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class CheckPermissionOnObjectEPStreamSerializer
        implements SelfRegisteringStreamSerializer<CheckPermissionOnObjectEP> {

    @Override
    public void write( ObjectDataOutput out, CheckPermissionOnObjectEP object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getAclIdForObject() );
        out.writeInt( object.getPermission().ordinal() );
    }

    @Override
    public CheckPermissionOnObjectEP read( ObjectDataInput in ) throws IOException {
        UUID aclId = UUIDStreamSerializer.deserialize( in );
        int ordinal = in.readInt();
        return new CheckPermissionOnObjectEP( Permission.getValues()[ ordinal ], aclId );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.CHECK_PERM_ON_OBJECT_EP.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<CheckPermissionOnObjectEP> getClazz() {
        return CheckPermissionOnObjectEP.class;
    }

}
