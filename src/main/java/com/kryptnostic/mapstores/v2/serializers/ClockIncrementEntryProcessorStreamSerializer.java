package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.ClockIncrementEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class ClockIncrementEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<ClockIncrementEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, ClockIncrementEntryProcessor object ) throws IOException {}

    @Override
    public ClockIncrementEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new ClockIncrementEntryProcessor();
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.CLOCK_INCREMENT_EP.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<ClockIncrementEntryProcessor> getClazz() {
        return ClockIncrementEntryProcessor.class;
    }

}
