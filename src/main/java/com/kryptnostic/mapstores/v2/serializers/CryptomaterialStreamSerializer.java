package com.kryptnostic.mapstores.v2.serializers;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.v2.crypto.CryptoMaterial;

public class CryptomaterialStreamSerializer implements SelfRegisteringStreamSerializer<CryptoMaterial> {

    @Override
    public void write( ObjectDataOutput out, CryptoMaterial object ) throws IOException {
        serialize( out, object );
    }

    public static void serialize( DataOutput out, CryptoMaterial materialType ) throws IOException {
        switch ( materialType ) {
            case CONTENTS:
                out.writeBoolean( true );
                out.writeBoolean( true );
                break;
            case IV:
                out.writeBoolean( true );
                out.writeBoolean( false );
                break;
            case SALT:
                out.writeBoolean( false );
                out.writeBoolean( true );
                break;
            case TAG:
                out.writeBoolean( false );
                out.writeBoolean( false );
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static CryptoMaterial deserialize( DataInput in ) throws IOException {
        boolean firstBit = in.readBoolean();
        boolean secondBit = in.readBoolean();
        if ( firstBit ) {
            if ( secondBit ) {
                // true, true
                return CryptoMaterial.CONTENTS;
            }
            // true, false
            return CryptoMaterial.IV;
        }
        if ( secondBit ) {
            // false, true
            return CryptoMaterial.SALT;
        }
        // false, false
        return CryptoMaterial.TAG;
    }

    @Override
    public CryptoMaterial read( ObjectDataInput in ) throws IOException {
        return deserialize( in );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.CRYPTO_MATERIAL.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<CryptoMaterial> getClazz() {
        return CryptoMaterial.class;
    }

}
