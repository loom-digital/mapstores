package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.kodex.v1.crypto.ciphers.Cypher;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class CypherStreamSerializer implements SelfRegisteringStreamSerializer<Cypher> {

    @Override
    public void write( ObjectDataOutput out, Cypher object ) throws IOException {
        out.writeUTF( object.name() );
    }

    @Override
    public Cypher read( ObjectDataInput in ) throws IOException {
        String name = in.readUTF();
        return Cypher.valueOf( name );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.CYPHER.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<Cypher> getClazz() {
        return Cypher.class;
    }

}
