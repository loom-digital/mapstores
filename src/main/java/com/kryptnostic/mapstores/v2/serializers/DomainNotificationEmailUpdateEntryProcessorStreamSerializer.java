package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.DomainNotificationEmailUpdateEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class DomainNotificationEmailUpdateEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<DomainNotificationEmailUpdateEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, DomainNotificationEmailUpdateEntryProcessor object ) throws IOException {
        out.writeUTF( object.getEmail() );
    }

    @Override
    public DomainNotificationEmailUpdateEntryProcessor read( ObjectDataInput in ) throws IOException {
        String email = in.readUTF();
        return new DomainNotificationEmailUpdateEntryProcessor( email );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.DOMAIN_NOTIFICATION_EMAIL_UPDATE_EP.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<DomainNotificationEmailUpdateEntryProcessor> getClazz() {
        return DomainNotificationEmailUpdateEntryProcessor.class;
    }

}
