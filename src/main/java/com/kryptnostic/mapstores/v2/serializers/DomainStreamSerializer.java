package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.directory.v1.domain.Domain;
import com.kryptnostic.directory.v1.domain.Domain.DomainBuilder;
import com.kryptnostic.directory.v1.domain.DomainSharingPolicy;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class DomainStreamSerializer implements SelfRegisteringStreamSerializer<Domain> {

    @Override
    public void write( ObjectDataOutput out, Domain object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getId() );
        out.writeUTF( object.getName() );
        out.writeBoolean( object.isConfirmationEmailRequired() );
        out.writeBoolean( object.isOpenRegistrationEnabled() );
        out.writeBoolean( object.isPubliclyListable() );
        out.writeUTF( object.getDomainSharingPolicy().toString() );
        out.writeInt( object.getSize() );
    }

    @Override
    public Domain read( ObjectDataInput in ) throws IOException {
        UUID id = UUIDStreamSerializer.deserialize( in );
        String name = in.readUTF();
        boolean confirmReq = in.readBoolean();
        boolean openReg = in.readBoolean();
        boolean pubList = in.readBoolean();
        String sharing = in.readUTF();
        int size = in.readInt();
        DomainBuilder builder = new DomainBuilder( id, name );
        return builder.emailConfirmationRequiredIs( confirmReq )
                .openRegistrationEnabled( openReg )
                .publiclyListableIs( pubList )
                .withSharingPolicy( DomainSharingPolicy.createDomainSharingPolicy( sharing ) )
                .withSize( size )
                .build();
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.DOMAIN_V2.ordinal();
    }

    @Override
    public void destroy() {
    }

    @Override
    public Class<Domain> getClazz() {
        return Domain.class;
    }
}
