package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.heracles.directory.v1.objects.Emails;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class EmailsStreamSerializer implements SelfRegisteringStreamSerializer<Emails> {

    @Override
    public void write( ObjectDataOutput out, Emails object ) throws IOException {
        SetStreamSerializers.serialize( out, object, ( String email ) -> out.writeUTF( email ) );
    }

    @Override
    public Emails read( ObjectDataInput in ) throws IOException {
        return new Emails( SetStreamSerializers.deserialize( in, ( ObjectDataInput input ) -> input.readUTF() ) );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.EMAILS.ordinal();
    }

    @Override
    public void destroy() {
    }

    @Override
    public Class<Emails> getClazz() {
        return Emails.class;
    }

}
