package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.BitSet;
import java.util.EnumSet;
import java.util.Set;

import com.google.common.base.Optional;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.v2.storage.models.Event;
import com.kryptnostic.v2.storage.models.Event.PropagationDirections;

public class EventStreamSerializer implements SelfRegisteringStreamSerializer<Event> {
    private static final PropagationDirections[] PDS = PropagationDirections.values();

    @Override
    public void write( ObjectDataOutput out, Event object ) throws IOException {
        out.writeLong( object.getTtlMillis() );
        Set<PropagationDirections> pds = object.getPropagationDirections();
        BitSet s = new BitSet( PDS.length );

        pds.forEach( pd -> s.set( pd.ordinal() ) );

        out.writeByteArray( s.toByteArray() );
        out.writeUTF( object.getContents() );
    }

    @Override
    public Event read( ObjectDataInput in ) throws IOException {
        long ttl = in.readLong();

        Set<PropagationDirections> pds = EnumSet.noneOf( PropagationDirections.class );

        BitSet s = BitSet.valueOf( in.readByteArray() );

        for ( int i = s.nextSetBit( 0 ); i >= 0; i = s.nextSetBit( i + 1 ) ) {
            pds.add( PDS[ i ] );
        }

        return new Event( pds, Optional.of( ttl ), in.readUTF() );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.EVENT.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<Event> getClazz() {
        return Event.class;
    }

}
