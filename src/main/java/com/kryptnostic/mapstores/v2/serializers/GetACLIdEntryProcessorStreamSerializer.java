package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.GetACLIdEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class GetACLIdEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<GetACLIdEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, GetACLIdEntryProcessor object ) throws IOException { /* no-op */}

    @Override
    public GetACLIdEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new GetACLIdEntryProcessor();
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.GET_ACL_ID_EP.ordinal();
    }

    @Override
    public void destroy() { /* no-op */}

    @Override
    public Class<GetACLIdEntryProcessor> getClazz() {
        return GetACLIdEntryProcessor.class;
    }

}
