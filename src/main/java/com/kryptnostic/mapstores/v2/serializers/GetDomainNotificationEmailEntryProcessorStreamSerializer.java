package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.GetDomainNotificationEmailEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class GetDomainNotificationEmailEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<GetDomainNotificationEmailEntryProcessor>{

    @Override
    public void write( ObjectDataOutput out, GetDomainNotificationEmailEntryProcessor object ) throws IOException {}

    @Override
    public GetDomainNotificationEmailEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new GetDomainNotificationEmailEntryProcessor();
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.GET_DOMAIN_EMAIL_EP.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<GetDomainNotificationEmailEntryProcessor> getClazz() {
        return GetDomainNotificationEmailEntryProcessor.class;
    }

}
