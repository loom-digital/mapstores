package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.GetObjectACLIdEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class GetObjectACLIdEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<GetObjectACLIdEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, GetObjectACLIdEntryProcessor object ) throws IOException { /* no-op */ }

    @Override
    public GetObjectACLIdEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new GetObjectACLIdEntryProcessor();
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.GET_OBJ_ACL_ID_EP.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<GetObjectACLIdEntryProcessor> getClazz() {
        return GetObjectACLIdEntryProcessor.class;
    }

}
