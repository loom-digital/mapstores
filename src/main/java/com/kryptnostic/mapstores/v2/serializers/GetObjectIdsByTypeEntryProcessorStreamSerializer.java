package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.GetObjectIdsByTypeEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class GetObjectIdsByTypeEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<GetObjectIdsByTypeEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, GetObjectIdsByTypeEntryProcessor object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getTypeIdToCheck() );
    }

    @Override
    public GetObjectIdsByTypeEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new GetObjectIdsByTypeEntryProcessor( UUIDStreamSerializer.deserialize( in ) );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.GET_OBJ_ID_BY_TYPE_EP.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<GetObjectIdsByTypeEntryProcessor> getClazz() {
        return GetObjectIdsByTypeEntryProcessor.class;
    }

}
