package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.entryprocessors.GetObjectsWithPermissionEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class GetObjectsWithPermissionEPStreamSerializer
        implements SelfRegisteringStreamSerializer<GetObjectsWithPermissionEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, GetObjectsWithPermissionEntryProcessor object ) throws IOException {
        out.writeInt( object.getPermission().ordinal() );
    }

    @Override
    public GetObjectsWithPermissionEntryProcessor read( ObjectDataInput in ) throws IOException {
        Permission permission = Permission.getValues()[ in.readInt() ];
        return new GetObjectsWithPermissionEntryProcessor( permission );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.GET_OBJS_WITH_PERM_EP.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<GetObjectsWithPermissionEntryProcessor> getClazz() {
        return GetObjectsWithPermissionEntryProcessor.class;
    }

}
