package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.List;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.IncomingShareRemoverEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

public class IncomingShareRemoverEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<IncomingShareRemoverEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, IncomingShareRemoverEntryProcessor object ) throws IOException {
        Iterable<VersionedObjectKey> backingCollection = object.getBackingCollection();
        out.writeInt( Iterables.size( backingCollection ) );
        for ( VersionedObjectKey objectKey : backingCollection ) {
            VersionedObjectKeyStreamSerializer.serialize( out, objectKey );
        }
    }

    @Override
    public IncomingShareRemoverEntryProcessor read( ObjectDataInput in ) throws IOException {
        int size = in.readInt();
        List<VersionedObjectKey> keys = Lists.newArrayListWithCapacity( size );
        for ( int i = 0; i < size; i++ ) {
            keys.add( VersionedObjectKeyStreamSerializer.deserialize( in ) );
        }
        return new IncomingShareRemoverEntryProcessor( keys );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.INCOMING_SHARE_REMOVER_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<IncomingShareRemoverEntryProcessor> getClazz() {
        return IncomingShareRemoverEntryProcessor.class;
    }

}
