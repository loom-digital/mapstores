package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.v2.search.IndexSegmentAddress;

public class IndexSegmentAddressStreamSerializer implements SelfRegisteringStreamSerializer<IndexSegmentAddress> {

    @Override
    public void write( ObjectDataOutput out, IndexSegmentAddress object ) throws IOException {
        out.writeByteArray( object.getAddress() );
    }

    @Override
    public IndexSegmentAddress read( ObjectDataInput in ) throws IOException {
        return new IndexSegmentAddress( in.readByteArray() );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.INDEX_SEGMENT_ADDRESS.ordinal();
    }

    @Override
    public void destroy() {
    }

    @Override
    public Class<IndexSegmentAddress> getClazz() {
        return IndexSegmentAddress.class;
    }
}
