package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.IndexSegmentCountEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class IndexSegmentCountEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<IndexSegmentCountEntryProcessor> {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.INDEX_SEGMENT_COUNT_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public void write( ObjectDataOutput out, IndexSegmentCountEntryProcessor object ) throws IOException {
        out.writeInt( object.getCount() );
    }

    @Override
    public IndexSegmentCountEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new IndexSegmentCountEntryProcessor( in.readInt() );
    }

    @Override
    public Class<IndexSegmentCountEntryProcessor> getClazz() {
        return IndexSegmentCountEntryProcessor.class;
    }

}
