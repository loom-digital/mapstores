package com.kryptnostic.mapstores.v2.serializers;

import com.google.common.base.Optional;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.mapstores.ObjectMetadataHierarchyKey;
import com.kryptnostic.rhizome.hazelcast.serializers.RhizomeUtils;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;
import com.kryptnostic.v2.storage.models.ScrollDirection;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

/**
 * Created by yao on 5/23/16.
 */
public class ObjectMetadataHierarchyKeyStreamSerializer implements SelfRegisteringStreamSerializer<ObjectMetadataHierarchyKey> {
    @Override
    public void write(ObjectDataOutput out, ObjectMetadataHierarchyKey object ) throws IOException {
        VersionedObjectKeyStreamSerializer.serialize( out, object.getParentObjectVersionedKey() );
        SetStreamSerializers.fastUUIDSetSerialize( out, object.getTypes() );

        RhizomeUtils.Serializers.serializeOptional( out,
                object.getOptionalObjectValue(),
                UUIDStreamSerializer::serialize );
        UUIDStreamSerializer.serialize( out, object.getClock() );
        RhizomeUtils.Serializers.serializeOptional( out,
                object.getPageSize(),
                ( objDataOut, val ) -> objDataOut.writeInt( val ) );
        out.writeInt( object.getDirection().ordinal() );
    }

    @Override
    public ObjectMetadataHierarchyKey read( ObjectDataInput in ) throws IOException {
        VersionedObjectKey parentObjectVersionedKey = VersionedObjectKeyStreamSerializer.deserialize( in );
        Set<UUID> types = SetStreamSerializers.fastUUIDSetDeserialize( in );

        ObjectMetadataHierarchyKey.Builder builder = new ObjectMetadataHierarchyKey.Builder( parentObjectVersionedKey, types );

        Optional<UUID> optionalValue = RhizomeUtils.Serializers.deserializeToOptional( in,
                UUIDStreamSerializer::deserialize );
        UUID clock = UUIDStreamSerializer.deserialize( in );

        builder = builder.withBaseClock( clock ).withObjectValue( optionalValue.orNull() );

        Optional<Integer> pageSize = RhizomeUtils.Serializers.deserializeToOptional( in,
                ( objDataIn ) -> objDataIn.readInt() );
        int directionOrdinal = in.readInt();
        ScrollDirection direction = ScrollDirection.values()[directionOrdinal];
        return builder.withPaging( pageSize, direction ).build();
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.OBJECT_METATDATA_HIERARCHY_KEY.ordinal();
    }

    @Override
    public void destroy() { /* no-op */}

    @Override
    public Class<ObjectMetadataHierarchyKey> getClazz() {
        return ObjectMetadataHierarchyKey.class;
    }
}
