package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import com.google.common.base.Optional;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.mapstores.ObjectMetadataLevelKey;
import com.kryptnostic.rhizome.hazelcast.serializers.RhizomeUtils;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;
import com.kryptnostic.v2.storage.models.ScrollDirection;

public class ObjectMetadataLevelKeyStreamSerializer implements SelfRegisteringStreamSerializer<ObjectMetadataLevelKey> {

    @Override
    public void write( ObjectDataOutput out, ObjectMetadataLevelKey object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getAclId() );
        SetStreamSerializers.fastUUIDSetSerialize( out, object.getTypes() );

        RhizomeUtils.Serializers.serializeOptional( out,
                object.getOptionalObjectValue(),
                UUIDStreamSerializer::serialize );
        UUIDStreamSerializer.serialize( out, object.getClock() );
        RhizomeUtils.Serializers.serializeOptional( out,
                object.getPageSize(),
                ( objDataOut, val ) -> objDataOut.writeInt( val.intValue() ) );
        out.writeInt( object.getDirection().ordinal() );
    }

    @Override
    public ObjectMetadataLevelKey read( ObjectDataInput in ) throws IOException {
        UUID aclIds = UUIDStreamSerializer.deserialize( in );
        Set<UUID> types = SetStreamSerializers.fastUUIDSetDeserialize( in );

        ObjectMetadataLevelKey.Builder builder = new ObjectMetadataLevelKey.Builder( aclIds, types );

        Optional<UUID> optionalValue = RhizomeUtils.Serializers.deserializeToOptional( in,
                UUIDStreamSerializer::deserialize );
        UUID clock = UUIDStreamSerializer.deserialize( in );

        builder = builder.withBaseClock( clock ).withObjectValue( optionalValue.orNull() );

        Optional<Integer> pageSize = RhizomeUtils.Serializers.deserializeToOptional( in,
                ( objDataIn ) -> Integer.valueOf( objDataIn.readInt() ) );
        int directionOrdinal = in.readInt();
        ScrollDirection direction = ScrollDirection.values()[directionOrdinal];
        return builder.withPaging( pageSize, direction ).build();
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.OBJECT_METADATA_LEVEL_KEY.ordinal();
    }

    @Override
    public void destroy() { /* no-op */}

    @Override
    public Class<ObjectMetadataLevelKey> getClazz() {
        return ObjectMetadataLevelKey.class;
    }
}

