package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import org.joda.time.DateTime;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.kodex.v1.crypto.ciphers.Cypher;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;
import com.kryptnostic.v2.storage.models.ObjectMetadata;

public class ObjectMetadataStreamSerializer implements SelfRegisteringStreamSerializer<ObjectMetadata> {

    @Override
    public void write( ObjectDataOutput out, ObjectMetadata object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getId() );
        UUIDStreamSerializer.serialize( out, object.getType() );
        UUIDStreamSerializer.serialize( out, object.getCreator() );
        out.writeLong( object.getVersion() );
        out.writeLong( object.getSize() );
        out.writeLong( object.getCreatedTime().getMillis() );
        out.writeUTF( object.getCipherMethod().name() );

        UUIDStreamSerializer.serialize( out, object.getACLId() );
    }

    @Override
    public ObjectMetadata read( ObjectDataInput in ) throws IOException {
        UUID id = UUIDStreamSerializer.deserialize( in );
        UUID type = UUIDStreamSerializer.deserialize( in );
        UUID creator = UUIDStreamSerializer.deserialize( in );
        long version = in.readLong();
        long size = in.readLong();
        long instant = in.readLong();
        DateTime time = new DateTime( instant );
        Cypher cypher = Cypher.valueOf( in.readUTF() );

        UUID aclId = UUIDStreamSerializer.deserialize( in );

        ObjectMetadata metadata = new ObjectMetadata( id, version, size, type, aclId, creator, cypher, time );
        return metadata;
    }

    @Override
    public void destroy() {}

    @Override
    public Class<ObjectMetadata> getClazz() {
        return ObjectMetadata.class;
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.OBJECT_METADATA_V2.ordinal();
    }

}
