package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;
import com.kryptnostic.v2.sharing.models.ObjectUserKey;

public class ObjectUserKeyStreamSerializer implements SelfRegisteringStreamSerializer<ObjectUserKey> {

    @Override
    public void write( ObjectDataOutput out, ObjectUserKey object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getObjectId() );
        UUIDStreamSerializer.serialize( out, object.getUserId() );
    }

    @Override
    public ObjectUserKey read( ObjectDataInput in ) throws IOException {
        UUID objectId = UUIDStreamSerializer.deserialize( in );
        UUID userId = UUIDStreamSerializer.deserialize( in );
        return new ObjectUserKey( objectId, userId );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.OBJECT_USER_KEY_V2.ordinal();
    }

    @Override
    public void destroy() { /* No-op */
    }

    @Override
    public Class<ObjectUserKey> getClazz() {
        return ObjectUserKey.class;
    }

}
