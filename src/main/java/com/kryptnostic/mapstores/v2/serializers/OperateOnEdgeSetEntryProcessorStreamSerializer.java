package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.OperateOnEdgeSetEntryProcessor;
import com.kryptnostic.mapstores.v2.entryprocessors.OperateOnEdgeSetEntryProcessor.EdgeSetOperation;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

public class OperateOnEdgeSetEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<OperateOnEdgeSetEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, OperateOnEdgeSetEntryProcessor object ) throws IOException {
        VersionedObjectKeyStreamSerializer.serialize( out, object.getValueToUse() );
        switch( object.getOperation() ) {
            case ADD:
                out.writeBoolean( true );
                break;
            case DELETE:
                out.writeBoolean( false );
                break;
            default:
                break;
        }
    }

    @Override
    public OperateOnEdgeSetEntryProcessor read( ObjectDataInput in ) throws IOException {
        OperateOnEdgeSetEntryProcessor.EdgeSetOperation operation;
        VersionedObjectKey key = VersionedObjectKeyStreamSerializer.deserialize( in );
        boolean add = in.readBoolean();
        if ( add ) {
            operation = EdgeSetOperation.ADD;
        } else {
            operation = EdgeSetOperation.DELETE;
        }
        return new OperateOnEdgeSetEntryProcessor( operation, key );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.OP_ON_EDGE_SET_ENT_PROC_STR_SERIALIZER.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<OperateOnEdgeSetEntryProcessor> getClazz() {
        return OperateOnEdgeSetEntryProcessor.class;
    }

}
