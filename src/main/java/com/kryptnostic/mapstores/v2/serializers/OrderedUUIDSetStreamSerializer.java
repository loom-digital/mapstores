package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.hazelcast.objects.OrderedUUIDSet;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class OrderedUUIDSetStreamSerializer implements SelfRegisteringStreamSerializer<OrderedUUIDSet> {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.DOCUMENT_KEY_SET.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public void write( ObjectDataOutput out, OrderedUUIDSet object ) throws IOException {
        SetStreamSerializers.fastUUIDSetSerialize( out, object );
    }

    @Override
    public OrderedUUIDSet read( ObjectDataInput in ) throws IOException {
        return SetStreamSerializers.fastOrderedUUIDSetDeserialize( in );
    }

    @Override
    public Class<OrderedUUIDSet> getClazz() {
        return OrderedUUIDSet.class;
    }

}
