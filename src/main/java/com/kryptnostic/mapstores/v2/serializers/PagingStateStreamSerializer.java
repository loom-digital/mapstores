package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.datastax.driver.core.PagingState;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class PagingStateStreamSerializer implements SelfRegisteringStreamSerializer<PagingState> {

    @Override
    public void write( ObjectDataOutput out, PagingState object ) throws IOException {
        out.writeByteArray( object.toBytes() );
    }

    @Override
    public PagingState read( ObjectDataInput in ) throws IOException {
        return PagingState.fromBytes( in.readByteArray() );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.PAGING_STATE.ordinal();
    }

    @Override
    public void destroy() { /* */ }

    @Override
    public Class<PagingState> getClazz() {
        return PagingState.class;
    }

}
