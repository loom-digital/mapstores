package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.entryprocessors.PermissionCheckEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class PermissionCheckEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<PermissionCheckEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, PermissionCheckEntryProcessor object ) throws IOException {
        out.writeInt( object.getPermission().ordinal() );
    }

    @Override
    public PermissionCheckEntryProcessor read( ObjectDataInput in ) throws IOException {
        Permission permission = Permission.getValues()[ in.readInt() ];
        return new PermissionCheckEntryProcessor( permission );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.PERM_CHECK_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() { /* no-op */}

    @Override
    public Class<PermissionCheckEntryProcessor> getClazz() {
        return PermissionCheckEntryProcessor.class;
    }

}
