package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.EnumSet;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.entryprocessors.PermissionUpdatingEntryProcessor;
import com.kryptnostic.mapstores.v2.entryprocessors.PermissionUpdatingEntryProcessor.PermissionOperation;
import com.kryptnostic.rhizome.hazelcast.serializers.RhizomeUtils;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class PermissionUpdatingEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<PermissionUpdatingEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, PermissionUpdatingEntryProcessor object ) throws IOException {
        out.writeBoolean( object.getOperation() == PermissionOperation.SET );
        RhizomeUtils.Serializers.serializeEnumSet( out, Permission.class, object.getPermissionsToUpdate() );
    }

    @Override
    public PermissionUpdatingEntryProcessor read( ObjectDataInput in ) throws IOException {
        PermissionOperation operation = in.readBoolean() ? PermissionOperation.SET : PermissionOperation.REMOVE;
        EnumSet<Permission> permissions = RhizomeUtils.Serializers.deSerializeEnumSet( in, Permission.class );
        switch( operation ) {
            case REMOVE:
                return PermissionUpdatingEntryProcessor.removePermissions( permissions );
            case SET:
                return PermissionUpdatingEntryProcessor.setPermissions( permissions );
            default:
                throw new IllegalArgumentException(
                        "The operation " + operation.name() + " is not a valid permission update operation" );
        }
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.PERMISSION_UPDATING_EP.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<PermissionUpdatingEntryProcessor> getClazz() {
        return PermissionUpdatingEntryProcessor.class;
    }

}
