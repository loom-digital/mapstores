package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.datastax.driver.core.Session;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.mapstores.ReadableObjectsSetProxy;
import com.kryptnostic.mapstores.v2.mapstores.UserObjectTypePair;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class ReadableObjectsSetProxyStreamSerializer
        implements SelfRegisteringStreamSerializer<ReadableObjectsSetProxy> {

    private Session session;

    public ReadableObjectsSetProxyStreamSerializer( Session session ) {
        this.session = session;
    }

    @Override
    public void write( ObjectDataOutput out, ReadableObjectsSetProxy object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getKeyPair().getUserId() );
        UUIDStreamSerializer.serialize( out, object.getKeyPair().getTypeId() );
        out.writeUTF( object.getTable() );
        out.writeUTF( object.getKeyspace() );
    }

    @Override
    public ReadableObjectsSetProxy read( ObjectDataInput in ) throws IOException {
        UUID userId = UUIDStreamSerializer.deserialize( in );
        UUID typeId = UUIDStreamSerializer.deserialize( in );
        UserObjectTypePair keyPair = new UserObjectTypePair( userId, typeId );
        String table = in.readUTF();
        String keyspace = in.readUTF();
        return new ReadableObjectsSetProxy( session, keyspace, table, keyPair );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.READABLE_OBJECTS_SETPROXY.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<ReadableObjectsSetProxy> getClazz() {
        return ReadableObjectsSetProxy.class;
    }

}
