package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.Set;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.RemoveFromUserSettingsEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class RemoveFromUserSettingsEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<RemoveFromUserSettingsEntryProcessor>{

    @Override
    public void write( ObjectDataOutput out, RemoveFromUserSettingsEntryProcessor object ) throws IOException {
        SetStreamSerializers.fastStringSetSerialize( out, object.getBackingCollection() );
    }

    @Override
    public RemoveFromUserSettingsEntryProcessor read( ObjectDataInput in ) throws IOException {
        Set<String> collection = SetStreamSerializers.fastStringSetDeserialize( in );
        return new RemoveFromUserSettingsEntryProcessor( collection );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.REMOVE_FROM_USER_SETTINGS_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<RemoveFromUserSettingsEntryProcessor> getClazz() {
        return RemoveFromUserSettingsEntryProcessor.class;
    }

}
