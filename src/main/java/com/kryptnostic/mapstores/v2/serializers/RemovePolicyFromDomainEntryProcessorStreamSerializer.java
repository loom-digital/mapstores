package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.RemovePolicyFromDomainEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class RemovePolicyFromDomainEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<RemovePolicyFromDomainEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, RemovePolicyFromDomainEntryProcessor object ) throws IOException {
        SetStreamSerializers.fastUUIDSetSerialize( out, object.getBackingCollection() );
    }

    @Override
    public RemovePolicyFromDomainEntryProcessor read( ObjectDataInput in ) throws IOException {
        UUIDSet collection = SetStreamSerializers.fastUUIDSetDeserialize( in );
        return new RemovePolicyFromDomainEntryProcessor( collection );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.REMOVE_POLICY_FROM_DOMAIN_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<RemovePolicyFromDomainEntryProcessor> getClazz() {
        return RemovePolicyFromDomainEntryProcessor.class;
    }

}
