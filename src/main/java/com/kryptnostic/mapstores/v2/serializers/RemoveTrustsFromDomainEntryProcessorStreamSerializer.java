package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.RemoveTrustsFromEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class RemoveTrustsFromDomainEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<RemoveTrustsFromEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, RemoveTrustsFromEntryProcessor object ) throws IOException {
            SetStreamSerializers.fastUUIDSetSerialize( out, object.getBackingCollection() );
    }

    @Override
    public RemoveTrustsFromEntryProcessor read( ObjectDataInput in ) throws IOException {
        UUIDSet collection = SetStreamSerializers.fastUUIDSetDeserialize( in );
        return new RemoveTrustsFromEntryProcessor( collection );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.REMOVE_TRUSTS_FROM_DOMAIN_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<RemoveTrustsFromEntryProcessor> getClazz() {
        return RemoveTrustsFromEntryProcessor.class;
    }

}
