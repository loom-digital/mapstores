package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import org.joda.time.DateTime;

import com.google.common.base.Optional;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.kodex.v1.crypto.ciphers.BlockCiphertext;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;
import com.kryptnostic.v2.sharing.models.Share;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

public class ShareStreamSerializer implements SelfRegisteringStreamSerializer<Share> {

    @Override
    public void write( ObjectDataOutput out, Share object ) throws IOException {
        serialize( out, object );
    }

    public static void serialize( ObjectDataOutput out, Share object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getSourceUserId() );
        VersionedObjectKeyStreamSerializer.serialize( out, object.getObjectKey() );
        boolean present = object.getEncryptedSharingPair().isPresent();
        out.writeBoolean( present );
        if ( present ) {
            BlockCiphertextStreamSerializer.serialize( out, object.getEncryptedSharingPair().get() );
        }
        out.writeByteArray( object.getPublicKeyEncryptedCryptoService() );
        out.writeLong( object.getCreationTime().getMillis() );
    }

    @Override
    public Share read( ObjectDataInput in ) throws IOException {
        return deserialize( in );
    }

    public static Share deserialize( ObjectDataInput in ) throws IOException {
        UUID sourceUserId = UUIDStreamSerializer.deserialize( in );
        VersionedObjectKey objectKey = VersionedObjectKeyStreamSerializer.deserialize( in );
        boolean ctextPresent = in.readBoolean();
        Optional<BlockCiphertext> ctxt = Optional.absent();
        if ( ctextPresent ) {
            BlockCiphertext ciphertext = BlockCiphertextStreamSerializer.deserialize( in );
            ctxt = Optional.<BlockCiphertext> of( ciphertext );
        }
        byte[] seal = in.readByteArray();
        long millis = in.readLong();

        return new Share( sourceUserId, objectKey, ctxt, seal, new DateTime( millis ) );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.SHARE.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<Share> getClazz() {
        return Share.class;
    }

}
