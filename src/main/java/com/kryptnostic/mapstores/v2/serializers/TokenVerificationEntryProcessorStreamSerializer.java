package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.TokenVerificationEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class TokenVerificationEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<TokenVerificationEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, TokenVerificationEntryProcessor object ) throws IOException {
        out.writeUTF( object.getConfirmationToken() );

    }

    @Override
    public TokenVerificationEntryProcessor read( ObjectDataInput in ) throws IOException {
        String confirmationToken = in.readUTF();
        return new TokenVerificationEntryProcessor( confirmationToken );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.TOKEN_VERIFICATION_EP.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<TokenVerificationEntryProcessor> getClazz() {
        return TokenVerificationEntryProcessor.class;
    }

}
