package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.UUIDSetContainsAnyEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class UUIDSetContainsAnyEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<UUIDSetContainsAnyEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, UUIDSetContainsAnyEntryProcessor object ) throws IOException {
        new UUIDSetStreamSerializer().write( out, new UUIDSet( object.getObjectsToCheck() ) );
    }

    @Override
    public UUIDSetContainsAnyEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new UUIDSetContainsAnyEntryProcessor( new UUIDSetStreamSerializer().read( in ) );
    }

    @Override
    public void destroy() {/* no-op */
    }

    @Override
    public Class<UUIDSetContainsAnyEntryProcessor> getClazz() {
        return UUIDSetContainsAnyEntryProcessor.class;
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.UUID_SET_CONTAINS_ANY_ENTRY_PROCESSOR.ordinal();
    }
}
