package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.UUIDSetContainsEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class UUIDSetContainsEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<UUIDSetContainsEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, UUIDSetContainsEntryProcessor object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getObject() );
    }

    @Override
    public UUIDSetContainsEntryProcessor read( ObjectDataInput in ) throws IOException {
        return new UUIDSetContainsEntryProcessor( UUIDStreamSerializer.deserialize( in ) );
    }

    @Override
    public void destroy() {
        /* no-op */
    }

    @Override
    public Class<UUIDSetContainsEntryProcessor> getClazz() {
        return UUIDSetContainsEntryProcessor.class;
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.UUID_SET_CONTAINS_ENTRY_PROCESSOR.ordinal();
    }
}
