package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class UUIDSetStreamSerializer implements SelfRegisteringStreamSerializer<UUIDSet> {

    @Override
    public void write( ObjectDataOutput out, UUIDSet object ) throws IOException {
        SetStreamSerializers.fastUUIDSetSerialize( out, object );
    }

    @Override
    public UUIDSet read( ObjectDataInput in ) throws IOException {
        return SetStreamSerializers.fastUUIDSetDeserialize( in );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.UUID_SET.ordinal();
    }

    @Override
    public void destroy() {
        /* no-op */
    }

    @Override
    public Class<UUIDSet> getClazz() {
        return UUIDSet.class;
    }

}
