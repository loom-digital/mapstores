package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.Set;

import com.google.common.collect.Sets;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.UpdateAndUploadProgressEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.v2.crypto.CryptoMaterial;

public class UpdateAndUploadProgressEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<UpdateAndUploadProgressEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, UpdateAndUploadProgressEntryProcessor object ) throws IOException {
        deserialize( out, object );
    }

    public static void deserialize( ObjectDataOutput out, UpdateAndUploadProgressEntryProcessor object )
            throws IOException {
        Set<CryptoMaterial> materials = object.getMaterialType();
        out.writeInt( materials.size() );
        for ( CryptoMaterial material : materials ) {
            CryptomaterialStreamSerializer.serialize( out, material );
        }
    }

    @Override
    public UpdateAndUploadProgressEntryProcessor read( ObjectDataInput in ) throws IOException {
        int size = in.readInt();
        Set<CryptoMaterial> materials = Sets.newHashSetWithExpectedSize( size );
        for ( int i = 0; i < size; i++ ) {
            materials.add( CryptomaterialStreamSerializer.deserialize( in ) );
        }
        return new UpdateAndUploadProgressEntryProcessor( materials );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.UPDATE_UPLOAD_PROGRESS_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {/* no-op */}

    @Override
    public Class<UpdateAndUploadProgressEntryProcessor> getClazz() {
        return UpdateAndUploadProgressEntryProcessor.class;
    }

}
