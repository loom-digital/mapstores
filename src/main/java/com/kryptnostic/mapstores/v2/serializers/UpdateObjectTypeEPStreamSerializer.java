package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.UpdateObjectTypeEntryProcessor;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class UpdateObjectTypeEPStreamSerializer
        implements SelfRegisteringStreamSerializer<UpdateObjectTypeEntryProcessor> {

    @Override
    public void write( ObjectDataOutput out, UpdateObjectTypeEntryProcessor object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getNewTypeId() );
    }

    @Override
    public UpdateObjectTypeEntryProcessor read( ObjectDataInput in ) throws IOException {
        UUID newType = UUIDStreamSerializer.deserialize( in );
        return new UpdateObjectTypeEntryProcessor( newType );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.UPDATE_OBJECT_TYPE_UP.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<UpdateObjectTypeEntryProcessor> getClazz() {
        return UpdateObjectTypeEntryProcessor.class;
    }

}
