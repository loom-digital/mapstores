package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.entryprocessors.UpdateReadableTypedObjectsEP;
import com.kryptnostic.mapstores.v2.entryprocessors.UpdateReadableTypedObjectsEP.Operation;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class UpdateReadableTypedObjectsEPStreamSerializer
        implements SelfRegisteringStreamSerializer<UpdateReadableTypedObjectsEP> {

    @Override
    public void write( ObjectDataOutput out, UpdateReadableTypedObjectsEP object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getId() );
        switch ( object.getOp() ) {
            case ADD:
                out.writeBoolean( true );
                break;
            case REMOVE:
                out.writeBoolean( false );
                break;
            default:
                throw new RuntimeException( "No operation specified for UpdateReadableTypedObjectsEP" );
        }
    }

    @Override
    public UpdateReadableTypedObjectsEP read( ObjectDataInput in ) throws IOException {
        UUID deserialize = UUIDStreamSerializer.deserialize( in );
        Operation operation = Operation.REMOVE;
        if ( in.readBoolean() ) {
            operation = Operation.ADD;
        }
        return new UpdateReadableTypedObjectsEP( deserialize, operation );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.ADD_TO_READABLE_TYPED_OBJECTS_EP.ordinal();
    }

    @Override
    public void destroy() {
        /* no-op */
    }

    @Override
    public Class<UpdateReadableTypedObjectsEP> getClazz() {
        return UpdateReadableTypedObjectsEP.class;
    }

}
