package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.mapstores.UserObjectTypePair;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class UserObjectTypePairStreamSerializer implements SelfRegisteringStreamSerializer<UserObjectTypePair> {

    @Override
    public void write( ObjectDataOutput out, UserObjectTypePair object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getUserId() );
        UUIDStreamSerializer.serialize( out, object.getTypeId() );
    }

    @Override
    public UserObjectTypePair read( ObjectDataInput in ) throws IOException {
        UUID userId = UUIDStreamSerializer.deserialize( in );
        UUID typeId = UUIDStreamSerializer.deserialize( in );
        return new UserObjectTypePair( userId, typeId );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.USER_OBJECT_TYPE_PAIR.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<UserObjectTypePair> getClazz() {
        return UserObjectTypePair.class;
    }

}
