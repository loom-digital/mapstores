package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.directory.v2.model.UserSettings;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.mapstores.UserSettingsKey;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;

public class UserSettingsKeyStreamSerializer implements SelfRegisteringStreamSerializer<UserSettingsKey>{

    @Override
    public void write( ObjectDataOutput out, UserSettingsKey object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getUserId() );
        out.writeUTF( object.getUserSetting().toString() );

    }

    @Override
    public UserSettingsKey read( ObjectDataInput in ) throws IOException {
        UUID userId = UUIDStreamSerializer.deserialize( in );
        String userSetting = in.readUTF();
        return new UserSettingsKey( userId, UserSettings.valueOf( userSetting ) );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.USER_SETTINGS_KEY.ordinal();
    }

    @Override
    public void destroy() {/* no-op */}

    @Override
    public Class<UserSettingsKey> getClazz() {
        return UserSettingsKey.class;
    }

}
