package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;
import com.kryptnostic.v2.storage.models.VersionedObjectKeySet;

public class VersionedObjectKeySetStreamSerializer implements SelfRegisteringStreamSerializer<VersionedObjectKeySet>{

    @Override
    public void write( ObjectDataOutput out, VersionedObjectKeySet object ) throws IOException {
        fastVersionedObjectKeySetSerialize( out, object );
    }

    @Override
    public VersionedObjectKeySet read( ObjectDataInput in ) throws IOException {
        return fastVersionedObjectKeySetDeserialize( in );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.VERSIONED_OBJECT_KEY_SET.ordinal();
    }

    public static void fastVersionedObjectKeySetSerialize( ObjectDataOutput out, Set<VersionedObjectKey> object )
            throws IOException {
        long[] most = new long[ object.size() ];
        long[] least = new long[ object.size() ];
        long[] versions = new long[ object.size() ];
        int i=0;
        for ( VersionedObjectKey key : object ) {
            versions[i] = key.getVersion();
            UUID id = key.getObjectId();
            most[i] = id.getMostSignificantBits();
            least[i] = id.getLeastSignificantBits();
            i++;
        }
        out.writeInt( i );
        out.writeLongArray( versions );
        out.writeLongArray( most );
        out.writeLongArray( least );
    }

    public static VersionedObjectKeySet fastVersionedObjectKeySetDeserialize( ObjectDataInput in ) throws IOException {
        int size = in.readInt();
        long[] versions = in.readLongArray();
        long[] most = in.readLongArray();
        long[] least = in.readLongArray();
        VersionedObjectKeySet set = new VersionedObjectKeySet( size );
        for ( int i=0; i < size; i++ ) {
            set.add( new VersionedObjectKey( new UUID( most[i], least[i] ), versions[i] ) );
        }
        return set;
    }

    @Override
    public void destroy() {
        /* no-op */
    }

    @Override
    public Class<VersionedObjectKeySet> getClazz() {
        return VersionedObjectKeySet.class;
    }

}
