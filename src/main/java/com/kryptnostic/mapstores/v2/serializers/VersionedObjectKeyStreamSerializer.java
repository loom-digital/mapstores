package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

public class VersionedObjectKeyStreamSerializer implements SelfRegisteringStreamSerializer<VersionedObjectKey> {

    @Override
    public void write( ObjectDataOutput out, VersionedObjectKey object ) throws IOException {
        serialize( out, object );
    }

    @Override
    public VersionedObjectKey read( ObjectDataInput in ) throws IOException {
        return deserialize( in );
    }

    public static void serialize( ObjectDataOutput out, VersionedObjectKey object ) throws IOException {
        UUIDStreamSerializer.serialize( out, object.getObjectId() );
        out.writeLong( object.getVersion() );
    }

    public static VersionedObjectKey deserialize( ObjectDataInput in ) throws IOException {
        UUID objectId = UUIDStreamSerializer.deserialize( in );
        long version = in.readLong();
        return new VersionedObjectKey( objectId, version );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.VERSIONED_OBJECT_KEY.ordinal();
    }

    @Override
    public void destroy() { /* no-op */ }

    @Override
    public Class<VersionedObjectKey> getClazz() {
        return VersionedObjectKey.class;
    }


}
