package com.kryptnostic.mapstores.v2.serializers;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.services.v1.serialization.UUIDStreamSerializer;
import com.kryptnostic.v2.storage.models.VersionedObjectUserKey;

public class VersionedObjectUserKeyStreamSerializer implements SelfRegisteringStreamSerializer<VersionedObjectUserKey> {

    @Override
    public void write( ObjectDataOutput out, VersionedObjectUserKey object ) throws IOException {
        UUID objectId = object.getObjectId();
        UUID userKey = object.getUserKey();
        long version = object.getVersion();
        UUIDStreamSerializer.serialize( out, objectId );
        UUIDStreamSerializer.serialize( out, userKey );
        out.writeLong( version );
    }

    @Override
    public VersionedObjectUserKey read( ObjectDataInput in ) throws IOException {
        UUID objectId = UUIDStreamSerializer.deserialize( in );
        UUID userKey = UUIDStreamSerializer.deserialize( in );
        long version = in.readLong();
        return new VersionedObjectUserKey( objectId, userKey, version );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.VERSIONED_OBJECT_USER_KEY_V2.ordinal();
    }

    @Override
    public void destroy() {/* no-op */}

    @Override
    public Class<VersionedObjectUserKey> getClazz() {
        return VersionedObjectUserKey.class;
    }

}
