package com.kryptnostic.mapstores.v2.services;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import javax.annotation.CheckForNull;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.kryptnostic.directory.v1.domain.DomainPolicy;
import com.kryptnostic.directory.v2.model.UserSettings;
import com.kryptnostic.heracles.directory.v1.Users;
import com.kryptnostic.mapstores.v2.Permission;
import com.kryptnostic.mapstores.v2.constants.HazelcastNames;
import com.kryptnostic.mapstores.v2.entryprocessors.AccessGrantingEntryProcessor;
import com.kryptnostic.mapstores.v2.entryprocessors.GetACLIdEntryProcessor;
import com.kryptnostic.mapstores.v2.entryprocessors.GetObjectTypeEntryProcessor;
import com.kryptnostic.mapstores.v2.entryprocessors.GetObjectVersionEntryProcessor;
import com.kryptnostic.mapstores.v2.entryprocessors.GetObjectsWithPermissionEntryProcessor;
import com.kryptnostic.mapstores.v2.entryprocessors.PermissionCheckEntryProcessor;
import com.kryptnostic.mapstores.v2.entryprocessors.PermissionUpdatingEntryProcessor;
import com.kryptnostic.mapstores.v2.entryprocessors.RemoveFromUserSettingsEntryProcessor;
import com.kryptnostic.mapstores.v2.entryprocessors.UpdateReadableTypedObjectsEP;
import com.kryptnostic.mapstores.v2.mapstores.ACLPermissionPair;
import com.kryptnostic.mapstores.v2.mapstores.UserACLId;
import com.kryptnostic.mapstores.v2.mapstores.UserObjectTypePair;
import com.kryptnostic.mapstores.v2.mapstores.UserSettingsKey;
import com.kryptnostic.mapstores.v2.utils.ServiceUtils;
import com.kryptnostic.v2.storage.models.ObjectMetadata;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

@Component
public class HazelcastObjectAuthorizationService implements ObjectAuthorizationService {

    private static final Logger                                     logger = LoggerFactory
                                                                                   .getLogger(
                                                                                           HazelcastObjectAuthorizationService.class );

    private final IMap<VersionedObjectKey, Set<VersionedObjectKey>> forwardEdgeMap;
    private final IMap<UUID, ObjectMetadata>                        objectMetadataMap;

    // ACL Maps
    private final IMap<UUID, Set<UUID>>                             userAccessibleObjectsMap;
    private final IMap<UserACLId, Set<Permission>>                  userAclToPermissionsMap;
    private final IMap<ACLPermissionPair, Set<UUID>>                reverseUserAclToPermssions;

    private final IMap<UserObjectTypePair, Set<UUID>>               readableTypedUserObjects;

    private final IMap<DomainPolicy, Set<UUID>>                     domainsWithPolicy;

    private final IMap<UserSettingsKey, Set<String>>                userSettings;

    @Inject
    public HazelcastObjectAuthorizationService( HazelcastInstance hazelcastInstance ) {
        readableTypedUserObjects = hazelcastInstance.getMap( HazelcastNames.Maps.READABLE_TYPED_USER_OBJECTS );
        this.forwardEdgeMap = hazelcastInstance.getMap( HazelcastNames.Maps.OBJECT_FORWARD_EDGE_MAP );
        this.objectMetadataMap = hazelcastInstance.getMap( HazelcastNames.Maps.NEW_OBJECT_METADATA );
        this.userAccessibleObjectsMap = hazelcastInstance.getMap( HazelcastNames.Maps.OBJECT_LOOKUP_ACL_MAP );
        this.userAclToPermissionsMap = hazelcastInstance.getMap( HazelcastNames.Maps.USER_LOOKUP_ACL_MAP );
        this.domainsWithPolicy = hazelcastInstance.getMap( HazelcastNames.Maps.DOMAINS_WITH_POLICY );
        this.userSettings = hazelcastInstance.getMap( HazelcastNames.Maps.USER_SETTINGS );
        this.reverseUserAclToPermssions = hazelcastInstance.getMap( HazelcastNames.Maps.REVERSE_USER_LOOKUP_ACL_MAP );
    }

    private Set<UUID> collectLatestDescendents( Set<UUID> objectIds ) {
        if ( objectIds == null || objectIds.isEmpty() ) {
            return ImmutableSet.<UUID> of();
        }
        Set<VersionedObjectKey> frontier = objectMetadataMap
                .executeOnKeys( objectIds, new GetObjectVersionEntryProcessor() )
                .entrySet()
                .stream()
                .filter( e -> e.getValue() != null )
                .map( e -> new VersionedObjectKey( e.getKey(), ( (Long) e.getValue() ).longValue() ) )
                .collect( Collectors.toSet() );

        Set<VersionedObjectKey> descendents = Sets.newHashSetWithExpectedSize( frontier.size() );
        while ( !frontier.isEmpty() ) {
            descendents.addAll( frontier );
            // TODO: inefficient
            // --the Sets.newHashSet( Sets.union( x, y ) ) in a loop using Sets.union iterates cubiclyyyy :(
            Set<VersionedObjectKey> nextLayer = forwardEdgeMap.getAll( frontier )
                    .values()
                    .stream()
                    .reduce( ImmutableSet.of(), ( x, y ) -> Sets.newHashSet( Sets.union( x, y ) ) );
            frontier = Sets.intersection( nextLayer, descendents );
        }
        return descendents
                .stream()
                .map( latestVersionedDescendent -> latestVersionedDescendent.getObjectId() )
                .collect( Collectors.toSet() );
    }

    @Override
    public Set<UUID> getReadableParents( UUID user ) {
        return getObjectRootsForUserWithPermission( user, Permission.READ );
    }

    @Override
    public Set<UUID> getReadableObjects( UUID user ) {
        Set<UUID> roots = getObjectRootsForUserWithPermission( user, Permission.READ );
        if ( roots == null || roots.isEmpty() ) {
            return ImmutableSet.of();
        }
        return collectLatestDescendents( roots );
    }

    @Override
    public Set<UUID> getWritableObjects( UUID user ) {
        Set<UUID> roots = getObjectRootsForUserWithPermission( user, Permission.WRITE );
        return collectLatestDescendents( roots );
    }

    @Override
    public Set<UUID> getOwnedObjects( UUID user ) {
        Set<UUID> roots = getObjectRootsForUserWithPermission( user, Permission.OWNER );
        return collectLatestDescendents( roots );
    }

    private Set<UUID> getObjectRootsForUserWithPermission( UUID user, Permission targetPermission ) {
        // get all objects in objectLookupACLMap --this should be all roots the user has access to
        // get all the permissions this user has for those objects
        Set<UUID> accessibleRootObjects = ServiceUtils.getNonnull( userAccessibleObjectsMap, user );
        if ( accessibleRootObjects == null || accessibleRootObjects.isEmpty() ) {
            return ImmutableSet.of();
        }
        Set<UserACLId> accesiblAsUserACLIDs = accessibleRootObjects.stream()
                .map( ( id ) -> new UserACLId( user, id ) )
                .collect( Collectors.toSet() );

        // filter only the roots that match the permission
        Map<UserACLId, Object> objectRootsWithPermission = userAclToPermissionsMap.executeOnKeys( accesiblAsUserACLIDs,
                new GetObjectsWithPermissionEntryProcessor( targetPermission ) );
        Set<UUID> collect = objectRootsWithPermission.entrySet().stream()
                .filter( ( entry ) -> entry.getValue() != null )
                .map( ( entry ) -> (UUID) entry.getValue() )
                .collect( Collectors.toSet() );
        return collect;
    }

    @Override
    public void setReadAccess( UUID user, Set<UUID> objectIds ) {
        setAccess( user, objectIds, EnumSet.of( Permission.READ ) );
    }

    @Override
    public void setWriteAccess( UUID user, Set<UUID> objectIds ) {
        setAccess( user, objectIds, EnumSet.of( Permission.WRITE, Permission.READ ) );
    }

    @Override
    public void setOwner( UUID user, Set<UUID> objectIds ) {
        setAccess( user, objectIds, EnumSet.of( Permission.OWNER, Permission.WRITE, Permission.READ ) );
    }

    private void setAccess( UUID user, Set<UUID> objectIds, Set<Permission> permissions ) {
        ArrayList<Future> futures = Lists.newArrayList();
        // update the objectLookupACLMap
        Future submitToKey = userAccessibleObjectsMap.submitToKey( user,
                new AccessGrantingEntryProcessor( objectIds ) );
        futures.add( submitToKey );

        Map<UUID, Object> objectsToTypes = objectMetadataMap.executeOnKeys( objectIds,
                new GetObjectTypeEntryProcessor() );
        Map<UUID, UserObjectTypePair> toUserObjectTypePairs = Maps.transformValues( objectsToTypes, ( value ) -> {
            return new UserObjectTypePair( user, (UUID) value );
        } );
        for ( Entry<UUID, UserObjectTypePair> entry : toUserObjectTypePairs.entrySet() ) {
            Future submit = readableTypedUserObjects.submitToKey( entry.getValue(),
                    UpdateReadableTypedObjectsEP.addToReadableTypedObjects( entry.getKey() ) );
            futures.add( submit );
        }

        Iterable<UserACLId> userACLIds = Iterables.transform( getAclIdsForObjects( objectIds ),
                ( objectId ) -> new UserACLId( user, objectId ) );
        Map<UserACLId, Set<Permission>> asMap = Maps.toMap( userACLIds, ( value ) -> permissions );
        userAclToPermissionsMap.putAll( asMap );

        for ( Future f : futures ) {
            try {
                f.get();
            } catch ( InterruptedException | ExecutionException e ) {
                logger.error( "failed waiting on a future in authz ", e );
            }
        }
    }

    @Override
    public void addNewUser( UUID userId ) {
        userAccessibleObjectsMap.set( userId, Sets.newHashSet() );
    }

    @Override
    public boolean removeAccessForAllUsers( UUID objectId ) {
        Iterable<UUID> usersWithPermissonOnObject = getUsersWithPermissonOnObject( Permission.READ, objectId );
        Set<UUID> usersWithPerms = Sets.newHashSet( usersWithPermissonOnObject );
        return removeAccessForUsers( usersWithPerms, objectId );
    }

    @Override
    public boolean removeAccessForUsers( Set<UUID> users, UUID objectId ) {
        if ( objectId == null || users == null || users.isEmpty() ) {
            logger.warn( "Tried to remove access to a null object or to en empty set of users" );
            return false;
        }
        if ( objectId.equals( getAclIdForObject( objectId ) ) ) {
            Set<UserACLId> asAclIds = users.stream()
                    .map( ( userId ) -> new UserACLId( userId, objectId ) )
                    .collect( Collectors.toSet() );
            removeAccessForUserAcls( asAclIds );
        }

        Object objectType = objectMetadataMap.executeOnKey( objectId,
                new GetObjectTypeEntryProcessor() );
        if ( objectType == null ) {
            logger.error( "Tried to remove access to an object with no type" );
            return false;
        }
        UUID type = (UUID) objectType;
        Set<UserObjectTypePair> uots = users.stream()
                .map( ( userId ) -> new UserObjectTypePair( userId, type ) )
                .collect( Collectors.toSet() );
        readableTypedUserObjects.executeOnKeys( uots,
                UpdateReadableTypedObjectsEP.removeFromReadableTypedObjects( objectId ) );
        Set<UserSettingsKey> userSettingsKeys = users.stream()
                .map( id -> new UserSettingsKey( id, UserSettings.SILENCED_CHANNELS ) )
                .collect( Collectors.toSet() );
        userSettings.executeOnKeys( userSettingsKeys,
                new RemoveFromUserSettingsEntryProcessor( ImmutableSet.of( objectId.toString() ) ) );
        return true;
    }

    @Override
    public boolean removeAccess( UUID user, Set<UUID> objectIds ) {
        if ( objectIds == null || objectIds.isEmpty() ) {
            return false;
        }

        Set<UserACLId> asAclIds = objectIds.stream()
                .filter( ( objectId ) -> objectId.equals( getAclIdForObject( objectId ) ) )
                .map( ( objectId ) -> new UserACLId( user, objectId ) )
                .collect( Collectors.toSet() );
        removeAccessForUserAcls( asAclIds );

        Map<UUID, Object> objectTypes = objectMetadataMap.executeOnKeys( objectIds,
                new GetObjectTypeEntryProcessor() );
        Map<UUID, UserObjectTypePair> toUserObjectTypePairs = Maps.transformValues( objectTypes,
                ( value ) -> new UserObjectTypePair( user, (UUID) value ) );
        for ( Entry<UUID, UserObjectTypePair> objectToType : toUserObjectTypePairs.entrySet() ) {
            readableTypedUserObjects.executeOnKey( objectToType.getValue(),
                    UpdateReadableTypedObjectsEP.removeFromReadableTypedObjects( objectToType.getKey() ) );
        }
        userSettings.executeOnKey( new UserSettingsKey( user, UserSettings.SILENCED_CHANNELS ),
                new RemoveFromUserSettingsEntryProcessor( objectIds.stream()
                        .map( id -> id.toString() )
                        .collect( Collectors.toSet() ) ) );
        return true;
    }

    private void removeAccessForUserAcls( Set<UserACLId> userAclIds ) {
        userAclToPermissionsMap.executeOnKeys( userAclIds,
                PermissionUpdatingEntryProcessor.removePermissions( EnumSet.<Permission> allOf( Permission.class ) ) );
    }

    @Override
    public Iterable<UUID> getUsersWithPermissonOnObject( Permission permission, UUID objectId ) {
        UUID aclIdForObject = getAclIdForObject( objectId );
        if ( aclIdForObject == null ) {
            return ImmutableList.of();
        }

        Set<UUID> safeGet = ServiceUtils.safeGet( reverseUserAclToPermssions,
                new ACLPermissionPair( aclIdForObject, permission ) );
        return safeGet;
    }

    @Override
    public boolean removeAccessForCurrentUser( Set<UUID> objectIds ) {
        UUID user = Users.getCurrentUserKey();
        return removeAccess( user, objectIds );
    }

    @Override
    public EnumSet<Permission> getUserPermissionsForObject( UUID userId, UUID objectId ) {
        EnumSet<Permission> permissions = EnumSet.<Permission> noneOf( Permission.class );
        UUID aclId = getAclIdForObject( objectId );
        if ( aclId == null ) {
            return permissions;
        }
        Set<Permission> perms = ServiceUtils.safeGet( userAclToPermissionsMap, new UserACLId( userId, aclId ) );
        permissions.addAll( perms );
        return permissions;
    }

    @Override
    public boolean checkUserHasPermissionOnObject( Permission permission, UUID userId, UUID objectId ) {
        UUID aclId = getAclIdForObject( objectId );
        if ( aclId == null ) {
            return false;
        }
        UserACLId userACLId = new UserACLId( userId, aclId );
        boolean hasPermission = (boolean) userAclToPermissionsMap.executeOnKey( userACLId,
                new PermissionCheckEntryProcessor( permission ) );
        return hasPermission;
    }

    private Iterable<UUID> getAclIdsForObjects( Set<UUID> objectIds ) {
        // Map<UUID, Object> aclIds = objectMetadataMap.executeOnKeys( objectIds, new GetACLIdEntryProcessor() );
        ArrayList<Future> futures = new ArrayList<>( objectIds.size() );
        for ( UUID uuid : objectIds ) {
            futures.add( objectMetadataMap.submitToKey( uuid, new GetACLIdEntryProcessor() ) );
        }

        Iterable<UUID> transform = Iterables.transform( futures, ( Future future ) -> {
            Object result = null;
            try {
                result = future.get();
            } catch ( Exception e ) {
                e.printStackTrace();
            }
            if ( result == null ) {
                return null;
            }
            return (UUID) result;
        } );
        return Iterables.filter( transform, Predicates.notNull() );
    }

    @CheckForNull
    private UUID getAclIdForObject( UUID objectId ) {
        Object aclId = objectMetadataMap.executeOnKey( objectId, new GetACLIdEntryProcessor() );
        if ( aclId == null ) {
            logger.error( "object {} has a null aclid", objectId );
            return null;
        }
        return (UUID) aclId;
    }

    @Override
    public boolean isSharingAllowed( UUID senderDomainId, UUID receiverDomainId ) {
        return getOutgoingSharesByEmailPermission( senderDomainId )
                && getIncomingSharesByEmailPermission( receiverDomainId );
    }

    @Override
    public boolean getOutgoingSharesByEmailPermission( UUID domainId ) {
        // Maybe need an EP later
        return !domainsWithPolicy.get( DomainPolicy.BlockOutgoingSharesByEmail ).contains( domainId );
    }

    @Override
    public boolean getIncomingSharesByEmailPermission( UUID domainId ) {
        // Maybe need an EP later
        return !domainsWithPolicy.get( DomainPolicy.BlockIncomingSharesByEmail ).contains( domainId );
    }

}
