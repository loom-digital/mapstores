package com.kryptnostic.mapstores.v2.services;

import java.util.EnumSet;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nonnull;

import com.kryptnostic.mapstores.v2.Permission;

public interface ObjectAuthorizationService {
    @Nonnull
    Set<UUID> getReadableObjects( UUID userId );

    @Nonnull
    Set<UUID> getReadableParents( UUID userId );

    @Nonnull
    Set<UUID> getWritableObjects( UUID userId );

    @Nonnull
    Set<UUID> getOwnedObjects( UUID userId );

    void setReadAccess( UUID userId, Set<UUID> objectIds );

    void setWriteAccess( UUID userId, Set<UUID> objectIds );

    void setOwner( UUID userId, Set<UUID> objectIds );

    EnumSet<Permission> getUserPermissionsForObject( UUID userId, UUID objectId );

    boolean checkUserHasPermissionOnObject( Permission permission, UUID userId, UUID objectId );

    boolean removeAccessForAllUsers( UUID objectId );

    boolean removeAccessForUsers( Set<UUID> users, UUID objectId );

    boolean removeAccess( UUID userId, Set<UUID> objectIds );

    boolean removeAccessForCurrentUser( Set<UUID> objectIds );

    Iterable<UUID> getUsersWithPermissonOnObject( Permission permission, UUID objectId );

    void addNewUser( UUID userId );
    
    boolean isSharingAllowed( UUID senderDomainId, UUID receiverDomainId );
    
    boolean getOutgoingSharesByEmailPermission( UUID domainId );

    boolean getIncomingSharesByEmailPermission( UUID domainId );
    
}
