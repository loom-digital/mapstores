package com.kryptnostic.mapstores.v2.test;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.kryptnostic.directory.v1.domain.Domain;
import com.kryptnostic.directory.v1.domain.DomainPolicy;
import com.kryptnostic.directory.v2.model.DomainResourcesUsage;
import com.kryptnostic.directory.v2.model.UserSettings;
import com.kryptnostic.heracles.directory.v1.objects.Emails;
import com.kryptnostic.heracles.directory.v2.objects.DomainResourcesKey;
import com.kryptnostic.kodex.v1.crypto.ciphers.BlockCiphertext;
import com.kryptnostic.kodex.v1.crypto.ciphers.Cypher;
import com.kryptnostic.kodex.v1.crypto.keys.Keys;
import com.kryptnostic.kodex.v1.models.Group;
import com.kryptnostic.kodex.v1.models.KryptnosticGroup;
import com.kryptnostic.mapstores.v2.mapstores.UserSettingsKey;
import com.kryptnostic.metrics.v1.MetricsUserStatsMetadata;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.v2.crypto.CryptoMaterial;
import com.kryptnostic.v2.search.IndexSegmentAddress;
import com.kryptnostic.v2.sharing.models.ObjectUserKey;
import com.kryptnostic.v2.sharing.models.Share;
import com.kryptnostic.v2.storage.models.CreateObjectRequest;
import com.kryptnostic.v2.storage.models.Event;
import com.kryptnostic.v2.storage.models.Event.PropagationDirections;
import com.kryptnostic.v2.storage.models.ObjectMetadata;
import com.kryptnostic.v2.storage.models.Scope;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;
import com.kryptnostic.v2.storage.models.VersionedObjectUserKey;

@VisibleForTesting
public class DataGeneratorFactory {

    private static final Map<Class<?>, Object> classData = Maps.newHashMap();

    static {
        classData.put( IndexSegmentAddress.class, new IndexSegmentAddress( newByteArray() ) );
        classData.put( Share.class, newShare() );
        classData.put( Emails.class, newEmails() );
        classData.put( Domain.class, new Domain( newUUID(), newString(), 0 ) );
        classData.put( Group.class,
                new KryptnosticGroup( newUUID(), newString(), ImmutableSet.of(), ImmutableSet.of() ) );
        classData.put( MetricsUserStatsMetadata.class, newMetricsUserStatsMetadata() );
        classData.put( LocalDate.class, new LocalDate() );
        classData.put( Scope.class, new Scope() );
        classData.put( VersionedObjectKey.class, newVersionedObjectKey() );
        classData.put( Integer.class, Integer.valueOf( 0 ) );
        classData.put( VersionedObjectUserKey.class, newVersionedObjectUserKey() );
        classData.put( BlockCiphertext.class, newBlockCiphertext() );
        classData.put( CryptoMaterial.class, newCryptoMat() );
        classData.put( Cypher.class, newCypher() );
        classData.put( UUID.class, UUID.randomUUID() );
        classData.put( byte[].class, newByteArray() );
        classData.put( String.class, newString() );
        classData.put( ObjectUserKey.class, newObjectUserKey() );
        classData.put( ObjectMetadata.class, newObjectMetadata() );
        classData.put( DomainResourcesKey.class, newDomainResourcesKey() );
        classData.put( DomainResourcesUsage.class, newDomainResourcesUsage() );
        classData.put( UUIDSet.class, newUUIDSet() );
        classData.put( DomainPolicy.class, newDomainPolicy() );
        classData.put( UserSettingsKey.class, newUserSettingsKey() );
        classData.put( Event.class, newEvent() );
    }

    public static <T> T createData( Class<T> clazz ) {
        return (T) classData.get( clazz );
    }

    private static Event newEvent() {
        return new Event(
                EnumSet.allOf( PropagationDirections.class ),
                Optional.of( System.currentTimeMillis() ),
                RandomStringUtils.random( 10 ) );
    }

    private static UUIDSet newUUIDSet() {
        Set<UUID> createSet = createSet( UUID.class );
        return new UUIDSet( createSet );
    }

    private static DomainResourcesUsage newDomainResourcesUsage() {
        return new DomainResourcesUsage( 1, 10 );
    }

    private static DomainResourcesKey newDomainResourcesKey() {
        return new DomainResourcesKey( newUUID(), newString() );
    }

    public static <T> Set<T> createSet( Class<T> clazz ) {
        T createData = createData( clazz );
        if ( createData == null ) {
            throw new RuntimeException( "Type " + clazz + " is missing a data generator" );
        }
        return ImmutableSet.of( createData );
    }

    public static ObjectMetadata newObjectMetadata() {
        return ObjectMetadata.newRootObject( new CreateObjectRequest(), newUUID(), newUUID() );
    }

    public static BlockCiphertext newBlockCiphertext() {
        return new BlockCiphertext( newByteArray(), newByteArray(), newByteArray() );
    }

    public static ObjectUserKey newObjectUserKey() {
        return new ObjectUserKey( newUUID(), newUUID() );
    }

    public static UUID newUUID() {
        return UUID.randomUUID();
    }

    public static PublicKey newPublicKey() throws NoSuchAlgorithmException {
        KeyPair pair = Keys.generateRsaKeyPair( 1024 );
        return pair.getPublic();
    }

    public static byte[] newByteArray() {
        return newString().getBytes();
    }

    public static String newString() {
        return RandomStringUtils.randomAlphanumeric( 10 );
    }

    public static Group newGroup() {
        return new KryptnosticGroup( UUID.randomUUID(), "doges_for_bernie", ImmutableSet.of( UUID.randomUUID(),
                UUID.randomUUID() ), ImmutableSet.of( UUID.randomUUID(), UUID.randomUUID() ) );
    }

    private static VersionedObjectUserKey newVersionedObjectUserKey() {
        return new VersionedObjectUserKey( newUUID(), newUUID(), 0l );
    }

    private static VersionedObjectKey newVersionedObjectKey() {
        return new VersionedObjectKey( UUID.randomUUID(), 0l );
    }

    private static Share newShare() {
        return new Share(
                newUUID(),
                newVersionedObjectKey(),
                Optional.of( newBlockCiphertext() ),
                newByteArray(),
                DateTime.now() );
    }

    private static Emails newEmails() {
        return new Emails();
    }

    private static MetricsUserStatsMetadata newMetricsUserStatsMetadata() {
        return new MetricsUserStatsMetadata( DataGeneratorFactory.newString() );
    }

    private static CryptoMaterial newCryptoMat() {
        return CryptoMaterial.IV;
    }

    private static Cypher newCypher() {
        return Cypher.DEFAULT;
    }

    private static DomainPolicy newDomainPolicy() {
        return DomainPolicy.Discoverable;
    }

    private static UserSettingsKey newUserSettingsKey() {
        return new UserSettingsKey( UUID.randomUUID(), UserSettings.SILENCED_CHANNELS );
    }

}
