package com.kryptnostic.mapstores.v2.utils;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.google.common.collect.Maps;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.kryptnostic.kodex.v1.crypto.ciphers.BlockCiphertext;
import com.kryptnostic.mapstores.v2.constants.HazelcastNames;

public class EncryptedObjectDistributedMap<K> {

    Logger                        logger = LoggerFactory.getLogger( EncryptedObjectDistributedMap.class );

    private final IMap<K, byte[]> tagMap;
    private final IMap<K, byte[]> ivMap;
    private final IMap<K, byte[]> contentMap;
    private final IMap<K, byte[]> saltMap;

    public EncryptedObjectDistributedMap( HazelcastInstance hazelcastInstance, String metaMapName ) {
        ivMap = hazelcastInstance.getMap( metaMapName + HazelcastNames.Suffixes.IV_MAP );
        tagMap = hazelcastInstance.getMap( metaMapName + HazelcastNames.Suffixes.TAG_MAP );
        saltMap = hazelcastInstance.getMap( metaMapName + HazelcastNames.Suffixes.SALT_MAP );
        contentMap = hazelcastInstance.getMap( metaMapName + HazelcastNames.Suffixes.CONTENT_MAP );
    }

    public int size() {
        return ivMap.size();
    }

    public boolean isEmpty() {
        return ivMap.isEmpty();
    }

    public boolean containsKey( K key ) {
        return ivMap.containsKey( key );
    }

    public BlockCiphertext get( K key ) {
        Future<byte[]> iv = ivMap.getAsync( key );
        Future<byte[]> salt = saltMap.getAsync( key );
        Future<byte[]> contents = contentMap.getAsync( key );
        Future<byte[]> tag = tagMap.getAsync( key );
        try {
            return new BlockCiphertext(
                    iv.get(),
                    salt.get(),
                    contents.get(),
                    Optional.absent(),
                    Optional.fromNullable( tag.get() ) );
        } catch ( InterruptedException | ExecutionException e ) {
            logger.error( "Async calls for encrypted content failed", e );
            throw new RuntimeException( e );
        }
    }

    public void set( K key, BlockCiphertext value ) {
        Optional<byte[]> tag = value.getTag();
        if ( tag.isPresent() ) {
            tagMap.putAsync( key, tag.get() );
        }

        ServiceUtils.async()
                .put( ivMap, key, value.getIv() )
                .put( saltMap, key, value.getSalt() )
                .put( contentMap, key, value.getContents() )
                .waitForCompletion();
    }

    public Map<K, BlockCiphertext> getAll( Set<K> keys ) {
        Map<K, BlockCiphertext> ret = Maps.newHashMapWithExpectedSize( keys.size() );
        Map<K, byte[]> ivs = ivMap.getAll( keys );
        Map<K, byte[]> tags = tagMap.getAll( keys );
        Map<K, byte[]> salts = saltMap.getAll( keys );
        Map<K, byte[]> contents = contentMap.getAll( keys );

        keys.stream().forEach( ( K key ) -> {
            byte[] iv = ivs.get( key );
            byte[] salt = salts.get( key );
            byte[] tag = tags.get( key );
            byte[] content = contents.get( key );
            BlockCiphertext ctxt = new BlockCiphertext(
                    iv,
                    salt,
                    content,
                    Optional.absent(),
                    Optional.fromNullable( tag ) );
            ret.put( key, ctxt );
        } );
        return ret;
    }

    public void setAll( Map<K, BlockCiphertext> values ) {
        Map<K, byte[]> ivs = Maps.newHashMapWithExpectedSize( values.size() );
        Map<K, byte[]> contents = Maps.newHashMapWithExpectedSize( values.size() );
        Map<K, byte[]> salts = Maps.newHashMapWithExpectedSize( values.size() );
        Map<K, byte[]> tags = Maps.newHashMapWithExpectedSize( values.size() );

        for (Map.Entry<K, BlockCiphertext> e : values.entrySet()) {
            K key = e.getKey();
            BlockCiphertext value = e.getValue();
            ivs.put( key, value.getIv() );
            contents.put( key, value.getContents() );
            salts.put( key, value.getSalt() );
            if (value.getTag().isPresent()) {
                tags.put( key, value.getTag().get() );
            }
        }

        ivMap.putAll( ivs );
        contentMap.putAll( contents );
        saltMap.putAll( salts );
        if (!tags.isEmpty()) {
            tagMap.putAll( tags );
        }
    }

    public void delete( K key ) {
        ServiceUtils.async()
                .remove( ivMap, key )
                .remove( tagMap, key )
                .remove( saltMap, key )
                .remove( contentMap, key )
                .waitForCompletion();
    }

    public Set<K> keySet() {
        return ivMap.keySet();
    }

    public byte[] getIvForObject( K key ) {
        return ivMap.get( key );
    }

    public byte[] getSaltForObject( K key ) {
        return saltMap.get( key );
    }

    public byte[] getContentForObject( K key ) {
        return contentMap.get( key );
    }

    public byte[] getTagForObject( K key ) {
        return tagMap.get( key );
    }

    public void setIvForObject( K key, byte[] iv ) {
        ivMap.set( key, iv );
    }

    public void setSaltForObject( K key, byte[] salt ) {
        saltMap.set( key, salt );
    }

    public void setContentForObject( K key, byte[] content ) {
        contentMap.set( key, content );
    }

    public void setTagForObject( K key, byte[] tag ) {
        tagMap.set( key, tag );
    }
}
