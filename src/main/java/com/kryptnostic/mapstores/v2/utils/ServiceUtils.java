package com.kryptnostic.mapstores.v2.utils;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.MultiMap;
import com.hazelcast.map.EntryProcessor;
import com.kryptnostic.heracles.directory.v1.Users;
import com.kryptnostic.kodex.v1.crypto.ciphers.BlockCiphertext;
import com.kryptnostic.mapstores.v2.entryprocessors.GetObjectVersionEntryProcessor;
import com.kryptnostic.v2.crypto.CryptoMaterial;
import com.kryptnostic.v2.sharing.models.ObjectUserKey;
import com.kryptnostic.v2.storage.models.ObjectMetadata;
import com.kryptnostic.v2.storage.models.VersionedObjectKey;

public final class ServiceUtils {

    public static final UUID    MIN_TIME_UUID = UUID.fromString( "00000000-0000-1000-8000-000000000000" );
    public static final UUID    MAX_TIME_UUID = UUID.fromString( "ffffffff-ffff-1fff-bfff-ffffffffffff" );

    private static final Logger logger        = LoggerFactory.getLogger( ServiceUtils.class );

    private ServiceUtils() { /* Utility class, do not instantiate */ }

    public static <K, V> Set<V> getNonnull( MultiMap<K, V> map, K id ) {
        Collection<V> collection = map.get( id );
        if ( collection == null ) {
            collection = ImmutableSet.of();
        }
        return (Set<V>) collection;
    }

    @Nonnull
    public static <K, V extends Set<?>> V getReadOnlyNonnull( IMap<K, V> map, K id ) {
        V set = ServiceUtils.safeGet( map, id );
        if ( set == null ) {
            set = (V) ImmutableSet.of();
        }
        return set;
    }

    @Nonnull
    public static <K, V extends Set<?>> V getMutableNonnull( IMap<K, V> map, K id ) {
        V set = ServiceUtils.safeGet( map, id );
        if ( set == null ) {
            set = (V) Sets.newHashSet();
        }
        return set;
    }

    @Nonnull
    public static <K, V extends Set<?>> V getNonnull( IMap<K, V> map, K id ) {
        return getReadOnlyNonnull( map, id );
    }

    public static <K, V> void safeDeleteFromMap( K key, IMap<K, V> map ) {
        map.lock( key );
        try {
            map.delete( key );
        } finally {
            map.unlock( key );
        }
    }

    public static <K> void updateByteArrayMapEntry(
            K key,
            byte[] contents,
            IMap<K, byte[]> map ) {
        // I've been thinking about this and I'm pretty sure we don't actually have to lock here
        // since the byte[] maps are write once read forever
        map.lock( key );
        try {
            map.set( key, contents );
        } finally {
            map.unlock( key );
        }
    }

    public static <K> BlockCiphertext getFromMapsAsBlockCipherText( K key, Map<CryptoMaterial, IMap<K, byte[]>> maps ) {
        BlockCiphertext.Builder builder = new BlockCiphertext.Builder();
        for ( Entry<CryptoMaterial, IMap<K, byte[]>> entry : maps.entrySet() ) {
            IMap<K, byte[]> map = entry.getValue();
            byte[] bytes = map.get( key );
            switch ( entry.getKey() ) {
                case CONTENTS:
                    builder.setContents( bytes );
                    break;
                case IV:
                    builder.setIv( bytes );
                    break;
                case SALT:
                    builder.setSalt( bytes );
                    break;
                case TAG:
                    builder.setTag( bytes );
                    break;
                default:
                    throw new RuntimeException();
            }
        }
        return builder.createBlockCiphertext();
    }

    public static Set<ObjectUserKey> transformToOUKs( Set<UUID> objectIds ) {
        Set<ObjectUserKey> transformedToKeys = Sets.newHashSetWithExpectedSize( objectIds.size() );
        for ( UUID objectId : objectIds ) {
            transformedToKeys.add( new ObjectUserKey( objectId, Users.getCurrentUserKey() ) );
        }
        return transformedToKeys;
    }

    public static VersionedObjectKey getLatestVersionedObjectKeyForObject(
            IMap<UUID, ObjectMetadata> map,
            UUID objectId ) {
        long version = ( (Long) map.executeOnKey( objectId, new GetObjectVersionEntryProcessor() ) ).longValue();
        return new VersionedObjectKey( objectId, version );
    }

    public static <K, V> V safeGet( IMap<K, V> map, K key ) {
        return map.get( key );
    }

    public static <K> BlockCiphertext safeGet( EncryptedObjectDistributedMap<K> map, K key ) {
        return map.get( key );
    }

    public static <K, V> V safeRemove( IMap<K, V> map, K key ) {
        return map.remove( key );
    }

    public static <K, V> boolean safeContainsKey( IMap<K, V> map, K key ) {
        return map.containsKey( key );
    }

    public static <K, V> void safeDelete( IMap<K, V> map, K key ) {
        map.delete( key );
    }

    public static <K, V> Future<V> safeAsyncDelete( IMap<K, V> map, K key ) {
        return map.removeAsync( key );
    }

    public static <K> void safeDelete( EncryptedObjectDistributedMap<K> map, K key ) {
        map.delete( key );
    }

    public static <K, V> IMap<K, V> safeGetMap( HazelcastInstance hazelcastInstance, String mapName ) {
        return hazelcastInstance.getMap( mapName );
    }

    public static boolean allDone( Future[] futures ) {
        for ( Future f : futures ) {
            if ( !f.isDone() ) {
                return false;
            }
        }
        return true;
    }

    public interface ChainedAsyncUtil {
        void waitForCompletion();
    }

    public interface SelectingChainedAsyncUtil extends ChainedAsyncUtil {
        <K, V> RemoveChainedAsyncUtil remove( IMap<K, V> map, K key );

        <K, V> AddChainedAsyncUtil put( IMap<K, V> map, K key, V value );

        <K, V> AddChainedAsyncUtil submitAsync( IMap<K, V> map, K key, EntryProcessor<K, V> entryProcessor );
    }

    public interface RemoveChainedAsyncUtil extends ChainedAsyncUtil {
        <K, V> RemoveChainedAsyncUtil remove( IMap<K, V> map, K key );
    }

    public interface AddChainedAsyncUtil extends ChainedAsyncUtil {
        <K, V> AddChainedAsyncUtil put( IMap<K, V> map, K key, V value );

        <K, V> AddChainedAsyncUtil submit( IMap<K, V> map, K key, EntryProcessor<K, V> entryProcessor );
    }

    public static SelectingChainedAsyncUtil async() {
        return new BranchChainUtil();
    }

    private static final class BranchChainUtil implements SelectingChainedAsyncUtil {
        public BranchChainUtil() {}

        @Override
        public void waitForCompletion() {
            return;
        }

        @Override
        public <K, V> RemoveChainedAsyncUtil remove( IMap<K, V> map, K key ) {
            return new RemoveChainUtil().remove( map, key );
        }

        @Override
        public <K, V> AddChainedAsyncUtil put( IMap<K, V> map, K key, V value ) {
            return new AddChainUtil().put( map, key, value );
        }

        @Override
        public <K, V> AddChainedAsyncUtil submitAsync( IMap<K, V> map, K key, EntryProcessor<K, V> entryProcessor ) {
            return new AddChainUtil().submit( map, key, entryProcessor );
        }
    }

    private static final class RemoveChainUtil implements RemoveChainedAsyncUtil {
        List<Tuple<IMap, Object>> imapsAndKeysToDeleteFrom = new LinkedList<>();

        public RemoveChainUtil() {}

        @Override
        public <K, V> RemoveChainedAsyncUtil remove( IMap<K, V> map, K key ) {
            imapsAndKeysToDeleteFrom.add( new Tuple<IMap, Object>( map, key ) );
            return this;
        }

        @Override
        public void waitForCompletion() {
            Future[] futures = new Future[ imapsAndKeysToDeleteFrom.size() ];
            int i = 0;
            for ( Tuple<IMap, Object> pair : imapsAndKeysToDeleteFrom ) {
                futures[ i ] = pair.getKey().removeAsync( pair.getVal() );
                i++;
            }
            ServiceUtils.waitUntilDone( futures );
        }
    }

    private static final class AddChainUtil implements AddChainedAsyncUtil {
        List<Tuple<IMap, Tuple<Object, Object>>>         imapsAndKeyValsToAdd    = new LinkedList<>();
        List<Tuple<IMap, Tuple<Object, EntryProcessor>>> entryProcessorsToSubmit = new LinkedList<>();

        public AddChainUtil() {}

        @Override
        public <K, V> AddChainedAsyncUtil put( IMap<K, V> map, K key, V value ) {
            imapsAndKeyValsToAdd.add( new Tuple<>( map, new Tuple<>( key, value ) ) );
            return this;
        }

        @Override
        public <K, V> AddChainedAsyncUtil submit( IMap<K, V> map, K key, EntryProcessor<K, V> entryProcessor ) {
            entryProcessorsToSubmit.add( new Tuple<>( map, new Tuple<>( key, entryProcessor ) ) );
            return this;
        }

        @Override
        public void waitForCompletion() {
            Future[] futures = new Future[ imapsAndKeyValsToAdd.size() + entryProcessorsToSubmit.size() ];
            int i = 0;
            for ( Tuple<IMap, Tuple<Object, Object>> pair : imapsAndKeyValsToAdd ) {
                Tuple<Object, Object> val = pair.getVal();
                futures[ i ] = pair.getKey().putAsync( val.getKey(), val.getVal() );
                i++;
            }
            for ( Tuple<IMap, Tuple<Object, EntryProcessor>> pair : entryProcessorsToSubmit ) {
                Tuple<Object, EntryProcessor> val = pair.getVal();
                futures[ i ] = pair.getKey().submitToKey( val.getKey(), val.getVal() );
                i++;
            }
            waitUntilDone( futures );
        }
    }

    private static final class Tuple<K, V> {
        private K key;
        private V val;

        public Tuple( K key, V val ) {
            this.key = key;
            this.val = val;
        }

        public K getKey() {
            return key;
        }

        public V getVal() {
            return val;
        }
    }

    public static void waitUntilDone( Future[] futures ) {
        try {
            for ( Future f : futures ) {
                f.get();
            }
        } catch ( InterruptedException | ExecutionException e ) {
            logger.error( "Failure while waiting on an async future", e );
        }
    }
}
