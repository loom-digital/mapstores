package com.kryptnostic.metrics.v1;

import org.joda.time.LocalDate;

import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;

public class MetricsDateKeyMapper implements SelfRegisteringKeyMapper<LocalDate>{
    @Override
    public String fromKey( LocalDate date ) {
        return date.toString();
    }

    @Override
    public LocalDate toKey( String value ) {
        return LocalDate.parse( value );
    }

    @Override
    public Class<LocalDate> getClazz() {
        return LocalDate.class;
    }
}
