package com.kryptnostic.metrics.v1;

import java.util.Map;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kryptnostic.instrumentation.v1.constants.InstrumentationConstants;
import com.kryptnostic.instrumentation.v1.constants.MetricsField;
import com.kryptnostic.instrumentation.v1.constants.MetricsIndex;
import com.kryptnostic.instrumentation.v1.constants.MetricsType;

public class MetricsLogRecord {
    Map<MetricsField, Object> map;
    MetricsIndex              index;
    MetricsType               type;
    String                    uuid;

    @JsonCreator
    public MetricsLogRecord(
            @JsonProperty( InstrumentationConstants.MESSAGE_FIELD ) String msg,
            @JsonProperty( InstrumentationConstants.MAP_FIELD ) Map<MetricsField, Object> map,
            @JsonProperty( InstrumentationConstants.INDEX_FIELD ) MetricsIndex index,
            @JsonProperty( InstrumentationConstants.TYPE_FIELD ) MetricsType type,
            @JsonProperty( InstrumentationConstants.USER_UUID_FIELD ) String uuid ) {
        map.put( MetricsField.MESSAGE_FIELD, msg );
        new DateTime();
        map.put( MetricsField.DATE_FIELD, DateTime.now() );
        this.map = map;
        this.index = index;
        this.type = type;
        this.uuid = uuid;

    }

    public MetricsIndex getIndex() {
        return index;
    }

    public MetricsType getType() {
        return type;
    }

    public Map<MetricsField, Object> getMap() {
        return map;
    }

    public String getUUID() {
        return uuid;
    }

}