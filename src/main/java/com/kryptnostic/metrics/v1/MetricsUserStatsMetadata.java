package com.kryptnostic.metrics.v1;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kryptnostic.instrumentation.v1.constants.InstrumentationConstants;

public class MetricsUserStatsMetadata implements Serializable {

    private static final long serialVersionUID = -8038252826514593142L;
    protected String          domain;
    
    @JsonCreator
    public MetricsUserStatsMetadata( @JsonProperty( InstrumentationConstants.DOMAIN_FIELD ) String domain ) {
        this.domain = domain;
    }
    
    public String getDomain() {
        return domain;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( domain == null ) ? 0 : domain.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( getClass() != obj.getClass() ) return false;
        MetricsUserStatsMetadata other = (MetricsUserStatsMetadata) obj;
        if ( domain == null ) {
            if ( other.domain != null ) return false;
        } else if ( !domain.equals( other.domain ) ) return false;
        return true;
    }
    
    @Override
    public String toString() {
        return "MetricsUserStatsMetadata [domain=" + domain + "]";
    }
    
}
