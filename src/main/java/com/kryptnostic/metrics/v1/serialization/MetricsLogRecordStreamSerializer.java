package com.kryptnostic.metrics.v1.serialization;

import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.metrics.v1.MetricsLogRecord;
import com.kryptnostic.services.v1.serialization.JacksonStreamSerializer;

public class MetricsLogRecordStreamSerializer extends JacksonStreamSerializer<MetricsLogRecord> {

    public MetricsLogRecordStreamSerializer() {
        super( MetricsLogRecord.class );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.LOG_RECORD.ordinal();
    }

}
