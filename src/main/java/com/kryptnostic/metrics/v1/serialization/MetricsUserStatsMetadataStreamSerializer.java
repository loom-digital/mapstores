package com.kryptnostic.metrics.v1.serialization;

import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.metrics.v1.MetricsUserStatsMetadata;
import com.kryptnostic.services.v1.serialization.JacksonStreamSerializer;

public class MetricsUserStatsMetadataStreamSerializer extends JacksonStreamSerializer<MetricsUserStatsMetadata> {

    public MetricsUserStatsMetadataStreamSerializer() {
        super( MetricsUserStatsMetadata.class );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.USERSTATS_METADATA.ordinal();
    }

}
