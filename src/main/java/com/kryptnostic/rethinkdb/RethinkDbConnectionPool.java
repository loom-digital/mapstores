package com.kryptnostic.rethinkdb;

import java.util.Queue;
import java.util.concurrent.TimeoutException;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.collect.Queues;
import com.kryptnostic.rhizome.configuration.rethinkdb.RethinkDbConfiguration;
import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;

public class RethinkDbConnectionPool {
    private final Queue<Connection>      clients = Queues.newConcurrentLinkedQueue();
    private final RethinkDbConfiguration config;
    private final static RethinkDB       rdb     = RethinkDB.r;

    public RethinkDbConnectionPool( @Nonnull RethinkDbConfiguration config ) throws TimeoutException {
        this.config = Preconditions.checkNotNull( config, "RethinkDb configuration shouldn't be null." );
        for ( int i = 0; i < 10; ++i ) {
            clients.add( newConnection() );
        }
    }

    @Nonnull
    public Connection acquire() throws TimeoutException {
        Connection connection = clients.poll();
        if ( connection == null ) {
            return newConnection();
        } else {
            return connection;
        }
    }

    public void release( @Nonnull Connection connection ) {
        clients.add( connection );
    }

    protected Connection newConnection() throws TimeoutException {
        return rdb.connection().hostname( config.getHostname() ).port( config.getPort() ).connect();
    }

}
