package com.kryptnostic.search.v1.processors;

import java.util.Arrays;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.kryptnostic.directory.v1.model.ObjectUserKey;
import com.kryptnostic.indexing.v1.ObjectSearchPair;
import com.kryptnostic.krypto.engine.KryptnosticEngine;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;

public class SearchEntryProcessor extends AbstractRhizomeEntryProcessor<ObjectUserKey, ObjectSearchPair, byte[]> {

    private static final long                 serialVersionUID = 3175158754029487078L;
    private static final Logger               logger           = LoggerFactory.getLogger( SearchEntryProcessor.class );
    private transient final KryptnosticEngine kryptEngine;
    private final byte[]                      clientHashFunction;
    private final byte[]                      encryptedSearchToken;

    public SearchEntryProcessor( byte[] chf, byte[] est ) {
        this.clientHashFunction = Preconditions.checkNotNull( chf, "ClientHashFunction was null" );
        this.encryptedSearchToken = Preconditions.checkNotNull( est, "EncryptedSearchToken was null" );
        this.kryptEngine = new KryptnosticEngine( clientHashFunction, encryptedSearchToken );
    }

    @Override
    public byte[] process( Entry<ObjectUserKey, ObjectSearchPair> entry ) {
        ObjectSearchPair sip = entry.getValue();
        if ( sip == null ) {
            logger.warn( "Encountered null search pair!" );
            return null;
        }
        // call getAddress(sharing type name = new type( arguments );pair)
        byte[] address = kryptEngine.calculateMetadataAddress( sip.getSearchPair() );
        return address;
    }

    public byte[] getEncryptedSearchToken() {
        return encryptedSearchToken;
    }

    public byte[] getClientHashFunction() {
        return clientHashFunction;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode( clientHashFunction );
        result = prime * result + Arrays.hashCode( encryptedSearchToken );
        result = prime * result + ( ( kryptEngine == null ) ? 0 : kryptEngine.hashCode() );
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( getClass() != obj.getClass() ) return false;
        SearchEntryProcessor other = (SearchEntryProcessor) obj;
        if ( !Arrays.equals( clientHashFunction, other.clientHashFunction ) ) return false;
        if ( !Arrays.equals( encryptedSearchToken, other.encryptedSearchToken ) ) return false;
        return true;
    }

}
