package com.kryptnostic.services.v1.serialization;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.heracles.authentication.v1.processors.AddUserToDomainEntryProcessor;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class AddUserToDomainEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<AddUserToDomainEntryProcessor> {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.ADD_USER_TO_DOMAIN_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public void write( ObjectDataOutput out, AddUserToDomainEntryProcessor object ) throws IOException {
        Iterable<UUID> collection = object.getBackingCollection();
        SetStreamSerializers.fastUUIDSetSerialize( out, collection );
    }

    @Override
    public AddUserToDomainEntryProcessor read( ObjectDataInput in ) throws IOException {
        UUIDSet collection = SetStreamSerializers.fastUUIDSetDeserialize( in );
        return new AddUserToDomainEntryProcessor( collection );
    }

    @Override
    public Class<AddUserToDomainEntryProcessor> getClazz() {
        return AddUserToDomainEntryProcessor.class;
    }

}
