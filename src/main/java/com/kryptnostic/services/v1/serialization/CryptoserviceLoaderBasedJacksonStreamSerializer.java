package com.kryptnostic.services.v1.serialization;

import com.google.common.base.Preconditions;
import com.kryptnostic.kodex.v1.crypto.keys.CryptoServiceLoader;
import com.kryptnostic.kodex.v1.serialization.jackson.KodexObjectMapperFactory;
import com.kryptnostic.rhizome.serializers.AbstractJacksonStreamSerializer;

/**
 * TODO figure out where the underlying serializers for this were used. This shouldn't be a thing at all
 * @author drew
 *
 * @param <T>
 */
public abstract class CryptoserviceLoaderBasedJacksonStreamSerializer<T> extends AbstractJacksonStreamSerializer<T> {

    public CryptoserviceLoaderBasedJacksonStreamSerializer( Class<T> clazz ) {
        super( clazz, KodexObjectMapperFactory.getSmileMapper() );
    }

    CryptoserviceLoaderBasedJacksonStreamSerializer( Class<T> clazz, CryptoServiceLoader loader ) {
        super( clazz, KodexObjectMapperFactory.getSmileMapper(
                Preconditions.checkNotNull(loader,"Cryptoservice loader cannot be null." ) ) );
    }

}
