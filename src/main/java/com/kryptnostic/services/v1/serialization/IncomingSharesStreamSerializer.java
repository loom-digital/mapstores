package com.kryptnostic.services.v1.serialization;

import java.io.IOException;
import java.util.Map;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.sharing.v1.models.IncomingShares;
import com.kryptnostic.sharing.v1.models.Share;

public class IncomingSharesStreamSerializer implements SelfRegisteringStreamSerializer<IncomingShares> {

    @Override
    public void write( ObjectDataOutput out, IncomingShares object ) throws IOException {
        out.writeInt( object.size() );
        for ( Map.Entry<String, Share> entry : object.entrySet() ) {
            out.writeUTF( entry.getKey() );
            ShareStreamSerializer.serialize( out, entry.getValue() );
        }
    }

    @Override
    public IncomingShares read( ObjectDataInput in ) throws IOException {
        int size = in.readInt();
        IncomingShares incomingShares = new IncomingShares( size );
        for ( int i = 0; i < size; ++i ) {
            incomingShares.put( in.readUTF(), ShareStreamSerializer.deserialize( in ) );
        }

        return incomingShares;
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.INCOMING_SHARES.ordinal();
    }

    @Override
    public void destroy() {

    }

    @Override
    public Class<IncomingShares> getClazz() {
        return IncomingShares.class;
    }

}
