package com.kryptnostic.services.v1.serialization;

import com.kryptnostic.kodex.v1.serialization.jackson.KodexObjectMapperFactory;
import com.kryptnostic.rhizome.serializers.AbstractJacksonStreamSerializer;

public abstract class JacksonStreamSerializer<T> extends AbstractJacksonStreamSerializer<T> {

    protected JacksonStreamSerializer( Class<T> clazz ) {
        super( clazz, KodexObjectMapperFactory.getSmileMapper() );
    }

}
