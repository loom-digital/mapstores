package com.kryptnostic.services.v1.serialization;

import java.io.IOException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.StreamSerializer;
import com.kryptnostic.kodex.v1.serialization.jackson.KodexObjectMapperFactory;

public abstract class JacksonTypeRefStreamSerializer<T> implements StreamSerializer<T> {
    private static ObjectMapper    mapper = KodexObjectMapperFactory.getSmileMapper();
    private final TypeReference<T> ref;

    protected JacksonTypeRefStreamSerializer( TypeReference<T> ref ) {
        this.ref = ref;
    }

    @Override
    public void destroy() {}

    @Override
    public void write( ObjectDataOutput out, T object ) throws IOException {
        byte[] bytes = mapper.writeValueAsBytes( object );
        out.writeInt( bytes.length );
        out.write( bytes );
    }

    @Override
    public T read( ObjectDataInput in ) throws IOException {
        byte[] bytes = new byte[ in.readInt() ];
        in.readFully( bytes );
        return mapper.readValue( bytes, ref );
    }

}
