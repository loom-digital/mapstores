package com.kryptnostic.services.v1.serialization;

import com.kryptnostic.kodex.v1.crypto.keys.CryptoServiceLoader;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.sharing.v1.models.NotificationPreference;

public class NotificationPreferenceStreamSerializer extends CryptoserviceLoaderBasedJacksonStreamSerializer<NotificationPreference> {

    public NotificationPreferenceStreamSerializer() {
        super( NotificationPreference.class );
    }

    NotificationPreferenceStreamSerializer( CryptoServiceLoader loader ) {
        super( NotificationPreference.class, loader );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.NOTIFICATION_PREFERENCE.ordinal();
    }

}