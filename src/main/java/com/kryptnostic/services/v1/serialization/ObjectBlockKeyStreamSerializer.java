package com.kryptnostic.services.v1.serialization;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.users.v1.ObjectBlockKey;

public class ObjectBlockKeyStreamSerializer implements SelfRegisteringStreamSerializer<ObjectBlockKey> {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.DOCUMENT_BLOCK_KEY.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public void write( ObjectDataOutput out, ObjectBlockKey object ) throws IOException {
        out.writeUTF( object.getObjectId() );
        out.writeInt( object.getVersion() );
        out.writeInt( object.getBlockIndex() );
    }

    @Override
    public ObjectBlockKey read( ObjectDataInput in ) throws IOException {
        String id = in.readUTF();
        int version = in.readInt();
        int blockIndex = in.readInt();
        return new ObjectBlockKey( id, blockIndex, version );
    }

    @Override
    public Class<ObjectBlockKey> getClazz() {
        return ObjectBlockKey.class;
    }
}
