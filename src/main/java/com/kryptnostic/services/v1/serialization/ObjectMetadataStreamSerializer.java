package com.kryptnostic.services.v1.serialization;

import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.storage.v1.models.ObjectMetadata;

@Deprecated
public class ObjectMetadataStreamSerializer extends JacksonStreamSerializer<ObjectMetadata> {

    public ObjectMetadataStreamSerializer() {
        super( ObjectMetadata.class );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.DOCUMENT_METADATA.ordinal();
    }

}
