package com.kryptnostic.services.v1.serialization;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.indexing.v1.ObjectSearchPair;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class ObjectSearchPairStreamSerializer implements SelfRegisteringStreamSerializer<ObjectSearchPair> {

    @Override
    public void write( ObjectDataOutput out, ObjectSearchPair object ) throws IOException {
        out.writeByteArray( object.getSearchPair() );

    }

    @Override
    public ObjectSearchPair read( ObjectDataInput in ) throws IOException {
        return new ObjectSearchPair( in.readByteArray() );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.INDEX_PAIR.ordinal();
    }

    @Override
    public void destroy() {

    }

    @Override
    public Class<ObjectSearchPair> getClazz() {
        return ObjectSearchPair.class;
    }

}
