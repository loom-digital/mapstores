package com.kryptnostic.services.v1.serialization;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.directory.v1.model.ObjectUserKey;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class ObjectUserKeyStreamSerializer implements SelfRegisteringStreamSerializer<ObjectUserKey> {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.OBJECT_USER_KEY.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public void write( ObjectDataOutput out, ObjectUserKey object ) throws IOException {
        out.writeUTF( object.getObjectId() );
        out.writeLong( object.getUserKey().getMostSignificantBits() );
        out.writeLong( object.getUserKey().getLeastSignificantBits() );
    }

    @Override
    public ObjectUserKey read( ObjectDataInput in ) throws IOException {
        String id = in.readUTF();
        long msb = in.readLong();
        long lsb = in.readLong();

        return new ObjectUserKey( id, new UUID( msb, lsb ) );
    }

    @Override
    public Class<ObjectUserKey> getClazz() {
        return ObjectUserKey.class;
    }
}
