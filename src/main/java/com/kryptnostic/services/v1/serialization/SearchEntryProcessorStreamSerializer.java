package com.kryptnostic.services.v1.serialization;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.search.v1.processors.SearchEntryProcessor;

public class SearchEntryProcessorStreamSerializer implements SelfRegisteringStreamSerializer<SearchEntryProcessor> {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.SEARCH_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public void write( ObjectDataOutput out, SearchEntryProcessor object ) throws IOException {
        out.writeByteArray( object.getClientHashFunction() );
        out.writeByteArray( object.getEncryptedSearchToken() );
    }

    @Override
    public SearchEntryProcessor read( ObjectDataInput in ) throws IOException {
        byte[] clientHashFunction = in.readByteArray();
        byte[] encryptedSearchToken = in.readByteArray();
        return new SearchEntryProcessor( clientHashFunction, encryptedSearchToken );
    }

    @Override
    public Class<SearchEntryProcessor> getClazz() {
        return SearchEntryProcessor.class;
    }

}
