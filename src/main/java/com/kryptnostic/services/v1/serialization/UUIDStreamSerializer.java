package com.kryptnostic.services.v1.serialization;

import java.io.IOException;
import java.util.UUID;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;

public class UUIDStreamSerializer implements SelfRegisteringStreamSerializer<UUID> {

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.UUID.ordinal();
    }

    @Override
    public void destroy() {

    }

    @Override
    public void write( ObjectDataOutput out, UUID object ) throws IOException {
        serialize( out, object );
    }

    @Override
    public UUID read( ObjectDataInput in ) throws IOException {
        return deserialize( in );
    }

    public static void serialize( ObjectDataOutput out, UUID object ) throws IOException {
        out.writeLong( object.getLeastSignificantBits() );
        out.writeLong( object.getMostSignificantBits() );
    }

    public static UUID deserialize( ObjectDataInput in ) throws IOException {
        long lsb = in.readLong();
        long msb = in.readLong();
        return new UUID( msb, lsb );
    }

    @Override
    public Class<UUID> getClazz() {
        return UUID.class;
    }
}
