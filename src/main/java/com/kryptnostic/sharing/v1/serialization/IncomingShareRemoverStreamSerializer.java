package com.kryptnostic.sharing.v1.serialization;

import java.io.IOException;
import java.util.Set;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.rhizome.hazelcast.serializers.SetStreamSerializers;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.sharing.v1.services.IncomingShareRemover;

public class IncomingShareRemoverStreamSerializer implements SelfRegisteringStreamSerializer<IncomingShareRemover> {

    @Override
    public void write( ObjectDataOutput out, IncomingShareRemover object ) throws IOException {
        SetStreamSerializers.serialize( out, object.getObjectIds(), ( String s ) -> {
            out.writeUTF( s );
        } );
    }

    @Override
    public IncomingShareRemover read( ObjectDataInput in ) throws IOException {
        Set<String> objectIds = SetStreamSerializers.deserialize( in, ( ObjectDataInput input ) -> {
            return input.readUTF();
        } );
        return new IncomingShareRemover( objectIds );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.INCOMING_SHARE_REMOVER.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<IncomingShareRemover> getClazz() {
        return IncomingShareRemover.class;
    }

}
