package com.kryptnostic.sharing.v1.serialization;

import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.services.v1.serialization.JacksonStreamSerializer;
import com.kryptnostic.sharing.v1.models.response.KeyUpdateResponse;

public class KeyUpdateResponseStreamSerializer extends JacksonStreamSerializer<KeyUpdateResponse> {
    public KeyUpdateResponseStreamSerializer() {
        super( KeyUpdateResponse.class );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.KEY_UPDATE_RESPONSE.ordinal();
    }
}
