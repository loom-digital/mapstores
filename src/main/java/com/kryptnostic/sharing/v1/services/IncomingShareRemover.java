package com.kryptnostic.sharing.v1.services;

import java.util.Collections;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import com.google.common.collect.ImmutableSet;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.sharing.v1.models.IncomingShares;

public class IncomingShareRemover extends AbstractRhizomeEntryProcessor<UUID, IncomingShares, Void> {
    private static final long serialVersionUID = 7919090128232882782L;
    private final Set<String> objectIds;

    public IncomingShareRemover( String objectId ) {
        objectIds = ImmutableSet.of( objectId );
    }

    public IncomingShareRemover( Set<String> objectIds ) {
        this.objectIds = objectIds;
    }

    @Override
    public Void process( Entry<UUID, IncomingShares> entry ) {
        IncomingShares shares = entry.getValue();
        if ( shares != null ) {
            shares.keySet().removeAll( objectIds );
            entry.setValue( shares );
        }
        return null;
    }

    public Set<String> getObjectIds() {
        return Collections.unmodifiableSet( objectIds );
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( objectIds == null ) ? 0 : objectIds.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) {
            return true;
        }
        if ( obj == null ) {
            return false;
        }
        if ( !( obj instanceof IncomingShareRemover ) ) {
            return false;
        }
        IncomingShareRemover other = (IncomingShareRemover) obj;
        if ( objectIds == null ) {
            if ( other.objectIds != null ) {
                return false;
            }
        } else if ( !objectIds.equals( other.objectIds ) ) {
            return false;
        }
        return true;
    }

}
