package com.kryptnostic.storage.v1.services.document.mappers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kryptnostic.directory.v1.model.ObjectUserKey;
import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;

public class ObjectUserKeyMapper implements SelfRegisteringKeyMapper<ObjectUserKey> {
    private static final Logger logger = LoggerFactory.getLogger( ObjectUserKeyMapper.class );
    @Override
    public String fromKey( ObjectUserKey key ) {
        return key.toString();
    }

    @Override
    public ObjectUserKey toKey( String value ) {
        return ObjectUserKey.fromString( value );
    }

    @Override
    public Class<ObjectUserKey> getClazz() {
        return ObjectUserKey.class;
    }
}
