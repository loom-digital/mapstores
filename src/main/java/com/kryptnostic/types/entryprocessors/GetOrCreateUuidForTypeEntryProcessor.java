package com.kryptnostic.types.entryprocessors;

import java.util.Map.Entry;
import java.util.UUID;

import javax.annotation.Nonnull;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.IMap;
import com.kryptnostic.mapstores.v2.constants.HazelcastNames.Maps;
import com.kryptnostic.rhizome.hazelcast.processors.AbstractRhizomeEntryProcessor;
import com.kryptnostic.v2.storage.models.Scope;

public class GetOrCreateUuidForTypeEntryProcessor extends AbstractRhizomeEntryProcessor<String, Scope, UUID> {
    private static final long serialVersionUID = 7788400302833749086L;

    private final String                 type;
    private transient IMap<UUID, String> typesToScopes;

    /**
     * This constructor should only be used when it is guaranteed to be run through the stream serializer first. The
     * most common case for this is when the HazelcastInstance is being run as a client.
     *
     * @param type
     */
    public GetOrCreateUuidForTypeEntryProcessor( String type ) {
        this( type, Hazelcast.getAllHazelcastInstances().iterator().next().getMap( Maps.TYPES_TO_SCOPES ) );
    }

    public GetOrCreateUuidForTypeEntryProcessor( String type, IMap<UUID, String> typesToScopes ) {
        this.type = type;
        this.typesToScopes = typesToScopes;
    }

    @Override
    public UUID process( Entry<String, Scope> entry ) {
        Scope scope = entry.getValue();
        if ( scope == null ) {
            scope = new Scope();
        }

        UUID typeId = scope.get( type );
        if ( typeId == null ) {
            typeId = generateUniqueUUID( entry.getKey() );
            scope.put( type, typeId );
            entry.setValue( scope );
        }

        return typeId;
    }

    private UUID generateUniqueUUID( String scopeName ) {
        UUID newId = UUID.randomUUID();
        while ( typesToScopes.putIfAbsent( newId, scopeName ) != null ) {
            newId = UUID.randomUUID();
        }
        return newId;
    }

    @Nonnull
    public String getType() {
        return type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( type == null ) ? 0 : type.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( !( obj instanceof GetOrCreateUuidForTypeEntryProcessor ) ) return false;
        GetOrCreateUuidForTypeEntryProcessor other = (GetOrCreateUuidForTypeEntryProcessor) obj;
        if ( type == null ) {
            if ( other.type != null ) return false;
        } else if ( !type.equals( other.type ) ) return false;
        return true;
    }

}