package com.kryptnostic.types.serializers;

import java.io.IOException;
import java.util.UUID;

import javax.inject.Inject;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.mapstores.v2.constants.HazelcastNames.Maps;
import com.kryptnostic.rhizome.pods.hazelcast.SelfRegisteringStreamSerializer;
import com.kryptnostic.types.entryprocessors.GetOrCreateUuidForTypeEntryProcessor;

public class GetOrCreateUuidForTypeEntryProcessorStreamSerializer
        implements SelfRegisteringStreamSerializer<GetOrCreateUuidForTypeEntryProcessor> {
    private transient IMap<UUID, String> typesToScopes;

    @Override
    public void write( ObjectDataOutput out, GetOrCreateUuidForTypeEntryProcessor object ) throws IOException {
        out.writeUTF( object.getType() );

    }

    @Override
    public GetOrCreateUuidForTypeEntryProcessor read( ObjectDataInput in ) throws IOException {
        String type = in.readUTF();
        return new GetOrCreateUuidForTypeEntryProcessor( type, typesToScopes );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.GET_OR_CREATE_UUID_FOR_TYPE_ENTRY_PROCESSOR.ordinal();
    }

    @Override
    public void destroy() {}

    @Override
    public Class<GetOrCreateUuidForTypeEntryProcessor> getClazz() {
        return GetOrCreateUuidForTypeEntryProcessor.class;
    }

    @Inject
    public void setHazelcastInstance( HazelcastInstance hazelcast ) {
        this.typesToScopes = hazelcast.getMap( Maps.TYPES_TO_SCOPES );
    }
}
