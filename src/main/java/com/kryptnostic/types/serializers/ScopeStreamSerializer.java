package com.kryptnostic.types.serializers;

import com.kryptnostic.mapstores.v1.constants.HazelcastSerializerTypeIds;
import com.kryptnostic.services.v1.serialization.JacksonStreamSerializer;
import com.kryptnostic.v2.storage.models.Scope;

public class ScopeStreamSerializer extends JacksonStreamSerializer<Scope>{

    public ScopeStreamSerializer() {
        super( Scope.class );
    }

    @Override
    public int getTypeId() {
        return HazelcastSerializerTypeIds.SCOPE.ordinal();
    }

}
