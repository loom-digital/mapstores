package com.kryptnostic.users.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kryptnostic.kodex.v1.constants.Names;

public class ObjectBlockKey {
    private final String                            objectId;
    private final int                               blockIndex;
    private final int                               version;

    @JsonCreator
    public ObjectBlockKey(
            @JsonProperty( Names.ID_FIELD ) String objectId,
            @JsonProperty( Names.INDEX_FIELD ) int blockIndex,
            @JsonProperty( Names.VERSION_FIELD ) int version ) {
        this.objectId = objectId;
        this.blockIndex = blockIndex;
        this.version = version;
    }

    @JsonProperty( Names.ID_FIELD )
    public String getObjectId() {
        return objectId;
    }

    @JsonProperty( Names.INDEX_FIELD )
    public int getBlockIndex() {
        return blockIndex;
    }

    @JsonProperty( Names.VERSION_FIELD )
    public int getVersion() {
        return version;
    }

}
