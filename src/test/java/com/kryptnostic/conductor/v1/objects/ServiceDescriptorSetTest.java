package com.kryptnostic.conductor.v1.objects;

import com.google.common.collect.ImmutableList;
import com.kryptnostic.utils.BaseJacksonSerializationTest;

public class ServiceDescriptorSetTest extends BaseJacksonSerializationTest<ServiceDescriptorSet> {

    @Override
    protected ServiceDescriptorSet getSampleData() {
        return new ServiceDescriptorSet( ImmutableList.of(
                new ServiceDescriptor(
                        "serviceName",
                        "localhost",
                        8085,
                        "http://localhost:8085/conductor/health",
                        "http://localhost:8085/conductor/deploy" ) ) );
    }

    @Override
    protected Class<ServiceDescriptorSet> getClazz() {
        return ServiceDescriptorSet.class;
    }

}
