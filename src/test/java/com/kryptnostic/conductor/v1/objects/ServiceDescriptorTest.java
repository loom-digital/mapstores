package com.kryptnostic.conductor.v1.objects;

import com.kryptnostic.utils.BaseJacksonSerializationTest;

public class ServiceDescriptorTest extends BaseJacksonSerializationTest<ServiceDescriptor> {

    @Override
    protected ServiceDescriptor getSampleData() {
        return new ServiceDescriptor(
                "MonitoringService",
                "localhost",
                8085,
                "localhost:8085/conductor/health",
                "localhost:8085/conductor/deploy" );
    }

    @Override
    protected Class<ServiceDescriptor> getClazz() {
        return ServiceDescriptor.class;
    }

}
