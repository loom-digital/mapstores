package com.kryptnostic.conductor.v1.serialization;

import jodd.mail.Email;

import org.mockito.Mockito;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;
import com.kryptnostic.conductor.v1.processors.MonitoringServiceEntryProcessor;
import com.kryptnostic.mapstores.v1.constants.HazelcastNames;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class MonitoringServiceEntryProcessorStreamSerializerTest
        extends BaseSerializerTest<MonitoringServiceEntryProcessorStreamSerializer, MonitoringServiceEntryProcessor> {

    private final HazelcastInstance hazelcast = Mockito.mock( HazelcastInstance.class );

    @Override
    protected MonitoringServiceEntryProcessorStreamSerializer createSerializer() {
        return new MonitoringServiceEntryProcessorStreamSerializer() {
            @Override
            protected HazelcastInstance getHazelcastInstanceUsingName( String hazelcastInstanceName ) {
                return hazelcast;
            }
        };
    }

    @Override
    protected MonitoringServiceEntryProcessor createInput() {
        IQueue<Email> q = Mockito.mock( IQE.class );
        Mockito.when( hazelcast.<Email> getQueue( HazelcastNames.Queues.MAIL_SERVICE ) ).thenReturn( q );
        Mockito.when( hazelcast.getName() ).thenReturn( "hz-test-1" );
        return new MonitoringServiceEntryProcessor( hazelcast, "yao@kryptnostic.com" );
    }

    public static interface IQE extends IQueue<Email> {

    }

}
