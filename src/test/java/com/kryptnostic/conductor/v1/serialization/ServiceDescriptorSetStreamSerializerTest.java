package com.kryptnostic.conductor.v1.serialization;

import com.kryptnostic.conductor.v1.objects.ServiceDescriptor;
import com.kryptnostic.conductor.v1.objects.ServiceDescriptorSet;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class ServiceDescriptorSetStreamSerializerTest
        extends BaseSerializerTest<ServiceDescriptorSetStreamSerializer, ServiceDescriptorSet> {

    @Override
    protected ServiceDescriptorSetStreamSerializer createSerializer() {
        return new ServiceDescriptorSetStreamSerializer();
    }

    @Override
    protected ServiceDescriptorSet createInput() {
        return new ServiceDescriptorSet( new ServiceDescriptor(
                "MonitoringService",
                "localhost",
                8085,
                "localhost:8085/conductor/health",
                "localhost:8085/conductor/deploy" ) );
    }

}
