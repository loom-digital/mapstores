package com.kryptnostic.conductor.v1.serialization;

import com.kryptnostic.conductor.v1.objects.ServiceDescriptor;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class ServiceDescriptorStreamSerializerTest extends
        BaseSerializerTest<ServiceDescriptorStreamSerializer, ServiceDescriptor> {

    @Override
    protected ServiceDescriptorStreamSerializer createSerializer() {
        return new ServiceDescriptorStreamSerializer();
    }

    @Override
    protected ServiceDescriptor createInput() {
        return new ServiceDescriptor(
                "MonitoringService",
                "localhost",
                8085,
                "localhost:8085/conductor/health",
                "localhost:8085/conductor/deploy" );
    }

}
