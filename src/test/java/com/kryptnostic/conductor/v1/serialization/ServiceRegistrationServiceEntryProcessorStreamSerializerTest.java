package com.kryptnostic.conductor.v1.serialization;

import com.kryptnostic.conductor.v1.objects.ServiceDescriptorSet;
import com.kryptnostic.conductor.v1.processors.ServiceRegistrationServiceEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class ServiceRegistrationServiceEntryProcessorStreamSerializerTest extends
        BaseSerializerTest<ServiceRegistrationServiceEntryProcessorStreamSerializer, ServiceRegistrationServiceEntryProcessor> {

    @Override
    protected ServiceRegistrationServiceEntryProcessorStreamSerializer createSerializer() {
        return new ServiceRegistrationServiceEntryProcessorStreamSerializer();
    }

    @Override
    protected ServiceRegistrationServiceEntryProcessor createInput() {
        return new ServiceRegistrationServiceEntryProcessor( new ServiceDescriptorSet() );
    }

}
