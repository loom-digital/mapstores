package com.kryptnostic.heracles.directory.v1.objects;

import com.google.common.collect.ImmutableSet;
import com.kryptnostic.utils.BaseJacksonSerializationTest;

public class EmailsTest extends BaseJacksonSerializationTest<Emails> {
    @Override
    protected Emails getSampleData() {
        return new Emails( ImmutableSet.of( "kryptodoge@kryptnostic.com", "krypto@kryptnostic.com" ) );
    }

    @Override
    protected Class<Emails> getClazz() {
        return Emails.class;
    }
}
