package com.kryptnostic.heracles.directory.v1.serialization;

import java.util.Arrays;

import com.kryptnostic.heracles.directory.v1.objects.Emails;
import com.kryptnostic.heracles.directory.v1.processors.AddEmailEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class AddEmailEntryProcessorStreamSerializerTest extends
        BaseSerializerTest<AddEmailEntryProcessorStreamSerializer, AddEmailEntryProcessor> {

    @Override
    protected AddEmailEntryProcessorStreamSerializer createSerializer() {
        return new AddEmailEntryProcessorStreamSerializer();
    }

    @Override
    protected AddEmailEntryProcessor createInput() {
        return new AddEmailEntryProcessor( new Emails( Arrays.asList( "krytodoge@kryptnostic.com",
                "krypto@kryptnostic.com" ) ) );
    }

}
