package com.kryptnostic.heracles.directory.v1.serialization;

import java.util.UUID;

import com.google.common.collect.ImmutableSet;
import com.kryptnostic.heracles.directory.v1.processors.RemoveGroupMembershipEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class RemoveGroupMembershipEntryProcessorStreamSerializerTest extends
        BaseSerializerTest<RemoveGroupMembershipEntryProcessorStreamSerializer, RemoveGroupMembershipEntryProcessor> {

    @Override
    protected RemoveGroupMembershipEntryProcessorStreamSerializer createSerializer() {
        return new RemoveGroupMembershipEntryProcessorStreamSerializer();
    }

    @Override
    protected RemoveGroupMembershipEntryProcessor createInput() {
        return new RemoveGroupMembershipEntryProcessor( new UUIDSet( ImmutableSet.of( UUID.randomUUID(), UUID.randomUUID() ) ) );
    }

}
