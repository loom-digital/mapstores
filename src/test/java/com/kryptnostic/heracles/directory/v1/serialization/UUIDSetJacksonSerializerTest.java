package com.kryptnostic.heracles.directory.v1.serialization;

import java.util.UUID;

import com.google.common.collect.ImmutableSet;
import com.kryptnostic.rhizome.hazelcast.objects.UUIDSet;
import com.kryptnostic.utils.BaseJacksonSerializationTest;

public class UUIDSetJacksonSerializerTest extends BaseJacksonSerializationTest<UUIDSet>{

    @Override
    protected UUIDSet getSampleData() {
        return new UUIDSet( ImmutableSet.of( UUID.randomUUID() , UUID.randomUUID() ) );
    }

    @Override
    protected Class<UUIDSet> getClazz() {
        return UUIDSet.class;
    }

}
