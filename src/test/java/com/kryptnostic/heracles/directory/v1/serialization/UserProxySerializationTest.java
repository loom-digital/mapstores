package com.kryptnostic.heracles.directory.v1.serialization;

import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import com.kryptnostic.directory.v1.principal.User;
import com.kryptnostic.heracles.directory.v1.objects.Emails;
import com.kryptnostic.heracles.directory.v1.objects.UserProxy;
import com.kryptnostic.heracles.directory.v1.services.UserProxyService;
import com.kryptnostic.utils.BaseJacksonSerializationTest;

public class UserProxySerializationTest extends BaseJacksonSerializationTest<User> {
    private static final String    EMAIL       = "kryptodoge@kryptnostic.com";
    private static final byte[]    certificate = RandomStringUtils.random( 4096 ).getBytes();
    private static final Set<UUID> uuids       = ImmutableSet.of( UUID.randomUUID(), UUID.randomUUID() );

    @Override
    protected User getSampleData() {
        final UUID userId = UUID.randomUUID();
        return new UserProxy( userId, new UserProxyService() {

            @Override
            public String getName( UUID userId ) {
                return "kryptodoge";
            }

            @Override
            public Emails getEmails( UUID userId ) {
                return Emails.of( EMAIL );

            }

            @Override
            public byte[] getCertificate( UUID userId ) {
                return certificate;
            }

            @Override
            public Set<String> getRoles( UUID userId ) {
                return ImmutableSet.of( "ROLE_USER", "ROLE_DEVELOPER" );
            }

            @Override
            public Set<UUID> getGroups( UUID userId ) {
                return uuids;
            }

            @Override
            public Optional<UUID> authenticate( String email, String authenticator ) {
                // TODO Auto-generated method stub
                throw new UnsupportedOperationException( "THIS METHOD HAS NOT BEEN IMPLEMENTED" );
            }

            @Override
            public Optional<String> authenticate( UUID id, String authenticator ) {
                // TODO Auto-generated method stub
                throw new UnsupportedOperationException( "THIS METHOD HAS NOT BEEN IMPLEMENTED" );
            }

            @Override
            public boolean getConfirmationStatus( UUID userId ) {
                return false;
            }

        } );
    }

    @Override
    protected Class<User> getClazz() {
        return User.class;
    }

}
