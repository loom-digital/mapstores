package com.kryptnostic.heracles.directory.v2.objects;

import com.kryptnostic.directory.v2.model.DomainResourcesUsage;
import com.kryptnostic.utils.BaseJacksonSerializationTest;

public class DomainResourcesUsageTest extends BaseJacksonSerializationTest<DomainResourcesUsage> {

    @Override
    protected DomainResourcesUsage getSampleData() {
        return new DomainResourcesUsage( 10L, 20L );
    }

    @Override
    protected Class<DomainResourcesUsage> getClazz() {
        return DomainResourcesUsage.class;
    }

}
