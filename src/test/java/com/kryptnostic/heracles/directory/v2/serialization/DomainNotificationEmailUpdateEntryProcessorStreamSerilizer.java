package com.kryptnostic.heracles.directory.v2.serialization;

import com.kryptnostic.mapstores.v2.entryprocessors.DomainNotificationEmailUpdateEntryProcessor;
import com.kryptnostic.mapstores.v2.serializers.DomainNotificationEmailUpdateEntryProcessorStreamSerializer;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class DomainNotificationEmailUpdateEntryProcessorStreamSerilizer extends BaseSerializerTest<DomainNotificationEmailUpdateEntryProcessorStreamSerializer, DomainNotificationEmailUpdateEntryProcessor>{

    @Override
    protected DomainNotificationEmailUpdateEntryProcessorStreamSerializer createSerializer() {
        return new DomainNotificationEmailUpdateEntryProcessorStreamSerializer();
    }

    @Override
    protected DomainNotificationEmailUpdateEntryProcessor createInput() {
        return new DomainNotificationEmailUpdateEntryProcessor( "test@test.com" );
    }

}
