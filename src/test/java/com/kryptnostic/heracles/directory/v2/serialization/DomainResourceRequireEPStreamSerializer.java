package com.kryptnostic.heracles.directory.v2.serialization;

import com.kryptnostic.heracles.directory.v2.entryprocessors.DomainResourceRequireEntryProcessor;
import com.kryptnostic.heracles.directory.v2.serializers.DomainResourceRequireEntryProcessorStreamSerializer;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class DomainResourceRequireEPStreamSerializer extends BaseSerializerTest<DomainResourceRequireEntryProcessorStreamSerializer, DomainResourceRequireEntryProcessor>{

    @Override
    protected DomainResourceRequireEntryProcessorStreamSerializer createSerializer() {
        return new DomainResourceRequireEntryProcessorStreamSerializer();
    }

    @Override
    protected DomainResourceRequireEntryProcessor createInput() {
        return new DomainResourceRequireEntryProcessor(Long.valueOf( -10 ));
    }

}
