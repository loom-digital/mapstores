package com.kryptnostic.heracles.directory.v2.serialization;

import java.util.UUID;

import com.kryptnostic.heracles.directory.v2.objects.DomainResourcesKey;
import com.kryptnostic.heracles.directory.v2.serializers.DomainResourcesKeyStreamSerializer;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class DomainResourcesKeyStreamSerializerTest extends BaseSerializerTest<DomainResourcesKeyStreamSerializer, DomainResourcesKey>{

    @Override
    protected DomainResourcesKeyStreamSerializer createSerializer() {
        return new DomainResourcesKeyStreamSerializer();
    }

    @Override
    protected DomainResourcesKey createInput() {
        return new DomainResourcesKey(UUID.randomUUID(), "Test");
    }

}
