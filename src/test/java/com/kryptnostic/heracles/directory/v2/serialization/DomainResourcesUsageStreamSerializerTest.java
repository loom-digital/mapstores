package com.kryptnostic.heracles.directory.v2.serialization;

import com.kryptnostic.directory.v2.model.DomainResourcesUsage;
import com.kryptnostic.heracles.directory.v2.serializers.DomainResourcesUsageStreamSerializer;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class DomainResourcesUsageStreamSerializerTest
        extends BaseSerializerTest<DomainResourcesUsageStreamSerializer, DomainResourcesUsage> {

    @Override
    protected DomainResourcesUsageStreamSerializer createSerializer() {
        return new DomainResourcesUsageStreamSerializer();
    }

    @Override
    protected DomainResourcesUsage createInput() {
        return new DomainResourcesUsage( 10L, 20L );
    }

}
