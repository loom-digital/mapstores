package com.kryptnostic.mail.v1.serialization;

import com.google.common.base.Optional;
import com.kryptnostic.mail.models.EmailRequest;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class EmailRequestStreamSerializerTest extends BaseSerializerTest<EmailRequestStreamSerializer, EmailRequest> {

    @Override
    protected EmailRequestStreamSerializer createSerializer() {
        return new EmailRequestStreamSerializer();
    }

    @Override
    protected EmailRequest createInput() {
        return new EmailRequest(
                Optional.absent(),
                new String[] { "yao@kryptnostic.com" },
                Optional.absent(),
                Optional.absent() );
    }

}
