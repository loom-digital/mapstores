package com.kryptnostic.mail.v1.serialization;

import java.util.Map;

import com.google.common.base.Optional;
import com.google.common.collect.Maps;
import com.kryptnostic.mail.constants.EmailTemplate;
import com.kryptnostic.mail.models.RenderableEmailRequest;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

import jodd.mail.att.ByteArrayAttachment;

public class RenderableEmailRequestStreamSerializerTest
        extends BaseSerializerTest<RenderableEmailRequestStreamSerializer, RenderableEmailRequest> {

    @Override
    protected RenderableEmailRequestStreamSerializer createSerializer() {
        return new RenderableEmailRequestStreamSerializer();
    }

    // TODO: Need more suitable and general tests
    @Override
    protected RenderableEmailRequest createInput() {
        Map<String, String> map = Maps.newHashMapWithExpectedSize( 2 );
        map.put( "name", "yao" );
        return new RenderableEmailRequest(
                Optional.absent(),
                new String[] { "yao@kryptnostic.com" },
                Optional.absent(),
                Optional.absent(),
                EmailTemplate.KODEX_INVITATION.getPath(),
                Optional.of( EmailTemplate.KODEX_INVITATION.getSubject() ),
                Optional.of( map ),
                Optional.of( new ByteArrayAttachment[] {} ),
                Optional.of( new String[] { "attachment" } ) );
    }

}
