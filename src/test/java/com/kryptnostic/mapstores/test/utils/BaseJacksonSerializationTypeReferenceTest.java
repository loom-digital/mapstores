package com.kryptnostic.mapstores.test.utils;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kryptnostic.kodex.v1.serialization.jackson.KodexObjectMapperFactory;

public abstract class BaseJacksonSerializationTypeReferenceTest<T> {
    private static final ObjectMapper MAPPER = KodexObjectMapperFactory.getObjectMapper();
    private static final ObjectMapper SMILE  = KodexObjectMapperFactory.getSmileMapper();

    @Test
    public void testSerdes() throws IOException {
        T data = getSampleData();
        String s = MAPPER.writeValueAsString( data );
        byte[] b = MAPPER.writeValueAsBytes( data );
        Assert.assertEquals( data, MAPPER.readValue( s, getType() ) );
        Assert.assertEquals( data, MAPPER.readValue( b, getType() ) );

        b = SMILE.writeValueAsBytes( data );
        Assert.assertEquals( data, SMILE.readValue( b, getType() ) );
    }

    protected abstract T getSampleData();

    protected abstract TypeReference<T> getType();
}
