package com.kryptnostic.mapstores.v2.serializers;

import java.util.UUID;

import com.google.common.collect.ImmutableSet;
import com.kryptnostic.mapstores.v2.entryprocessors.AddPolicyToDomainEntryProcessor;
import com.kryptnostic.mapstores.v2.serializers.AddPolicyToDomainEntryProcessorStreamSerializer;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class AddPolicyToDomainEntryProcessorStreamSerializerTest
        extends BaseSerializerTest<AddPolicyToDomainEntryProcessorStreamSerializer, AddPolicyToDomainEntryProcessor> {

    @Override
    protected AddPolicyToDomainEntryProcessorStreamSerializer createSerializer() {
        return new AddPolicyToDomainEntryProcessorStreamSerializer();
    }

    @Override
    protected AddPolicyToDomainEntryProcessor createInput() {
        return new AddPolicyToDomainEntryProcessor( ImmutableSet.of( UUID.randomUUID(), UUID.randomUUID() ) );
    }

}
