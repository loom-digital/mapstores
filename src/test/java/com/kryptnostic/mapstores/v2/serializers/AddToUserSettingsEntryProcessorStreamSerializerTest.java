package com.kryptnostic.mapstores.v2.serializers;

import com.google.common.collect.ImmutableSet;
import com.kryptnostic.mapstores.v2.entryprocessors.AddToUserSettingsEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class AddToUserSettingsEntryProcessorStreamSerializerTest
        extends BaseSerializerTest<AddToUserSettingsEntryProcessorStreamSerializer, AddToUserSettingsEntryProcessor> {

    @Override
    protected AddToUserSettingsEntryProcessorStreamSerializer createSerializer() {
        return new AddToUserSettingsEntryProcessorStreamSerializer();
    }

    @Override
    protected AddToUserSettingsEntryProcessor createInput() {
        return new AddToUserSettingsEntryProcessor( ImmutableSet.of( "test1", "test2" ) );
    }

}
