package com.kryptnostic.mapstores.v2.serializers;

import java.util.UUID;

import com.google.common.collect.ImmutableSet;
import com.kryptnostic.mapstores.v2.entryprocessors.AddTrustsToDomainEntryProcessor;
import com.kryptnostic.mapstores.v2.serializers.AddTrustsToDomainEntryProcessorStreamSerializer;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class AddTrustsToDomainEntryProcessorStreamSerializerTest
        extends BaseSerializerTest<AddTrustsToDomainEntryProcessorStreamSerializer, AddTrustsToDomainEntryProcessor> {

    @Override
    protected AddTrustsToDomainEntryProcessorStreamSerializer createSerializer() {
        return new AddTrustsToDomainEntryProcessorStreamSerializer();
    }

    @Override
    protected AddTrustsToDomainEntryProcessor createInput() {
        return new AddTrustsToDomainEntryProcessor( ImmutableSet.of( UUID.randomUUID(), UUID.randomUUID() ) );
    }

}
