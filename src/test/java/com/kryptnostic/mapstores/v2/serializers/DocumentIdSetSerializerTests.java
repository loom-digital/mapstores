package com.kryptnostic.mapstores.v2.serializers;

import java.util.UUID;

import com.kryptnostic.rhizome.hazelcast.objects.OrderedUUIDSet;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class DocumentIdSetSerializerTests extends BaseSerializerTest<OrderedUUIDSetStreamSerializer, OrderedUUIDSet> {

    @Override
    protected OrderedUUIDSetStreamSerializer createSerializer() {
        return new OrderedUUIDSetStreamSerializer();
    }

    @Override
    protected OrderedUUIDSet createInput() {
        OrderedUUIDSet s = new OrderedUUIDSet();
        s.add( new UUID( 0L, 0L ) );
        s.add( new UUID( 0L, 1L ) );
        s.add( new UUID( 0L, 2L ) );
        return s;
    }

}
