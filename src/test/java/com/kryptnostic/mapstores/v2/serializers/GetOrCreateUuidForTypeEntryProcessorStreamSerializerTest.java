package com.kryptnostic.mapstores.v2.serializers;

import java.util.UUID;

import org.mockito.Mockito;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.kryptnostic.mapstores.v2.constants.HazelcastNames.Maps;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;
import com.kryptnostic.types.entryprocessors.GetOrCreateUuidForTypeEntryProcessor;
import com.kryptnostic.types.serializers.GetOrCreateUuidForTypeEntryProcessorStreamSerializer;

public class GetOrCreateUuidForTypeEntryProcessorStreamSerializerTest extends
        BaseSerializerTest<GetOrCreateUuidForTypeEntryProcessorStreamSerializer, GetOrCreateUuidForTypeEntryProcessor> {
    private static final HazelcastInstance HAZELCAST_INSTANCE     = Mockito.mock( HazelcastInstance.class );
    private static final IMapUUIDString    TYPES_TO_SCOPES = Mockito.mock( IMapUUIDString.class );
    private static final String            TYPE          = "type_for_test";

    public GetOrCreateUuidForTypeEntryProcessorStreamSerializerTest() {
        super();
    }

    @Override
    protected GetOrCreateUuidForTypeEntryProcessorStreamSerializer createSerializer() {
        GetOrCreateUuidForTypeEntryProcessorStreamSerializer ss = new GetOrCreateUuidForTypeEntryProcessorStreamSerializer();
        Mockito.when( HAZELCAST_INSTANCE.<UUID, String> getMap( Maps.TYPES_TO_SCOPES ) )
                .thenReturn( TYPES_TO_SCOPES );
        ss.setHazelcastInstance( HAZELCAST_INSTANCE );
        return ss;
    }

    @Override
    protected GetOrCreateUuidForTypeEntryProcessor createInput() {
        return new GetOrCreateUuidForTypeEntryProcessor( TYPE, TYPES_TO_SCOPES );
    }

    static interface IMapUUIDString extends IMap<UUID, String> {

    }

}
