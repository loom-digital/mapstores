package com.kryptnostic.mapstores.v2.serializers;

import com.google.common.collect.ImmutableSet;
import com.kryptnostic.mapstores.v2.entryprocessors.RemoveFromUserSettingsEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class RemoveFromUserSettingsEntryProcessorStreamSerializerTest extends
        BaseSerializerTest<RemoveFromUserSettingsEntryProcessorStreamSerializer, RemoveFromUserSettingsEntryProcessor> {

    @Override
    protected RemoveFromUserSettingsEntryProcessorStreamSerializer createSerializer() {
        return new RemoveFromUserSettingsEntryProcessorStreamSerializer();
    }

    @Override
    protected RemoveFromUserSettingsEntryProcessor createInput() {
        return new RemoveFromUserSettingsEntryProcessor( ImmutableSet.of( "test1", "test2" ) );
    }

}
