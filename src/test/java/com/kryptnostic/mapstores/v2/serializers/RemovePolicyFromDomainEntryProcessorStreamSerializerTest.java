package com.kryptnostic.mapstores.v2.serializers;

import java.util.UUID;

import com.google.common.collect.ImmutableSet;
import com.kryptnostic.mapstores.v2.entryprocessors.RemovePolicyFromDomainEntryProcessor;
import com.kryptnostic.mapstores.v2.serializers.RemovePolicyFromDomainEntryProcessorStreamSerializer;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class RemovePolicyFromDomainEntryProcessorStreamSerializerTest extends
        BaseSerializerTest<RemovePolicyFromDomainEntryProcessorStreamSerializer, RemovePolicyFromDomainEntryProcessor> {

    @Override
    protected RemovePolicyFromDomainEntryProcessorStreamSerializer createSerializer() {
        return new RemovePolicyFromDomainEntryProcessorStreamSerializer();
    }

    @Override
    protected RemovePolicyFromDomainEntryProcessor createInput() {
        return new RemovePolicyFromDomainEntryProcessor( ImmutableSet.of( UUID.randomUUID(), UUID.randomUUID() ) );
    }

}
