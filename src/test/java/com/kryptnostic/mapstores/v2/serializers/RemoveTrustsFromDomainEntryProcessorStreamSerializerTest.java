package com.kryptnostic.mapstores.v2.serializers;

import java.util.UUID;

import com.google.common.collect.ImmutableSet;
import com.kryptnostic.mapstores.v2.entryprocessors.RemoveTrustsFromEntryProcessor;
import com.kryptnostic.mapstores.v2.serializers.RemoveTrustsFromDomainEntryProcessorStreamSerializer;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class RemoveTrustsFromDomainEntryProcessorStreamSerializerTest extends
        BaseSerializerTest<RemoveTrustsFromDomainEntryProcessorStreamSerializer, RemoveTrustsFromEntryProcessor> {

    @Override
    protected RemoveTrustsFromDomainEntryProcessorStreamSerializer createSerializer() {
        return new RemoveTrustsFromDomainEntryProcessorStreamSerializer();
    }

    @Override
    protected RemoveTrustsFromEntryProcessor createInput() {
        return new RemoveTrustsFromEntryProcessor( ImmutableSet.of( UUID.randomUUID(), UUID.randomUUID() ) );
    }

}
