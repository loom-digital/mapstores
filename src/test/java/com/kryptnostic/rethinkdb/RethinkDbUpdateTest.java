package com.kryptnostic.rethinkdb;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Ignore;
import org.junit.Test;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.google.common.base.Stopwatch;
import com.google.common.util.concurrent.Futures;
import com.kryptnostic.mapstores.v2.search.QueryResultRethinkDbProxy;
import com.kryptnostic.rhizome.configuration.rethinkdb.RethinkDbConfiguration;
import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;

@Ignore
public class RethinkDbUpdateTest {
    RethinkDbConnectionPool pool;

    @Test
    public void testProxy() throws TimeoutException {
        pool = new RethinkDbConnectionPool( new RethinkDbConfiguration( "localhost", 28015 ) );
        UUID queryId = new UUID( 0 , 0);
        UUID metaObjId = new UUID( 0 , 1);

        QueryResultRethinkDbProxy proxy = new QueryResultRethinkDbProxy( pool, "kryptnostic" , "proxyTest", queryId );
        for( int i = 0 ; i < 10 ; ++i ) {
            proxy.updateQueryResults( metaObjId, RandomUtils.nextDouble( 0, 1 ) );
        }

        System.out.println( proxy.getNextBatch( metaObjId, 5, 10 ).toString() );
    }

    @Test
    public void fooTest() throws TimeoutException {
        pool = new RethinkDbConnectionPool( new RethinkDbConfiguration( "localhost", 28015 ) );
        Connection conn = pool.acquire();
        Connection conn2 = pool.acquire();
        if ( !(boolean) RethinkDB.r.dbList().contains( "kryptnosticTest" ).run( conn ) ) {
            RethinkDB.r.dbCreate( "kryptnosticTest" ).run( conn );
        }

        if ( !(boolean) RethinkDB.r.db( "kryptnosticTest" ).tableList().contains( "queryResult" ).run( conn ) ) {
            RethinkDB.r.db( "kryptnosticTest" ).tableCreate( "queryResult" ).run( conn );
        }

        byte[] val = RandomUtils.nextBytes( 16 );
        Object[] insertVals = new Object[ 1 ];
        Arrays.fill( insertVals,
                RethinkDB.r.hashMap( "id", UUID.randomUUID().toString() + UUID.randomUUID().toString() ).with( "value",
                        RethinkDB.r.binary( val ) )
                        .with( "score", 0 ) );
        Stopwatch w = Stopwatch.createStarted();
        RethinkDB.r.db( "kryptnosticTest" ).table( "queryResult" )
                .insert( RethinkDB.r.array( insertVals ) )
                .run( conn );
        RethinkDB.r.db( "kryptnosticTest" ).table( "queryResult" )
                .insert( RethinkDB.r.array( insertVals ) )
                .run( conn2 );
        // System.out.println( RethinkDB.r.db( "kryptnosticTest" ).table( "queryResult" ).indexCreate( "value" )
        // .run( conn ).toString() );

        System.out.println( "Insert time: " + Long.toString( w.elapsed( TimeUnit.MILLISECONDS ) ) + "ms" );
        w.reset();
        w.start();
        Object o = RethinkDB.r.db( "kryptnosticTest" ).table( "queryResult" ).getAll( RethinkDB.r.binary( val ) )
                .optArg( "index", "value" )
                .update( score -> RethinkDB.r.hashMap( "score",
                        score.g( "score" ).add( 5 ) ) )
                .run( conn );
        System.out.println( "Elapsed: " + Long.toString( w.elapsed( TimeUnit.MILLISECONDS ) ) + "ms" );
        System.out.println( o.toString() );
    }

    @Test
    public void doCassandraTest() throws InterruptedException, ExecutionException {
        Cluster cluster = new Cluster.Builder()
                .withPoolingOptions( new PoolingOptions() )
                .withProtocolVersion( ProtocolVersion.V3 )
                .addContactPoints( "127.0.0.1" )
                .build();
        Session session = cluster.newSession();

        session.execute(
                "CREATE KEYSPACE IF NOT EXISTS kryptnostic WITH replication = {'class':'SimpleStrategy', 'replication_factor':1};" );
        session.execute( "USE kryptnostic" );
        session.execute( "CREATE TABLE IF NOT EXISTS kryptnostic.perfTest \n(id text, score int, PRIMARY KEY( id ) )" );

        PreparedStatement ps = session.prepare(
                QueryBuilder.insertInto( "perfTest" ).value( "id", QueryBuilder.bindMarker() ).value( "score", 0 ) );

        PreparedStatement prs = session.prepare( QueryBuilder.select().all().from( "perfTest" ) );
        ResultSetFuture[] rsfl = new ResultSetFuture[ 100000 ];
        Stopwatch w = Stopwatch.createStarted();
        for ( int i = 0; i < 100000; ++i ) {
            rsfl[ i ] = session.executeAsync( ps.bind( UUID.randomUUID().toString() ) );
        }
        List<ResultSet> rs = Futures.allAsList( Arrays.asList( rsfl ) ).get();
        long elapsed = w.elapsed( TimeUnit.MILLISECONDS );
        System.out.println( "Insert time: " + Long.toString( elapsed ) + "ms" );
        System.out.println( "# Inserted: " + Integer.toString( rs.size() ) );
        System.out.println( "Rate: " + Long.toString( ( 1000 * rs.size() ) / elapsed ) );

        w.reset();
        w.start();

        ResultSet rsf = session.executeAsync( prs.bind() ).get();
        List<Row> row = rsf.all();
        elapsed = w.elapsed( TimeUnit.MILLISECONDS );

        System.out.println( "Insert time: " + Long.toString( elapsed ) + "ms" );
        System.out.println( "# Read: " + Integer.toString( row.size() ) );
        System.out.println( "Rate: " + Long.toString( ( 1000 * row.size() ) / elapsed ) );
    }
}
