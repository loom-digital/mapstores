package com.kryptnostic.services.v1.serialization;

import java.util.UUID;

import com.google.common.collect.ImmutableSet;
import com.kryptnostic.heracles.authentication.v1.processors.AddUserToDomainEntryProcessor;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class AddUserToDomainEntryProcessorStreamSerializerTest extends
        BaseSerializerTest<AddUserToDomainEntryProcessorStreamSerializer, AddUserToDomainEntryProcessor> {

    @Override
    protected AddUserToDomainEntryProcessorStreamSerializer createSerializer() {
        return new AddUserToDomainEntryProcessorStreamSerializer();
    }

    @Override
    protected AddUserToDomainEntryProcessor createInput() {
        return new AddUserToDomainEntryProcessor( ImmutableSet.of( UUID.randomUUID() , UUID.randomUUID() ) );
    }
}