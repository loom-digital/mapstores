package com.kryptnostic.services.v1.serialization;

import com.kryptnostic.heracles.authentication.v1.processors.ContactQueryEmailEntryProcessor;
import com.kryptnostic.heracles.v1.serializers.ContactQueryEmailEntryProcessorStreamSerializer;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class ContactQueryEmailEntryProcessorStreamSerializerTest extends
        BaseSerializerTest<ContactQueryEmailEntryProcessorStreamSerializer, ContactQueryEmailEntryProcessor> {
    public static final String TEST_EMAIL = "kryptodoge@kryptnostic.com";

    @Override
    protected ContactQueryEmailEntryProcessorStreamSerializer createSerializer() {
        return new ContactQueryEmailEntryProcessorStreamSerializer();
    }

    @Override
    protected ContactQueryEmailEntryProcessor createInput() {
        return new ContactQueryEmailEntryProcessor( TEST_EMAIL );
    }
}