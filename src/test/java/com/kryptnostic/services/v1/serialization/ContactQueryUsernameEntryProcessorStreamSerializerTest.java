package com.kryptnostic.services.v1.serialization;

import com.kryptnostic.heracles.authentication.v1.processors.ContactQueryUsernameEntryProcessor;
import com.kryptnostic.heracles.v1.serializers.ContactQueryUsernameEntryProcessorStreamSerializer;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class ContactQueryUsernameEntryProcessorStreamSerializerTest extends
        BaseSerializerTest<ContactQueryUsernameEntryProcessorStreamSerializer, ContactQueryUsernameEntryProcessor> {

    @Override
    protected ContactQueryUsernameEntryProcessorStreamSerializer createSerializer() {
        return new ContactQueryUsernameEntryProcessorStreamSerializer();
    }

    @Override
    protected ContactQueryUsernameEntryProcessor createInput() {
        return new ContactQueryUsernameEntryProcessor( "kryptodoge@kryptnostic.com" );
    }
}