package com.kryptnostic.services.v1.serialization;

import java.util.UUID;

import com.google.common.base.Optional;
import com.kryptnostic.directory.v1.domain.DomainSharingPolicy;
import com.kryptnostic.directory.v1.model.DomainUpdate;
import com.kryptnostic.heracles.directory.v1.processors.DomainUpdateEntryProcessor;
import com.kryptnostic.heracles.directory.v1.serialization.DomainUpdateEntryProcessorStreamSerializer;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;

public class DomainUpdateStreamSerializerTest extends
        BaseSerializerTest<DomainUpdateEntryProcessorStreamSerializer, DomainUpdateEntryProcessor> {

    @Override
    protected DomainUpdateEntryProcessorStreamSerializer createSerializer() {
        return new DomainUpdateEntryProcessorStreamSerializer();
    }

    @Override
    protected DomainUpdateEntryProcessor createInput() {
        return new DomainUpdateEntryProcessor( new DomainUpdate(
                UUID.randomUUID(),
                Optional.of( DomainSharingPolicy.AllDomains ),
                Optional.absent(),
                Optional.of( true ),
                Optional.absent()) );
    }
}
