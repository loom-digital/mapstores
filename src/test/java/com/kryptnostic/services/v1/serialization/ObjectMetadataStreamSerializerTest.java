package com.kryptnostic.services.v1.serialization;

import java.util.UUID;

import com.kryptnostic.kodex.v1.serialization.crypto.DefaultChunkingStrategy;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;
import com.kryptnostic.storage.v1.models.ObjectMetadata;

@Deprecated
public class ObjectMetadataStreamSerializerTest extends
        BaseSerializerTest<ObjectMetadataStreamSerializer, ObjectMetadata> {

    @Override
    protected ObjectMetadataStreamSerializer createSerializer() {
        return new ObjectMetadataStreamSerializer();
    }

    @Override
    protected ObjectMetadata createInput() {
        return new ObjectMetadata( "test", 1, 4, null, new DefaultChunkingStrategy(), UUID.randomUUID() );
    }
}