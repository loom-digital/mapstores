package com.kryptnostic.services.v1.serialization;

import org.apache.commons.lang3.RandomUtils;

import com.kryptnostic.krypto.engine.KryptnosticEngine;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;
import com.kryptnostic.search.v1.processors.SearchEntryProcessor;

public class SearchEntryProcessorStreamSerializerTest extends BaseSerializerTest<SearchEntryProcessorStreamSerializer, SearchEntryProcessor>{

    @Override
    protected SearchEntryProcessorStreamSerializer createSerializer() {
        return new SearchEntryProcessorStreamSerializer();
    }

    @Override
    protected SearchEntryProcessor createInput() {
        return new SearchEntryProcessor( RandomUtils.nextBytes( KryptnosticEngine.CLIENT_HASH_FUNCTION_LENGTH ), RandomUtils.nextBytes( KryptnosticEngine.SEARCH_PAIR_LENGTH ) );
    }
}
