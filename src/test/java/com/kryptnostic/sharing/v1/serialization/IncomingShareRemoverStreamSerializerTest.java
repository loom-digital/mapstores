package com.kryptnostic.sharing.v1.serialization;

import java.util.Set;
import java.util.UUID;

import com.google.common.collect.Sets;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;
import com.kryptnostic.sharing.v1.services.IncomingShareRemover;

public class IncomingShareRemoverStreamSerializerTest extends BaseSerializerTest<IncomingShareRemoverStreamSerializer, IncomingShareRemover>{

    @Override
    protected IncomingShareRemoverStreamSerializer createSerializer() {
        return new IncomingShareRemoverStreamSerializer();
    }

    @Override
    protected IncomingShareRemover createInput() {
        Set<String> objectIds = Sets.newHashSet( UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString() );
        return new IncomingShareRemover( objectIds );
    }   

}
