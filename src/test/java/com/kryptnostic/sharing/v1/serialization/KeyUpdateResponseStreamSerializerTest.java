package com.kryptnostic.sharing.v1.serialization;

import java.util.UUID;

import com.google.common.collect.Sets;
import com.kryptnostic.rhizome.hazelcast.serializers.BaseSerializerTest;
import com.kryptnostic.sharing.v1.models.response.KeyUpdateResponse;

public class KeyUpdateResponseStreamSerializerTest extends
        BaseSerializerTest<KeyUpdateResponseStreamSerializer, KeyUpdateResponse> {

    @Override
    protected KeyUpdateResponseStreamSerializer createSerializer() {
        return new KeyUpdateResponseStreamSerializer();
    }

    @Override
    protected KeyUpdateResponse createInput() {
        return new KeyUpdateResponse( Sets.newHashSet( UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID
                .randomUUID()
                .toString(), UUID.randomUUID().toString() ) );
    }

}
