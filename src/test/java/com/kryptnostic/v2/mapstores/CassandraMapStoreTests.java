package com.kryptnostic.v2.mapstores;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Timer;
import com.codahale.metrics.Timer.Context;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.hazelcast.core.MapStore;
import com.kryptnostic.mapstores.v2.constants.HazelcastNames;
import com.kryptnostic.mapstores.v2.pods.CassandraMapStoresPod;
import com.kryptnostic.mapstores.v2.pods.CodecsPod;
import com.kryptnostic.mapstores.v2.pods.KeymappersPod;
import com.kryptnostic.mapstores.v2.pods.ValueMappersPod;
import com.kryptnostic.rhizome.configuration.RhizomeConfiguration;
import com.kryptnostic.rhizome.configuration.cassandra.CassandraConfiguration;
import com.kryptnostic.rhizome.mappers.SelfRegisteringKeyMapper;
import com.kryptnostic.rhizome.mappers.SelfRegisteringValueMapper;
import com.kryptnostic.rhizome.mapstores.TestableSelfRegisteringMapStore;
import com.kryptnostic.rhizome.pods.CassandraPod;
import com.kryptnostic.rhizome.pods.RegistryBasedMappersPod;
import com.kryptnostic.rhizome.pods.hazelcast.RegistryBasedHazelcastInstanceConfigurationPod;

public class CassandraMapStoreTests {

    static String                                      keyspace               = "cass_tests";

    static CassandraConfiguration                      cassandraConfiguration = new CassandraConfiguration(
                                                                                      Optional.absent(),
                                                                                      Optional.absent(),
                                                                                      Optional.of(
                                                                                              ImmutableList
                                                                                                      .of( "localhost" ) ),
                                                                                      Optional.of(
                                                                                              keyspace ),
                                                                                      Optional.absent(),
                                                                                      Optional.absent(),
                                                                                      Optional.absent(),
                                                                                      Optional.absent(),
                                                                                      Optional.absent() );

    static RhizomeConfiguration                        rhizomeConfig          = new RhizomeConfiguration(
                                                                                      Optional.absent(),
                                                                                      Optional.absent(),
                                                                                      Optional.absent(),
                                                                                      Optional.of(
                                                                                              cassandraConfiguration ),
                                                                                      Optional.absent(),
                                                                                      Optional.absent(),
                                                                                      Optional.absent(),
                                                                                      Optional.absent(),
                                                                                      Optional.absent() );
    static CassandraPod                                cassPod                = new CassandraPod();

    static RegistryBasedMappersPod                     mappers                = new RegistryBasedMappersPod();
    static CassandraMapStoresPod                       cassMapStoresPod       = new CassandraMapStoresPod();

    private static final Logger                        logger                 = LoggerFactory
                                                                                      .getLogger(
                                                                                              CassandraMapStoreTests.class );

    static List<TestableSelfRegisteringMapStore<?, ?>> stores                 = Lists.newArrayList();

    private static Set<String>                         namesThatAreLiteralSets;

    @BeforeClass
    public static void setup() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        RegistryBasedHazelcastInstanceConfigurationPod hazelcastInstanceConfigPod = new RegistryBasedHazelcastInstanceConfigurationPod();

        cassPod.setRhizomeConfig( rhizomeConfig );

        // valuemappers
        ValueMappersPod valueMappersPod = new ValueMappersPod();
        valueMappersPod.setSerializationConfig( hazelcastInstanceConfigPod.getSerializationConfig() );
        Set<SelfRegisteringValueMapper<?>> valMappers = Sets.newHashSet();
        Method[] valueMappers = valueMappersPod.getClass().getMethods();
        for ( Method method : valueMappers ) {
            if ( method.getParameterCount() == 0
                    && SelfRegisteringValueMapper.class.isAssignableFrom( method.getReturnType() ) ) {
                valMappers.add( (SelfRegisteringValueMapper<?>) method.invoke( valueMappersPod ) );
                logger.info( "Added(v) " + method.getName() );
            }
        }
        mappers.registerValueMappers( valMappers );

        // keymappers
        Set<SelfRegisteringKeyMapper<?>> kMappers = Sets.newHashSet();
        KeymappersPod keymappersPod = new KeymappersPod();
        Method[] keyMappers = keymappersPod.getClass().getMethods();
        for ( Method method : keyMappers ) {
            if ( method.getParameterCount() == 0
                    && SelfRegisteringKeyMapper.class.isAssignableFrom( method.getReturnType() ) ) {
                kMappers.add( (SelfRegisteringKeyMapper<?>) method.invoke( keymappersPod ) );
                logger.info( "Added(k) " + method.getName() );
            }
        }
        mappers.registerKeyMappers( kMappers );

        CodecsPod codecsPod = new CodecsPod();
        codecsPod.setMappers( mappers );
        Set<TypeCodec<?>> codecs = Sets.newHashSet();
        Method[] codecsMethods = codecsPod.getClass().getMethods();
        for ( Method method : codecsMethods ) {
            if ( method.getParameterCount() == 0
                    && TypeCodec.class.isAssignableFrom( method.getReturnType() ) ) {
                codecs.add( (TypeCodec<?>) method.invoke( codecsPod ) );
            }
        }
        cassPod.setCodecs( codecs );

        cassMapStoresPod.setMappers( mappers );
        cassMapStoresPod.setSession( cassPod.cluster().newSession() );
        cassMapStoresPod.setCassandraConfig( cassandraConfiguration );

        cassMapStoresPod.init();

        cleanup();

        Method[] mapstoreMethods = cassMapStoresPod.getClass().getMethods();
        for ( Method method : mapstoreMethods ) {
            if ( method.getName().equals( "readOnlyLevelClockValueMap" )
                    || method.getName().equals( "indexedPermissionsMapstore" ) ) {
                continue;
            }
            if ( method.getParameterCount() == 0 && MapStore.class.isAssignableFrom( method.getReturnType() ) ) {
                logger.info( "creating mapstore " + method.getName() );
                stores.add( (TestableSelfRegisteringMapStore<?, ?>) method.invoke( cassMapStoresPod ) );
            }
        }

        namesThatAreLiteralSets = Sets.newHashSet();
        namesThatAreLiteralSets.add( HazelcastNames.Maps.USER_EMAILS );
        // namesThatAreLiteralSets.add( HazelcastNames.Maps.INDEX_METADATA );
        namesThatAreLiteralSets.add( HazelcastNames.Maps.INDEX_SEGMENT_IDS_INLINE );

    }

    @SuppressWarnings( { "unchecked", "rawtypes" } )
    @Test
    public void testMapstoreStorageAndLoading() throws Exception {
        for ( TestableSelfRegisteringMapStore store : stores ) {
            logger.info( "Trying store for [map=" + store.getMapName() + ",table=" + store.getTable() + "]" );
            Object key = store.generateTestKey();
            Object val = store.generateTestValue();
            Context time = new Timer().time();
            store.store( key, val );
            long timeTaken = time.stop();
            logger.info( "store to " + store.getMapName() + " took " + timeTaken / 1_000_000 + " milliseconds" );
            logger.info( "stored" );
            time = new Timer().time();
            Object loadedValue = store.load( key );
            timeTaken = time.stop();
            logger.info( "load from " + store.getMapName() + " took " + timeTaken / 1_000_000 + " milliseconds" );
            logger.info( "loaded" );
            Assert.assertNotNull( loadedValue );
            if ( val instanceof byte[] ) {
                Assert.assertArrayEquals( (byte[]) val, (byte[]) loadedValue );
                // } else if ( !( val instanceof MetadataEntries ) ) {
                // Assert.assertEquals( val, loadedValue );
            }
        }
    }

    @SuppressWarnings( { "unchecked", "rawtypes", "serial" } )
    @Test
    public void testMapstoreBulkStorageAndLoading() throws Exception {
        for ( TestableSelfRegisteringMapStore store : stores ) {
            logger.info( "Trying bulk store for [map=" + store.getMapName() + ",table=" + store.getTable() + "]" );
            Object key = store.generateTestKey();
            Object val = store.generateTestValue();
            Context time = new Timer().time();
            store.storeAll( new HashMap() {
                {
                    put( key, val );
                }
            } );
            long timeTaken = time.stop();
            logger.info( "storeAll to " + store.getMapName() + " took " + timeTaken / 1_000_000 + " milliseconds" );
            Map results = store.loadAll( Lists.newArrayList( key ) );
            Assert.assertEquals( 1, results.size() );
            Object loadedValue = results.values().iterator().next();
            Assert.assertNotNull( loadedValue );
            if ( val instanceof byte[] ) {
                Assert.assertArrayEquals( (byte[]) val, (byte[]) loadedValue );
                // } else if ( !( val instanceof MetadataEntries ) ) {
                // Assert.assertEquals( val, loadedValue );
            }
        }
    }

    @Test
    @SuppressWarnings( "unchecked" )
    public void testMapstoreDelete() throws Exception {
        for ( TestableSelfRegisteringMapStore store : stores ) {
            logger.info(
                    "Trying delete for [map=" + store.getMapName() + ",table=" + store.getTable() + "]" );
            Object key = store.generateTestKey();
            Object val = store.generateTestValue();
            store.store( key, val );
            Context time = new Timer().time();
            store.delete( key );
            long timeTaken = time.stop();
            logger.info( "deletion from " + store.getMapName() + " took " + timeTaken / 1_000_000 + " milliseconds" );
            Object hopefullyNull = store.load( key );
            if ( store.generateTestValue() instanceof Set<?>
                    && !namesThatAreLiteralSets.contains( store.getMapName() ) ) {
                Assert.assertTrue( ( (Set<?>) hopefullyNull ).isEmpty() );
            } else {
                Assert.assertNull( hopefullyNull );
            }
        }
    }

    @Test
    @SuppressWarnings( "unchecked" )
    public void testMapstoreDeleteAll() throws Exception {
        for ( TestableSelfRegisteringMapStore store : stores ) {
            logger.info(
                    "Trying delete all for [map=" + store.getMapName() + ",table=" + store.getTable() + "]" );
            Object key = store.generateTestKey();
            Object val = store.generateTestValue();
            store.store( key, val );
            Context time = new Timer().time();
            store.deleteAll( Lists.newArrayList( key ) );
            long timeTaken = time.stop();
            logger.info( "delete all from " + store.getMapName() + " took " + timeTaken / 1_000_000 + " milliseconds" );
            Object hopefullyNull = store.load( key );

            if ( store.generateTestValue() instanceof Set<?>
                    && !namesThatAreLiteralSets.contains( store.getMapName() ) ) {
                Assert.assertTrue( ( (Set<?>) hopefullyNull ).isEmpty() );
            } else {
                Assert.assertNull( hopefullyNull );
            }
        }
    }

    public static void cleanup() {
        Session newSession = cassPod.cluster().newSession();
        for ( TestableSelfRegisteringMapStore store : stores ) {
            newSession.execute( QueryBuilder.truncate( keyspace, store.getTable() ) );
        }
    }

}
